import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name='pyha',
    version='0.1',
    author='Lothar Maisenbacher',
    author_email='lothar.maisenbacher@mpq.mpg.de',
    description='Data analysis of pythonic hydrogen (pyh).',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://gitlab.mpcdf.mpg.de/lmaisen/pyha',
    packages=['pyha', 'pyha.misc', 'pyha.util'],
    package_data={'pyha': ['static/data_formats/*', 'static/freq_samplings/*', 'static/signals/*']},
    install_requires=[
        'numpy',
        'scipy',
        'pandas',
        'matplotlib',
        'tomli',
        'pyhs@git+ssh://git@gitlab.mpcdf.mpg.de/lmaisen/pyhs.git',
        ],
)
