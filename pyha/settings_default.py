# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ

Settings for analysis of simulation data using pyha (PYthonic Hydrogen Analysis).
"""

import os.path
import numpy as np
from collections import OrderedDict

# pyha
import pyha.def_delays

## Input files
script_dir = os.path.dirname(os.path.abspath(__file__))
# Relative paths to simulation results
dir_obe_corr_ = '../OBE correction'
dir_lfs_sim_ = os.path.join(dir_obe_corr_, 'Light force shift')
dir_big_model_grids_ = os.path.join(dir_obe_corr_, 'Big model grids')
dir_big_model_grid_inters_ = os.path.join(dir_obe_corr_, 'Big model interpolations')
dir_det_eff_ = os.path.join(dir_obe_corr_, 'Detection efficiency simulation')
dir_small_model_ = os.path.join(dir_obe_corr_, 'Small model')
dir_mcwfm_ = os.path.join(dir_obe_corr_, 'LFS MCWFM')
dir_eh_ = os.path.join(dir_obe_corr_, 'LFS EH')
dir_trajs_ = 'results/trajs'
dir_delay_vels_ = 'results/delay_vels'
# Absolute paths to simulation results
dir_obe_corr = os.path.abspath(os.path.join(script_dir, dir_obe_corr_))
dir_lfs_sim = os.path.abspath(os.path.join(script_dir, dir_lfs_sim_))
dir_big_model_grids = os.path.abspath(os.path.join(script_dir, dir_big_model_grids_))
dir_big_model_grid_inters = os.path.abspath(os.path.join(script_dir, dir_big_model_grid_inters_))
dir_det_eff = os.path.abspath(os.path.join(script_dir, dir_det_eff_))
dir_small_model = os.path.abspath(os.path.join(script_dir, dir_small_model_))
dir_mcwfm = os.path.abspath(os.path.join(script_dir, dir_mcwfm_))
dir_eh = os.path.abspath(os.path.join(script_dir, dir_eh_))
dir_trajs = os.path.abspath(os.path.join(script_dir, dir_trajs_))
dir_delay_vels = os.path.abspath(os.path.join(script_dir, dir_delay_vels_))
# Absolute paths
dir_daq = 'C:\\Users\\Lothar\\Dropbox\\MPQ\\DAQ'
dir_metadata = os.path.abspath(os.path.join(
    dir_daq, 'pyh_metadata'))

### Simulation data
## Default 2S-nP big model simulation
sim_params_default = {
    'GridInterUID': '',
    '2SnPLaserPower': -1,
    'SignalID': 'Lyman',
    'FitFuncID': 'Voigt',
    'TrajUID': '',
    'FS': '6P12',
    'FreqSampling': 'ExpShotNoise',
    }
## 1S-2S trajectories metadata
# Absolute path to trajectories metadata
sim_traj_metadata_dir = dir_trajs
## Delay velocities derived from combination of 1S-2S trajectories and 2S-nP big model
# Absolute path to delay velocities
sim_delay_vels_dir = dir_delay_vels
# Delay velocity file(s)
# Settings this to None will load all files ending in '_delay_vels.dat'
# with the directory defined in sim_delay_vels_dir
sim_delay_vels_filenames_default = None

### Line scan fits and derived fits
## Line scan fits
# Define list of available fit functions
available_fit_func_ids_default = [
    'Lorentzian', 'FanoLorentzian', 'Voigt', 'FanoVoigt',
    'VoigtDoublet', 'VoigtDoubletEqualAmps',
    'GenericLineshape',
    ]
# Define list of fit functions used for line scans by default
fit_func_ids_default = ['Lorentzian', 'FanoLorentzian', 'Voigt', 'FanoVoigt']
# Initial/start value for fit parameter Lorentzian FWHM (in Hz)
fit_start_gamma_l = 3.9e6
# Initial/start value for fit parameter Gaussian FWHM (in Hz)
fit_start_gamma_g = 1e7
# MCS channel containing simulation data
MCSchannels = OrderedDict()
MCSchannels['Sim'] = {
    'Name': 'Sim.',
    'LongName': 'Simulation',
    }
fit_mcs_channel_ids_default = ['Sim']
# Frequency sources containing simulation data
freqSrcs = OrderedDict()
freqSrcs['Sim'] = {
    'Name': 'Sim.',
    'PrefixExp': 1,
    'Digits': 0,
    'ControlParam': 'Freq',
    'CreateControl': False,
    'TrigMeasFlag': False,
    'MeasFlag': '',
    'SettlingTime': np.nan,
    'FreqShiftMultiplier': 1,
    'AddFreqShiftParam': True,
    'CombFreq': '',
    }
## Delay analysis
# Define methods to further process scan fits
# e.g. averaging over delays
fit_derivs = OrderedDict()
fit_derivs['DlysAvg'] = {
    'Name': 'Delay',
#    'FitDerivParamIDs': ['Avg'],
    'FitFuncID': 'Avg',
    }
fit_derivs['DlysLin'] = {
    'Name': 'Delay',
#    'FitDerivParamIDs': ['Slope', 'Offset', 'Cov'],
    'FitFuncID': 'Linear',
    }
# Default fit parameters to analyze over delays
fit_derivs_fit_param_ids_default = ['CFR', 'Eta']
## Fit algorithm options,
## used with Levenberg-Marquardt routine LMDIF of MINPACK,
## wrapped through scipy.optimize.leastsq/least_squares/curve_fit
# Value for ftol, xtol, gtol
fit_tol = 1e-14
# Maximum iterations
fit_max_fev = 1000
# A variable used in determining a suitable step length for the
# forward- difference approximation of the Jacobian, only used if no
# analytical definition of Jacobian is supplied.
# If set to None, machine precision will be used.
fit_eps_fcn = 1e-14

### Counter (time) delays
## Multichannel scaler (MCS) delays
delay_sets = pyha.def_delays.delay_sets.copy()
delay_sets_settings = pyha.def_delays.delay_sets_settings.copy()
## Delay set ID of default delay set
delay_set_id_default = '2S6P'

### Experimental parameters
## Frequencies and derived quantities
# 2S-6P frequencies (in Hz)
# CODATA 2014 + using R_inf and r_p from Horbatsch and Hessels 2016 + Kramida 2010 HFS
nu_2s6p_approx = 7.30690e14

### Add additional simulation parameters to DataFrame data format
# Relative path to where JSON files are stored within pyh package
subdir_add_data_formats_pyh = 'static/data_formats'
# List of relative paths to JSON files to load from within pyh package
add_data_format_filepaths_pyh = [
    'pyh_data_format.json',
    'H2S6P2019_data_format.json',
    ]
# List of absolute paths to additional JSON files to load from outside pyh package
add_data_format_filepaths = []
