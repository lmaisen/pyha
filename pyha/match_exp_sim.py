# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ

Match simulation and experimental data to apply frequency corrections,
for both line scan fits and line scan delay extrapolations.
"""

import numpy as np
import pandas as pd

# pyhs
import pyhs.gen
import pyhs.fitfunc
import pyhs.statfunc

# pyha
import pyha.def_params
import pyha.data_ext
import pyha.exp_analysis
import pyha.util.data
import pyha.def_line_sampling

logger = pyhs.gen.get_command_line_logger()

## Shorthands
# Experimental and simulation parameters in DataFrames
from pyha.def_shorthands import dsp__, sp__, dep__, ep__

def get_scans_fit_params_corr(
        DataClass,
        dfAll_s_e,
        analysis_params,
        SimFitClass,
        SimGrids,
        SimGridInters,
        match_scans_params_lfs=None,
        match_data_params_lfs=None,
        match_scans_params_big_model=None,
        match_data_params_big_model=None,
        debug=False
        ):
    """
    Extension of method `pyh.Fit.FitClass.get_scans_fit_params`, which in addition to returning
    experimental fit results also returns the corresponding simulation fit results.
    """

    # DataFrame is modified below, create copy
    dfAll_s_e = dfAll_s_e.copy()

    # Detector definitions
    detectors = pyha.def_params.detectors

    # Match on scans
    group = 'dfData_ScanUID'

    fit_func_id = analysis_params['FitFuncID']
    mcs_channel_ids = [
        detectors[detector_id]['MCSchannelID']
        for detector_id in analysis_params['Detectors']
        ]
    delay_ids = analysis_params['DelayIDs']
    fit_param_ids = analysis_params.get('ReqFitParamIDs')
    param_x = analysis_params.get('ParamX', 'FreqSrc')
    match_exp_sim = analysis_params.get('MatchExpSim', False)
    column_prefix_fit = 'ScanFits_'+fit_func_id+'_'
    column_prefix_params = 'ScanFitParams_'+fit_func_id+'_'

    if not match_exp_sim:

        logger.info(
            'Method \'get_scans_fit_params_corr\' falling back on'
            +' \'DataClass.FitClass.get_scans_fit_params\'')

        # Fall back on DataClass results without simulation corrections
        dfScansFitParams_e, fit_param_ids_retrieved_exp = (
            DataClass.FitClass.get_scans_fit_params(
                dfAll_s_e,
                fit_func_id,
                mcs_channel_ids,
                delay_ids,
                fit_param_ids,
                group=group,
                param_x=param_x,
                return_fit_param_ids=True,
                debug=debug,
                exclude_failed_fits=analysis_params.get('ExclFailedFitsExp', True)
                ))
        dfAll_s_es = dfAll_s_e
        logger.info('Found {:d} experimental fit result(s)'
            .format(len(dfScansFitParams_e)))
        dfScansFitParams_es = dfScansFitParams_e

        # Update analysis parameters
        analysis_params['RetrievedFitParamIDs'] = fit_param_ids_retrieved_exp

    else:

        # Pre-select simulation data
        dfAll_s_s = SimFitClass.get_dataframe('dfAll_scans')

        # Simulation parameters to match
        if match_scans_params_lfs is None:
            match_scans_params_lfs = [
                'Isotope',
                'FS',
                '2SnPLaserPower',
                'AlphaOffset',
                ]
        if match_data_params_lfs is None:
            match_data_params_lfs = [
                '1S2SLaserPower',
                ]
        force_sim_params = (
            list(analysis_params['ForceSimParamsLFS'].keys())
            if analysis_params.get('ForceSimParamsLFS') is not None else [])
        ignore_sim_params = (
            analysis_params['IgnoreSimParamsLFS']
            if analysis_params.get('IgnoreSimParamsLFS') is not None else [])
        match_scans_params_lfs = [
            param for param in match_scans_params_lfs
            if param not in force_sim_params and param not in ignore_sim_params]
        match_data_params_lfs = [
            param for param in match_data_params_lfs
            if param not in force_sim_params and param not in ignore_sim_params]
        match_params_lfs = (
            match_scans_params_lfs
            + match_data_params_lfs
            )
        sp_match_params_lfs = (
            [sp__+param for param in match_scans_params_lfs]
            + [dsp__+param for param in match_data_params_lfs]
            )
        ep_match_params_lfs = (
            [ep__+param for param in match_scans_params_lfs]
            + [dep__+param for param in match_data_params_lfs]
            )
        if match_scans_params_big_model is None:
            match_scans_params_big_model = [
                'Isotope',
                'FS',
                '2SnPLaserPower',
                'AlphaOffset',
                ]
            if analysis_params['UseDetEff']:
                match_scans_params_big_model += ['ThetaL']
        if match_data_params_big_model is None:
            match_data_params_big_model = [
                '1S2SLaserPower',
                ]
        force_sim_params = (
            list(analysis_params['ForceSimParamsBigModel'].keys())
            if analysis_params.get('ForceSimParamsBigModel') is not None else [])
        ignore_sim_params = (
            analysis_params['IgnoreSimParamsBigModel']
            if analysis_params.get('IgnoreSimParamsBigModel') is not None else [])
        match_scans_params_big_model = [
            param for param in match_scans_params_big_model
            if param not in force_sim_params and param not in ignore_sim_params]
        match_data_params_big_model = [
            param for param in match_data_params_big_model
            if param not in force_sim_params and param not in ignore_sim_params]
        match_params_big_model = (
            match_scans_params_big_model
            + match_data_params_big_model
            )
        sp_match_params_big_model = (
            [sp__+param for param in match_scans_params_big_model]
            + [dsp__+param for param in match_data_params_big_model]
            )
        ep_match_params_big_model = (
            [ep__+param for param in match_scans_params_big_model]
            + [dep__+param for param in match_data_params_big_model]
            )
        nm_data_columns = ['1S2SLaserPower']
        nm_scans_columns = []

        # Add 1S-2S laser power of underlying 2S trajectory set to
        # to simulation line scans
        dfTrajParams = SimFitClass.Trajs.dfTrajParams
        dfAll_s_s = (
            dfAll_s_s
            .merge(
                dfTrajParams
                [[
                    'LaserPower',
#                    'AtomicDetuning',
                    ]]
                .rename(columns={
                    'LaserPower': dsp__+'1S2SLaserPower',
#                    'AtomicDetuning': dsp__+'1S2SDetuning',
                    }),
                left_on=sp__+'TrajUID',
                right_index=True,
                how='left',
                ))

        # Select big model simulation results
        logger.info('Finding big model simulation results...')

        # Select 2S trajectory sets
        mask_traj_params = (
            pd.Series(True, index=dfTrajParams.index))
        for traj_param in [
                'SpeedDistExp', 'SpeedDistExpSupprCutOff',
                'NozzleTemp', 'NozzleRadius'
                ]:
            if analysis_params.get(f'Traj{traj_param}') is not None:
                mask_traj_params &= (
                    dfTrajParams[traj_param]
                    == analysis_params[f'Traj{traj_param}'])

        if analysis_params.get('ReqTrajUIDsBigModel') is not None:
            traj_uids_big_model = np.atleast_1d(analysis_params['ReqTrajUIDsBigModel'])
            logger.info(
                f'Using supplied list of {len(traj_uids_big_model):d} 2S trajectory set(s): '
                + ', '.join([f'{elem}' for elem in traj_uids_big_model]))
        else:
            traj_uids_big_model = (
                dfTrajParams[mask_traj_params].index.unique())
            logger.info(
                f'Found {np.sum(mask_traj_params):d} 2S trajectory set(s).')

        # Find grid interpolations on trajectory sets
        dfSimGridInters = SimGridInters.dfSimGridInters
        mask_big_model_grid_inters = (
            pd.Series(True, index=dfSimGridInters.index)
            & (dfSimGridInters['Model'].isin(['BigModel']))
            & (dfSimGridInters['Type'].isin(['TrajInter']))
            & (dfSimGridInters['QI'] == True)
            & (dfSimGridInters['Backdecay'] == True)
            & (dfSimGridInters['BackdecayModel'] == 'NDpD0')
            & (dfSimGridInters['TrajUID'].isin(traj_uids_big_model))
            )
        if analysis_params.get('ReqGridInterUIDsBigModel') is not None:
            grid_inter_uids_big_model = np.atleast_1d(analysis_params['ReqGridInterUIDsBigModel'])
            logger.info(
                f'Using supplied list of {len(grid_inter_uids_big_model):d} grid interpolation(s): '
                + ', '.join([f'{elem}' for elem in grid_inter_uids_big_model]))
            mask_big_model_grid_inters &= (
                (dfSimGridInters['GridInterUID'].isin(grid_inter_uids_big_model)))
        logger.info(
            f'Found {np.sum(mask_big_model_grid_inters):d} big model '
            + 'simulation grid interpolation(s): '
            + ', '.join([f'{elem}' for elem in
                         dfSimGridInters[mask_big_model_grid_inters].index]))
        traj_uids_big_model_selected = (
            dfSimGridInters[mask_big_model_grid_inters]['TrajUID'].unique())
        logger.info(
            'Grid interpolation(s) contain '
            + f'{len(traj_uids_big_model_selected):d} 2S trajectory set(s): '
            + ', '.join([f'{elem}' for elem in
                         traj_uids_big_model_selected]))

        # Find line scans for the selected grid interpolations
        mask_big_model_columns = [
            sp__+'Vbin',
            sp__+'SignalID',
            ]
        mask_big_model_elems_list = [
            False,
            analysis_params.get('SignalID', 'Lyman'),
            ]
        if analysis_params['UseDetEff']:
            mask_big_model_columns += [sp__+'DetEffSetToUnity', sp__+'DetEffUID']
            mask_big_model_elems_list += [False, analysis_params['DetEffUID']]
        else:
            mask_big_model_columns += [sp__+'DetEffSetToUnity']
            mask_big_model_elems_list += [True]
        if (force_sim_params := analysis_params.get('ForceSimParamsBigModel')) is not None:
            for sim_param_id, values in force_sim_params.items():
                mask_big_model_columns += [sp__+sim_param_id]
                values_ = np.atleast_1d(values)
                mask_big_model_elems_list += [values_]
                logger.info(
                    'Forcing big model simulation parameter \'%s\' to: %s',
                    sim_param_id, ', '.join([f'{elem}' for elem in values_])
                    )
        # Apply filters
        dfAll_mask_big_model = pd.Series(True, index=dfAll_s_s.index)
        column_grid = sp__+'GridInterUID'
        elems_grid = dfSimGridInters[mask_big_model_grid_inters].index
        dfAll_mask_big_model_grid = dfAll_s_s[column_grid].isin(elems_grid)
        mask_count = {}
        mask_str = {}
        mask_count[column_grid] = np.sum(dfAll_mask_big_model_grid)
        elems_str = pyha.util.data.elems_to_str(elems_grid)
        mask_str[column_grid] = f'\'{column_grid}\' in [{elems_str}]'
        for column, elems in zip(mask_big_model_columns, mask_big_model_elems_list):
            if elems is not None:
                dfAll_mask_big_model_sub = (
                    dfAll_mask_big_model_grid
                    & (dfAll_s_s[column].isin(np.atleast_1d(elems)))
                    )
                elems_str = pyha.util.data.elems_to_str(elems)
                mask_count[column] = np.sum(dfAll_mask_big_model_sub)
                mask_str[column] = f'(\'{column_grid}\' in [...]) & (\'{column}\' in [{elems_str}])'
                dfAll_mask_big_model &= dfAll_mask_big_model_sub
        dfAll_mask_big_model_line_sampling = pyha.def_line_sampling.select_line_samplings(
            dfAll_s_s, analysis_params['LineSampling'],
            mask=dfAll_mask_big_model_grid, column_prefix=sp__, verbose=False)
        mask_count['LineSampling'] = dfAll_mask_big_model_line_sampling.sum()
        mask_str['LineSampling'] = (
            '('
            +', '.join([f'\'{sp__}{key}\'' for key, _ in analysis_params['LineSampling'].items()])
            +') == ('
            +', '.join([f'\'{elem}\'' for _, elem in analysis_params['LineSampling'].items()])
            +')')
        dfAll_mask_big_model &= dfAll_mask_big_model_line_sampling
        logger.info(
            'Found %d big model simulation scan(s) (filters: %s)',
            np.sum(dfAll_mask_big_model),
            ', '.join([f'{mask_str[column]}: {count:d}' for column, count in mask_count.items()]))

        dfAll_mask_big_model_dets = {}
        dfAll_s_s_big_model_dets = {}
        for detector_id in analysis_params['Detectors']:

            dfAll_mask_big_model_dets[detector_id] = (
                dfAll_mask_big_model
                & (dfAll_s_s[sp__+'Detector'] == detector_id)
                )
            logger.info(
                f'Found {np.sum(dfAll_mask_big_model_dets[detector_id]):d} '
                +  'big model simulation scan(s) '
                + f'for detector \'{detector_id}\''
                + f' from {len(dfAll_s_s[dfAll_mask_big_model][sp__+"GridInterUID"].unique()):d}'
                + ' simulation grid interpolation(s): '
                + ', '.join([f'{elem}' for elem in
                             dfAll_s_s[dfAll_mask_big_model][sp__+"GridInterUID"].unique()]))

            dfAll_s_s_big_model_dets[detector_id] = (
                dfAll_s_s[dfAll_mask_big_model_dets[detector_id]]
                .sort_values(by='dfScans_Timestamp')
                .drop_duplicates(
                    subset=sp_match_params_big_model, keep='last')
                )
            logger.info(
                f'{len(dfAll_s_s_big_model_dets[detector_id]):d} big model '
                + 'simulation scan(s) remaining '
                + 'after dropping duplicates with same simulation parameters')

            for match_param, sp_match_param, ep_match_param in zip(
                    match_params_big_model,
                    sp_match_params_big_model,
                    ep_match_params_big_model):
                match_param_exp_unique_str = [
                    f'{elem}' for elem
                    in dfAll_s_e[ep_match_param].unique()[:10]]
                if len(dfAll_s_e[ep_match_param].unique()) > 10:
                    match_param_exp_unique_str += ['...']
                match_param_sim_unique_str = [
                    f'{elem}' for elem
                    in dfAll_s_s_big_model_dets[detector_id][sp_match_param].unique()]
                logger.info(
                    f'Unique entries for big model simulation match parameter \'{match_param}\' '
                    + f'and detector \'{detector_id}\':'
                    ' Exp.: ' + ', '.join(match_param_exp_unique_str)
                    + '; Big model sim.: '
                    + ', '.join(match_param_sim_unique_str)
                    )

        # Select LFS simulation results
        logger.info('Finding LFS simulation results...')

        traj_uids_lfs = None
        if analysis_params.get('ReqTrajUIDsLFS') is not None:
            traj_uids_lfs = np.atleast_1d(analysis_params['ReqTrajUIDsLFS'])
            logger.info(
                f'Using supplied list of {len(traj_uids_lfs):d} 2S trajectory set(s): '
                + ', '.join([f'{elem}' for elem in traj_uids_lfs]))

        if (lfs_grid_type := analysis_params.get('GridTypeLFS', 'MCDpDavg')) == 'MCDpDavg':
            SimGridsLikeLFS = SimGrids
            logger.info('Using MC simulation grids for LFS correction')
        elif lfs_grid_type == 'TrajInter':
            SimGridsLikeLFS = SimGridInters
            logger.info('Using simulation grid interpolation(s) for LFS correction')
        else:
            msg = (
                f'LFS grid type \'{lfs_grid_type}\' not implemented'
                +', should be either \'MCDpDavg\' or \'TrajInter\'')
            logger.error(msg)
            raise Exception(msg)
        dfSimGridsLikeLFS = SimGridsLikeLFS.dfs[SimGridsLikeLFS.df_id_grids]

        mask_lfs_grids = (
            pd.Series(True, index=dfSimGridsLikeLFS.index)
            & (dfSimGridsLikeLFS['Model'].isin(['DMS']))
            & (dfSimGridsLikeLFS['Type'].isin([lfs_grid_type]))
            & (dfSimGridsLikeLFS['Backdecay'] == True)
            & (dfSimGridsLikeLFS['BackdecayModel'] == 'NDpD1')
            )
        if analysis_params.get('ReqGridInterUIDsLFS') is not None:
            grid_inter_uids_lfs = np.atleast_1d(analysis_params['ReqGridInterUIDsLFS'])
            logger.info(
                f'Using supplied list of {len(grid_inter_uids_lfs):d} grid interpolation(s): '
                + ', '.join([f'{elem}' for elem in grid_inter_uids_lfs]))
            mask_lfs_grids &= (
                (dfSimGridsLikeLFS['GridInterUID'].isin(grid_inter_uids_lfs)))
        if traj_uids_lfs is not None:
            mask_lfs_grids &= (
                (dfSimGridsLikeLFS['TrajUID'].isin(traj_uids_lfs)))
        logger.info(
            f'Found {np.sum(mask_lfs_grids):d} LFS simulation grid(s): '
            + ', '.join([f'{elem}' for elem in dfSimGridsLikeLFS[mask_lfs_grids].index]))
        traj_uids_lfs_selected = (
            dfSimGridsLikeLFS[mask_lfs_grids]['TrajUID'].unique())
        logger.info(
            'Grid(s) contain '
            + f'{len(traj_uids_lfs_selected):d} 2S trajectory set(s): '
            + ', '.join([f'{elem}' for elem in
                         traj_uids_lfs_selected]))

        # Find line scans for the selected LFS grids
        mask_lfs_columns = [
            sp__+'CalcSubset',
            sp__+'SignalID',
            ]
        mask_lfs_elems_list = [
            -1,
            analysis_params.get('SignalID', 'Lyman'),
            ]
        if (force_sim_params := analysis_params.get('ForceSimParamsLFS')) is not None:
            for sim_param_id, values in force_sim_params.items():
                mask_lfs_columns += [sp__+sim_param_id]
                values_ = np.atleast_1d(values)
                mask_lfs_elems_list += [values_]
                logger.info(
                    'Forcing LFS simulation parameter \'%s\' to: %s',
                    sim_param_id, ', '.join([f'{elem}' for elem in values_])
                    )
        # Apply filters
        dfAll_mask_lfs = pd.Series(True, index=dfAll_s_s.index)
        elems_grid = dfSimGridsLikeLFS[mask_lfs_grids].index
        column_grid = sp__+SimGridsLikeLFS.grid_index_name
        dfAll_mask_lfs_grid = dfAll_s_s[column_grid].isin(elems_grid)
        mask_count = {}
        mask_str = {}
        mask_count[column_grid] = np.sum(dfAll_mask_lfs_grid)
        elems_str = pyha.util.data.elems_to_str(elems_grid)
        mask_str[column_grid] = f'\'{column_grid}\' in [{elems_str}]'
        for column, elems in zip(mask_lfs_columns, mask_lfs_elems_list):
            if elems is not None:
                dfAll_mask_lfs_sub = (
                    dfAll_mask_lfs_grid
                    & (dfAll_s_s[column].isin(np.atleast_1d(elems)))
                    )
                elems_str = pyha.util.data.elems_to_str(elems)
                mask_count[column] = np.sum(dfAll_mask_lfs_sub)
                mask_str[column] = f'(\'{column_grid}\' in [...]) & (\'{column}\' in [{elems_str}])'
                dfAll_mask_lfs &= dfAll_mask_lfs_sub
        dfAll_mask_lfs_line_sampling = pyha.def_line_sampling.select_line_samplings(
            dfAll_s_s, analysis_params['LineSampling'],
            mask=dfAll_mask_lfs_grid, column_prefix=sp__, verbose=False)
        mask_count['LineSampling'] = dfAll_mask_lfs_line_sampling.sum()
        mask_str['LineSampling'] = (
            '('
            +', '.join([f'\'{sp__}{key}\'' for key, _ in analysis_params['LineSampling'].items()])
            +') == ('
            +', '.join([f'\'{elem}\'' for _, elem in analysis_params['LineSampling'].items()])
            +')')
        dfAll_mask_lfs &= dfAll_mask_lfs_line_sampling

        logger.info(
            'Found %d LFS simulation scan(s) from %d grid(s): %s (filters: %s)',
            np.sum(dfAll_mask_lfs),
            len(dfAll_s_s[dfAll_mask_lfs][sp__+"GridUID"].unique()),
            ', '.join(
                [f'{elem}' for elem in dfAll_s_s[dfAll_mask_lfs][sp__+"GridUID"].unique()]),
            ', '.join([f'{mask_str[column]}: {count:d}' for column, count in mask_count.items()]))

        dfAll_s_s_lfs = (
            dfAll_s_s[dfAll_mask_lfs]
            .sort_values(by='dfScans_Timestamp')
            .drop_duplicates(
                subset=sp_match_params_lfs,
                keep='last')
            )
        logger.info(
            f'{len(dfAll_s_s_lfs):d} LFS simulation scan(s) remaining '
            + 'after dropping duplicates with same simulation parameters')

        for match_param, sp_match_param, ep_match_param in zip(
                match_params_lfs,
                sp_match_params_lfs,
                ep_match_params_lfs):
            match_param_exp_unique_str = [
                f'{elem}' for elem
                in dfAll_s_e[ep_match_param].unique()[:10]]
            if len(dfAll_s_e[ep_match_param].unique()) > 10:
                match_param_exp_unique_str += ['...']
            match_param_sim_unique_str = [
                f'{elem}' for elem
                in dfAll_s_s_lfs[sp_match_param].unique()]
            logger.info(
                f'Unique entries for LFS simulation match parameter \'{match_param}\' '
                + f'and detector \'{detector_id}\':'
                ' Exp.: ' + ', '.join(match_param_exp_unique_str)
                + '; LFS sim.: '
                + ', '.join(match_param_sim_unique_str)
                )

        # Match experimental scans with simulation scans
        exp_param_columns = [
            column for column in dfAll_s_s.columns if column.startswith('dfScans_ExpParams')]
        sim_param_columns = [
            column for column in dfAll_s_e.columns if column.startswith('dfScans_SimParams')]

        def add_nearest_match_param(dfAll_s_s_, dfAll_s_e,
                                    unique_param,
                                    nm_column,
                                    sp_match_params_big_model,
                                    ep_match_params_big_model,
                                    sim_id,
                                    ):
            """
            Add nearest match in simulations for continuous experimental
            parameters.
            """
            mask_s_match = (
                (pyha.util.data.get_mask_unique(
                    dfAll_s_s_,
                    unique_param)))
            ep_unique_param = unique_param.rename(
                {sp_col: ep_col for sp_col, ep_col
                 in zip(sp_match_params_big_model, ep_match_params_big_model)})
            mask_e_match = (
                (pyha.util.data.get_mask_unique(
                    dfAll_s_e,
                    ep_unique_param)))
            unique_sim_values = (
                dfAll_s_s_
                [mask_s_match][_sp__+nm_column].unique())
            exp_values_to_match = (
                dfAll_s_e[mask_e_match][_ep__+nm_column].values)
            match_values = (
                unique_sim_values[
                    np.abs(
                        exp_values_to_match[:, np.newaxis]
                        -unique_sim_values[np.newaxis, :]
                    ).argmin(axis=-1)])
            dfAll_s_e.loc[
                mask_e_match,
                _ep__+nm_column+f'_Nearest_{sim_id}'] = (
                    match_values)

            return dfAll_s_e

        # Find nearest match in simulations for continuous experimental
        # parameters, taking into account that there are different
        # simulations for different non-continous experimental parameters
        for _ep__, _sp__, nm_columns in zip(
                [dep__, ep__],
                [dsp__, sp__],
                [nm_data_columns, nm_scans_columns],
                ):
            for nm_column in nm_columns:

                # Big model
                dfAll_s_e[_ep__+nm_column+'_Nearest_BigModel'] = np.nan
                for detector_id in analysis_params['Detectors']:

                    dfAll_s_s_ = dfAll_s_s_big_model_dets[detector_id]
                    unique_params = (
                        dfAll_s_s_.drop_duplicates(
                            subset=sp_match_params_big_model)
                        [sp_match_params_big_model])
                    if _sp__+nm_column in unique_params:
                        unique_params = unique_params.drop(columns=_sp__+nm_column)

                    for _, unique_param in unique_params.iterrows():

                        dfAll_s_e = (
                            add_nearest_match_param(
                                dfAll_s_s_, dfAll_s_e,
                                unique_param,
                                nm_column,
                                sp_match_params_big_model,
                                ep_match_params_big_model,
                                'BigModel'))

                # LFS
                dfAll_s_e[_ep__+nm_column+'_Nearest_LFS'] = np.nan
                dfAll_s_s_ = dfAll_s_s_lfs
                unique_params = (
                    dfAll_s_s_.drop_duplicates(
                        subset=sp_match_params_lfs)
                    [sp_match_params_lfs])
                if _sp__+nm_column in unique_params:
                    unique_params = unique_params.drop(columns=_sp__+nm_column)

                for _, unique_param in unique_params.iterrows():

                    dfAll_s_e = (
                        add_nearest_match_param(
                            dfAll_s_s_, dfAll_s_e,
                            unique_param,
                            nm_column,
                            sp_match_params_lfs,
                            ep_match_params_lfs,
                            'LFS'))

        ep_match_scans_params_big_model_nearest = [
            ep__+elem+'_Nearest_BigModel' if elem in nm_data_columns
            else ep__+elem
            for elem in match_scans_params_big_model
            ]
        ep_match_data_params_big_model_nearest = [
            dep__+elem+'_Nearest_BigModel' if elem in nm_data_columns
            else dep__+elem
            for elem in match_data_params_big_model
            ]
        ep_match_scans_params_lfs_nearest = [
            ep__+elem+'_Nearest_LFS' if elem in nm_data_columns
            else ep__+elem
            for elem in match_scans_params_lfs
            ]
        ep_match_data_params_lfs_nearest = [
            dep__+elem+'_Nearest_LFS' if elem in nm_data_columns
            else dep__+elem
            for elem in match_data_params_lfs
            ]

        # Add scans of big model simulation results to matching experimental scans
        dfAll_s_es = (
            dfAll_s_e.copy()
            .drop(columns=sim_param_columns))
        _s_big_model = '_BigModel'
        _s_big_model_ = _s_big_model+'_'
        for detector_id in analysis_params['Detectors']:

            _s_big_model_det = _s_big_model_+detector_id
            dfAll_s_es = (
                dfAll_s_es
                .reset_index()
                .merge(
                    dfAll_s_s_big_model_dets[detector_id]
                    .drop(columns=exp_param_columns)
                    .sort_values(by='dfScans_Timestamp')
                    .rename(columns=lambda x: x+_s_big_model_det)
                    ,
                    left_on=(
                        ep_match_scans_params_big_model_nearest
                        + ep_match_data_params_big_model_nearest),
                    right_on=[
                        param+_s_big_model_det
                        for param in sp_match_params_big_model],
                    how='inner',
                    suffixes=('', _s_big_model_det),
                )
                .set_index('ScanUID')
                )
            ratio = (
                len(dfAll_s_es)/len(dfAll_s_e) if len(dfAll_s_e) > 0
                else np.nan)
            logger.info(
                'Found big model simulation result(s) for '
                + f'detector \'{detector_id}\' for '
                + f'{len(dfAll_s_es):d} '
                + f'of {len(dfAll_s_e):d} experimental scans(s) ({ratio*1e2:.2f}%)')

        # Add scans of LFS simulation results to matching experimental scans
        _s_lfs = '_LFS'
        dfAll_s_es = (
            dfAll_s_es
            .reset_index()
            .merge(
                dfAll_s_s_lfs
                .drop(columns=exp_param_columns)
                .sort_values(by='dfScans_Timestamp')
                .rename(columns=lambda x: x+_s_lfs)
                ,
                left_on=(
                    ep_match_scans_params_lfs_nearest
                    + ep_match_data_params_lfs_nearest),
                right_on=[
                    param+_s_lfs
                    for param in sp_match_params_lfs],
                how='inner',
                suffixes=('', _s_lfs),
            )
            .set_index('ScanUID')
            )
        ratio = (
            len(dfAll_s_es)/len(dfAll_s_e) if len(dfAll_s_e) > 0
            else np.nan)
        logger.info(
            f'Found LFS simulation result(s) for {len(dfAll_s_es):d} '
            + f'of {len(dfAll_s_e):d} experimental scans(s) ({ratio*1e2:.2f}%)')

        # Simulation scans to load, i.e. that were matched to experimental data
        for i_detector, detector_id in enumerate(analysis_params['Detectors']):
            scan_uids_big_model = (
                dfAll_s_es['dfData_ScanUID'+_s_big_model_+detector_id].unique())
            dfAll_s_s_big_model_dets[detector_id] = (
                dfAll_s_s_big_model_dets[detector_id].loc[scan_uids_big_model])
        scan_uids_lfs = dfAll_s_es['dfData_ScanUID'+_s_lfs].unique()
        dfAll_s_s_lfs = dfAll_s_s_lfs.loc[scan_uids_lfs]

        for detector_id in analysis_params['Detectors']:
            for match_param, sp_match_param, ep_match_param in zip(
                    match_params_big_model,
                    sp_match_params_big_model,
                    ep_match_params_big_model):
                match_param_sim_str = (
                    ', '.join([str(elem) for elem
                               in dfAll_s_es[sp_match_param+_s_big_model_+detector_id].unique()]))
                logger.info(
                    f'Values used for big model simulation parameter \'{match_param}\' '
                    + f'and detector \'{detector_id}\': '
                    + match_param_sim_str)

        for match_param, sp_match_param, ep_match_param in zip(
                match_params_lfs,
                sp_match_params_lfs,
                ep_match_params_lfs):
            match_param_sim_str = (
                ', '.join([str(elem) for elem
                           in dfAll_s_es[sp_match_param+_s_lfs].unique()]))
            logger.info(
                f'Values used for LFS simulation parameter \'{match_param}\': '
                + match_param_sim_str)

        # Match experimental and big model fit results
        dfScansFitParams_es = pd.DataFrame()
        for i_detector, detector_id in enumerate(analysis_params['Detectors']):

            detector = detectors[detector_id]

            if analysis_params.get('ApplyChExcl', True):
                ch_excl_column = ep__+f'ChExcl_{detector["MCSchannelID"]}'
                mask_ch_excl = (
                    (dfAll_s_e[ch_excl_column] == False)
                    )
                logger.info(
                    f'{len(dfAll_s_e)-np.sum(mask_ch_excl):d} experimental scan(s) '
                    + 'are excluded by metadata '
                    + f'for detector \'{detector_id}\'')
            else:
                mask_ch_excl = pd.Series(True, index=dfAll_s_e.index)

            # Get experimental fit results
            dfScansFitParams_e_det, fit_param_ids_retrieved_exp = (
                DataClass.FitClass.get_scans_fit_params(
                    dfAll_s_e[mask_ch_excl],
                    fit_func_id,
                    [detectors[detector_id]['MCSchannelID']],
                    delay_ids,
                    fit_param_ids,
                    group=group,
                    param_x=param_x,
                    return_fit_param_ids=True,
                    debug=debug,
                    exclude_failed_fits=analysis_params.get('ExclFailedFitsExp', True)
                    ))
            # Drop empty simulation parameter columns
            dfScansFitParams_e_det = (
                dfScansFitParams_e_det.drop(columns=sim_param_columns))
            logger.info(
                f'Found {len(dfScansFitParams_e_det):d} experimental fit result(s) '
                + f'for detector \'{detector_id}\'')

            # Get big model simulation fit results
            dfScansFitParams_big_model_det, _ = (
                SimFitClass.get_scans_fit_params(
                    dfAll_s_s_big_model_dets[detector_id],
                    fit_func_id,
                    ['Sim'],
                    delay_ids,
                    fit_param_ids_retrieved_exp,
                    group=group,
                    param_x='FreqSrc',
                    return_fit_param_ids=True,
                    debug=debug,
                    exclude_failed_fits=analysis_params.get('ExclFailedFitsBigModel', True)
                    ))
            # from IPython import embed; embed()
            # Drop empty experimental parameter columns
            dfScansFitParams_big_model_det = (
                dfScansFitParams_big_model_det.drop(columns=exp_param_columns))
            logger.info(
                f'Found {len(dfScansFitParams_big_model_det):d} big model simulation fit result(s)')

            # Merge experimental fit results with simulation fit results
            match_params_fits = (
                []
                + ['dfScanFits_DelaySetID']
                + ['dfScanFits_DelayID']
                )

            dfScansFitParams_es_det = (
                dfScansFitParams_e_det
                .reset_index()
                .merge(
                    dfScansFitParams_big_model_det
                    .rename(columns=lambda x: x+_s_big_model),
                    left_on=(
                        match_params_fits
                        + ep_match_scans_params_big_model_nearest
                        + ep_match_data_params_big_model_nearest),
                    right_on=(
                        [param+_s_big_model for param in match_params_fits]
                        + [param+_s_big_model for param in sp_match_params_big_model]),
                    how='inner',
                    suffixes=('', _s_big_model),
                )
                .set_index('ScanUID')
                )

            ratio = (
                len(dfScansFitParams_es_det)/len(dfScansFitParams_e_det)
                if len(dfScansFitParams_e_det) > 0 else np.nan)
            logger.info(
                f'Found big model simulation result(s) for {len(dfScansFitParams_es_det):d} of '
                + f'{len(dfScansFitParams_e_det):d} experimental fit result(s) ({ratio*1e2:.2f}%) '
                + f'for detector \'{detector_id}\'')

            if i_detector == 0:
                dfScansFitParams_es = dfScansFitParams_es_det
            else:
                dfScansFitParams_es = pd.concat([dfScansFitParams_es, dfScansFitParams_es_det])

        # Get LFS simulation fit results
        dfScansFitParams_lfs, _ = SimFitClass.get_scans_fit_params(
            dfAll_s_s_lfs,
            fit_func_id,
            ['Sim'],
            delay_ids,
            fit_param_ids_retrieved_exp,
            group=group,
            param_x='FreqSrc',
            return_fit_param_ids=True,
            debug=debug,
            exclude_failed_fits=analysis_params.get('ExclFailedFitsLFS', True)
            )
        # Drop empty experimental parameter columns
        dfScansFitParams_lfs = (
            dfScansFitParams_lfs.drop(columns=exp_param_columns))
        logger.info(
            f'Found {len(dfScansFitParams_lfs):d} LFS simulation fit result(s)')

        num_fits_pre_lfs = len(dfScansFitParams_es)

        dfScansFitParams_es = (
            dfScansFitParams_es
            .reset_index()
            .merge(
                dfScansFitParams_lfs
                .rename(columns=lambda x: x+_s_lfs),
                left_on=(
                    match_params_fits
                    + ep_match_scans_params_lfs_nearest
                    + ep_match_data_params_lfs_nearest),
                right_on=(
                    [param+_s_lfs for param in match_params_fits]
                    + [param+_s_lfs for param in sp_match_params_lfs]),
                how='inner',
                suffixes=('', _s_lfs),
            )
            .set_index('ScanUID')
            )

        ratio = (
            len(dfScansFitParams_es)/num_fits_pre_lfs
            if num_fits_pre_lfs > 0 else np.nan)
        logger.info(
            f'Found LFS simulation result(s) for {len(dfScansFitParams_es):d} of '
            + f'{num_fits_pre_lfs:d} experimental fit result(s) ({ratio*1e2:.2f}%)')

        # Merge delay velocities for each fit result,
        # using simulation parameters of big model results
        dfScansFitParams_es = (
            SimFitClass.DelayVels.add_delay_vels_to_scan_fit_params(
                dfScansFitParams_es,
                sim_params_suffix=_s_big_model,
                exclude_sim_param_ids=analysis_params.get('IgnoreSimParamsDelayVels'),
                force_sim_params=analysis_params.get('ForceSimParamsDelayVels'),
                ))
        # Defragmenting DataFrame
        dfScansFitParams_es = dfScansFitParams_es.copy()

        # Calculate angle alpha between atomic beam and laser beam from
        # splitting of the two Doppler-shifted peaks of fit function VoigtDoublet
        if analysis_params['FitFuncID'] == 'VoigtDoublet':

            (dfScansFitParams_es[column_prefix_params+'AlphaDeltaNu_Value'],
            dfScansFitParams_es[column_prefix_params+'AlphaDeltaNu_Sigma']) = (
                pyhs.fitfunc.VoigtDoublet_AlphaDeltaNu(
                    dfScansFitParams_es[column_prefix_params+'DeltaNu_Value'],
                    dfScansFitParams_es[column_prefix_params+'DeltaNu_Sigma'],
                    dfScansFitParams_es['dfDelayVels_V_Mean_Value'],
                    analysis_params['Freq2SnP'],
                    ))

            sim_id = 'BigModel'
            (dfScansFitParams_es[column_prefix_params+f'AlphaDeltaNu_Value_{sim_id}'],
            dfScansFitParams_es[column_prefix_params+f'AlphaDeltaNu_Sigma_{sim_id}']) = (
                pyhs.fitfunc.VoigtDoublet_AlphaDeltaNu(
                    dfScansFitParams_es[column_prefix_params+f'DeltaNu_Value_{sim_id}'],
                    dfScansFitParams_es[column_prefix_params+f'DeltaNu_Sigma_{sim_id}'],
                    dfScansFitParams_es['dfDelayVels_V_Mean_Value'],
                    analysis_params['Freq2SnP'],
                    ))

            # Cast dtype of just assigned columns as float
            for column in [
                    column_prefix_params+'AlphaDeltaNu_Value',
                    column_prefix_params+'AlphaDeltaNu_Sigma',
                    column_prefix_params+f'AlphaDeltaNu_Value_{sim_id}',
                    column_prefix_params+f'AlphaDeltaNu_Sigma_{sim_id}',
                    ]:
                dfScansFitParams_es[column] = (
                    dfScansFitParams_es[column].astype(float))

        # Calculate second-order Doppler shift for each fit, using
        # RMS delay velocities
        if analysis_params.get('Freq2SnP') is not None:
            sod_value, _ = pyha.exp_analysis.calc_sec_doppler_shift(
                dfScansFitParams_es['dfDelayVels_V_RMS_Value'],
                dfScansFitParams_es['dfDelayVels_V_RMS_Sigma'],
                analysis_params.get('Freq2SnP'))
            dfScansFitParams_es[column_prefix_params+'CFR_Value_SOD'] = sod_value
            # Cast dtype of column as float
            dfScansFitParams_es[column_prefix_params+'CFR_Value_SOD'] = (
                dfScansFitParams_es[column_prefix_params+'CFR_Value_SOD'].astype(float))
        else:
            logger.info(
                'No laser frequency for second-order Doppler correction given '
                'in analysis parameters (\'Freq2SnP\')')
            dfScansFitParams_es[column_prefix_params+'CFR_Value_SOD'] = np.nan

        # Scale simulation corrections if requested
        for sim_id in ['BigModel', 'LFS', 'SOD']:
            # Copy non-scaled value
            dfScansFitParams_es[column_prefix_params+f'CFR_Value_{sim_id}_Nonscaled'] = (
                dfScansFitParams_es[column_prefix_params+f'CFR_Value_{sim_id}'])
            if (scale_factor := analysis_params.get(f'ScaleCorrection{sim_id}')) is not None:
                dfScansFitParams_es[column_prefix_params+f'CFR_Value_{sim_id}'] = (
                    scale_factor*dfScansFitParams_es[column_prefix_params+f'CFR_Value_{sim_id}'])
                logger.info('Scaled %s correction by scale factor %.3e', sim_id, scale_factor)

        # Correct frequencies, scale uncertainty by red. χ^2
        fit_param_id_to_correct = 'CFR'
        param_id_y_ = column_prefix_params+fit_param_id_to_correct
        if param_id_y_+'_Value' in dfScansFitParams_es:
            if analysis_params.get('ScaleByScanFitRCS', False):
                # Scale uncertainty of fit result of each line scan
                # by sqrt. of red. χ^2
                column_to_scale = param_id_y_+'_Sigma'
                dfScansFitParams_es[column_to_scale] = (
                    pyhs.statfunc.scale_by_rcs(
                        dfScansFitParams_es[column_to_scale],
                        dfScansFitParams_es[column_prefix_fit+'RedChiSq'],
                        ))
            # Keep uncorrected value
            dfScansFitParams_es[param_id_y_+'_Value_Exp'] = (
                dfScansFitParams_es[param_id_y_+'_Value'])
            for analysis_param_key, sim_id, sim_name in zip(
                    ['CorrectCFRBigModel', 'CorrectCFRLFS', 'CorrectSOD'],
                    ['BigModel', 'LFS', 'SOD'],
                    ['using big model simulation', 'using LFS simulation',
                     'for second-order Doppler shift']
                    ):
                if analysis_params.get(analysis_param_key, False):
                    # The measured frequencies are corrected with the simulation results by
                    # *subtracting* the resonance frequency observed in the simulation, because
                    # the latter corresponds to the shift of resonance by the effects contained
                    # in the simulation. By subtracting the shift from the measured resonance
                    # frequencies, the shift is corrected for.
                    dfScansFitParams_es[column_prefix_params+'CFR_Value'] -= (
                        dfScansFitParams_es[column_prefix_params+f'CFR_Value_{sim_id}'])
                    avg_correction = np.mean(
                        -dfScansFitParams_es[column_prefix_params+f'CFR_Value_{sim_id}'])
                    logger.info(
                        'Corrected frequency (fit param \'%s\') '
                        + 'of experimental results %s (\'%s correction\'), '
                        + 'avg. correction %.2f Hz',
                        fit_param_id_to_correct, sim_name, sim_id, avg_correction)
                    analysis_params[f'AvgCorrection{sim_id}'] = avg_correction

        # Update analysis parameters
        analysis_params['RetrievedFitParamIDs'] = fit_param_ids_retrieved_exp
        sim_ids = ['BigModel', 'LFS']
        grid_uids_used = {}
        for sim_id in sim_ids:
            grid_uids_used[sim_id] = (
                dfScansFitParams_es[f'dfScans_SimParams_GridUID_{sim_id}'].unique())
            analysis_params[f'UsedGridUIDs{sim_id}'] = grid_uids_used[sim_id]
        sim_id = 'BigModel'
        grid_inter_uids_used = (
            dfScansFitParams_es[f'dfScans_SimParams_GridInterUID_{sim_id}'].unique())
        analysis_params[f'UsedGridInterUIDs{sim_id}'] = grid_inter_uids_used
        analysis_params[f'UsedTrajUIDs{sim_id}'] = (
            dfSimGridInters.loc[grid_inter_uids_used]['TrajUID'].values)
        sim_id = 'LFS'
        grid_inter_uids_used = (
            dfScansFitParams_es[f'dfScans_SimParams_GridInterUID_{sim_id}'].unique())
        analysis_params[f'UsedGridInterUIDs{sim_id}'] = grid_inter_uids_used
        grid_like_uids_used = (
            dfScansFitParams_es
            [f'dfScans_SimParams_{SimGridsLikeLFS.grid_index_name}_{sim_id}'].unique())
        analysis_params[f'UsedTrajUIDs{sim_id}'] = (
            dfSimGridsLikeLFS.loc[grid_like_uids_used]['TrajUID'].values)
        analysis_params['UsedDelayVelsUIDs'] = (
            dfScansFitParams_es['dfDelayVels_DelayVelsUID'].unique())

    return dfAll_s_es, dfScansFitParams_es, analysis_params

def get_scans_fit_derivs_corr(
        DataClass,
        dfAll_s_es,
        fit_func_id,
        mcs_channel_ids,
        fit_param_id,
        dfScansFitParams_es=None,
        dfScanFitDerivs=None,
        group='dfData_ScanUID',
        param_x='FreqSrc',
        recalculate_derivs=False,
        use_joined_delay_vels=True,
        **kwargs
        ):

    # Fit scan delays with corrected frequencies
    if recalculate_derivs:
        dfScanFitDerivs_es = DataClass.FitClass._fit_scans_delays(
            dfAll_s_es, dfScansFitParams_es,
            fit_func_id, fit_param_id,
            param_x=param_x, use_joined_delay_vels=use_joined_delay_vels)
    else:
        if dfScanFitDerivs is None:
            dfScanFitDerivs_es = DataClass.FitClass.dfScanFitDerivs
        else:
            dfScanFitDerivs_es = dfScanFitDerivs

    # Get dfScansFitDerivs
    dfScansFitDerivs_es = DataClass.FitClass.get_scans_fit_derivs(
        dfAll_s_es,
        fit_func_id,
        mcs_channel_ids,
        fit_param_id,
        group=group,
        param_x=param_x,
        dfScanFitDerivs=dfScanFitDerivs_es,
        **kwargs
        )
    logger.info(
        'Calculated %d scan fit derivative(s) with simulation corrections',
        len(dfScansFitDerivs_es))

    return dfScanFitDerivs_es, dfScansFitDerivs_es
