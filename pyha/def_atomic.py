# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ

Various constants and functions describing atomic properties.
"""

import numpy as np

### Atomic properties
atomic_properties = {}
## Hydrogen
atomic_properties['H6P12'] = {
    # Transition frequency (Hz)
    'TransitionFreq': 730690111486.4e3,
    # Recoil velocity (m/s)
    'RecoilVelocity': 0.965016,
    # Natural linewidth (Hz)
    'Gamma': 3894977,
    }
# Duplicate entry under legacy key
atomic_properties['6P12'] = atomic_properties['H6P12']
atomic_properties['H6P32'] = {
    'TransitionFreq': 730690516650.9e3,
    'RecoilVelocity': 0.965016,
    'Gamma': 3894983,
    }
# Duplicate entry under legacy key
atomic_properties['6P32'] = atomic_properties['H6P32']
atomic_properties['H4P12'] = {
    'TransitionFreq': 616520152558.5e3,
    'RecoilVelocity': 0.814232,
    'Gamma': 12920479,
    }
# Duplicate entry under legacy key
atomic_properties['4P12'] = atomic_properties['H4P12']
atomic_properties['H4P32'] = {
    'TransitionFreq': 616521519991.8e3,
    'RecoilVelocity': 0.814234,
    'Gamma': 12920522,
    }
# Duplicate entry under legacy key
atomic_properties['4P32'] = atomic_properties['H4P32']
## Deuterium
atomic_properties['D6P12'] = {
    'TransitionFreq': 730888823e6,
    'RecoilVelocity': 0.483010,
    'Gamma': 3898157,
    }
atomic_properties['D6P32'] = {
    'TransitionFreq': 730889230e6,
    'RecoilVelocity': 0.483010,
    'Gamma': 3898162,
    }

# Ratio of squared Rabi frequency for 2S-nP, J transition relative to J = 1/2
relative_rabi_frequency_sqr_2snp12 = {
    '4P12': 1,
    '4P32': 2,
    '6P12': 1,
    '6P32': 2,
    }

# Intensity emission patterns of spherical components
def decay_spherical_comp_int_pi(theta1, theta, phi):
    """
    Intensity emission pattern in spherical coordinates with polar angle `theta` (rad, flat),
    and azimuthal angle `phi` (rad, float) for pi (q=0) decay,
    oriented at angle `theta1` (rad, float) from vertical.
    """
    return (
        3/(8*np.pi)
        * (1 - (np.sin(theta1)*np.sin(theta)*np.cos(phi) + np.cos(theta)*np.cos(theta1))**2))

def decay_spherical_comp_int_sigma(theta1, theta, phi):
    """
    Intensity emission pattern in spherical coordinates with polar angle `theta` (rad, flat),
    and azimuthal angle `phi` (rad, float) for sigma+/- (q=+/-1) decay,
    oriented at angle `theta1` (rad, float) from vertical.
    """
    return (
        3/(16*np.pi)
        * (1 + (np.sin(theta1)*np.sin(theta)*np.cos(phi) + np.cos(theta)*np.cos(theta1))**2))

def quenched_2s_lifetime(field):
    """
    Lifetime (s) of 2S F=0 level with static electric field of
    strength `field` (V/m, float) applied, valid better than 1% for `field` > 47 V/m.
    """
    return 2.6/field**2

def dc_stark_2s(field):
    """
    dc-Stark shift (Hz) of  2S F=0 level with static electric field of
    strength `field` (V/m) applied.
    """
    return 442e-3*field**2
