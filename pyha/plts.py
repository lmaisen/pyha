# -*- coding: utf-8 -*-
"""
Created on Sun May  5 12:05:18 2019

@author: Lothar Maisenbacher/MPQ

Set plot style as used for my thesis.

Check that all required fonts are installed on your system.
To check which fonts have been found by Matplotlib, it is easiest to check the
JSON cache file 'fontlist-vXXX.json', which is located in the cache folder
found by the command `matplotlib.get_cachedir()`.
To refresh this file, delete it and reimport Matplotlib.
If Matplotlib does not assign the correct parameters to the font files
(there is some guessing involved), one can either manually edit this file
(but it will overwritten the next time the cache is built, which does not seem
to happen often) or point Matplotlib explicitly to the font file instead of
setting parameters:
font_manual = font_manager.FontProperties(fname=fpath)
where fpath is the path to the font file, e.g.
'C:\\Windows\\Fonts\\MyriadPro-Cond.otf'.
It might also be necessary to monkey-patch as Matplotlib, as described in:
https://stackoverflow.com/a/33962423
On Windows, make sure that the fonts needed are actually installed in 'C:\\Windows\\Fonts',
which is only the case if the font is installed for all users. By
default, it is only installed for the active user and store elsewhere, where it
is not found by Matplotlib. To install for all users, right click on the
font file and select 'Install for all users' (on Windows 10, 11).
On Linux, it seems sufficient to place the font files in the local directory '~/.fonts'.

List of fonts that need to be installed:
MYRIADPRO-BOLD.OTF
MYRIADPRO-BOLDCOND.OTF
MYRIADPRO-BOLDCONDIT.OTF
MYRIADPRO-BOLDIT.OTF
MYRIADPRO-COND.OTF
MYRIADPRO-CONDIT.OTF
MyriadPro-It.otf
MyriadPro-Light.otf
MYRIADPRO-REGULAR.OTF
MYRIADPRO-SEMIBOLD.OTF
MYRIADPRO-SEMIBOLDIT.OTF
Apple Chancery Regular.ttf
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import font_manager
from matplotlib import colormaps

# pyhs
import pyhs.gen

logger = pyhs.gen.get_command_line_logger()

# Page width (linewidth in Latex) of document (in mm)
# For \documentclass[a4paper,12pt]{book}
#pagewidth_mm = 161.9
# For \documentclass[a4paper,11pt]{book}
pagewidth_mm = 155.5
# In inch
pagewidth = pagewidth_mm/25.4

# Default Matplotlib parameters
params = {
    'FontsizeReg': 9,
    'FontsizeMed': 9,
    'FontsizeSmall': 8,
    }

params_rc_default = {
    # Use mathtext of Matplotlib, not LaTeX for math rendering,
    # mathtext is much faster
    'text.usetex' : False,
    'font.size' : params['FontsizeReg'],
    'font.family' : 'sans-serif',
    'font.sans-serif' : 'Myriad Pro',
    'font.stretch' : 'normal',
    'mathtext.fontset' : 'custom',
    'mathtext.rm': 'Myriad Pro',
    'mathtext.it': 'Myriad Pro:italic',
    'mathtext.bf': 'Myriad Pro:bold',
    'mathtext.sf': 'Myriad Pro',
    'mathtext.cal': 'Apple Chancery',
    'mathtext.fallback': 'cm',
#    'mathtext.fallback_to_cm': True,
#    # Default figure size (in)
    'figure.figsize': (pagewidth, 3.5),
#    # DPI of display figures (adapt to screen as needed)
    'figure.dpi': 150,
#    # DPI of saved figures
    'savefig.dpi': 600,
#    # Use unicode minus symbol
    'axes.unicode_minus':  True,
#    # Width of axes
    'axes.linewidth': 0.7,
#    # Width of axes x and y ticks
    'xtick.major.width': 0.7,
    'xtick.minor.width': 0.7,
    'ytick.major.width': 0.7,
    'ytick.minor.width': 0.7,
#    # Setting pdf.fonttype to 3 will outline fonts in the PDF,
#    # which is necessary for math text
    'pdf.fonttype': 3,
    }

# Function definitions
def set_params(params_rc=None):
    """Set Matplotlib parameters."""
    if params_rc is None:
        params_rc = {}
    plt.rcParams.update({
        **params_rc_default, **params_rc})

def set_fontsize(fontsize_id):
    """Set Matplotlib fontsize."""
    params_rc = {'font.size' : params[fontsize_id]}
    plt.rcParams.update(params_rc)

def get_marker(i, markers=None):
    """
    Get marker with index `i` from list `markers`,
    repeating after `i` has reached length of list.
    If `marker` is set to None, a default list defined inside the funcion is used.
    """
    if markers is None:
        markers = ['o', 'd', 's', 'p', 'v', '^']
    return markers[np.mod(i, len(markers))]

def get_linestyle(i, linestyles=None):
    """
    Get linestyle with index `i` from list `linestyles`,
    repeating after `i` has reached length of list.
    If `linestyles` is set to None, a default list defined inside the funcion is used.
    """
    if linestyles is None:
        linestyles = ['-', '--', ':', '-.']
    return linestyles[np.mod(i, len(linestyles))]

def get_line_colors(num_colors=None, cmap=None):
    """
    Get array of integer `num_colors` line colors,
    repeating after N colors, where N is given by the colormap used.
    """
    if cmap is None:
        cmap = 'tab20'
    cmaps_unique_colors = {
        'tab10': 10,
        'tab20': 20,
        'tab20b': 20,
        'tab20c': 20,
        }
    unique_colors = cmaps_unique_colors.get(cmap, 10)
    if num_colors is None:
        num_colors = unique_colors
    h_cmap = colormaps.get_cmap(cmap)
    colors = h_cmap(np.linspace(0, 1, unique_colors))
    colors = (np.tile(
        colors,
        [int(np.ceil(num_colors/unique_colors)), 1])
        [:num_colors])
    return colors

def get_cont_colors(num, start, stop, cmap=None):
    """
    Get list of colors of length `num` from colormap `cmap` (default None, in which case 'GnBu' is
    used), from colormap index `start` to `stop`.
    """
    if cmap is None:
        cmap = 'GnBu'
    x = np.linspace(start, stop, num)
    return colormaps.get_cmap(cmap)(x)[:, :3]

def get_delay_colors(delay_ids=None, colors=None, delay_color_inds_fixed=None):
    """Get dictionary of colors to use to visualize delays."""
    if delay_ids is None:
        delay_ids = [f'{i:d}' for i in range(1, 17)]
    if colors is None:
        colors = get_line_colors(len(delay_ids), cmap='tab20')
    if delay_color_inds_fixed is None:
        delay_color_inds_fixed = {
            delay_id: 2*i for i, delay_id in enumerate([
                '2', '6', '10', '13', '14', '15', '16'])
            }
#        delay_color_inds_fixed = {
#            **delay_color_inds_fixed,
#            '8': 6,
#            '10': 4,
#            }
    colors_inds_avail = (
        np.arange(0, len(delay_ids))
        [~np.in1d(np.arange(0, len(delay_ids)), list(delay_color_inds_fixed.values()))
        ])
    delay_color_inds = delay_color_inds_fixed.copy()
    i_color = 0
    for delay_id in delay_ids:
        if delay_id in delay_color_inds_fixed.keys():
            continue
        delay_color_inds[delay_id] = colors_inds_avail[i_color]
        i_color += 1
    delay_colors = {
        delay_id: colors[color_ind]
        for delay_id, color_ind in delay_color_inds.items()}

    return delay_colors

def get_data_group_colors(data_group_ids=None):
    """Get dictionary of colors to use to visualize data groups."""
    c_tab20b = get_line_colors(cmap='tab20b')
    c_tab20c = get_line_colors(cmap='tab20c')
    dg_colors = {
        'G1A': c_tab20b[13],
        'G1B': c_tab20c[0],
        'G2': c_tab20c[1],
        'G3': c_tab20c[2],
        'G7A': c_tab20b[9],
        'G7B': c_tab20c[4],
        'G8': c_tab20c[5],
        'G9': c_tab20c[6],
        'G1C': c_tab20b[15],
        'G13': c_tab20c[8],
        'G14': c_tab20c[10],
        'G4': c_tab20c[12],
        'G5': c_tab20c[13],
        'G6': c_tab20c[14],
        'G10': c_tab20c[16],
        'G11': c_tab20c[17],
        'G12': c_tab20c[18],
        }

    return dg_colors

def autofmt_xdate(ax, rotation=30, ha='right'):
    """
    Rotate and align date labels on x-axis,
    imitating `plt.autofmt_xdate` but with support for constrained layout.
    """
    for label in ax.get_xticklabels():
        label.set_ha(ha)
        label.set_rotation(rotation)

# Set default Matplotlib parameters
set_params()

# Get default font
font_default = font_manager.FontProperties()

# Get condensend font
font_condensed = font_default.copy()
font_condensed.set_stretch('condensed')
# Set font manually by directly pointing to file
#fpath = 'C:\\Windows\\Fonts\\MyriadPro-Cond.otf'
#if os.path.exists(fpath):
#    font_condensed = font_manager.FontProperties(fname=fpath)

# Title parameters
title_params = {
    'fontsize': params['FontsizeReg']
    }

def get_title_params(**kwargs):
    params_ = {**title_params, **kwargs}
    return params_

# Legend parameters
leg_params_default = {
    'fontsize': params['FontsizeSmall'],
    'handlelength': 0.7,
    'handletextpad': 0.5,
    'borderpad': 0.2,
    'columnspacing': 0.7,
    'edgecolor': '1',
    'facecolor': '1',
    'framealpha': 0.7,
    'frameon': True,
    'fancybox': True,
    'loc': 'upper right',
    }
leg_params = leg_params_default.copy()

def get_leg_params(**kwargs):
    params_ = {**leg_params_default, **kwargs}
    font_leg_ = font_condensed.copy()
    font_leg_.set_size(params_['fontsize'])
    params_ = {**params_, 'prop': font_leg_}
    return params_

def set_leg_title_fontsize(h_leg, leg_params=None):
    """
    Set fontsize of legend title for legend with handle `h_leg` and legend formatting dict
    `leg_params`. If `leg_params` is set to None (default), the default legend parameters from
    `leg_params_default` are used.
    """
    if leg_params is None:
        leg_params = leg_params_default
    h_leg.get_title().set_fontsize(leg_params['fontsize'])

point_params = {
    'marker':'o',
    'linestyle': '',
    'linewidth': 0.7,
    'markersize': 2,
    }

def get_point_params(**kwargs):
    params_ = {**point_params, **kwargs}
    return params_

errorbar_params = {
    'marker':'o',
    'linestyle': '',
    'capsize': 1,
    'markersize': 1,
    'linewidth': 0.7,
#    'mew': 1,
#    'mec': 'k',
    }

def get_errorbar_params(**kwargs):
    params_ = {**errorbar_params, **kwargs}
    return params_

line_params = {
    'marker':'',
    'linestyle': '-',
    'linewidth': 0.7,
    'markersize': 2,
    }

def get_line_params(**kwargs):
    params_ = {**line_params, **kwargs}
    return params_

fill_params = {
    }

def get_fill_params(**kwargs):
    params_ = {**fill_params, **kwargs}
    return params_

hist_params = {
    'edgecolor':'k',
    'linewidth': 0.7,
    }

def get_hist_params(**kwargs):
    params_ = {**hist_params, **kwargs}
    return params_

arrow_params = {
    'head_width': 0.3,
    'head_length': 0.1,
    'length_includes_head': False,
    }

def get_arrow_params(**kwargs):
    params_ = {**arrow_params, **kwargs}
    return params_

errorbar_capthick = 2
# Get default line colors
colors = get_line_colors(cmap='tab20')
chs_alpha_lims = [0.5, 1]

# Default output folder for plots
out_folder = 'plots'
