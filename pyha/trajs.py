# -*- coding: utf-8 -*-
"""
Created on Tue Mar 30 12:08:37 2021

@author: Lothar Maisenbacher/MPQ

Definition of class `Trajs`, which manages trajectory sets, such as set of trajectories of 2S atoms.
"""

import numpy as np
import pandas as pd
import time
import glob
from pathlib import Path

# pyhs
import pyhs.gen
logger = pyhs.gen.get_command_line_logger()

# pyha
import pyha.generic_container_class
import pyha.defs

class Trajs(pyha.generic_container_class.GenericContainerClass):
    """
    Class managing trajectory sets,
    such as sets of trajectories of 2S atoms.
    """

    PARAMS_COLUMNS_GEN = [
        ]
    PARAMS_COLUMNS_FLOAT = [
        'NozzleRadius', 'NozzleTemp', 'DetectionDist', 'ChopperFreq', 'AtomicDetuning',
        'LaserWaistRadius', 'LaserPower', 'LaserWaistPos',
        'Aperture1Width', 'Aperture1Height', 'Aperture1Dist',
        'Aperture2Width', 'Aperture2Height', 'Aperture2Dist',
        'DetectorLength', 'SpeedDistExp', 'SpeedDistExpSupprCutOff',
        'NucleusMass', 'AtomicMass', 'IntDelayWidth',
        'TrialProb', 'ThetaMax', 'FractionTrajsSpectroscopyRegion',
        ]
    PARAMS_COLUMNS_STR = [
        'TrajUID', 'Geometry', 'Isotope', 'DelaySetID',
        'Filename', 'Folder', 'Subdir', 'AbsPath',
        ]
    PARAMS_COLUMNS_BOOL = [
        ]
    PARAMS_COLUMNS_INT = [
        'NTrajs', 'NDelays', 'NMultipleChopperCycles', 'NMinIntDelays',
        ]
    PARAMS_FORMAT = {
        'ColumnsGen': PARAMS_COLUMNS_GEN,
        'ColumnsFloat': PARAMS_COLUMNS_FLOAT,
        'ColumnsStr': PARAMS_COLUMNS_STR,
        'ColumnsBool': PARAMS_COLUMNS_BOOL,
        'ColumnsInt': PARAMS_COLUMNS_INT,
        'ColumnsDate': ['Timestamp'],
        'IndexName': 'TrajUID',
        'KeepIndexAsColumn': True,
        'UniqueIndex': True,
        }

    ANALYSIS_COLUMNS_GEN = [
        ]
    ANALYSIS_COLUMNS_FLOAT = [
        'ExcProbPerTraj', 'ExcProbPerInterTraj', 'IoniProbPerTraj', 'IoniProbPerInterTraj',
        'DecayProbDet', 'FractionInterTraj', 'LaserInterTime', 'LaserInterTimeInterTraj',
        ]
    ANALYSIS_COLUMNS_STR = [
        'TrajUID', 'DelayID', 'AbsPath',
        ]
    ANALYSIS_COLUMNS_BOOL = [
        ]
    ANALYSIS_COLUMNS_INT = [
        'NIntDelays', 'NTrajsInter', 'N2SAtoms',
        ]
    ANALYSIS_FORMAT = {
        'ColumnsGen': ANALYSIS_COLUMNS_GEN,
        'ColumnsFloat': ANALYSIS_COLUMNS_FLOAT,
        'ColumnsStr': ANALYSIS_COLUMNS_STR,
        'ColumnsBool': ANALYSIS_COLUMNS_BOOL,
        'ColumnsInt': ANALYSIS_COLUMNS_INT,
        'ColumnsDate': [],
        'IndexName': 'TrajUID',
        'KeepIndexAsColumn': True,
        }

    PARAMS_ANALYSIS_FORMAT = {
        'ColumnsGen': list(np.unique(
            PARAMS_FORMAT['ColumnsGen']+ANALYSIS_FORMAT['ColumnsGen'])),
        'ColumnsFloat': list(np.unique(
            PARAMS_FORMAT['ColumnsFloat']+ANALYSIS_FORMAT['ColumnsFloat'])),
        'ColumnsStr': list(np.unique(
            PARAMS_FORMAT['ColumnsStr']+ANALYSIS_FORMAT['ColumnsStr'])),
        'ColumnsBool': list(np.unique(
            PARAMS_FORMAT['ColumnsBool']+ANALYSIS_FORMAT['ColumnsBool'])),
        'ColumnsInt': list(np.unique(
            PARAMS_FORMAT['ColumnsInt']+ANALYSIS_FORMAT['ColumnsInt'])),
        'ColumnsDate': list(np.unique(
            PARAMS_FORMAT['ColumnsDate']+ANALYSIS_FORMAT['ColumnsDate'])),
        'IndexName': 'TrajUID',
        'LoadFromFiles': False,
        'KeepIndexAsColumn': True,
        }

    DF_FORMATS = {
        'dfTrajParams': PARAMS_FORMAT,
        'dfTrajAnalysis': ANALYSIS_FORMAT,
        'dfTrajsAnalysis': PARAMS_ANALYSIS_FORMAT,
        }

    def __init__(self):
        """
        Initialize trajectory class.
        """
        super().__init__()

        # Init empty dict to store trajectory set data
        self.traj_sets = {}

    @property
    def dfTrajParams(self):
        """Get pandas DataFrame `dfTrajParams`."""
        return self.dfs['dfTrajParams']

    @dfTrajParams.setter
    def dfTrajParams(self, dfTrajParams):
        """Set pandas DataFrame `dfTrajParams`."""
        self.dfs['dfTrajParams'] = dfTrajParams

    @property
    def dfTrajAnalysis(self):
        """Get pandas DataFrame `dfTrajAnalysis`."""
        return self.dfs['dfTrajAnalysis']

    @dfTrajAnalysis.setter
    def dfTrajAnalysis(self, dfTrajAnalysis):
        """Set pandas DataFrame `dfTrajParams`."""
        self.dfs['dfTrajAnalysis'] = dfTrajAnalysis

    @property
    def dfTrajsAnalysis(self):
        """Get pandas DataFrame `dfTrajsAnalysis`."""
        return self.dfs['dfTrajsAnalysis']

    @dfTrajsAnalysis.setter
    def dfTrajsAnalysis(self, dfTrajsAnalysis):
        """Set pandas DataFrame `dfTrajsAnalysis`."""
        self.dfs['dfTrajsAnalysis'] = dfTrajsAnalysis

    def load_trajs_metadata(self, filepaths, clear=False):
        """
        Load trajectory set metadata from file(s) with paths `filepaths` (str or list)
        into this class, where it is stored in DataFrame `dfTrajParams`.
        If `clear` is set to True (default is False), the currently loaded metadata are
        cleared first.
        """
        dfTrajParams = self.df_templates['dfTrajParams'] if clear else self.dfs['dfTrajParams']
        filepaths = np.atleast_1d(filepaths)
        for filepath in filepaths:
            try:
                dfTrajParams_ = self.read_df_from_csv(filepath, 'dfTrajParams')
            except FileNotFoundError as e:
                msg = (
                    f'Could not load trajectory set metadata from file \'{filepath}\''
                    +f' (error was \'{e}\')')
                logger.error(msg)
                raise pyha.defs.PyhaError(msg)
            dfTrajParams = pd.concat([dfTrajParams, dfTrajParams_])
        # Add width of integration delays if not present
        if 'IntDelayWidth' not in dfTrajParams.columns:
            dfTrajParams['IntDelayWidth'] = np.nan
        dfTrajParams['IntDelayWidth'] = dfTrajParams['IntDelayWidth'].fillna(10e-6)
        # Add length of detector if not present
        if 'DetectorLength' not in dfTrajParams.columns:
            dfTrajParams['DetectorLength'] = np.nan
        dfTrajParams['DetectorLength'] = dfTrajParams['DetectorLength'].fillna(2*26.2e-3)
        # Add minimum number of integration delays
        if 'NMinIntDelays' not in dfTrajParams.columns:
            dfTrajParams['NMinIntDelays'] = 5
        dfTrajParams['NMinIntDelays'] = dfTrajParams['NMinIntDelays'].fillna(5)
        # Add number of trajectories that have seen multiple chopper cycles
        if 'NMultipleChopperCycles' not in dfTrajParams.columns:
            dfTrajParams['NMultipleChopperCycles'] = -1
        dfTrajParams['NMultipleChopperCycles'] = dfTrajParams['NMultipleChopperCycles'].fillna(-1)

        # Cast to data format
        dfTrajParams = self.cast_containers_to_data_format(
            dfTrajParams, 'dfTrajParams')

        # Drop duplicates
        dfTrajParams = self.drop_duplicates_and_reindex(
            dfTrajParams, 'dfTrajParams')

        self.dfs['dfTrajParams'] = dfTrajParams

        logger.info(
            'Loaded metadata for %d trajectory set(s)', len(dfTrajParams))

    def load_trajs_analysis_results(self, filepaths, clear=False):
        """
        Load analysis results of trajectory sets from file(s) with paths `filepaths`
        (str or list) into this class, where it is stored in DataFrame `dfTrajAnalysis`.
        If `clear` is set to True (default is False), the currently loaded analysis results are
        cleared first.
        """
        dfTrajAnalysis = (
            self.df_templates['dfTrajAnalysis'] if clear else self.dfs['dfTrajAnalysis'])
        filepaths = np.atleast_1d(filepaths)
        for filepath in filepaths:
            try:
                dfTrajAnalysis_ = self.read_df_from_csv(filepath, 'dfTrajAnalysis')
            except FileNotFoundError as e:
                msg = (
                    f'Could not load trajectory set analysis results from file \'{filepath}\''
                    +f' (error was \'{e}\')')
                logger.error(msg)
                raise pyha.defs.PyhaError(msg)
            dfTrajAnalysis = pd.concat([dfTrajAnalysis, dfTrajAnalysis_])

        # Add excitation probabilities per trajectory
        if 'ExcProbPerTraj' not in dfTrajAnalysis:
            dfTrajAnalysis['ExcProbPerTraj'] = dfTrajAnalysis['ExcProb']/dfTrajAnalysis['NTrajs']

        # Cast to data format
        dfTrajAnalysis = self.cast_containers_to_data_format(
            dfTrajAnalysis, 'dfTrajAnalysis')

        # Drop duplicates
        dfTrajAnalysis = self.drop_duplicates_and_reindex(
            dfTrajAnalysis, 'dfTrajAnalysis')

        # Merge `dfTrajAnalysis` with `dfTrajParams` to create `dfTrajsAnalysis`
        dfTrajsAnalysis = dfTrajAnalysis.merge(
            self.dfs['dfTrajParams'], left_index=True, right_index=True)

        # Cast to data format of `dfTrajsAnalysis`
        dfTrajsAnalysis = self.cast_containers_to_data_format(
            dfTrajsAnalysis, 'dfTrajsAnalysis')

        self.dfs['dfTrajAnalysis'] = dfTrajAnalysis
        self.dfs['dfTrajsAnalysis'] = dfTrajsAnalysis

        logger.info(
            'Loaded analysis results for %d trajectory set(s)'
            +', analysis results now available for %s of the %s loaded trajectory set(s)',
            len(dfTrajAnalysis.index.unique()), len(dfTrajsAnalysis.index.unique()),
            len(self.dfs['dfTrajParams'].index.unique()))

    def find_and_load_trajs_metadata(self, dirs_metadata, **kwargs):
        """
        Searches directory or list-like of directories `dirs_metadata` (str or list-like of str)
        for files containing trajectory set metadata, identified by their filenames ending with
        ' Params.dat', and loads these files.
        """
        dirs_metadata = np.atleast_1d(dirs_metadata)
        for dir_metadata in dirs_metadata:
            # Load trajectory metadata
            dir_metadata = Path(dir_metadata)
            if not dir_metadata.is_dir():
                logger.warning(
                    'Directory \'%s\' to be searched for trajectory set metadata'
                    ' does not exist',
                    dir_metadata)
            metadata_filepaths = glob.glob(str(Path(dir_metadata, '* Params.dat')))
            logger.info(
                'Found %d file(s) containing trajectory set metadata in directory \'%s\'',
                len(metadata_filepaths), dir_metadata)
            self.load_trajs_metadata(metadata_filepaths, **kwargs)

    def find_and_load_trajs_analysis_results(self, dirs_analysis, **kwargs):
        """
        Searches directory or list-like of directories `dirs_metadata` (str or list-like of str)
        for files containing trajectory set analysis results, identified by their filenames ending
        with ' Analysis.dat', and loads these files.
        """
        dirs_analysis = np.atleast_1d(dirs_analysis)
        for dir_analysis in dirs_analysis:
            # Load trajectory metadata
            dir_analysis = Path(dir_analysis)
            if not dir_analysis.is_dir():
                logger.warning(
                    'Directory \'%s\' to be searched for trajectory set analysis results'
                    ' does not exist',
                    dir_analysis)
            metadata_filepaths = glob.glob(str(Path(dir_analysis, '* Analysis.dat')))
            logger.info(
                'Found %d file(s) containing trajectory set analysis results in directory \'%s\'',
                len(metadata_filepaths), dir_analysis)
            self.load_trajs_analysis_results(metadata_filepaths, **kwargs)

    def load_traj_set(self, traj_uid, dir_sets):
        """
        Load trajectory set with UID `traj_uid` (string) from disk.
        Metadata for this trajectory set, stored in DataFrame `dfTrajParams` in this class,
        must be loaded first, using method `load_load_trajs_metadata`.

        Returned is either an open NPZ archive `traj_set`, if available,
        or for the legacy case where only a (gzipped) CSV file was saved,
        a dict `traj_set` mimicking the NPZ archive.

        `traj_set` contains:
            'TrajUID': string
                UID of trajectory set.
            'Positions': 2D array
                (x, y, z) position of trajectories
            'Vels': 2D array
                (vx, vy, vz) velocities of trajectories
            '2SExcProbs_IntDelaySum': 1D array
                For each experimental delay,
                a sum of the 2S excitation probabilities over the integration delay
                contained in each experimental delay
            'IoniProbs_IntDelaySum': 1D array
                For each experimental delay,
                a sum of the ionization probabilities over the integration delay
                contained in each experimental delay
            'Seed': 0D array
                Seed used for the pseudo RNG (only saved in NPZ archives) when
                the trajectory set was generated
            'Param': dict
                Parameters used to create this trajectory set. This is here taken from the
                separately read metadata, but is also present in NPZ archives
        """
        if not traj_uid in self.dfs['dfTrajParams'].index:
            msg = (
                f'Metadata of trajectory set \'{traj_uid}\' not loaded '
                +'(use method `load_trajs_metadata(metadata_filepaths)`, '
                +'where `metadata_filepaths` is a list of filepaths, to load the metadata)')
            logger.error(msg)
            raise pyha.defs.PyhaError(msg)

        sr_param = self.dfs['dfTrajParams'].loc[traj_uid]

        # Load results
        filename = Path(sr_param['Filename'])
        load_archive = True
        if str(filename).endswith('.gz'):
            # Filename is that of legacy gzipped CSV file
            filename_npz = Path(filename.stem).with_suffix('.npz')
            # Check whether NumPy NPZ archive also exists, if yes, load that instead of CSV file
            load_archive = Path(dir_sets, filename_npz).exists()
        else:
            filename_npz = filename
        start_time = time.time()
        if load_archive:
            # Load from NumPy NPZ archive
            logger.info('Loading trajectory set NPZ archive \'%s\'...', filename_npz)
            traj_set = np.load(
                Path(dir_sets, filename_npz),
                allow_pickle=True)
            logger.info('Loading file took %.3f s', time.time()-start_time)
        else:
            # Load from legacy gzipped CSV file
            logger.info('Loading legacy trajectory set file \'%s\'...', filename)
            loaded_data = np.loadtxt(Path(dir_sets, filename))
            logger.info('Loading file took %.3f s', time.time()-start_time)
            ioni_probs_int_delay_sum = np.zeros(loaded_data[:, 6:].shape)
            ioni_probs_int_delay_sum[:] = np.nan
            traj_set = {
                'TrajUID': traj_uid,
                'Positions': loaded_data[:, 0:3],
                'Vels': loaded_data[:, 3:6],
                '2SExcProbs_IntDelaySum': loaded_data[:, 6:],
                # Ionization probability not saved in legacy files
                'IoniProbs_IntDelaySum': ioni_probs_int_delay_sum,
                # Seed of pseudo RNG not saved in legacy files
                'Seed': np.array(-1),
                'Param': sr_param,
                }

        self.traj_sets[traj_uid] = traj_set
        return traj_set

    def close_traj_set(self, traj_uid):
        """
        Close trajectory set with UID `traj_uid`, if it has been loaded from disk.
        This closes the file and frees memory.
        """
        if traj_uid in self.traj_sets.keys():
            if isinstance(self.traj_sets[traj_uid], np.lib.npyio.NpzFile):
                self.traj_sets[traj_uid].close()
            del self.traj_sets[traj_uid]

    @staticmethod
    def convert_trajs_to_avp(positions, vels, detection_dist, waist_radius):
        """
        Convert trajectory set of N trajectories in 3D (x-y-z), given by
        initial positions (`x`, `y`, `z`) on the nozzle `positions` (2D array of size N x 3)
        and velocities `vels` (`vx`, `vy`, `vz`) (2D array of size N x 3)
        into alpha/speed/laser power (AVP) format in 2D (x, z').
        The 3D trajectories can be reduced into 2D trajectories because the intensity over time
        seen by atoms crossing a laser beam propagating along the x-axis with a symmetric Gaussian
        beam profile along the x- and y-axis is the same independent of the y-speed, as long as the
        power in the laser beam `P` is scaled accordingly.
        The transformation is given by
            `vx -> vx`,
            `vz -> sqrt(vy^2+vz^2) = vz'`,
            `v -> v`
            `P -> power_scaling P = P'`,
        with
            `offset = ((detection_dist-z)*vy+vz*y)/sqrt(vy^2+vz^2)`,
            `power_scaling = exp(-2 offset^2/waist_radius^2)`.
        `offset` is the distances from the point of closest approach to center of 2S-nP laser beam,
        and `power_scaling` is the effective peak 2S-nP laser power relative to the absolute peak
        power.
        `v` is the speed, `detection_dist` (float) is the distance between the nozzle and the laser
        crossing point, and `waist_radius` (float) is the 1/e^2 waist radius of the 2S-nP laser
        beam.
        The AVP format then consists of the angle `alpha` between `vx` and `vz'`, the speed `v`,
        and the laser beam power `P'`, with
            `alpha = arctan(vx/vz')`.
        Importantly, while `vx = vx'`, `alpha` is *not* identical to the angle between `vx` and `vz`
        in 3D, given by `arctan(vx/vz) != arctan(vx/vz')`.
        Returned here are the angle `alpha` as `alphas` (1D array of size N),
        the speeds `v` as `vs` (1D array of size N),
        the offsets `offset` as `offsets` (1D array of size N),
        and the power scalings `power_scaling` as `powers_scaling` (1D array of size N).
        """
        vs = np.sqrt(np.sum(vels**2, axis=1))
        # z-velocity `vz'` in 2D
        vzd = np.sqrt(np.sum(vels[:, 1:]**2, axis=1))
        # angle (mrad) `alpha` from z-axis towards x-axis in 2D
        alphas = 1e3*np.arctan(vels[:, 0]/vzd)
        # Distance (m) from point of closest approach to center of 2S-nP laser beam
        offsets = (
            ((detection_dist-positions[:, 2])*vels[:, 1]+vels[:, 2]*positions[:, 1])
            /np.sqrt(vels[:, 1]**2+vels[:, 2]**2))
        # Effective peak 2S-nP laser power, relative to absolute peak power,
        # as given by offset and laser beam radius
        powers_scaling = np.exp(-2*offsets**2/waist_radius**2)

        return alphas, vs, offsets, powers_scaling
