# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ

Definition of delays (for both experiment and simulations).
Here, a delay is a time bin for the time-resolved fluorescence collected from hydrogen atoms.
"""

from collections import OrderedDict
import numpy as np

# pyha
import pyha.util.data

### Delay sets
delay_sets = OrderedDict()
delay_sets_settings = OrderedDict()
## Default settings, will be overwritten by delay- or delay set-specific settings
delay_defaults = {
    'Name': '',
    'Description': '',
    # Defines which delays are included in the simulations
    # and the experimental delay analysis by default
    'Analysis': False,
    # Defines which delays are measurement delays,
    # i.e. delays that are included in line fits and covered by AI
    'Measurement': True,
    # Start and end time (in s)
    'StartTime': np.nan,
    'EndTime': np.nan,
    }
delay_set_settings_defaults = {
    # Timing device for which these delays will be used,
    # i.e. either 'MCS' for multi channel scaler (counter)
    # or 'AI' for analog input
    'TimingDeviceID': 'MCS',
    # Offset (in s) of the device trigger relative to the chopper trigger
    'TriggerOffset': 0,
    # Whether the delay set has fixed velocities (True), e.g. for simulation
    # data of known velocity, or whether the velocities itself are derived from
    # a simulation depending on other parameters and are loaded later on
    'FixedV': False,
    }
## 2S-4P legacy delays for counters
delay_set_id = '2S4P'
# Delays
delays = OrderedDict()
delays['P'] = {
    'Name': 'Prompt',
    'StartTime': 0e-6,
    'EndTime': 10e-6,
    }
meas_delays_start = np.array([10, 60, 110, 160, 210, 260, 310, 410, 610, 810]) * 1e-6
meas_delays_end = np.array([60, 110, 160, 210, 260, 310, 410, 610, 810, 512 * 5]) * 1e-6
for (delay_id, start_time, end_time) in zip(
        [str(elem) for elem in np.arange(1, len(meas_delays_start)+1)],
        meas_delays_start,
        meas_delays_end,
        ):
    delays[delay_id] = {
        'Name': delay_id,
        'StartTime': start_time,
        'EndTime': end_time,
        'Analysis': True,
        }
delays['All'] = {
    'Name': 'All',
    'StartTime': np.min(meas_delays_start),
    'EndTime': np.max(meas_delays_end),
    }
delays['Post'] = {
    'Name': 'Post',
    'StartTime': np.max(meas_delays_end),
    'EndTime': 3125e-6,
    'Measurement': False,
    }
delays['Bright'] = {
    'Name': 'Bright',
    'StartTime': 3125e-6,
    'EndTime': 1024 * 5e-6,
    'Measurement': False,
    }
# Add default settings
delays = OrderedDict({id_: {**delay_defaults, **params} for id_, params in delays.items()})
# Add to delay sets
delay_sets[delay_set_id] = delays
# Add delay set settings
delay_sets_settings[delay_set_id] = {
    **delay_set_settings_defaults,
    **{
        'TimingDeviceID': 'MCS',
        'FixedV': False,
    }
    }
## 2S-6P delays for counters
delay_set_id = '2S6P'
# Delays
delays = OrderedDict()
delays['P'] = {
    'Name': 'Prompt',
    'StartTime': 0e-6,
    'EndTime': 10e-6,
    }
meas_delays_start = np.array([
    10, 60, 110, 160, 210, 260, 310, 410, 610, 810
    ]) * 1e-6
meas_delays_end = np.array([
    60, 110, 160, 210, 260, 310, 410, 610, 810, 512 * 5
    ]) * 1e-6
meas_delays_start = np.array([
    10, 60, 110, 160, 210, 260, 310, 360, 410, 510, 610, 710, 910, 1210, 1510, 2010
    ]) * 1e-6
meas_delays_end = np.array([
    60, 110, 160, 210, 260, 310, 360, 410, 510, 610, 710, 910, 1210, 1510, 2010, 512 * 5
    ]) * 1e-6
for (delay_id, start_time, end_time) in zip(
        [str(elem) for elem in np.arange(1, len(meas_delays_start)+1)],
        meas_delays_start,
        meas_delays_end,
        ):
    delays[delay_id] = {
        'Name': delay_id,
        'StartTime': start_time,
        'EndTime': end_time,
        'Analysis': True,
        }
delays['All'] = {
    'Name': 'All',
    'StartTime': np.min(meas_delays_start),
    'EndTime': np.max(meas_delays_end),
    }
delays['Post'] = {
    'Name': 'Post',
    'StartTime': np.max(meas_delays_end),
    'EndTime': 3125e-6,
    'Measurement': False,
    }
delays['Bright'] = {
    'Name': 'Bright',
    'StartTime': 3125e-6,
    'EndTime': 1024 * 5e-6,
    'Measurement': False,
    }
# Add default settings
delays = OrderedDict({id_: {**delay_defaults, **params} for id_, params in delays.items()})
# Add to delay sets
delay_sets[delay_set_id] = delays
# Add delay set settings
delay_sets_settings[delay_set_id] = {
    **delay_set_settings_defaults,
    **{
        'TimingDeviceID': 'MCS',
        'FixedV': False,
    }
    }
## Fixed velocity delays for simulation data
delay_set_id = 'FixedV'
# Delays
delays = OrderedDict()
for v in np.linspace(5, 1200, 240):
    delay_id = 'V{:.0f}'.format(v)
    delays[delay_id] = {
        'Name': '{:.0f} m/s'.format(v),
        'StartTime': np.nan,
        'EndTime': np.nan,
        'Analysis': True,
        'V_Mean_Value': v,
        'V_Mean_Sigma': 0.,
        'V_RMS_Value': v,
        'V_RMS_Sigma': 0.,
        }
# Add default settings
delays = OrderedDict({id_: {**delay_defaults, **params} for id_, params in delays.items()})
# Add to delay sets
delay_sets[delay_set_id] = delays
# Add delay set settings
delay_sets_settings[delay_set_id] = {
    **delay_set_settings_defaults,
    **{
        'TimingDeviceID': 'Sim',
        'FixedV': True,
    }
    }

### Add running integer ID (IID) to delays with key 'IID' and to subset of analysis delays with key
# 'AnalysisIID'
for delay_set_id, delays in delay_sets.items():
    delays = pyha.util.data.addIIDToDictofDicts(delays)
    analysis_delays = {
        delay_id: delay for delay_id, delay in delays.items() if delay['Analysis']}
    analysis_delays = pyha.util.data.addIIDToDictofDicts(
        analysis_delays, iid_key='AnalysisIID')
    delays = {
        delay_id: (analysis_delays[delay_id] if delay_id in analysis_delays.keys() else delay)
        for delay_id, delay in delays.items()}
    delay_sets[delay_set_id] = delays
