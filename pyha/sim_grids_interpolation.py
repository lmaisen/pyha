# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ

Functions to nonlinearly interpolate additional points on
calculated regular grid of OBE results to create a dense grid.
This dense grid can then be used to interpolate the OBE results for
an arbitrary trajectory, i.e. a set of points for
alpha offset/speed/laser power (AVP) or
transverse velocity/speed/laser power.

For spline interpolation, the internal function
`scipy.interpolate._bsplines.make_interp_spline`
is used instead of the usual interface `scipy.interpolate.interp1d`,
since the internal function allows setting the spline boundary conditions.
"""

import numpy as np
import scipy.optimize
import scipy.interpolate
import time
from scipy.interpolate import RegularGridInterpolator

# pyhs
import pyhs.gen

# pyha
import pyha.defs

logger = pyhs.gen.get_command_line_logger()

### Default options
## Nonlinear power interpolation
# Options for fit of signal with laser-power-saturated signal
power_fit_opt = {'Tol': 1e-14, 'MaxFEv': 1000}
# Spline degree of interpolation of residuals of fit
power_spline_degree = 3
## Transverse velocity (parameterized as alpha offset or vx) interpolation using splines
# Spline degree
transverse_vel_param_spline_degree = 3
## Speed interpolation using splines
# Spline degree
v_spline_degree = 3

def saturated_signal(p, a, p0):
    """
    Expected behavior of laser-power-saturated signal,
    used in nonlinear interpolation of power points.
    """
    return a*(1-np.exp(-p/p0))

def add_power_points(powers, signals, powers_interp,
                     spline_degree=power_spline_degree,
                     spline_bc_type='natural'):
    """
    Nonlinear interpolation of power points,
    using expected behavior of laser-power-saturated signal.
    Input data is simulated signals `signals` (1D array)
    for given powers `powers` (1D array). `powers` needs to be sorted.
    The interpolated signal at powers `powers_interp` (1D array) is returned.
    The signal is assumed to be zero at zero power, and a corresponding point
    is added prior to interpolation.
    The signal is first fit with the laser-power-saturated signal.
    The fit residuals are interpolated
    using a spline of degree `spline_degree` (int) and with boundary conditions
    `spline_bc_type` (string).
    The interpolated signals are given by the sum of the fit and the residuals.
    """
    # Add zero power point, where signal is zero
    x = np.hstack(([0], powers))
    y = np.hstack(([0], signals))
    if np.array_equal(x, powers_interp):
        # Input powers and powers to be interpolated are identical, no interpolation necessary
        signals_interp = y
    else:
        # Fit signal vs laser power
        pstart = [np.max(y), 10]
        popt, pcov, infodict, mesg, ier = scipy.optimize.curve_fit(
            saturated_signal, x, y, pstart,
            method='lm', ftol=power_fit_opt['Tol'], xtol=power_fit_opt['Tol'],
            gtol=power_fit_opt['Tol'],
            maxfev=power_fit_opt['MaxFEv'], full_output=True, epsfcn=1e-14)
        # Interpolate fit residuals using spline
        power_fit_res = scipy.interpolate._bsplines.make_interp_spline(
            x, y-saturated_signal(x, *popt), k=spline_degree, bc_type=spline_bc_type)
        # Create signal at additional power points, given by sum of
        # fit prediction and interpolated fit residuals
        signals_interp = saturated_signal(powers_interp, *popt)+power_fit_res(powers_interp)
    return signals_interp

def add_speed_points(vs, signals, vs_interp,
                     spline_degree=v_spline_degree,
                     spline_bc_type='natural'):
    """
    Interpolation of speed points,
    using a spline of degree `spline_degree` (int) and with boundary conditions
    `spline_bc_type` (string).
    Input data is simulated signals `signals` (1D array)
    for given speeds `vs` (1D array). `vs` must be sorted.
    The interpolated signal at speeds `vs_interp` (1D array) is returned.
    """
    # Interpolate signal using spline
    signal_interp_func = scipy.interpolate._bsplines.make_interp_spline(
        vs, signals, k=spline_degree, bc_type=spline_bc_type)
    # Interpolate signal at additional speed points
    return signal_interp_func(vs_interp)

def add_transverse_vel_param_points(transverse_vels_param, signals, transverse_vels_param_interp,
                                    spline_degree=transverse_vel_param_spline_degree,
                                    spline_bc_type='natural'):
    """
    Interpolation of transverse velocity parametrization points,
    parameterized as either alpha offset (angle to x-axis) or vx (velocity in x-direction),
    using a spline of degree `spline_degree` (int) and with boundary conditions
    `spline_bc_type` (string).
    Input data is simulated signals `signals` (1D array)
    for given (positive) parametrizations of transverse velocity `transverse_vels_param` (1D array).
    `transverse_vels_param` must be sorted.
    The interpolated signal at transverse velocity parametrization points
    `transverse_vels_param_interp` (1D array) is returned.
    The signal is assumed to be symmetric in transverse velocity,
    and the signal for negative transverse velocities is created by copying and flipping
    the signal for positive transverse velocities.
    """
    # Create signal for negative transverse velocities by flipping signal for zero or positive
    # transverse velocities
    x_ = np.hstack((-np.flipud(transverse_vels_param), transverse_vels_param))
    y_ = np.hstack((np.flipud(signals), signals))
    # If a zero transverse velocity point is present, it will now be contained twice,
    # drop duplicate by only keeping unique points
    x, unique_index = np.unique(x_, return_index=True)
    y = y_[unique_index]
    # Interpolate signal using spline
    signal_interp_func = scipy.interpolate._bsplines.make_interp_spline(
        x, y, k=spline_degree, bc_type=spline_bc_type)
    # Interpolate signal at additional points
    return signal_interp_func(transverse_vels_param_interp)

def get_dense_grid_line_interpolator(dense_grid, dense_grid_data, method='linear'):
    """
    Returns line interpolator function `get_lines` for AVP/VxVP points
    for dense grid with metadata `dense_grid` (dict) and data `dense_grid_data` (array).
    The parameters for the line interpolator function depend on the transverse velocity
    (along the x-direction) parametrization of the dense grid,
    given by `dense_grid['TransverseVelParametrization']`.
    For the value `AlphaOffset`, the transverse velocity is parametrized using the angle to the
    x-axis, and `get_lines` takes as arguments this angle, the speed, and the laser power.
    For the value `Vx`, the transverse velocity is parametrized using the velocity in the
    x-direction, and `get_lines` takes as arguments this velocity, the speed, and the laser power.
    Internally, `scipy.interpolate.RegularGridInterpolator` is called to handle the interpolation,
    and the argument `method` ('linear' for linear interpolation or 'nearest' for nearest-neighbor
    interpolation) is passed onto this function.
    """
    num_detunings = dense_grid_data.shape[3]
    # Construct grid interpolators for each detuning
    start_time = time.perf_counter()
    grid_rinterps_addp = np.empty(num_detunings, dtype=object)
    if dense_grid['TransverseVelParametrization'] == 'AlphaOffset':
        transverse_velocities_param_interp = dense_grid['InterpAlphaOffsets']
    elif dense_grid['TransverseVelParametrization'] == 'Vx':
        transverse_velocities_param_interp = dense_grid['InterpVxs']
    else:
        msg = (
            'Unknown transverse velocity parametrization'
            +f' \'{dense_grid["TransverseVelParametrization"]}\'')
        logger.error(msg)
        raise pyha.defs.PyhaError(msg)
    for i_detuning in range(num_detunings):
        grid_rinterps_addp[i_detuning] = RegularGridInterpolator(
            (transverse_velocities_param_interp,
             dense_grid['InterpSpeeds'],
             dense_grid['InterpPowers']),
            dense_grid_data[:, :, :, i_detuning],
            method=method
            )
    logger.info(
        f'Constructing dense grid interpolators ({method}) took '
        f'{time.perf_counter()-start_time:.2f} s'
        )

    def get_lines(transverse_velocities_param, vs, powers):
        transverse_velocities_param = np.abs(np.atleast_1d(transverse_velocities_param))
        vs = np.atleast_1d(vs)
        powers = np.atleast_1d(powers)
        yli = np.zeros((len(transverse_velocities_param), num_detunings))
        for i_detuning in range(num_detunings):
            yli[:, i_detuning] = grid_rinterps_addp[i_detuning](
                (transverse_velocities_param, vs, powers))
        return yli

    return get_lines

def check_avp_points(trajs_transverse_vel_params, trajs_vs, trajs_powers,
                     dense_grid, dense_grid_data):
    """
    Check whether AVP/VxVP points are within interpolation range of dense grid with metadata
    `dense_grid` (dict) and data `dense_grid_data` (array).
    """
    vs_interp = dense_grid['InterpSpeeds']
    powers_interp = dense_grid['InterpPowers']

    if dense_grid['TransverseVelParametrization'] == 'AlphaOffset':
        alphas_interp = dense_grid['InterpAlphaOffsets']
        mask_avp_invalid_alphas_min = (trajs_transverse_vel_params < -alphas_interp.max())
        mask_avp_invalid_alphas_max = (trajs_transverse_vel_params > alphas_interp.max())
        mask_avp_valid_transverse_vel_params = (
            ~mask_avp_invalid_alphas_min & ~mask_avp_invalid_alphas_max)
        logger.info(
            'Alpha offset outliers: '
            +f'{mask_avp_invalid_alphas_min.sum()+mask_avp_invalid_alphas_max.sum()} '
            +f'({mask_avp_invalid_alphas_min.sum()} below {-alphas_interp.max():.1f} mrad'
            +f', {mask_avp_invalid_alphas_max.sum()} above {alphas_interp.max():.1f} mrad)'
            )
    elif dense_grid['TransverseVelParametrization'] == 'Vx':
        # If the transverse velocity parametrization is the velocity in x-direction (vx),
        # the dense grid might contain NaN values for some vx values that are outside the
        # experimental range and have not been calculated. In general, the maximum vx value depends
        # on the speed, which is why here the interpolation range check is done for each speed
        # separately.
        # For each speed `v`, the range of speeds from the next smaller speed up to, but not
        # including, `v`, is checked, unless `v` is the highest speed, in which case it is
        # included in the check.
        vxs_interp = dense_grid['InterpVxs']
        mask_avp_valid_vx = np.empty(len(trajs_transverse_vel_params), dtype=bool)
        mask_avp_valid_vx[:] = False
        for (i_v, v) in enumerate(vs_interp):
            if i_v == 0:
                continue
            max_vx = np.max(vxs_interp[~np.isnan(dense_grid_data[:, i_v-1, 0, 0])])
            if i_v == len(vs_interp)-1:
                mask_v = ((trajs_vs >= vs_interp[i_v-1]) & (trajs_vs <= v))
            else:
                mask_v = ((trajs_vs >= vs_interp[i_v-1]) & (trajs_vs < v))
            mask_avp_valid_vx_ = (mask_v & (np.abs(trajs_transverse_vel_params) <= max_vx))
            mask_avp_valid_vx |= mask_avp_valid_vx_
            num_outliers = mask_v.sum()-mask_avp_valid_vx_.sum()
            if num_outliers > 0:
                logger.info(
                    'Transverse velocity (vx) outliers (|vx| > %.2f m/s)'
                    +' for speed between %.2f m/s and %.2fs m/s: %d',
                    max_vx, vs_interp[i_v-1], v, num_outliers)
        mask_avp_valid_transverse_vel_params = mask_avp_valid_vx
        logger.info(
            'Transverse velocity (vx) outliers: %d',
            np.sum(~mask_avp_valid_transverse_vel_params))
    else:
        msg = (
            'Unknown transverse velocity parametrization'
            +f' \'{dense_grid["TransverseVelParametrization"]}\'')
        logger.error(msg)
        raise pyha.defs.PyhaError(msg)

    mask_avp_invalid_vs_min = (trajs_vs < vs_interp.min())
    mask_avp_invalid_vs_max = (trajs_vs > vs_interp.max())
    mask_avp_valid_vs = ~mask_avp_invalid_vs_min & ~mask_avp_invalid_vs_max
    logger.info(
        f'Speed outliers: {mask_avp_invalid_vs_min.sum()+mask_avp_invalid_vs_max.sum()} '
        +f'({mask_avp_invalid_vs_min.sum()} below {vs_interp.min():.1f} m/s'
        +f', {mask_avp_invalid_vs_max.sum()} above {vs_interp.max():.1f} m/s)'
        )

    mask_avp_invalid_powers_min = (trajs_powers < powers_interp.min())
    mask_avp_invalid_powers_max = (trajs_powers > powers_interp.max())
    mask_avp_valid_powers = ~mask_avp_invalid_powers_min & ~mask_avp_invalid_powers_max
    logger.info(
        '2S-nP laser power outliers: '
        +f'{mask_avp_invalid_powers_min.sum()+mask_avp_invalid_powers_max.sum()} '
        +f'({mask_avp_invalid_powers_min.sum()} below {powers_interp.min():.1f} µW'
        +f', {mask_avp_invalid_powers_max.sum()} above {powers_interp.max():.1f} µW)'
        )

    return mask_avp_valid_transverse_vel_params, mask_avp_valid_vs, mask_avp_valid_powers
