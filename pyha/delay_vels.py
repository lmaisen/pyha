# -*- coding: utf-8 -*-
"""
Created on Tue Mar 30 12:08:37 2021

@author: Lothar Maisenbacher/MPQ

Definition of class `DelayVels`, which manages delay velocities.
"""

import numpy as np
import pandas as pd
import glob
from pathlib import Path

# pyhs
import pyhs.gen

# pyha
import pyha.generic_container_class
import pyha.defs

## Shorthands
# Experimental and simulation parameters in DataFrames
from pyha.def_shorthands import sp__

logger = pyhs.gen.get_command_line_logger()

class DelayVels(pyha.generic_container_class.GenericContainerClass):
    """
    Class managing delay velocities.
    """

    DELAY_VELS_FORMAT = {
        'ColumnsGen': [],
        'ColumnsFloat': [
            'AlphaOffset', '2SnPLaserPower',
            'V_Mean_Value', 'V_Mean_Sigma', 'V_RMS_Value', 'V_RMS_Sigma',
            'Amp_Value', 'Amp_Sigma', 'Amp_Fit_Value', 'Amp_Fit_Sigma',
            'Amp_Unbinned_Value', 'Amp_Unbinned_Sigma',
            'ExcProbnPOnRes',
            'V_Gaussian_ChiSq', 'V_Gaussian_RedChiSq',
            'V_Gaussian_Error', 'V_Gaussian_CFR_Value', 'V_Gaussian_Amp_Value',
            'V_Gaussian_Offset_Value', 'V_Gaussian_GammaG_Value',
            'V_Gaussian_CFR_Sigma', 'V_Gaussian_Amp_Sigma',
            'V_Gaussian_Offset_Sigma', 'V_Gaussian_GammaG_Sigma',
            'V_Gaussian_Gamma_Value', 'V_Gaussian_RelOffset_Value',
            'V_Gaussian_Gamma_Sigma', 'V_Gaussian_RelOffset_Sigma',
            ],
        'ColumnsStr': [
            'DelayVelsUID', 'GridInterUID', 'GridUID', 'TrajUID', 'BaseTrajUID',
            'Isotope', 'FS', 'SignalSetID', 'DelaySetID', 'SignalID', 'FitFuncID', 'DelayID',
            'FreqSampling', 'FixedFreqListID', 'CountWeighting', 'AbsPath',
            ],
        'ColumnsBool': ['BinnedDataAvailable', 'UnbinnedDataAvailable'],
        'ColumnsInt': [],
        'ColumnsDate': ['Timestamp'],
        'IndexName': 'DelayVelsUID',
        'KeepIndexAsColumn': True,
        }

    DF_FORMATS = {
        'dfDelayVels': DELAY_VELS_FORMAT,
        }

    def __init__(self):
        """
        Initialize delay velocities class.
        """
        super().__init__()

    @property
    def dfDelayVels(self):
        """Get pandas DataFrame `dfDelayVels`."""
        return self.dfs['dfDelayVels']

    @dfDelayVels.setter
    def dfDelayVels(self, dfDelayVels):
        """Set pandas DataFrame `dfDelayVels`."""
        self.dfs['dfDelayVels'] = dfDelayVels

    def load_delay_vels(self, filepaths):
        """
        Load delay velocities from file(s) with paths `filepaths` (str or list-like)
        into this class, where it is stored in DataFrame `dfDelayVels`.
        """
        dfDelayVels = self.dfs['dfDelayVels']
        num_delay_vels_init = len(dfDelayVels)
        filepaths = np.atleast_1d(filepaths)
        for filepath in filepaths:
            try:
                dfDelayVels = pd.concat([
                    dfDelayVels,
                    self.cast_containers_to_data_format(
                        self.read_df_from_csv(filepath, 'dfDelayVels'), 'dfDelayVels')
                    ])
            except FileNotFoundError as e:
                msg = (
                    f'Could not load delay velocities from file \'{filepath}\''
                    +f' (error was \'{e}\')')
                logger.error(msg)
                raise pyha.defs.PyhaError(msg)
        num_delay_vels_loaded = len(dfDelayVels)-num_delay_vels_init

        # Cast to data format
        dfDelayVels = self.cast_containers_to_data_format(dfDelayVels, 'dfDelayVels')

        # Drop duplicates, keeping newest entries
        dfDelayVels = self.drop_duplicates_and_reindex(
            dfDelayVels.sort_values(by='Timestamp', ascending=False),
            'dfDelayVels',
            subset=[
                'GridInterUID', 'GridUID', 'TrajUID', 'BaseTrajUID',
                'FS', 'SignalSetID', 'DelaySetID', 'SignalID', 'FitFuncID', 'DelayID',
                'FreqSampling', 'FixedFreqListID', 'CountWeighting',
                'AlphaOffset', '2SnPLaserPower',
                ])

        self.dfs['dfDelayVels'] = dfDelayVels

        logger.info(
            'Loaded %d delay velocities from %d file(s)'
            +', resulting in %d additional delay velocities after dropping duplicates',
            num_delay_vels_loaded, len(filepaths), len(dfDelayVels)-num_delay_vels_init)

    def cast_containers_to_data_format(self, df, df_id):
        """
        Cast columns of DataFrame `df` into correct DType, add default values,
        as specified in DataFrame format with ID `df_id` (str).
        """
        if df_id in ['dfDelayVels']:

            # Make sure all columns processed below are actually present in `df`
            columns = ['FreqSampling', 'Isotope']
            df = df.reindex(
                columns=[
                    *columns,
                    *df.columns[~np.in1d(df.columns, columns)]],
                fill_value=np.nan)

            # Add default values to legacy data
            df['FreqSampling'] = df['FreqSampling'].fillna('Exp')
            df['Isotope'] = df['Isotope'].fillna('H')

        return super().cast_containers_to_data_format(df, df_id)

    def find_and_load_delay_vels(self, dirs):
        """
        Searches directory or list-like of directories `dirs` (str or list-like of str)
        for files containing delay velocities, identified by their filenames ending with
        ' Params.dat', and loads these files.
        """
        dirs = np.atleast_1d(dirs)
        for dir_ in dirs:
            # Load delay velocities
            if not Path(dir_).exists():
                logger.warning(
                    'Directory \'%s\' to be searched for delay velocities does not exist', dir_)
            filepaths = glob.glob(str(Path(dir_, '*_delay_vels.dat')))
            logger.info(
                'Found %d file(s) containing delay velocities in directory \'%s\'',
                len(filepaths), dir_)
            self.load_delay_vels(filepaths)

    def add_delay_vels_to_scan_fit_params(self, dfScansFitParams,
                                          sim_param_ids=None, exclude_sim_param_ids=None,
                                          force_sim_params=None,
                                          sim_params_suffix=None,
                                          verbose=True):
        """
        Add delay velocities to scan fit results in DataFrame `dfScansFitParams`,
        matching on simulation parameters (with column suffix `sim_params_suffix` in
        `dfScansFitParams`) and delay ID and fit func ID.
        These delay velocities can then be used directly to calculate the scan
        fit derivatives such as the Doppler slope.

        Parameters
        ----------
        dfScansFitParams : pandas DataFrame
            DataFrame of scan fit results (`pyh.fit.FitClass.dfScanFitParams`).
        sim_param_ids : str or list-like of str or None, optional
            Simulation parameter ID(s) used for matching delay velocities and scan fit results.
            The default is None, for which the simulation parameter IDs listed in
            `match_sim_param_ids_default` below are used.
        exclude_sim_param_ids : str or list-like of str or None, optional
            Simulation parameter ID(s) to exclude from `sim_param_ids`, if `sim_param_ids` is not
            None, or `match_sim_param_ids_default` defined below, if `sim_param_ids` is None.
            The default is None, for no simulation parameter ID(s) are excluded.
        force_sim_params : dict or None, optional
            ID(s) and value(s) of simulation parameters that will be forced in matching, overriding
            any values in `dfScansFitParams`. `force_sim_params` is a dict with the simulation
            parameter ID(s) as key(s) and their values as elements.
            If set to None, no values are overridden.
        sim_params_suffix : str or None, optional
            Suffix for columns in `dfScansFitParams` containing simulation parameters, i.e.,
            columns searched for are named 'dfScans_SimParams_{sim_param}_{sim_params_suffix}',
            where '{sim_param}' is the simulation parameter name.
            The default is None, for which no suffix is used.
        verbose : bool, optional
            Verbose output. The default is True.

        Returns
        -------
        dfScansFitParams_dlys : pandas DataFrame
            `dfScansFitParams` with new columns prefixed with 'dfDelayVels_', containing columns
            of matched delay velocities from `DelayVels.dfDelayVels`.

        """
        exclude_sim_param_ids = (
            list(np.atleast_1d(exclude_sim_param_ids)) if exclude_sim_param_ids is not None else [])
        if force_sim_params is None:
            force_sim_params = {}
        if sim_params_suffix is None:
            sim_params_suffix = ''

        dfDelayVels = self.dfDelayVels

        # Default simulation parameter IDs used for matching
        match_sim_param_ids_default = [
            'Isotope',
            'FS',
            'GridInterUID',
            'TrajUID',
            '2SnPLaserPower',
            'AlphaOffset',
            'FreqSampling',
            'FixedFreqListID',
            'CountWeighting',
            'SignalID',
            'DelayID',
            'FitFuncID',
            ]

        # Simulation parameter IDs used for matching
        match_sim_param_ids = (
            sim_param_ids if sim_param_ids is not None else match_sim_param_ids_default)
        # Remove simulation parameter IDs excluded by `exclude_sim_param_ids`
        match_sim_param_ids = [
            sim_param_id for sim_param_id in match_sim_param_ids
            if sim_param_id not in exclude_sim_param_ids]
        # Array of simulation parameter IDs to be matched,
        # including those forced to be matched to a certain value with `force_sim_params`
        # (keep order of `match_sim_param_ids`)
        _, unique_indices = np.unique(
            match_sim_param_ids + list(force_sim_params.keys()), return_index=True)
        match_sim_param_ids = np.array(match_sim_param_ids)[np.sort(unique_indices)]

        # List of simulation parameters that are prefixed with 'dfScanFits_' in `dfScansFitParams`
        sim_param_ids_scan_fits = [
            'DelayID',
            'FitFuncID',
            ]
        # Array of columns in `dfScansFitParams` to match on
        match_columns_scan_fits  = np.array([
            'dfScanFits_'+sim_param_id if sim_param_id in sim_param_ids_scan_fits
            else sp__+sim_param_id+sim_params_suffix
            for sim_param_id in match_sim_param_ids
            ])
        # Array of columns in `dfDelayVels` to match on
        match_columns_delay_vels = np.array(match_sim_param_ids)

        # Remove simulation parameter IDs that are not present in either `dfScansFitParams` or
        # `dfDelayVels`
        mask_column_present = [
            match_column_scan_fits in dfScansFitParams.columns
            and match_column_delay_vels in dfDelayVels.columns
            for  match_column_scan_fits, match_column_delay_vels
            in zip(match_columns_scan_fits, match_columns_delay_vels)]
        match_sim_param_ids = match_sim_param_ids[mask_column_present]
        match_columns_scan_fits = match_columns_scan_fits[mask_column_present]
        match_columns_delay_vels = match_columns_delay_vels[mask_column_present]

        # Mask of simulation parameter IDs that have a forced value
        mask_sim_param_ids_forced = np.array([
            sim_param_id in force_sim_params for sim_param_id in match_sim_param_ids])

        num_fits_pre_delay_vels = len(dfScansFitParams)
        source_to_add = 'dfDelayVels'
        combined_columns = np.unique(
            list(dfScansFitParams.columns)
            +[source_to_add+'_'+elem for elem in dfDelayVels.columns]
            )
        dfScansFitParams_dlys = pd.DataFrame(columns=combined_columns)
        if len(match_sim_param_ids) == 0:
            logger.warning(
                'No simulation parameters to match delay velocities to scan fit results remaining')
        elif len(dfScansFitParams) == 0:
            logger.warning('No scan fit results provided to be matched with delay velocities')
        else:
            # For 'FreqSampling' set to 'Fixed', 'FixedFreqListID' must be matched, while
            # for 'FreqSampling' set to other than 'Fixed', 'FixedFreqListID' need not be matched.
            # This is because for other than 'Fixed', 'FixedFreqListID' in `dfScanFitParams` will
            # contain the actual frequency list ID used for each particular scan, while the delay
            # velocity metadata in `dfDelayVels` will have an empty field, as the delay velocities
            # may be derived from scans with different frequency list IDs, but all the same
            # 'FreqSampling'.
            for mask_ids, mask_scans_fit_params, case in zip(
                    [np.ones(len(match_sim_param_ids), dtype=bool),
                     [sim_param_id != 'FixedFreqListID' for sim_param_id in match_sim_param_ids]],
                    [dfScansFitParams[sp__+'FreqSampling'+sim_params_suffix] == 'Fixed',
                     dfScansFitParams[sp__+'FreqSampling'+sim_params_suffix] != 'Fixed'],
                    ['with \'FreqSampling\' == \'Fixed\'', 'with \'FreqSampling\' != \'Fixed\'']):
                if mask_scans_fit_params.sum() == 0:
                    continue
                mask_count = {}
                mask_str = {}
                mask_dlys_match = pd.Series(True, index=dfDelayVels.index)
                mask_dlys = pd.Series(True, index=dfDelayVels.index)
                for sim_param_id, match_column_scan_fits, match_column_delay_vels, forced \
                        in zip(match_sim_param_ids[mask_ids],
                               match_columns_scan_fits[mask_ids],
                               match_columns_delay_vels[mask_ids],
                               mask_sim_param_ids_forced[mask_ids]):
                    if forced:
                        elems = np.atleast_1d(force_sim_params[sim_param_id])
                    else:
                        elems = (
                            dfScansFitParams[mask_scans_fit_params][match_column_scan_fits]
                            .unique())
                    mask_dlys_match_sub = dfDelayVels[match_column_delay_vels].isin(elems)
                    elems_str = pyha.util.data.elems_to_str(elems)
                    mask_count[match_column_delay_vels] = np.sum(mask_dlys_match_sub)
                    mask_str[match_column_delay_vels] = (
                        f'(\'{match_column_delay_vels}\' in [{elems_str}])')
                    mask_dlys_match &= mask_dlys_match_sub
                    if forced:
                        mask_dlys &= mask_dlys_match_sub
                if verbose:
                    logger.info(
                        'Found %d delay velocities'
                        +' matching any of the fit results parameters (%s) (filters: %s)',
                        np.sum(mask_dlys_match), case,
                        ', '.join([f'{mask_str[column]}: {count:d}'
                                   for column, count in mask_count.items()]))
                dfScansFitParams_dlys = pd.concat([
                    dfScansFitParams_dlys,
                    dfScansFitParams[mask_scans_fit_params]
                    .reset_index()
                    .merge(
                        dfDelayVels[mask_dlys]
                        .drop_duplicates(
                            subset=match_columns_delay_vels[mask_ids & ~mask_sim_param_ids_forced])
                        .rename(columns=lambda x: source_to_add+'_'+x),
                        left_on=list(
                            match_columns_scan_fits[mask_ids & ~mask_sim_param_ids_forced]),
                        right_on=list([
                            source_to_add+'_'+elem for elem
                            in match_columns_delay_vels[mask_ids & ~mask_sim_param_ids_forced]]),
                        how='inner',
                        )
                    .set_index('ScanUID')
                    ])
            # Re-arrange `dfScansFitParams_dlys` to match order of `dfScansFitParams`
            scan_uids = dfScansFitParams.index.unique()
            dfScansFitParams_dlys = (
                dfScansFitParams_dlys.loc[
                    scan_uids[np.in1d(scan_uids, dfScansFitParams_dlys.index)]])
        ratio = (
            len(dfScansFitParams_dlys)/num_fits_pre_delay_vels
            if num_fits_pre_delay_vels > 0 else np.nan)
        if verbose:
            logger.info(
                f'Found delay velocities for {len(dfScansFitParams_dlys):d} of '
                + f'{num_fits_pre_delay_vels:d} fit result(s) ({ratio*1e2:.2f}%)')

        return dfScansFitParams_dlys
