# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ

Functions and definitions for various experimental and simulation parameters.
"""

import numpy as np

# pyhs
import pyhs.statfunc

# pyha
import pyha.def_atomic
from pyha.def_shorthands import sp__

import pyha.plts as plts

#### Experimental detectors
colors = plts.get_line_colors(
    4, 'tab10')
detectors = {}
detectors['Top'] = {
    'DetectorID': 'Top',
    'MCSchannelID': 'Ch0',
    'Name': 'Top detector',
    'ShortName': 'Top det.',
    'NameLC': 'top detector',
    'ShortNameLC': 'top det.',
    'Abbr': 'TD',
    'PointParams': {
        'color': colors[0],
        'marker': 'o',
        'markersize': 2,
        },
    }
detectors['Bottom'] = {
    'DetectorID': 'Bottom',
    'MCSchannelID': 'Ch1',
    'Name': 'Bottom detector',
    'ShortName': 'Bottom det.',
    'NameLC': 'bottom detector',
    'ShortNameLC': 'bottom det.',
    'Abbr': 'BD',
    'PointParams': {
        'color': colors[1],
        'marker': 's',
        'markersize': 2,
        },
    }
detectors['Avg'] = {
    'DetectorID': 'Avg',
    'MCSchannelID': 'Avg',
    'Name': 'Detectors average',
    'ShortName': 'Det. avg.',
    'NameLC': 'detector average',
    'ShortNameLC': 'det. avg.',
    'Abbr': 'DA',
    'PointParams': {
        'color': colors[2],
        'marker': 'd',
        'markersize': 2,
        },
    }
detectors['Sim'] = {
    'DetectorID': 'Sim',
    'MCSchannelID': 'Sim',
    'Name': 'Simulation data.',
    'ShortName': 'Sim.',
    'NameLC': 'simulation data',
    'ShortNameLC': 'sim.',
    'Abbr': 'Sim',
    'PointParams': {
        'color': colors[3],
        'marker': 'x',
        'markersize': 2,
        },
    }
# MCS channel IDs as keys
detectors_mcs = {
    elem['MCSchannelID']: elem
    for key, elem in detectors.items()}

#### Simulations
simulations = {}
simulations['BigModel'] = {
    'Name': 'Big model simulation',
    'ShortName': 'Big model sim.',
    'NameLC': 'big model',
    'ShortNameLC': 'big model',
    'Abbr': 'BM',
    'PointParams': {
        'marker': 'x',
        'mew': 0.7,
        'markersize': 4,
        'alpha': 0.7,
        },
    }
simulations['LFS'] = {
    'Name': 'Light force shift simulation',
    'ShortName': 'LFS sim.',
    'NameLC': 'light force shift sim.',
    'ShortNameLC': 'LFS sim.',
    'Abbr': 'LFS',
    'PointParams': {
        'marker': '+',
        'mew': 0.7,
        'markersize': 4,
        'alpha': 0.7,
        },
    }
simulations['SOD'] = {
    'Name': 'Second-order Doppler shift simulation',
    'ShortName': 'SOD sim.',
    'NameLC': 'second-order Doppler shift sim.',
    'ShortNameLC': 'SOD sim.',
    'Abbr': 'SOD',
    'PointParams': {
        'marker': '+',
        'mew': 0.7,
        'markersize': 4,
        'alpha': 0.7,
        },
    }

#### Combination of fine structure components

fs_non_unique_columns = [
    sp__+'FS',
    sp__+'GridUID',
    sp__+'2SnPLaserPower',
#    'dfSimGrids_FS',
    ]
fs_combinations = {}
fs_combinations['6P12'] = {
    'Name': r'2S-6P$_{1/2}$',
    'Symbol': r'$6\mathrm{P}_{1/2}$',
    'FS': ['6P12'],
    'FS_Weights': [1.],
    'Eqn_Value': lambda fs: fs,
    'Eqn_Sigma': lambda fs, fs_sigma: fs_sigma,
    }
fs_combinations['6P32'] = {
    'Name': r'2S-6P$_{3/2}$',
    'Symbol': r'$6\mathrm{P}_{3/2}$',
    'FS': ['6P32'],
    'FS_Weights': [1.],
    'Eqn_Value': lambda fs: fs,
    'Eqn_Sigma': lambda fs, fs_sigma: fs_sigma,
    }
fs_combinations['6PFScent'] = {
    'Name': '2S-6P fine structure centroid',
    'Symbol': r'$6\mathrm{P}_\mathrm{cent}$',
    'FS': ['6P12', '6P32'],
    'FS_Weights': [1/3, 2/3],
    'Eqn_Value': lambda fs12, fs32: 1/3*fs12+2/3*fs32,
    'Eqn_Sigma': (
        lambda fs12, fs32, fs12_sigma, fs32_sigma:
        pyhs.statfunc.addUncert(
            fs12_sigma, fs32_sigma, 0,
            [1/3] * len(fs12_sigma),
            [2/3] * len(fs32_sigma))),
    }
fs_combinations['6PFSavg'] = {
    'Name': '2S-6P fine structure average',
    'Symbol': r'$6\mathrm{P}_\mathrm{avg}$',
    'FS': ['6P12', '6P32'],
    'FS_Weights': [1/2, 1/2],
    'Eqn_Value': lambda fs12, fs32: 1/2*fs12+1/2*fs32,
    'Eqn_Sigma': (
        lambda fs12, fs32, fs12_sigma, fs32_sigma:
        pyhs.statfunc.addUncert(
            fs12_sigma, fs32_sigma, 0, 1/2, 1/2)),
    }
fs_combinations['6PFSsplit'] = {
    'Name': '2S-6P fine structure splitting',
    'Symbol': r'$6\mathrm{P}_\mathrm{FS}$',
    'FS': ['6P12', '6P32'],
    'FS_Weights': [-1, 1],
    'Eqn_Value': lambda fs12, fs32: fs32-fs12,
    'Eqn_Sigma': (
        lambda fs12, fs32, fs12_sigma, fs32_sigma:
        pyhs.statfunc.addUncert(
            fs12_sigma, fs32_sigma, 0, 1, -1)),
    }
fs_combinations['4P12'] = {
    'Name': r'2S-4P$_{1/2}$',
    'Symbol': r'$4\mathrm{P}_{1/2}$',
    'FS': ['4P12'],
    'FS_Weights': [1.],
    'Eqn_Value': lambda fs: fs,
    'Eqn_Sigma': lambda fs, fs_sigma: fs_sigma,
    }
fs_combinations['4P32'] = {
    'Name': r'2S-4P$_{3/2}$',
    'Symbol': r'$4\mathrm{P}_{3/2}$',
    'FS': ['4P32'],
    'FS_Weights': [1.],
    'Eqn_Value': lambda fs: fs,
    'Eqn_Sigma': lambda fs, fs_sigma: fs_sigma,
    }

#### Simulation parameters

### Metadata of simulation parameters
# Default metadata
sim_params_meta_defaults = {
    'Derived': False,
    'DerivedFrom': [],
    'DerivedParams': [],
    }
# Metadata
sim_params_meta = {}
sim_params_meta[sp__+'2SnPLaserPower'] = {
    'Derived': False,
    }
sim_params_meta[sp__+'2SnP12LaserPowerEquivalent'] = {
    'Derived': True,
    'DerivedFrom': [sp__+'2SnPLaserPower'],
    }
sim_params_meta[sp__+'AlphaOffset'] = {
    'Derived': False,
    }
sim_params_meta[sp__+'TransverseVelVrec'] = {
    'Derived': True,
    'DerivedFrom': [sp__+'AlphaOffset'],
    }
# Add default metadata
sim_params_meta = {key: {**sim_params_meta_defaults, **meta}\
                   for key, meta in sim_params_meta.items()}
# Add derived params
for key, meta in sim_params_meta.items():
    if meta['Derived']:
        for derived_from in meta['DerivedFrom']:
            sim_params_meta[derived_from]['DerivedParams'] = list(set(
                sim_params_meta[derived_from]['DerivedParams']
                + [key]
                ))

### Derived simulation parameters
## Laser power equivalent
def convert_to_laser_power_equivalent_2snp12(fs_component, laser_power):
    """
    Mapping of 2S-nP laser power to 2S-nP 1/2 laser power equivalent,
    i.e. laser power that would give same Rabi frequency for J=1/2 component
    as the Rabi frequency for given fine-structure component `fs_component` (string)
    and laser power `laser_power` (float).
    """
    return (
        np.array(laser_power)
        * pyha.def_atomic.relative_rabi_frequency_sqr_2snp12.get(fs_component, np.nan))

def convert_from_laser_power_equivalent_2snp12(fs_component, laser_power_equivalent):
    """
    Mapping of 2S-nP 1/2 laser power equivalent to 2S-nP laser power,
    i.e. laser power that gives same Rabi frequency
    for given fine-structure component `fs_component` (string) and laser power
    `laser_power` (float) as the laser power would give for J=1/2 component.
    """
    return (
        np.array(laser_power_equivalent)
        / pyha.def_atomic.relative_rabi_frequency_sqr_2snp12.get(fs_component, np.nan))
