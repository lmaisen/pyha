# -*- coding: utf-8 -*-
"""
Created on Mon Feb 25 14:23:05 2019

@author: Lothar Maisenbacher/MPQ

Literature:

Livesey1998: Livesey, R. G. (1998).
Flow of Gases through Tubes and Orifices.
In Foundations of vacuum science and technology.

Walraven1982: Walraven, J. T. M., & Silvera, I. F. (1982).
Helium-temperature beam source of atomic hydrogen.
Review of Scientific Instruments, 53(8), 1167.
https://doi.org/10.1063/1.1137152

Souers1977: Souers, P. C., Briggs, C. K., Pyper, J. W., & Tsugawa, R. T. (1977).
Hydrogen vapor pressures from 4 K to 30 K: A review.

Buckingham1968: Buckingham, R. A., & Gal, E. (1968).
Applications of quantum theory to the viscosity of dilute gases. Advan. At. Mol. Phys., 4(37), 37–61.

Vincenti1975: Vincenti, W. G., & Kruger, C. H. (1975).
Introduction to Physical Gas Dynamics. Krieger.

Ramsey1956: Ramsey, N. F. (1956). Molecular Beams. Oxford University Press.
"""

import numpy as np
import scipy.constants as con

'''
Constants
'''

# Room/laboratory temperature
room_temperature = con.zero_Celsius + 20

# Use hydrogen atomic weight as default particle weight
atomic_weight_hydrogen = 1.673533e-27
atomic_weight_default = atomic_weight_hydrogen

'''
Thermal ideal gases,
flow conversions
'''

# Temperature (K) and pressure (Pa) to which flow meter
# (Bronkhorst F-111B) is referenced (0 deg C, 101325 Pa),
# which corresponds to standard temperature and pressure (STP),
# but is called "normal" by Bronkhorst. However, normal temperature
# and pressure is commonly defined as 20 deg C, 101325 Pa.
flow_meter_ref_temperature = con.zero_Celsius
flow_meter_ref_pressure = 101325

def mean_thermal_vel(temperature=room_temperature,
                     atomic_weight=atomic_weight_default):
    '''
    Mean of the magnitude of the velocity of the atoms
    '''

    return np.sqrt(8*con.Boltzmann*temperature/(np.pi*atomic_weight))

def most_common_thermal_vel(temperature=room_temperature,
                            atomic_weight=atomic_weight_default):
    '''
    Most common thermal speed of the atoms (alpha in Ramsey1956, Eq. (II. 27))
    '''

    return np.sqrt(2*con.Boltzmann*temperature/(atomic_weight))

def mean_effusive_vel(temperature=room_temperature,
                      atomic_weight=atomic_weight_default):
    '''
    Mean of the magnitude of the velocity of the atoms for an effusive beam,
    see Ramsey1956, Eq. (II. 31)
    '''

    return (3/4)*np.sqrt(2*np.pi*con.Boltzmann*temperature/(atomic_weight))

def volume_flow_to_particle_flow(flow_mln_per_min):
    '''
    Convert volume flow (mln/min) referenced
    to 0 deg C and 101325 Pa, as used by Bronkhorst F-111B flow meter,
    to particle flow (particles/s)
    '''

    flow_m3_per_s = flow_mln_per_min*1e-6/60
    n_per_s = (
        (flow_meter_ref_pressure*flow_m3_per_s)
        /(con.Boltzmann*flow_meter_ref_temperature))
    return n_per_s

def volume_flow_to_leak_rate(flow_mln_per_min,
                              temperature=room_temperature):
    '''
    Convert volume flow (mln/min) referenced
    to 0 deg C and 101325 Pa, as used by Bronkhorst F-111B flow meter,
    to leak rate (mbar l/s) at temperature temperature
    '''

    n_per_s = volume_flow_to_particle_flow(flow_mln_per_min)
    leak_rate = n_per_s * con.Boltzmann * temperature
    leak_rate *= 1e3*1e-2
    return leak_rate

def particle_flow_to_volume_flow(n_per_s):
    '''
    Convert particle flow (particle/s) to
    volume flow (mln/min) referenced
    to 0 deg C and 101325 Pa, as used by Bronkhorst F-111B flow meter
    '''

    flow_m3_per_s = (
        n_per_s*con.Boltzmann*flow_meter_ref_temperature
        / flow_meter_ref_pressure)
    flow_mln_per_min = flow_m3_per_s*60/1e-6
    return flow_mln_per_min

'''
Flow through orifice as used in hydrogen discharge
'''

def equiv_length_rel(l, r):
    '''
    Livesey1998, Eq. (2.25)
    '''

    return 1 + 1/(3+3/7*(l/r))

def alpha_approx(l, r):

    le = equiv_length_rel(l, r) * l
    return 1/(1+(3/8)*(le/r))

def conductance(l, r, alpha, temperature=room_temperature,
                atomic_weight=atomic_weight_default):
    '''
    Livesey1998, Eq. (2.21)
    '''

    A = np.pi*r**2
    return (
        A
        * np.sqrt(con.Boltzmann*temperature/(2*np.pi*atomic_weight))
        * alpha)
