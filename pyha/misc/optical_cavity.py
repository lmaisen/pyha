# -*- coding: utf-8 -*-
"""
Created on Tue Mar  2 14:33:18 2021

@author: Lothar Maisenbacher/MPQ

Various calculations concerning optical cavities.
"""

import numpy as np
import scipy.optimize
import scipy.integrate

def refl_deg(t, a, d=0.):
    """
    Reflectivity of degraded mirror,
    assuming losses due to transmissivity `t`,
    scattering and absorption of non-degraded mirror `a`,
    and degradation `d`.
    """
    return (1 - d)*(1 - t - a)

def refl(t, a):
    """
    Reflectivity of non-degraded mirror.
    """
    return refl_deg(t, a, d=0.)

def power_enh_deg(t1, t2, a1, a2, d1=0., d2=0.):
    """
    Power enhancement of cavity with degraded mirrors.
    """
    return (
        (1 - d1)*t1
        / (1 -
            np.sqrt(refl_deg(t1, a1, d1)
            * refl_deg(t2, a2, d2))
            )**2
        )

def power_enh(t1, t2, a1, a2):
    """
    Power enhancement of cavity with non-degraded mirrors.
    """
    return power_enh_deg(t1, t2, a1, a2, d1=0., d2=0.)

def finesse_deg(t1, t2, a1, a2, d1=0., d2=0.):
    """
    Finesse of cavity with degraded mirrors.
    """
    return (
        np.pi*(refl_deg(t1, a1, d1)*refl_deg(t2, a2, d2))**(1/4)
        / (1 - np.sqrt(refl_deg(t1, a1, d1)*refl_deg(t2, a2, d2)))
        )

def finesse(t1, t2, a1, a2):
    """
    Finesse of cavity with non-degraded mirrors.
    """
    return finesse_deg(t1, t2, a1, a2, d1=0., d2=0.)

def rayleigh_length(w0, wavelength):
    """
    Rayleigh length for beam radius `w0` and
    laser wavelength `wavelength`.
    """
    return np.pi*w0**2/wavelength

def w(w0, dz, wavelength):
    """
    Beam radius at distance `dz` from waist with radius `w0`.
    """
    return w0*np.sqrt(1+(dz/rayleigh_length(w0, wavelength))**2)

def peak_intensity(w0, power=1.):
    """
    Peak intensity of Gaussian beam with waist radius `w0` and
    laser power `power` in beam.
    """
    return power*2/(np.pi*w0**2)

def e_field(x, y, w0, x0=0, y0=0):
    """
    Electric field amplitude of cavity mode at position of beam waist.
    """
    e_field = np.sqrt(peak_intensity(w0))*np.exp(-((x-x0)**2+(y-y0)**2)/w0**2)
    return e_field

def intensity(x, y, w0, x0=0, y0=0):
    """
    Transverse intensity profile of cavity mode at position of beam waist.
    """
    return np.abs(e_field(x, y, w0, x0, y0))**2

def calc_loss_rect_aper(a, b, w0, x0=0, y0=0):
    """
    Transmission losses on rectangular aperture of size `a` * `b`.
    """
    overlap = scipy.integrate.dblquad(
        lambda y, x: intensity(x, y, w0, x0, y0),
        -a/2, a/2, -b/2, b/2
    )
    return 1-overlap[0]

def calc_loss_circ_aper(r, w0):
    '''
    Transmission losses on circular aperture of radius `r`.
    '''
    overlap = scipy.integrate.dblquad(
        lambda phi, r: (
            r
            * intensity(
                r*np.cos(phi), r*np.sin(phi), w0)),
        0, r, 0, 2*np.pi
    )
    return 1-overlap[0]
