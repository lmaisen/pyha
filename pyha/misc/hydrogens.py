# -*- coding: utf-8 -*-
"""

@author: Lothar Maisenbacher/MPQ

Literature:
Souers1977: Souers, P. C., Briggs, C. K., Pyper, J. W., & Tsugawa, R. T. (1977).
Hydrogen vapor pressures from 4 K to 30 K: A review.

Crampton1980: Crampton, S. B. (1980).
Resonance studies of H atoms adsorbed on frozen H2 surfaces.
Le Journal de Physique Colloques, 41(C7), C7-249-C7-255.
https://doi.org/10.1051/jphyscol:1980739

Walraven1982: Walraven, J. T. M., & Silvera, I. F. (1982).
Helium-temperature beam source of atomic hydrogen.
Review of Scientific Instruments, 53(8), 1167.
https://doi.org/10.1063/1.1137152
"""

import numpy as np
import scipy.constants as con

from pyha.misc import gases

'''
Constants
'''
# Hydrogen atomic weight
atomic_weight_hydrogen = 1.673533e-27

# H2 gas density at STP (g/L)
density_h2_gas_stp = 0.08988

# Solid H2 density (kg/m^3) at 4.8 K from Souers1977, Fig. 5
density_h2_solid_4_8K = 4.43E4*con.Avogadro*(2*atomic_weight_hydrogen)

'''
Coefficients for vapor pressure of liquid hydrogen isotopes,
taken from Table 1 of Souers1977
'''
hs_liquid_vapor_pressure_coefficients = {}
hs_liquid_vapor_pressure_coefficients['eH2'] = {
    'A': 15.46688,
    'B': -1.013378e2,
    'C': 5.432005e-2,
    'D': -1.105632e-4
    }
hs_liquid_vapor_pressure_coefficients['nH2'] = {
    'A': 15.52059,
    'B': -1.027498e2,
    'C': 5.338981e-2,
    'D': -1.105632e-4
    }
hs_liquid_vapor_pressure_coefficients['HD'] = {
    'A': 16.52000,
    'B': -1.272167e2,
    'C': 3.405523e-2,
    'D': 0.
    }
hs_liquid_vapor_pressure_coefficients['nD2'] = {
    'A': 18.89988,
    'B': -1.612823e2,
    'C': -4.861678e-2,
    'D': 10.56887e-4
    }
hs_liquid_vapor_pressure_coefficients['T2'] = {
    'A': 19.11365,
    'B': -1.820038e2,
    'C': -2.560401e-2,
    'D': 5.133943e-4
    }

'''
Coefficients for vapor pressure of solid hydrogen isotopes,
taken from Table 6 of Souers1977
'''
hs_solid_vapor_pressure_coefficients = {}
hs_solid_vapor_pressure_coefficients['eH2'] = {
    'A': 7.416223,
    'B': -85.35199,
    'B\'': 2.903253,
    }
hs_solid_vapor_pressure_coefficients['nH2'] = {
    'A': 7.570953,
    'B': -86.94152,
    'B\'': 2.860678,
    }
hs_solid_vapor_pressure_coefficients['HD'] = {
    'A': 8.866980,
    'B': -112.7637,
    'B\'': 2.615288,
    }
hs_solid_vapor_pressure_coefficients['nD2'] = {
    'A': 9.801089,
    'B': -136.1893,
    'B\'': 2.463629,
    }

def hs_liquid_vapor_pressure(temperature, isotope='nH2'):
    '''
    Vapor pressure (Pa) of liquid hydrogen isotopes (eH2, nH2, HD, nD2, T2)
    at temperature temperature (K),
    using Eq. (13) and coefficients from Table 1 of Souers1977
    '''

    if isotope not in hs_liquid_vapor_pressure_coefficients.keys():
        raise Exception(f'Unknown hydrogen isotope \'{isotope}\'')
    coefficients = hs_liquid_vapor_pressure_coefficients[isotope]
    log_p = (
        coefficients['A'] + coefficients['B']/temperature
        + coefficients['C']*temperature + coefficients['D']*temperature**2)
    return np.exp(log_p)

def hs_solid_vapor_pressure(temperature, isotope='nH2'):
    '''
    Vapor pressure (Pa) of solid hydrogen isotopes (eH2, nH2, HD, nD2)
    at temperature temperature (K),
    using Eq. (11a) and coefficients from Table 6 of Souers1977
    '''

    if isotope not in hs_solid_vapor_pressure_coefficients.keys():
        raise Exception(f'Unknown hydrogen isotope \'{isotope}\'')
    coefficients = hs_solid_vapor_pressure_coefficients[isotope]
    log_p = (
        coefficients['A'] + coefficients['B']/temperature
        + coefficients['B\'']*np.log(temperature))
    return np.exp(log_p)

# Scale measurement of recombination rate from Crampton1980
# Adsorption energy (in units of k) as given in Walraven1982, p. 1172
crampton_epsilon_a = 38
# Gamma coefficient at 4.2 K as given in Crampton1980,
# with the recombination rate per collision given by this coefficient
# times the density
crampton_gamma_coeff_meas = 4e-22
# K_S2 as given in Walraven1982, p. 1172 (m^4/s)
crampton_ks2 = 3e-20
# Measurement temperature for K_S2 in Crampton1980
crampton_temperature_meas = 4.2
# Scale ks2 from measurement of ks20 as given in Walraven1980, p. 1179
crampton_ks20 = (
    crampton_ks2
    / (1/np.sqrt(crampton_temperature_meas)
    * np.exp(2*crampton_epsilon_a/crampton_temperature_meas))
    )

def gamma_h2_coeff(temperature):
    '''
    Recombination coefficient of H for each collision with H2 at temperature temperature (K),
    with the probability per collision given by multiplying with the density (m^-3),
    scaled from the measurement of the adsorption energy of H on solid H2
    of Crampton1980, as described in Walraven1982
    '''

    v_th = gases.mean_thermal_vel(temperature)

    ks2 = (
        crampton_ks20
        * 1/np.sqrt(temperature) * np.exp(2*crampton_epsilon_a/temperature))
    return (4/v_th)*ks2

'''
Transport of hydrogen atoms (H).
The degree of dissociation after multiple elements each
leading to a certain value of alpha_i is just given
by the product of the alpha_i. This also holds for the
gas purity.
'''

# Recombination probability for Teflon
# as given in Walraven1982, citation 21
# First-order recombination probability per collision
# Best value observed for commercial Teflon in Walraven1982
gamma_teflon_best = 1e-4
# Typical value observed for commercial Teflon in Walraven1982
gamma_teflon_typical = 4.5e-4
# Best match for our values
#gamma_teflon = 2.75e-4
# Volumen recombination coefficient (m^6 atom^-2 s^-1)
k_v_teflon = 1.2e-20

def alpha_eq9b(gamma, num_collisions):
    '''
    Degree of dissocation alpha after tube with first-order recombination,
    as is the case for Teflon,
    Walraven1982, Eq. (9b)
    '''

    return 1/np.cosh(np.sqrt(2*gamma*num_collisions))

def alpha_to_purity(alpha):
    '''
    Convert degree of dissociation alpha to gas purity
    alpha = N_H/(N_H + 2*N_H2)
    Purity = N_H/(N_H + N_H2)
    '''

    purity = 2*alpha/(alpha+1)
    return purity

def purity_to_alpha(purity):
    '''
    Convert gas purity purity to degree of dissociation alpha
    '''

    alpha = purity/(2-purity)
    return alpha

def h_fraction_of_particles(alpha):
    '''
    Fraction of particles that are H atoms
    for a degree of dissociation alpha,
    which is the same as the purity
    '''

    return alpha_to_purity(alpha)

def h2_fraction_of_particles(alpha):
    '''
    Fraction of particles that are H2 atoms
    for a degree of dissociation alpha,
    which is the same as the 1-purity
    '''

    return 1-alpha_to_purity(alpha)

def h_fraction_of_mass(alpha):
    '''
    Fraction of the particle mass that is in the form of H atoms
    for a degree of dissociation alpha
    '''

    return alpha

def h2_fraction_of_mass(alpha):
    '''
    Fraction of the particle mass that is in the form of H2 molecules
    for a degree of dissociation alpha
    '''

    return (1-alpha)

def n0_clausing(l, r, flow_in, temperature=gases.room_temperature):
    '''
    Particle density at beginning of tube with lenght l and
    radius r for input flow flow_in (particles/s),
    Walraven1982, Eq. (5)
    '''

    v_th = gases.mean_thermal_vel(temperature)
    n0 = (3/(2*np.pi))*(l/(r**3*v_th))*flow_in
    return n0

def n_critical_teflon_volume_recomb(r,
                                    gamma_teflon=gamma_teflon_typical,
                                    temperature=gases.room_temperature):
    '''
    Critical density of atoms before volume recombination becomes
    important for Teflon,
    Walraven1982, Eq. (5).
    I can however not reproduce the number given in Walraven1982.
    '''

    v_th = gases.mean_thermal_vel(temperature)
    n_critical = np.sqrt(gamma_teflon*v_th/(2*r*k_v_teflon))
    return n_critical

def h_fraction_of_particles_after_nozzle(alpha_nozzle_in,
                                          pressure_ratio_nozzle_cold):
    '''
    Fraction of particles leaving the nozzle that are H molecules (i.e. purity),
    assuming that the degree of dissociation at the nozzle input is alpha_nozzle_in
    and pressure_ratio_nozzle_cold is the pressure ratio between the nozzle warm,
    i.e. no hydrogen freezing on the nozzle and all particles that flow into the nozzle
    leave it, and nozzle cold, i.e. some H2 freezes on the nozzle,
    but no H recombines and freezes on the nozzle
    '''

    ratio_h2_to_h = (
        1/2*(pressure_ratio_nozzle_cold*(1+(1-alpha_nozzle_in)/alpha_nozzle_in)-1)
        )
    h2_fraction_of_particles = ratio_h2_to_h/(ratio_h2_to_h+1)
    return 1-h2_fraction_of_particles
