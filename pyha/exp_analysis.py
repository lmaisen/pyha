# -*- coding: utf-8 -*-
"""
Created on Fri Jul  3 16:15:06 2020

@author: Lothar Maisenbacher/MPQ

Various functions for the analysis of experimental data.
"""

import pandas as pd
import numpy as np
import scipy.constants

# pyhs
import pyhs.gen
logger = pyhs.gen.get_command_line_logger()
import pyhs.statfunc
import pyhs.fit

def combine_det_fit_derivs(dfScansFitDerivs, analysis_params,
                           mask_derivs=None, how=None):
    """
    Match scan fit derivatives contained in DataFrame `dfScansFitDerivs`
    from two detectors defined in `analysis_params` (dict).
    """
    if mask_derivs is None:
        mask_derivs = pd.Series(True, index=dfScansFitDerivs.index)
    if how is None:
        how = 'outer'
    mcs_channel_ids = analysis_params['MCSchannelIDs']
    mask_derivs_ch0 = (
        mask_derivs
        & (dfScansFitDerivs['dfScanFits_MCSchannelID'] == mcs_channel_ids[0]))
    mask_derivs_ch1 = (
        mask_derivs
        & (dfScansFitDerivs['dfScanFits_MCSchannelID'] == mcs_channel_ids[1]))
    # Match results for same scans from detectors
    suffixes = ['_'+elem for elem in mcs_channel_ids[0:2]]
    dfScansFitDerivsChs = (
        dfScansFitDerivs[mask_derivs_ch0]
        .reset_index()
        .merge(
            dfScansFitDerivs[mask_derivs_ch1],
            how=how,
            on=['dfData_ScanUID'],
            suffixes=suffixes,
        )
        .set_index('ScanUID')
        )
    # Defragmenting DataFrame
    dfScansFitDerivsChs = dfScansFitDerivsChs.copy()
    dfScansFitDerivsChs['dfScanFits_MCSchannelID'] = 'Avg'
    logger.info(
        f'Matched ({how} join) '
        + f'{np.sum(mask_derivs_ch0):d}/{np.sum(mask_derivs_ch1):d} '
        + 'scan fit derivative(s) '
        + f'from MCS channels {mcs_channel_ids[0]}/{mcs_channel_ids[1]}'
        + f', resulting in {len(dfScansFitDerivsChs)} '
        + 'combined scan fit derivative(s)'
        )

    return dfScansFitDerivsChs, suffixes

def calc_sec_doppler_shift(v_rms_value, v_rms_sigma, nu):
    """
    Calculate second-order Doppler shift for root-mean-square velocity
    `v_rms_value` (float or array-like) (with uncertainty `v_rms_sigma` (float or array-like))
    and frequency `nu` (Hz, float or array-like).
    To correct a measurement, this shift needs to be subtracted from the
    experimental result.
    """
    sod_value = -(v_rms_value/scipy.constants.c)**2*nu/2
    sod_sigma = v_rms_value/scipy.constants.c**2*nu*v_rms_sigma
    return sod_value, sod_sigma

def get_scan_averaged_delay_results(df, analysis_params,
                                    super_set, dfDataFormat, mcs_channel_id,
                                    delay_ids=None, sim_ids=None, param_id_x=None,
                                    param_id_weights=None):
    """
    Average line scan fit results for each delay for
    various fit parameters and the corresponding simulation data.
    """
    super_set = {
        'UseSDUncert': False,
        'ScaleUncertByRCS': False,
        **super_set,
        }
    delay_ids = analysis_params['DelayIDs'] if delay_ids is None else delay_ids
    sim_ids = ['BigModel', 'LFS', 'SOD'] if sim_ids is None else sim_ids
    param_id_x = 'dfDelayVels_V_Mean_Value' if param_id_x is None else param_id_x
    param_id_y = super_set['ParamID']
    param_row = dfDataFormat.loc[param_id_y]
    param_has_uncert = param_row['SigmaName'] != ''

    column_mcs_channel_id = (
        super_set['ColumnMCSchannelID'] if super_set.get('ColumnMCSchannelID') is not None
        else 'dfScanFits_MCSchannelID')
    column_delay_id = (
        super_set['ColumnDelayID'] if super_set.get('ColumnDelayID') is not None
        else 'dfScanFits_DelayID')
    column_param_y = (
        super_set['ColumnParamID'] if super_set.get('ColumnParamID') is not None else param_id_y)
    column_param_y_sigma = (
        super_set['ColumnParamIDSigma'] if super_set.get('ColumnParamIDSigma') is not None
        else param_row['Source']+'_'+param_row['SigmaName'])

    mask_scans_ch = (
        (df[column_mcs_channel_id] == mcs_channel_id))

    delay_ids_avail = delay_ids
    x_w_avgs_array = np.full(
        (len(pyhs.statfunc.avg_uncorr_meas(np.nan, return_dict=False)), len(delay_ids_avail)),
        np.nan)
    w_avgs_array = np.full(
        (len(pyhs.statfunc.avg_uncorr_meas(np.nan, return_dict=False)), len(delay_ids_avail)),
        np.nan)
    w_avgs_ratio_exp_sim_array = {
        sim_id: np.full(
            (len(pyhs.statfunc.avg_uncorr_meas(np.nan, return_dict=False)), len(delay_ids_avail)),
            np.nan)
        for sim_id in sim_ids}
    # Uncertainty of parameter for each averaged delay. Identical to uncertainty of weighted average
    # over delay (`w_avgs_array[1]`) if parameter has associated uncertainty. If not, either
    # standard deviation of parameter over delay is used (if `super_set['ScaleUncertByRCS']` is
    # True) or no uncertainty is given.
    y_sigma = np.full(len(delay_ids_avail), np.nan)
    y_sim = {sim_id: np.full(len(delay_ids_avail), np.nan) for sim_id in sim_ids}
    y_sd_sim = {sim_id: np.full(len(delay_ids_avail), np.nan) for sim_id in sim_ids}
    y_sigma_ratio_exp_sim = {sim_id: np.full(len(delay_ids_avail), np.nan) for sim_id in sim_ids}
    if param_id_weights is None:
        # No weight parameter specified, using simple average of delay speed
        x_weights_dly = None
    for i_delay, delay_id in enumerate(delay_ids_avail):
        # Get data (scans or average of scans) in this delay
        mask_scans_delay = (
            mask_scans_ch
            & (df[column_delay_id] == delay_id)
            & (~np.isnan(df[column_param_y])))
        x_dly = (
            df[mask_scans_delay][param_id_x].values)
        y_dly = df[mask_scans_delay][column_param_y].values
        if (column_red_chi_sq := super_set.get('ColumnRCS')) is not None:
            red_chi_sq_dlys = df[mask_scans_delay][column_red_chi_sq].values
        else:
            red_chi_sq_dlys = None
        if param_has_uncert:
            # Parameter has uncertainity, weighted average
            y_sigma_dly = (
                df[mask_scans_delay][column_param_y_sigma].values)
            if super_set.get('ScaleInputUncertByRCS', False) and red_chi_sq_dlys is not None:
                y_sigma_dly = pyhs.statfunc.scale_by_rcs(y_sigma_dly, red_chi_sq_dlys, True)
        else:
            # Parameter has no uncertainity, simple average
            y_sigma_dly = None
        # Weighted average over data (scans or average of scans) in this delay
        w_avgs_array[:, i_delay] = pyhs.statfunc.avg_uncorr_meas(
            y_dly, y_sigma_dly, return_dict=False)
        if param_id_weights is not None:
            # Get weights of data (scans or average of scans) from uncertainty of weight parameter
            # (`param_id_weights`) to calculate mean speed of delay
            # (only of importance if there is more than one velocity available
            # from simulations).
            # These weights are also used for a weighted average of simulation corrections
            # below.
            # The idea of using the uncertainty of a separate weight parameter and not the
            # uncertainty of the parameter averaged here is that (a) there might not be an
            # uncertainty available for the parameter and (b) we want the mean speeds to be the
            # same for averages for the given delay (e.g., the same mean speed for an average of the
            # resonance frequency as for the line width).
            # A natural choice for the weight parameter is the resonance frequency (`CFR`).
            x_weights_dly = (
                df[mask_scans_delay][param_id_weights].values)
            if super_set.get('ScaleWeightsXByRCS', False) and \
                    (column_weights_red_chi_sq := super_set.get('ColumnWeightsXRCS')) is not None:
                x_weights_dly = pyhs.statfunc.scale_by_rcs(
                    x_weights_dly, df[mask_scans_delay][column_weights_red_chi_sq].values, True)
        # Weighted average over speeds in delay
        x_w_avgs_array[:, i_delay] = pyhs.statfunc.avg_uncorr_meas(
            x_dly, x_weights_dly, return_dict=False)
        if super_set['UseSDUncert']:
            y_sigma[i_delay] = (
                np.std(y_dly, ddof=1)
                if len(y_dly) > 1 else np.nan)
        elif param_has_uncert:
            if super_set['ScaleUncertByRCS']:
                y_sigma[i_delay] = pyhs.statfunc.scale_by_rcs(
                    w_avgs_array[1, i_delay], w_avgs_array[3, i_delay])
            else:
                y_sigma[i_delay] = w_avgs_array[1, i_delay]

        # Simulation results for this delay
        for sim_id in sim_ids:
            column_sim = (
                super_set[f'Column{sim_id}'] if super_set.get(f'Column{sim_id}') is not None
                else param_id_y+'_'+sim_id)
            if column_sim in df:
                # Weighted average of simulation result.
                # The weights `weights` determined above are used, not necessarily the
                # uncertainty of the parameter averaged here.
                # We are only interested in the mean value, and not the uncertainty,
                # of the weighted average, since the simulation data is not considered to
                # be associated with an uncertainty here.
                y_sim_dly = df[mask_scans_delay][column_sim].values
                w_avg_sim = pyhs.statfunc.avg_uncorr_meas(y_sim_dly, x_weights_dly)
                y_sim[sim_id][i_delay] = w_avg_sim['Mean']
                y_sd_sim[sim_id][i_delay] = (
                    np.std(y_sim_dly, ddof=1) if len(y_sim_dly) > 1 else np.nan)

                # Ratio exp./sim. for all data (scans or average of scans), then weighted average.
                # As weights, the uncertainty of the parameter averaged here is used,
                # as we are interested in not only the mean value, but also the uncertainty
                # of the weighted average, as the ratio contains the meaningful uncertainty
                # of the experimental data.
                y_ratio_exp_sim_dly = y_dly/y_sim_dly
                if y_sigma_dly is not None:
                    # Propagation of uncertainty for A/B, where A is the experimental data,
                    # B is the simulation data. The uncertainty of A is `y_sigma_dly`,
                    # while B has no uncertainty associated with it at this stage.
                    ratio_weights = y_sigma_dly/y_sim_dly
                else:
                    ratio_weights = None
                w_avgs_ratio_exp_sim_array[sim_id][:, i_delay] = pyhs.statfunc.avg_uncorr_meas(
                    y_ratio_exp_sim_dly, ratio_weights, return_dict=False)

                if super_set['UseSDUncert']:
                    y_sigma_ratio_exp_sim[sim_id][i_delay] = (
                        np.std(y_ratio_exp_sim_dly, ddof=1)
                        if len(y_ratio_exp_sim_dly) > 1 else np.nan)
                else:
                    if super_set['ScaleUncertByRCS']:
                        y_sigma_ratio_exp_sim[sim_id][i_delay] = (
                            pyhs.statfunc.scale_by_rcs(
                                w_avgs_ratio_exp_sim_array[sim_id][1, i_delay],
                                w_avgs_ratio_exp_sim_array[sim_id][3, i_delay]))
                    else:
                        y_sigma_ratio_exp_sim[sim_id][i_delay] = (
                            w_avgs_ratio_exp_sim_array[sim_id][1, i_delay])

    # Convert 2D arrays into dicts of 1D arrays
    w_avgs = {
        key: w_avgs_array[i, :]
        for i, key in enumerate(pyhs.statfunc.avg_uncorr_meas(np.nan, full_output=True).keys())}
    x_w_avgs = {
        key: x_w_avgs_array[i, :]
        for i, key in enumerate(pyhs.statfunc.avg_uncorr_meas(np.nan, full_output=True).keys())}
    # If no weights have been used in weighted average over speeds in delay, replace uncertainty
    # of weighted average - which is NaN in this case - with None
    if param_id_weights is None:
        x_w_avgs['Sigma'] = None
    w_avgs_ratio_exp_sim = {
        sim_id: {
            key: w_avgs_ratio_exp_sim_array[sim_id][i, :]
            for i, key in enumerate(pyhs.statfunc.avg_uncorr_meas(np.nan, full_output=True).keys())}
        for sim_id in sim_ids}
    return (
        x_w_avgs, w_avgs, y_sigma, y_sim, y_sd_sim,
        w_avgs_ratio_exp_sim, y_sigma_ratio_exp_sim, param_has_uncert)

def save_scan_averaged_delay_results(dfScansFitParams, analysis_params,
                                     super_sets, dfDataFormat, set_analysis_uid,
                                     sim_ids, param_id_weights):
    """
    Compute (weighted) average of various parameters over all scans with each delay,
    and save results to returned DataFrame `dfSetAnalysisDelays`.
    Additionally, the scan-averaged results are then averaged and extrapolated over all
    delays.
    This procedure was used for the hydrogen 2S-4P measurement.
    Note that this will give identical results to first averaging or extrapolation over the
    delays of a single scan and then averaging over the scans, if and only if no scaling
    (with the reduced chi-squared) happens in-between. It will however in general give
    different reduced chi-squared values for the scan and delay averages.
    Here, only results for the individual detectors are computed, and no average with or
    without correlations is attempted.

    Parameters
    ----------
    dfScansFitParams : pandas DataFrame
        DataFrame containing the fit results of each scan for each delay.
    analysis_params : dict
        Analysis parameters, see `H2S6P2019.exp_scan_analysis`.
    super_sets : dict
        Metadata for parameters for which scan-average will be computed.
    dfDataFormat : pandas DataFrame
        DataFrame containing data format of parameters.
    set_analysis_uid : str
        UID of analysis set, used as index for returned DataFrame.
    sim_ids : list
        IDs of simulation for which results should be included.
    param_id_weights : str
        Column of `dfScansFitParams` whose uncertainty will be used as weight for the average within
        each delay over the delay velocities and other parameters without uncertainty.

    Returns
    -------
    dfSetAnalysisDelays : pandas DataFrame
        DataFrame containing scan-averaged delay results and their delay average and extrapolation.

    """
    myFit = pyhs.fit.Fit()

    dfSetAnalysisDelays_chs = {}
    delay_ids_with_avg = list(analysis_params['DelayIDs']) + ['Avg', 'Slope', 'Offset']
    for mcs_channel_id in analysis_params['MCSchannelIDs']:
        dfSetAnalysisDelays_chs[mcs_channel_id] = pd.DataFrame(index=delay_ids_with_avg)

    for i_super_set, (super_set_id, super_set) in enumerate(super_sets.items()):

        type_ = super_set.get('Type')
        if type_ is None:
            continue

        for i_cs_channel, mcs_channel_id in enumerate(analysis_params['MCSchannelIDs']):

            (v_w_avgs, w_avgs, y_sigma, y_sim, y_sd_sim,
             w_avgs_ratio_exp_sim, y_sigma_ratio_exp_sim, param_has_uncert) = (
                get_scan_averaged_delay_results(
                    dfScansFitParams, analysis_params,
                    super_set, dfDataFormat, mcs_channel_id,
                    sim_ids=sim_ids,
                    param_id_x='dfDelayVels_V_Mean_Value',
                    param_id_weights=param_id_weights))

            # Add delay speeds
            v = v_w_avgs['Mean']
            dfSetAnalysisDelays_chs[mcs_channel_id].loc[
                analysis_params['DelayIDs'], 'V_Mean_Value'] = v
            # Add mean delay speed, found by weighted average over delay speeds using weights
            # from `param_id_weights`
            for key in ['Avg', 'Offset', 'Slope']:
                v_delay_avg = pyhs.statfunc.avg_uncorr_meas(v, v_w_avgs['Sigma'])
                dfSetAnalysisDelays_chs[mcs_channel_id].loc[
                    key, 'V_Mean_Value'] = v_delay_avg['Mean']

            if type_ == 'ExpData':
                dfSetAnalysisDelays_chs[mcs_channel_id].loc[
                    analysis_params['DelayIDs'], f'{super_set_id}_Value'] = w_avgs['Mean']
                dfSetAnalysisDelays_chs[mcs_channel_id].loc[
                    analysis_params['DelayIDs'], f'{super_set_id}_Sigma'] = y_sigma
                y_sigma_lin = y_sigma
                delay_avg = pyhs.statfunc.avg_uncorr_meas(w_avgs['Mean'], y_sigma)
                dfSetAnalysisDelays_chs[mcs_channel_id].loc[
                    'Avg', f'{super_set_id}_Value'] = delay_avg['Mean']
                dfSetAnalysisDelays_chs[mcs_channel_id].loc[
                    'Avg', f'{super_set_id}_Sigma'] = delay_avg['Sigma']
                if param_has_uncert:
                    for key in ['RedChiSq', 'RedChiSqSigma']:
                        dfSetAnalysisDelays_chs[mcs_channel_id].loc[
                            analysis_params['DelayIDs'], f'{super_set_id}_{key}'] = (
                                w_avgs[key])
                        dfSetAnalysisDelays_chs[mcs_channel_id].loc[
                            'Avg', f'{super_set_id}_{key}'] = delay_avg[key]

            for sim_id in sim_ids:
                if type_ == f'SimRatio_{sim_id}':
                    if super_set.get('NormSimRatio', False):
                        norm = np.mean(w_avgs_ratio_exp_sim[sim_id]['Mean'])
                    else:
                        norm = 1
                    dfSetAnalysisDelays_chs[mcs_channel_id].loc[
                        analysis_params['DelayIDs'], f'{super_set_id}_Value'] = (
                            w_avgs_ratio_exp_sim[sim_id]['Mean']/norm)
                    dfSetAnalysisDelays_chs[mcs_channel_id].loc[
                        analysis_params['DelayIDs'], f'{super_set_id}_Sigma'] = (
                            y_sigma_ratio_exp_sim[sim_id]/norm)
                    y_sigma_lin = y_sigma_ratio_exp_sim[sim_id]/norm
                    # Experimentally weighted average of simulation results
                    delay_avg = pyhs.statfunc.avg_uncorr_meas(
                        w_avgs_ratio_exp_sim[sim_id]['Mean']/norm,
                        y_sigma_ratio_exp_sim[sim_id]/norm)
                    dfSetAnalysisDelays_chs[mcs_channel_id].loc[
                        'Avg', f'{super_set_id}_Value'] = delay_avg['Mean']
                    dfSetAnalysisDelays_chs[mcs_channel_id].loc[
                        'Avg', f'{super_set_id}_Sigma'] = delay_avg['Sigma']
                    if param_has_uncert:
                        for key in ['RedChiSq', 'RedChiSqSigma']:
                            dfSetAnalysisDelays_chs[mcs_channel_id].loc[
                                analysis_params['DelayIDs'], f'{super_set_id}_{key}'] = (
                                    w_avgs_ratio_exp_sim[sim_id][key])
                            dfSetAnalysisDelays_chs[mcs_channel_id].loc[
                                'Avg', f'{super_set_id}_{key}'] = (
                                    delay_avg[key])
                if type_ == f'SimData_{sim_id}':
                    dfSetAnalysisDelays_chs[mcs_channel_id].loc[
                        analysis_params['DelayIDs'], f'{super_set_id}_Value'] = y_sim[sim_id]
                    # Experimentally weighted average of simulation results
                    delay_avg = pyhs.statfunc.avg_uncorr_meas(y_sim[sim_id], y_sigma)
                    dfSetAnalysisDelays_chs[mcs_channel_id].loc[
                        'Avg', f'{super_set_id}_Value'] = delay_avg['Mean']
                    y_sigma_lin = y_sigma

            # Fit data with linear function
            y_lin = (
                dfSetAnalysisDelays_chs[mcs_channel_id]
                .loc[analysis_params['DelayIDs'], f'{super_set_id}_Value'])
            fit_func = myFit.fit_funcs['Linear']
            fit_result, error_msg = myFit.do_fit(
                fit_func, v, y_lin, y_sigma_lin, out_func=logger.info, remove_nan_points=True)
            sr_linear_fit = myFit.fit_result_to_dict(
                fit_result, fit_func)
            for key in ['Offset', 'Slope']:
                dfSetAnalysisDelays_chs[mcs_channel_id].loc[
                    key, f'{super_set_id}_Value'] = sr_linear_fit[f'{key}_Value']
                dfSetAnalysisDelays_chs[mcs_channel_id].loc[
                    key, f'{super_set_id}_Sigma'] = sr_linear_fit[f'{key}_Sigma']
                dfSetAnalysisDelays_chs[mcs_channel_id].loc[
                    key, f'{super_set_id}_RedChiSq'] = sr_linear_fit['RedChiSq']
                dfSetAnalysisDelays_chs[mcs_channel_id].loc[
                    key, f'{super_set_id}_RedChiSqSigma'] = (
                        np.sqrt(2/(len(v)-2))
                        if not np.isnan(sr_linear_fit['RedChiSq']) else np.nan)

    # Add number of scans in each delay
    num_scan_delays = (
        dfScansFitParams.groupby(by=['dfScanFits_MCSchannelID', 'dfScanFits_DelayID']).size())
    for mcs_channel_id in analysis_params['MCSchannelIDs']:
        if mcs_channel_id in \
                num_scan_delays.index.get_level_values('dfScanFits_MCSchannelID').unique():
            (dfSetAnalysisDelays_chs[mcs_channel_id]
             .loc[num_scan_delays.loc[mcs_channel_id].index, 'NScansDelay']) = (
                 num_scan_delays.loc[mcs_channel_id])
            for key in ['Avg', 'Offset', 'Slope']:
                (dfSetAnalysisDelays_chs[mcs_channel_id]
                 .loc[key, 'NScansDelay']) = np.mean(num_scan_delays.loc[mcs_channel_id])
        else:
            dfSetAnalysisDelays_chs[mcs_channel_id]['NScansDelay'] = np.nan

    # Add DataFrame with delay results to DataFrame `dfSetAnalysisDelays`
    dfSetAnalysisDelays = pd.concat(dfSetAnalysisDelays_chs)
    dfSetAnalysisDelays.index.names = ['MCSchannelID', 'DelayID']
    dfSetAnalysisDelays = dfSetAnalysisDelays.reset_index()
    dfSetAnalysisDelays.index = [set_analysis_uid] * len(dfSetAnalysisDelays)
    dfSetAnalysisDelays.index.name = 'SetAnalysisUID'

    return dfSetAnalysisDelays
