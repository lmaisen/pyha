# -*- coding: utf-8 -*-
"""
Created on Sun Feb 12 21:35:46 2023

@author: Lothar Maisenbacher/MPQ
"""

import numpy as np
import tomli
import argparse
from pathlib import Path

# pyhs
import pyhs.gen
logger = pyhs.gen.get_command_line_logger()

def load_config_file(config_path=None, parser=None, always_load_global=False):
    """
    Load configuration from TOML file (see `pyhs.gen.load_config_file`), and process configuration.
    Here, all relevant absolute directories and relative subdirectories are converted to
    instances of `pathlib.Path`.
    Furthermore, for all relative subdirectories, the corresponding absolute path is found and
    added to configuration. E.g., the path 'dir_metadata' is formed from 'dir_trajs' and
    'subdir_metadata', unless and entry for 'dir_metadata' already exists.

    Parameters
    ----------
    config_path : str or list-like of str or None, optional
        Path(s) to TOML configuration file(s).
        This always takes precedence over other search locations. If multiple paths are given,
        all files will be loaded and the resulting dictionaries will be merged at the top-level,
        with each newly-loaded file taking precedence over the previously loaded files.
        I.e., `config = config | config_new`, where `config` and `config_new` are the dicts of the
        previously loaded and newly-loaded files, respectively.
        If set to None, but a path is specified through the command line argument (see below),
        this path is used.
        Otherwise, the current working directory is searched for `config.toml`, which is loaded
        if found.
        If not found, it is attempted to load the global configuration file `pyh.toml` from the
        user's home directory.
        The default is None.
    parser : instance of `argparse.ArgumentParser` or None, optional
        Instance of `ArgumentParser` to use to parse input arguments.
        If set to None, a new instance of `ArgumentParser` is used.
        The argument '-c' ('--config') is added, through which the path to the TOML configuration
        file can be set.
        The default is None.
    always_load_global : bool, optional
        If set to True, the global configuration file `pyh.toml` from the user's home directory is
        added to the start of the list of files to be loaded (i.e., the other files to be loaded
        will take precedence). If this file is not found, but other config files are to be loaded,
        a warning, but no error, is issued. Otherwise, a `FileNotFoundError` exception is thrown.
        The default is False.

    Returns
    -------
    data : dict
        The loaded configuration.

    """
    # Filepath for global config file
    global_config_filepath = Path(Path.home(), 'pyh.toml')
    if parser is None:
        parser = argparse.ArgumentParser(
            description='pyh default config parser.')
    parser.add_argument(
        '-c','--config', dest='configpath', help='Path to configuration file', required=False,
        default=None)
    config_paths = []
    if config_path is not None:
        config_paths = [Path(config_path_) for config_path_ in np.atleast_1d(config_path)]
    elif (config_path_ := parser.parse_args().configpath) is not None:
        config_paths = [Path(config_path_).absolute()]
    else:
        # Search for config file in local directory
        local_config_path = Path('config.toml')
        if local_config_path.exists():
            config_paths = [local_config_path.resolve()]

    config_paths_required = [True for i in range(len(config_paths))]
    if len(config_paths) == 0 or always_load_global:
        # Add global `.pyhconfig` configuration file
        config_paths.insert(0, global_config_filepath)
        config_paths_required.insert(0, len(config_paths) == 1)

    config = {}
    for config_path, config_path_required in zip(config_paths, config_paths_required):
        # Read config file
        logger.info('Reading configuration from file \'%s\'', config_path)
        if not config_path.exists():
            msg = f'Configuration file \'{config_path}\' does not exist'
            if config_path_required:
                logger.error(msg)
                raise FileNotFoundError(msg)
            else:
                logger.warning(msg)
        else:
            with open(config_path, 'rb') as f:
                config = config | tomli.load(f)

    # Convert absolute directories and relative subdirectory to instances of `pathlib.Path`
    for key, subkeys in zip(
            [
                'MonteCarloTrajectories', 'MeasurementMetadata', 'MeasurementData',
                'SimulationGrids', 'DetectionEfficiencySimulations'],
            [
                ['DirTrajs', 'SubdirMetadataInput', 'SubdirMetadata',
                 'SubdirPlots', 'SubdirSets'],
                ['DirMetadata', 'DirSimReqs'],
                ['DirData', 'DirLogs', 'DirCounterData'],
                ['DirGrids', 'DirBaseGrids', 'DirGridInterpolations'],
                ['DirDetEffSims'],
            ]):
        if key not in config.keys():
            continue
        for subkey in subkeys:
            if (dir_ := config[key].get(subkey)) is not None:
                config[key][subkey] = Path(dir_)
    # Join subdirectories with directory to create absolute path
    if (config.get('MonteCarloTrajectories') is not None) and \
            (dir_trajs := config['MonteCarloTrajectories'].get('DirTrajs')) is not None:
        for key in ['MetadataInput', 'Metadata', 'Plots', 'Sets']:
            if (subdir := config['MonteCarloTrajectories'].get(f'Subdir{key}')) is None:
                continue
            if config['MonteCarloTrajectories'].get(f'Dir{key}') is None:
                config['MonteCarloTrajectories'][f'Dir{key}'] = dir_trajs / subdir

    return config
