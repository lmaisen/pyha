# -*- coding: utf-8 -*-
"""
Created on Fri Jun 19 15:09:27 2020

@author: Lothar Maisenbacher/MPQ
"""

import numpy as np
import pandas as pd
import logging

# pyhs
import pyhs.gen

logger = pyhs.gen.get_command_line_logger()

## Shorthands
# Experimental parameters in DataFrames
from pyha.def_shorthands import ep__

def filter_scans(dfAll_scans, scan_filters=None, data_group=None,
                 quiet=False, ExpMetadata=None):
    """
    Filter scan data contained in DataFrame `dfAll_scans`, with filters defined
    in `scan_filters` (dict).
    """
    def var_to_array(var):
        return np.array([var]).flatten()

    if scan_filters is None:
        scan_filters = {}
    if data_group is not None:
        scan_filters_data_group = {
            'ScanType': data_group['ScanType'],
            'FS': data_group['FS'],
            '2SnPLaserPower': data_group['2SnPLaserPower'],
            'ThetaL': data_group['ThetaL'],
            'AlphaOffset': data_group['AlphaOffset'],
            'MeasRun': data_group['MeasRun'],
            }
        scan_filters = {
            **scan_filters,
            **scan_filters_data_group,
            }
        logger.info(f'Filtering for data group {data_group.name}')
    logger_level = logger.getEffectiveLevel()
    if quiet:
        logger.setLevel(logging.WARNING)

    data_masks_scans = {}

    ## Filter on scan data
    dataMask_scans_true = pd.Series(True, index=dfAll_scans.index)
    dataMask_scans = dataMask_scans_true.copy()
    logger.info(f'Filtering on {np.sum(dataMask_scans):d} scan(s) in total')

    # Scan data only
    data_masks_scans['Scans'] = (
        dfAll_scans['dfData_Scan'] == True)
    # Completed scans only
    data_masks_scans['CompletedScans'] = (
        dfAll_scans['dfScans_Completed'] == 1.)
    # 2S-6P scans
    data_masks_scans['2S6PScans'] = (
        dfAll_scans['dfScans_FreqSrc'] == 'AOM2S6P')
    # 1S-2S scans
    data_masks_scans['1S2SScans'] = (
        dfAll_scans['dfScans_FreqSrc'] == 'AOM1S2S')
    # No fields applied
    dataMask_scans_no_fields = dataMask_scans_true.copy()
    for ao_channel_iid in range(0, 6):
        dataMask_scans_no_fields &= (
            dfAll_scans[f'dfData_AO_AO{ao_channel_iid:d}_Volt'] == 0.)
    if 'dfData_Volt_BlockMeshUpper_Volt' in dfAll_scans:
        dataMask_scans_no_fields &= (
            dfAll_scans['dfData_Volt_BlockMeshUpper_Volt'] == 0.)
    if 'dfData_Volt_BlockMeshLower_Volt' in dfAll_scans:
        dataMask_scans_no_fields &= (
            dfAll_scans['dfData_Volt_BlockMeshLower_Volt'] == 0.)
    data_masks_scans['NoFields'] = dataMask_scans_no_fields

    ## Apply scan filters

    # Apply predefined scan masks
    for key, data_mask_scan in data_masks_scans.items():
        output_0 = f'\'{key}\' (predef.) ({np.sum(data_mask_scan)} sc.)'
        if key in scan_filters.get('PredefDataMasks', {}):
            dataMask_scans &= data_mask_scan
            logger.info(output_0+f', applied: {np.sum(dataMask_scans)} sc.')
        else:
            logger.info(output_0+', not applied')

    # Filter by experimental parameters
    filter_columns = {
        'MeasRun': ep__+'MeasRun',
        'FS': ep__+'FS',
        'ThetaL': ep__+'ThetaL',
        'AlphaOffset': ep__+'AlphaOffset',
        '2SnPLaserPower': ep__+'2SnPLaserPower',
        '2SnP12LaserPowerEquivalent': ep__+'2SnP12LaserPowerEquivalent',
        'ScanType': ep__+'ScanType',
        'FCUID': ep__+'FCUID',
        'ScanExcl': ep__+'ScanExcl',
        'ChExcl_Ch0': ep__+'ChExcl_Ch0',
        'ChExcl_Ch1': ep__+'ChExcl_Ch1',
        }
    for filter_name, filter_column in filter_columns.items():
        if scan_filters.get(filter_name) is not None:
            filter_values = var_to_array(scan_filters.get(filter_name))
            dataMask_scans_named_filter = (
                dfAll_scans[filter_column].isin(filter_values))
            dataMask_scans &= dataMask_scans_named_filter
            filter_values_str = (
                ', '.join([str(elem) for elem in filter_values]))
            logger.info(
                f'Exp. param \'{filter_name}\': {filter_values_str}'
                + f' ({np.sum(dataMask_scans_named_filter)} sc.)'
                + f', applied: {np.sum(dataMask_scans)} sc.'
                )

    # Filter by state of AFR shutter (open, closed)
    if scan_filters.get('AFRShutter') is not None:
        shutter_states_numerical = []
        shutter_states = var_to_array(scan_filters.get('AFRShutter'))
        if 'Open' in shutter_states:
            shutter_states_numerical.append(0.)
        if 'Closed' in shutter_states:
            shutter_states_numerical.append(1.)
        dataMask_scans_afr_shutter = (
            dfAll_scans['dfData_MeasFlag_AFRshutter_State'].isin(
                shutter_states_numerical))
        dataMask_scans &= dataMask_scans_afr_shutter
        logger.info(
            f'AFR shutter: {", ".join(shutter_states)}'
            + f' ({np.sum(dataMask_scans_afr_shutter)} sc.)'
            + f', applied: {np.sum(dataMask_scans)} sc.'
            )

    if quiet:
        logger.setLevel(logger_level)

    if scan_filters.get('SimScanDataGroupID') is not None \
            and ExpMetadata is not None:
        # Select only scans that are part of a simultaneous scan and
        # where the other scan belongs to the data group given in
        # scan_filters['SimScanDataGroupID']

        data_group_id_sim_scan = scan_filters['SimScanDataGroupID']

        dfScansMetas_ = (
            ExpMetadata.dfs['dfScansMetas']
            [ExpMetadata.dfs['dfScansMetas']['ScanExcl'] == False]
            )
        dfAll_scans_sim_scans = (
            dfAll_scans[dataMask_scans][['dfData_ScanUID', 'dfScans_ScanSetUID']]
            .merge(
                dfAll_scans[['dfData_ScanUID', 'dfScans_ScanSetUID']]
                .merge(
                    dfScansMetas_.loc[:, ['DataGroupID']],
                    left_index=True,
                    right_index=True,
                    )
                ,
                how='left',
                left_on=['dfScans_ScanSetUID'],
                right_on=['dfScans_ScanSetUID'],
                )
            .set_index('dfScans_ScanSetUID')
            )
        mask_scan_set_uids = (
            (dfAll_scans_sim_scans['dfData_ScanUID_x'] != dfAll_scans_sim_scans['dfData_ScanUID_y'])
            & (dfAll_scans_sim_scans['DataGroupID'] == data_group_id_sim_scan)
            )
        scan_set_uids = (
            dfAll_scans_sim_scans[mask_scan_set_uids].index)

        dataMask_scans_sim_scan = (
            dataMask_scans
            & (dfAll_scans['dfScans_ScanSetUID'].isin(scan_set_uids))
            )
        scan_set_uids_found = (
            dfAll_scans[dataMask_scans_sim_scan]['dfScans_ScanSetUID'])
        logger.info(
            f'Found {len(scan_set_uids_found):d} simultaneous scan(s) in current '
            + 'selection that are '
            + f'simultaneous scan(s) with data group \'{data_group_id_sim_scan}\''
            )
        dataMask_scans = dataMask_scans_sim_scan

    # Total number of scans from scan filtering
    num_scans_found = np.sum(dataMask_scans)
    logger.info(f'Found {num_scans_found:d} scan(s) in total')

    return dataMask_scans

def filter_line_scans_red_chi_sq(dfScansFitParams, analysis_params,
                                 df_to_mask=None, df_to_mark=None):
    """
    Filter line scans by red. χ^2, with any line scans for with at least one
    containing line scan delay fit has a red. χ^2 higher than
    `analysis_params['RedChiSqCutoff']` is excluded.
    Done for each MCS channel individually and the combination of all channels.
    """
    scan_uids_leq_cutoffs = {}
    num_fits_excluded_per_delay = {}
    filter_strs = {}
    cutoff_masks = {}
    for key, mcs_channel_ids in zip(
            analysis_params['MCSchannelIDs'] + ['Avg']
            ,
            [[elem] for elem in analysis_params['MCSchannelIDs']]
            + [analysis_params['MCSchannelIDs']]
            ):
        mask_channel_id = (
            (dfScansFitParams['dfScanFits_MCSchannelID'].isin(mcs_channel_ids))
            )
        scan_uids = dfScansFitParams[mask_channel_id]['dfData_ScanUID'].unique()
        num_fits_excluded_per_delay[key] = {
            delay_id: 0 for delay_id in analysis_params['DelayIDs']}
        if df_to_mark is not None:
            column = f'ScanExcl_RedChiSqCutoff_{key}'
            if column in df_to_mark:
                df_to_mark.loc[scan_uids, column] = False
            column = f'DelaysExcl_RedChiSqCutoff_{key}'
            if column in df_to_mark:
                df_to_mark.loc[scan_uids, column] = ''
        if analysis_params.get('RedChiSqCutoff') is not None \
                and analysis_params.get('FitFuncID') is not None \
                and not np.isnan(analysis_params['RedChiSqCutoff']):
            red_chi_sq_cutoff = analysis_params.get('RedChiSqCutoff')
            mask_scans_above_cutoff = pd.Series(False, index=dfScansFitParams.index)
            mask_scans_above_cutoff |= (
                dfScansFitParams['ScanFits_'+analysis_params['FitFuncID']+'_RedChiSq']
                > red_chi_sq_cutoff)
            mask_scans_above_cutoff &= mask_channel_id
            scan_uids_above_cutoff = (
                dfScansFitParams[mask_scans_above_cutoff]['dfData_ScanUID'].unique())
            num_excluded = len(scan_uids_above_cutoff)

            num_fits_excluded_per_delay[key] = (
                dfScansFitParams[mask_scans_above_cutoff]
                .groupby('dfScanFits_DelayID').size().to_dict())

            logger.info(
                f'Removing {len(scan_uids_above_cutoff)} of '
                + f'{len(scan_uids):d} line scans '
                + f'for MCS channel \'{key}\' '
                + 'because at least one '
                + 'line scan delay fit in each line scan'
                + f' (total {np.sum(mask_scans_above_cutoff):d})'
                + f' has a red. χ^2 above {red_chi_sq_cutoff:.0f}'
                + '\nDelay ID: number of fits exluded:\n'
                + ', '.join([f'\'{delay_id}\': {num:d}'
                             for delay_id, num
                             in num_fits_excluded_per_delay[key].items()])
                )

            if df_to_mark is not None:
                column = f'ScanExcl_RedChiSqCutoff_{key}'
                if column in df_to_mark:
                    df_to_mark.loc[scan_uids_above_cutoff, column] = True
                column = f'DelaysExcl_RedChiSqCutoff_{key}'
                if column in df_to_mark:
                    None
                    df_to_mark.loc[scan_uids_above_cutoff, column] = (
                        dfScansFitParams[mask_scans_above_cutoff]
                        .groupby('dfData_ScanUID')['dfScanFits_DelayID'].unique())

            scan_uids_leq_cutoffs[key] = (
                scan_uids[~np.in1d(scan_uids, scan_uids_above_cutoff)])

            filter_strs[key] = (
                f'{num_excluded:d}' + r' scans w/ $\chi^2_\mathrm{red}$ > '
                + f'{red_chi_sq_cutoff} excl.'
                )
        else:
            scan_uids_leq_cutoffs[key] = scan_uids
            num_excluded = 0
            filter_strs[key] = 'no scans excl.'

        if df_to_mask is not None:
            cutoff_masks[key] = pd.Series(False, index=df_to_mask.index)
            cutoff_masks[key].loc[scan_uids_leq_cutoffs[key]] = True
        else:
            cutoff_masks[key] = None

    return (
        scan_uids_leq_cutoffs, num_fits_excluded_per_delay,
        filter_strs, cutoff_masks, df_to_mark)

def filter_line_scans_valid_num_points(dfScansFitParams, analysis_params,
                                       df_to_mask=None, df_to_mark=None):
    """
    Filter line scans by number of valid frequency points,
    with any line scans containing less than
    `analysis_params['NMinValidPoints']` points excluded.
    """
    scan_uids = dfScansFitParams["dfData_ScanUID"].unique()
    if df_to_mark is not None:
        column = 'ScanExcl_NMinValidPoints'
        if column in df_to_mark:
            df_to_mark.loc[scan_uids, column] = False
        column = 'NPointsExcl_NMinValidPoints'
        if column in df_to_mark:
            df_to_mark.loc[scan_uids, column] = 0
    if analysis_params.get('NMinValidPoints') is not None:
        num_min_valid_points = analysis_params.get('NMinValidPoints')
        mask_scans_below_min = (
            dfScansFitParams['dfScanFits_NPoints'] < num_min_valid_points)
        scan_uids_below_min = (
            dfScansFitParams[mask_scans_below_min]['dfData_ScanUID'].unique())
        scan_uids_valid_points = (
            dfScansFitParams[~mask_scans_below_min]['dfData_ScanUID'].unique())
        num_excluded = len(scan_uids_below_min)
        logger.info(
            f'Removing {num_excluded:d} of '
            + f'{len(scan_uids):d} line scans because these scans have '
            + f'less than {num_min_valid_points:d} valid points'
            )
        filter_str = (
            f'{num_excluded:d} scans w/ < {num_min_valid_points:d} points excluded'
            )
        if df_to_mark is not None:
            column = 'ScanExcl_NMinValidPoints'
            if column in df_to_mark:
                df_to_mark.loc[scan_uids_below_min, column] = True
            column = 'NPointsExcl_NMinValidPoints'
            if column in df_to_mark:
                df_to_mark.loc[scan_uids_below_min, column] = (
                    dfScansFitParams[mask_scans_below_min]
                    .groupby('dfData_ScanUID')['dfScanFits_NPoints'].mean())
    else:
        scan_uids_valid_points = scan_uids
        num_excluded = 0
        filter_str = 'no scans excl.'

    if df_to_mask is not None:
        cutoff_mask = pd.Series(False, index=df_to_mask.index)
        cutoff_mask.loc[scan_uids_valid_points] = True
    else:
        cutoff_mask = None

    return (
        scan_uids_valid_points, num_excluded,
        filter_str, cutoff_mask, df_to_mark)

def filter_line_scans_point_cutoffs(dfAll_points, analysis_params,
                                    df_to_mask=None, df_to_mark=None):
    """Filter line scan data points by applying cutoffs on various parameters."""
    dataMask_include = pd.Series(True, index=dfAll_points.index)
    scan_uids = dfAll_points['dfData_ScanUID'].unique()
    min_cutoff_params = {
        'AFRBackreflectionMinCutoff': 'dfData_PD2SnPAFR_WinAvg',
        '2SnPPMTMinCutoff': 'dfData_PD2SnPIntStab2_WinAvg',
        }
    dataMasks_include = {}
    num_points_excluded = {}
    num_scans_excluded = {}
    for analysis_param_key, param_id in min_cutoff_params.items():
        if df_to_mark is not None:
            column = f'ScanExcl_{analysis_param_key}'
            if column in df_to_mark:
                df_to_mark.loc[scan_uids, column] = False
            column = f'NPointsExcl_{analysis_param_key}'
            if column in df_to_mark:
                df_to_mark.loc[scan_uids, column] = 0
        if analysis_params.get(analysis_param_key) is not None \
                and not np.isnan(analysis_params[analysis_param_key]):
            dataMasks_include[analysis_param_key] = (
                dfAll_points[param_id]
                >= analysis_params[analysis_param_key])
            num_points_excluded[analysis_param_key] = np.sum(
                ~dataMasks_include[analysis_param_key])
            num_scans_excluded[analysis_param_key] = len((
                dfAll_points[~dataMasks_include[analysis_param_key]]
                ['dfData_ScanUID'].unique()))
            logger.info(
                'Applying data point(s) cutoff: '
                + f'{param_id} >= {analysis_params[analysis_param_key]:.3f}, '
                + f'excluding {num_points_excluded[analysis_param_key]:d} point(s) and '
                + f'{num_scans_excluded[analysis_param_key]:d} scan(s)')
            if df_to_mark is not None:
                scan_uids_to_exclude = (
                    dfAll_points[~dataMasks_include[analysis_param_key]]
                    ['dfData_ScanUID'].unique())
                column = f'ScanExcl_{analysis_param_key}'
                if column in df_to_mark:
                    df_to_mark.loc[scan_uids_to_exclude, column] = True
                column = f'NPointsExcl_{analysis_param_key}'
                if column in df_to_mark:
                    df_to_mark.loc[scan_uids_to_exclude, column] = (
                        dfAll_points[~dataMasks_include[analysis_param_key]]
                        .groupby('dfData_ScanUID').size())

        else:
            num_points_excluded[analysis_param_key] = 0
            num_scans_excluded[analysis_param_key] = 0

    for _, mask in dataMasks_include.items():
        dataMask_include &= mask
    point_uids_to_include = (
        dfAll_points[dataMask_include].index)
    scan_uids_to_exclude = (
        dfAll_points[~dataMask_include]['dfData_ScanUID'].unique())
    scan_uids_to_include = (
        scan_uids[~np.in1d(scan_uids, scan_uids_to_exclude)])
    logger.info(
    f'Excluding {len(dfAll_points)-len(point_uids_to_include):d} point(s) '
    + f'and {len(scan_uids_to_exclude):d} scan(s) from data point cutoff(s) in total')

    if len(scan_uids_to_exclude) > 0:
        filter_str = (
            f'{len(scan_uids_to_exclude):d} scans excluded by data point cutoff(s)'
            )
    else:
        filter_str = 'no scans excl.'

    if df_to_mask is not None:
        scans_mask = pd.Series(True, index=df_to_mask.index)
        scans_mask.loc[scan_uids_to_exclude] = False
    else:
        scans_mask = None

    return (
        point_uids_to_include, scan_uids_to_include,
        num_points_excluded, num_scans_excluded, filter_str, scans_mask, df_to_mark)
