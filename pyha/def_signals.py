# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ

Define the signals (emitted photons) used in the simulation data.
"""

import numpy as np
import pandas as pd
import json
from pathlib import Path

# pyhs
import pyhs.gen
logger = pyhs.gen.get_command_line_logger()
import pyhs.greek_alphabet

# pyha
import pyha.defs

# Dict of names of spectrial series for final quantum number n (n: Name)
spectral_series_names = {
    1: 'Lyman',
    2: 'Balmer',
    3: 'Paschen',
    4: 'Brackett',
    5: 'Pfund',
    6: 'Humphreys',
    }
spectral_series_shorthand = {
    1: 'Ly',
    2: 'Ba',
    3: 'Pa',
    4: 'Br',
    5: 'Pf',
    6: 'Hu',
    }

# Dict of names of decay spherical components
decay_spherical_comp_names = {
    -1: 'sigma-',
    0: 'pi',
    1: 'sigma+',
    }

# Dict of symbols of decay spherical components
decay_spherical_comp_symbols = {
    -1: r'$\sigma^-$',
    0: r'$\pi$',
    +1: r'$\sigma^+$',
    }

# Dict of decay energies, from big model OBE derivation.
# Energies are for decay from l=1, J=1/2, F=1 to l=0, J=1/2, F=1
# Using decay from 6P state as default energy for spectral series.
# In Hz
decay_energies_hz = {
    'Lyman-alpha': 2.466060015034037e15,
    'Lyman-beta': 2.922742613075312e15,
    'Lyman-gamma': 3.08258107748582e15,
    'Lyman-delta': 3.156563262308017e15,
    'Lyman-epsilon': 3.19675103641739e15,
    'Lyman': 3.19675103641739e15,
    'Balmer-alpha': 4.5668151060044e14,
    'Balmer-beta': 6.16519975010948e14,
    'Balmer-gamma': 6.905021598331455e14,
    'Balmer-delta': 7.30689933942518e14,
    'Balmer': 7.30689933942518e14,
    'Paschen-alpha': 1.598381407632892e14,
    'Paschen-beta': 2.338203255854867e14,
    'Paschen-gamma': 2.740080996948592e14,
    'Paschen': 2.740080996948592e14,
    'Brackett-alpha': 7.39820480452087e13,
    'Brackett-beta': 1.141698221545812e14,
    'Brackett': 1.141698221545812e14,
    'Pfund-alpha': 4.01877040195152e13,
    'Pfund': 4.01877040195152e13,
}

# In eV
planck_constant_ev_hz_codata2018 = 4.135667696e-15
decay_energies_ev = {
    key: elem*planck_constant_ev_hz_codata2018 for key, elem in decay_energies_hz.items()}

### Build signals/decays for 2S-nP and mapping to simulation result files

## Init DataFrames
# Empty DataFrame containing signal sets
dfSignalSets = pd.DataFrame(
    columns=['NLegacySignalColumns', 'NLegacyPopulationColumns', 'FS'])
dfSignalSets_list = []
# Empty DataFrame containing signal metadata
dfSignals = pd.DataFrame(
    columns=['Upper_n', 'AvgUpper_n',
             'Lower_n', 'AvgLower_n',
             'AvgDecaySphericalComp', 'DecaySphericalComp',
             'Label', 'LabelShort',
             'EnergyHz', 'EnergyeV',
             ])
dfSignals_list = []
# Empty DataFrame containing mapping of signals to simulation result files
dfSignalMappings = pd.DataFrame(
    columns=['SignalSetID', 'ColumnIndex', 'Weight'])
dfSignalMappings_list = []

## Define signal sets
# Generic 2S-6P
dfSignalSets_list.append(pd.Series({
    'NLegacySignalColumns': 1,
    'FS': '2S6P',
    }, name='Lyman'))
dfSignalSets_list.append(pd.Series({
    'NLegacySignalColumns': 3,
    'FS': '2S6P',
    }, name='LymanBalmer'))
# Big model for 2S-6P
dfSignalSets_list.append(pd.Series({
    'NLegacySignalColumns': 42,
    'NLegacyPopulationColumns': 8,
    'FS': '2S6P',
    }, name='All2S6P'))
dfSignalSets_list.append(pd.Series({
    'NLegacySignalColumns': 12,
    'FS': '2S6P',
    }, name='Lyman2S6P'))
dfSignalSets_list.append(pd.Series({
    'NLegacySignalColumns': 3,
    'FS': '2S6P',
    }, name='LymanSum2S6P'))
dfSignalSets_list.append(pd.Series({
    'NLegacySignalColumns': 3,
    'FS': '2S6P',
    }, name='LymanEpsilon'))
# Big model for 2S-6P, including dc electric field, which allows the Lyman-delta decay
dfSignalSets_list.append(pd.Series({
    'FS': '2S6P',
    }, name='All2S6PDCEField'))
# 2+1 level DMS model for 2S-nP, OBE implementation, for hydrogen (H) and deuterium (D)
dfSignalSets_list.append(pd.Series({
    'NLegacySignalColumns': 4,
    'FS': '2S6P',
    }, name='OBEDMS'))
dfSignalSets_list.append(pd.Series({
    'FS': '2S4P',
    }, name='OBEDMS2S4P'))
dfSignalSets_list.append(pd.Series({
    'FS': '2S6P',
    }, name='OBEDMS2S6P'))
dfSignalSets_list.append(pd.Series({
    'FS': '2S6P',
    }, name='OBEDMSD2S6P'))
dfSignalSets_list.append(pd.Series({
    'NLegacySignalColumns': 1,
    'FS': '2S6P',
    }, name='OBEDMSLymanEpsilon'))
# 2+1 level DMS model for 2S-nP, Effective Hamiltonian + MCWFM implementation
dfSignalSets_list.append(pd.Series({
    'NLegacySignalColumns': 11,
    'FS': '2S6P',
    }, name='EffHamilMCWFMDMS'))
dfSignalSets_list.append(pd.Series({
    'NLegacySignalColumns': 10,
    'FS': '2S6P',
    }, name='EffHamilMCWFM'))
dfSignalSets_list.append(pd.Series({
    'NLegacySignalColumns': 1,
    'FS': '2S4P',
    }, name='EffHamilDMS2S4PYue'))
# Convert list of Series to DataFrame
dfSignalSets = pd.concat([dfSignalSets, pd.DataFrame(dfSignalSets_list)])
dfSignalSets.index.name = 'SignalSetID'
# Add default values
dfSignalSets['NLegacySignalColumns'] = dfSignalSets['NLegacySignalColumns'].fillna(0)
dfSignalSets['NLegacyPopulationColumns'] = dfSignalSets['NLegacyPopulationColumns'].fillna(0)

# Detection efficieny of Lyman decays for the H2S6P2019 measurement campaign.
# Assuming that detection efficiency is dominated by electron emission probability of aluminum.
# From Fig. B2 (A) of my thesis, normalized to Lyman-epsilon.
# Used to generate signal 'Lyman-H2S6P2019'.
weights_h2s6p2019 = {
    'Lyman-alpha': 0.2,
    'Lyman-beta': 0.72,
    'Lyman-gamma': 0.91,
    'Lyman-delta': 0.97,
    'Lyman-epsilon': 1.,
    }

## Define signals and signal mappings for big model 2S-6P
dfSignals_list.append(pd.Series({
    'Lower_n': -1,
    'AvgLower_n': True,
    'Upper_n': -1,
    'AvgUpper_n': True,
    'DecaySphericalComp': -2,
    'AvgDecaySphericalComp': True,
    'Label': 'All',
    'LabelShort': 'All',
    }, name='All'))
for n_max, signal_set_id, skip_nm1_lyman in zip(
        # Values of n of spectroscopy state
        [6, 6],
        # Signal set IDs
        ['All2S6P', 'All2S6PDCEField'],
        # Whether to skip the Lyman decay from the n-1 levels
        [True, False],
        ):
    signal_iid = 0
    for n_lower in range(1, n_max):
        signal_id_n_lower = spectral_series_names[n_lower]
        dfSignals_list.append(pd.Series({
            'Lower_n': n_lower,
            'Upper_n': -1,
            'AvgUpper_n': True,
            'DecaySphericalComp': -2,
            'AvgDecaySphericalComp': True,
            'Label': spectral_series_names[n_lower],
            'LabelShort': spectral_series_shorthand[n_lower],
            'EnergyHz': decay_energies_hz[signal_id_n_lower],
            'EnergyeV': decay_energies_ev[signal_id_n_lower],
            }, name=signal_id_n_lower))
        if signal_id_n_lower == 'Lyman':
            # Lyman signals weighted by detection efficieny for H2S6P2019 measurement campaign
            dfSignals_list.append(pd.Series({
                **dfSignals_list[-1],
                'Label': 'Lyman-H2S6P2019',
                'LabelShort': 'Ly-H2S6P2019',
                }, name='Lyman-H2S6P2019'))
        for i, n_upper in enumerate(range(n_lower+1, n_max+1)):
            # The Lyman decay from the n-1 levels is not allowed in the unperturbed system,
            # since only S and P levels are populated for n = n_max - 1
            if skip_nm1_lyman and n_upper == n_max-1 and n_lower == 1:
                continue
            signal_id_n_lower_upper = signal_id_n_lower+f'-{pyhs.greek_alphabet.lower_names[i]}'
            label_n_lower_upper = (
                '{}-{}'
                .format(
                    spectral_series_names[n_lower],
                    pyhs.greek_alphabet.symbols_tex[pyhs.greek_alphabet.lower_names[i]],
                    ))
            label_short_n_lower_upper = (
                '{}-{}'
                .format(
                    spectral_series_shorthand[n_lower],
                    pyhs.greek_alphabet.symbols_tex[pyhs.greek_alphabet.lower_names[i]],
                    ))
            dfSignals_list.append(pd.Series({
                'Lower_n': n_lower,
                'Upper_n': n_upper,
                'AvgUpper_n': False,
                'DecaySphericalComp': -2,
                'AvgDecaySphericalComp': True,
                'Label': label_n_lower_upper,
                'LabelShort': label_short_n_lower_upper,
                'EnergyHz': decay_energies_hz[signal_id_n_lower_upper],
                'EnergyeV': decay_energies_ev[signal_id_n_lower_upper],
                }, name=signal_id_n_lower_upper))
            for q in [-1, 0, 1]:
                signal_id_n_lower_q = signal_id_n_lower+f'-{decay_spherical_comp_names[q]}'
                label_n_lower_q = (
                    spectral_series_names[n_lower]+f' {decay_spherical_comp_symbols[q]}')
                label_short_n_lower_q = (
                    spectral_series_shorthand[n_lower]+f' {decay_spherical_comp_symbols[q]}')
                if i == 0:
                    dfSignals_list.append(pd.Series({
                        'Lower_n': n_lower,
                        'Upper_n': -1,
                        'AvgUpper_n': True,
                        'DecaySphericalComp': q,
                        'AvgDecaySphericalComp': False,
                        'Label': label_n_lower_q,
                        'LabelShort': label_short_n_lower_q,
                        }, name=signal_id_n_lower_q))
                    if signal_id_n_lower == 'Lyman':
                        # Lyman signals weighted by detection efficieny for H2S6P2019 measurement
                        # campaign
                        dfSignals_list.append(pd.Series({
                            **dfSignals_list[-1],
                            'Label': f'Lyman-H2S6P2019 {decay_spherical_comp_symbols[q]}',
                            'LabelShort': f'Ly-H2S6P2019 {decay_spherical_comp_symbols[q]}',
                            }, name=f'Lyman-H2S6P2019-{decay_spherical_comp_names[q]}'))
                signal_id = signal_id_n_lower_upper+f'-{decay_spherical_comp_names[q]}'
                label = label_n_lower_upper+f' {decay_spherical_comp_symbols[q]}'
                label_short = label_short_n_lower_upper+f' {decay_spherical_comp_symbols[q]}'
                dfSignals_list.append(pd.Series({
                    'Lower_n': n_lower,
                    'Upper_n': n_upper,
                    'AvgUpper_n': False,
                    'DecaySphericalComp': q,
                    'AvgDecaySphericalComp': False,
                    'Label': label,
                    'LabelShort': label_short,
                    'EnergyHz': decay_energies_hz[signal_id_n_lower_upper],
                    'EnergyeV': decay_energies_ev[signal_id_n_lower_upper],
                    }, name=signal_id))
                # Add signal column index
                dfSignalMappings_list.append(pd.Series({
                    'SignalSetID': signal_set_id,
                    'ColumnIndex': signal_iid,
                    }, name=signal_id_n_lower))
                if signal_id_n_lower == 'Lyman':
                    # Lyman signals weighted by detection efficieny for H2S6P2019 measurement
                    # campaign
                    dfSignalMappings_list.append(pd.Series({
                        'SignalSetID': signal_set_id,
                        'ColumnIndex': signal_iid,
                        'Weight': weights_h2s6p2019.get(signal_id_n_lower_upper, 1.),
                        }, name=f'{signal_id_n_lower}-H2S6P2019'))
                    dfSignalMappings_list.append(pd.Series({
                        'SignalSetID': signal_set_id,
                        'ColumnIndex': signal_iid,
                        'Weight': weights_h2s6p2019.get(signal_id_n_lower_upper, 1.),
                        }, name=f'Lyman-H2S6P2019-{decay_spherical_comp_names[q]}'))
                dfSignalMappings_list.append(pd.Series({
                    'SignalSetID': signal_set_id,
                    'ColumnIndex': signal_iid,
                    }, name=signal_id_n_lower_upper))
                dfSignalMappings_list.append(pd.Series({
                    'SignalSetID': signal_set_id,
                    'ColumnIndex': signal_iid,
                    }, name=signal_id_n_lower_q))
                dfSignalMappings_list.append(pd.Series({
                    'SignalSetID': signal_set_id,
                    'ColumnIndex': signal_iid,
                    }, name=signal_id))
                dfSignalMappings_list.append(pd.Series({
                    'SignalSetID': signal_set_id,
                    'ColumnIndex': signal_iid,
                    }, name='All'))
                signal_iid += 1
# Convert list of Series to DataFrame
dfSignals = pd.concat([dfSignals, pd.DataFrame(dfSignals_list)])
dfSignals.index.name = 'SignalID'
dfSignals['AvgLower_n'] = dfSignals['AvgLower_n'].fillna(False)
dfSignals['EnergyHz'] = dfSignals['EnergyHz'].fillna(np.nan)
dfSignals['EnergyeV'] = dfSignals['EnergyeV'].fillna(np.nan)
# Drop duplicate signals
dfSignals = dfSignals.drop_duplicates()
dfSignalMappings = pd.concat([dfSignalMappings, pd.DataFrame(dfSignalMappings_list)])
dfSignalMappings.index.name = 'SignalID'

## Generic 2S-6P
# Lyman and Balmer
signal_set_id = 'LymanBalmer'
dfSignalMappings_list = []
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 0,
    }, name='All'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 1,
    }, name='Lyman-epsilon'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 1,
    }, name='Lyman'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 2,
    }, name='Balmer-delta'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 2,
    }, name='Balmer'))
# Lyman only
signal_set_id = 'Lyman'
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 0,
    }, name='Lyman-epsilon'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 0,
    }, name='Lyman'))
dfSignalMappings = pd.concat([dfSignalMappings, pd.DataFrame(dfSignalMappings_list)])
dfSignalMappings.index.name = 'SignalID'

## Legacy 2S-6P big model grid interpolation, using sum of all Lyman signals
signal_set_id = 'LymanSum2S6P'
dfSignalMappings_list = []
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 0,
    }, name='Lyman-sigma-'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 1,
    }, name='Lyman-pi'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 2,
    }, name='Lyman-sigma+'))
for column_index in range(3):
    dfSignalMappings_list.append(pd.Series({
        'SignalSetID': signal_set_id,
        'ColumnIndex': column_index,
        }, name='Lyman'))
dfSignalMappings = pd.concat([dfSignalMappings, pd.DataFrame(dfSignalMappings_list)])
dfSignalMappings.index.name = 'SignalID'

## 2S-6P big model grid interpolation, using only Lyman-epsilon signals
## Added 12.12.2021
signal_set_id = 'LymanEpsilon'
# Select only Lyman-epsilon-pi/sigma decays from signal set 'All2S6P'
mask = dfSignalMappings['SignalSetID'] == 'All2S6P'
signal_ids_lyman = [
    elem for elem in dfSignalMappings[mask].index if elem.startswith('Lyman-epsilon')]
dfSignalMappings_append = dfSignalMappings[mask].loc[np.unique(signal_ids_lyman)].copy()
dfSignalMappings_append['SignalSetID'] = signal_set_id
dfSignalMappings_append['ColumnIndex'] -= dfSignalMappings_append['ColumnIndex'].min()
dfSignalMappings_append.sort_values(by='ColumnIndex', inplace=True)
dfSignalMappings = pd.concat([dfSignalMappings, dfSignalMappings_append])

## Big model grid interpolation for D2S-6P
## Added 08.12.2021
signal_set_id = 'Lyman2S6P'
# Select only Lyman decays from signal set 'All2S6P'
mask = dfSignalMappings['SignalSetID'] == 'All2S6P'
signal_ids_lyman = [elem for elem in dfSignalMappings[mask].index if elem.startswith('Lyman')]
dfSignalMappings_append = dfSignalMappings[mask].loc[np.unique(signal_ids_lyman)].copy()
dfSignalMappings_append['SignalSetID'] = signal_set_id
dfSignalMappings_append.sort_values(by='ColumnIndex', inplace=True)
dfSignalMappings = pd.concat([dfSignalMappings, dfSignalMappings_append])

## 2+1 level DMS model, OBE implementation, for hydrogen (H) and deuterium (D)
# Lyman and Balmer, H 2S-6P, but without populations
signal_set_id = 'OBEDMS'
dfSignalMappings_list = []
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 0,
    }, name='Lyman-epsilon'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 0,
    }, name='Lyman'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 1,
    }, name='Balmer-delta'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 1,
    }, name='Balmer'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 2,
    }, name='LymanNoBD'))
# Lyman signal after 1 backdecay has occured (maximum possible for this model)
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 3,
    }, name='LymanBDCorrOnly'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 3,
    }, name='LymanBD1'))
dfSignalMappings = pd.concat([dfSignalMappings, pd.DataFrame(dfSignalMappings_list)])
dfSignalMappings.index.name = 'SignalID'

# Lyman and Balmer, H 2S-4P
signal_set_id = 'OBEDMS2S4P'
dfSignalMappings_list = []
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 0,
    }, name='Lyman-gamma'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 0,
    }, name='Lyman'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 1,
    }, name='Balmer-beta'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 1,
    }, name='Balmer'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 2,
    }, name='LymanNoBD'))
# Lyman signal after 1 backdecay has occured (maximum possible for this model)
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 3,
    }, name='LymanBDCorrOnly'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 3,
    }, name='LymanBD1'))
dfSignalMappings = pd.concat([dfSignalMappings, pd.DataFrame(dfSignalMappings_list)])
dfSignalMappings.index.name = 'SignalID'

# Lyman and Balmer, H 2S-6P
signal_set_id = 'OBEDMS2S6P'
dfSignalMappings_list = []
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 0,
    }, name='Lyman-epsilon'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 0,
    }, name='Lyman'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 1,
    }, name='Balmer-delta'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 1,
    }, name='Balmer'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 2,
    }, name='LymanNoBD'))
# Lyman signal after 1 backdecay has occured (maximum possible for this model)
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 3,
    }, name='LymanBDCorrOnly'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 3,
    }, name='LymanBD1'))
dfSignalMappings = pd.concat([dfSignalMappings, pd.DataFrame(dfSignalMappings_list)])
dfSignalMappings.index.name = 'SignalID'

# Lyman and Balmer, D 2S-6P with back decay with pi or sigma polarization
signal_set_id = 'OBEDMSD2S6P'
dfSignalMappings_list = []
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 0,
    }, name='Lyman-epsilon'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 0,
    }, name='Lyman'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 1,
    }, name='Balmer-delta'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 1,
    }, name='Balmer'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 2,
    }, name='LymanNoBD'))
# Lyman signal after 1 back decay with either pi or sigma polarization has occured
# (maximum possible for this model)
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 3,
    }, name='LymanBDCorrOnly'))
# Lyman signal after 1 back decay with pi polarization has occured
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 4,
    }, name='LymanBD1pi'))
# Lyman signal after 1 back decay with sigma polarization has occured
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 5,
    }, name='LymanBD1sigma'))
dfSignalMappings = pd.concat([dfSignalMappings, pd.DataFrame(dfSignalMappings_list)])
dfSignalMappings.index.name = 'SignalID'

## 2+1 level DMS model, OBE implementation, Lyman-epsilon only
signal_set_id = 'OBEDMSLymanEpsilon'
dfSignalMappings_list = []
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 0,
    }, name='Lyman-epsilon'))
dfSignalMappings = pd.concat([dfSignalMappings, pd.DataFrame(dfSignalMappings_list)])
dfSignalMappings.index.name = 'SignalID'

## 2+1 level DMS model, Effective Hamiltonian + MCWFM implementation
signal_set_id = 'EffHamilMCWFM'
dfSignalMappings_list = []
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 0,
    }, name='Lyman-epsilon'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 0,
    }, name='Lyman'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 1,
    }, name='Balmer-delta'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 1,
    }, name='Balmer'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 2,
    }, name='LymanNoBD'))
# Lyman signal after at least 1 backdecay has occured
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 3,
    }, name='LymanBDCorrOnly'))
# Lyman signal after exactly n backdecays have occured
for num_backdecays in range(1, 7):
    dfSignalMappings_list.append(pd.Series({
        'SignalSetID': signal_set_id,
        'ColumnIndex': 3+num_backdecays,
        }, name=f'LymanBD{num_backdecays}'))
dfSignalMappings = pd.concat([dfSignalMappings, pd.DataFrame(dfSignalMappings_list)])
dfSignalMappings.index.name = 'SignalID'

## 2+1 level DMS model, Effective Hamiltonian + MCWFM implementation
# Added 03.01.2020, includes MC Lyman signal for 0 backdecay cycles
signal_set_id = 'EffHamilMCWFMDMS'
dfSignalMappings_list = []
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 0,
    }, name='Lyman-epsilon'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 0,
    }, name='Lyman'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 1,
    }, name='Balmer-delta'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 1,
    }, name='Balmer'))
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 2,
    }, name='LymanNoBD'))
# Lyman signal after at least 1 backdecay has occured
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 3,
    }, name='LymanBDCorrOnly'))
# Lyman signal after exactly n backdecays have occured
for num_backdecays in range(0, 7):
    dfSignalMappings_list.append(pd.Series({
        'SignalSetID': signal_set_id,
        'ColumnIndex': 4+num_backdecays,
        }, name=f'LymanBD{num_backdecays}'))
dfSignalMappings = pd.concat([dfSignalMappings, pd.DataFrame(dfSignalMappings_list)])
dfSignalMappings.index.name = 'SignalID'

## 2+1 level DMS model, Effective Hamiltonian without backdecay
# 2S-4P, with decay rate set to lifetime as done in LFS theory paper
# Added 06.02.2020
signal_set_id = 'EffHamilDMS2S4PYue'
dfSignalMappings_list = []
dfSignalMappings_list.append(pd.Series({
    'SignalSetID': signal_set_id,
    'ColumnIndex': 0,
    }, name='All'))
dfSignalMappings = pd.concat([dfSignalMappings, pd.DataFrame(dfSignalMappings_list)])
dfSignalMappings.index.name = 'SignalID'

## Cast data format
columns = ['NLegacySignalColumns', 'NLegacyPopulationColumns']
dfSignalSets[columns] = dfSignalSets[columns].astype(int)
columns = ['Label', 'LabelShort']
dfSignals[columns] = dfSignals[columns].astype(str)
columns = ['AvgUpper_n', 'AvgLower_n', 'AvgDecaySphericalComp']
dfSignals[columns] = dfSignals[columns].astype(bool)
columns = ['Upper_n', 'Lower_n', 'DecaySphericalComp']
dfSignals[columns] = dfSignals[columns].astype(int)
dfSignalMappings['Weight'] = dfSignalMappings['Weight'].fillna(1)
columns = ['ColumnIndex']
dfSignalMappings[columns] = dfSignalMappings[columns].astype(int)
columns = ['Weight']
dfSignalMappings[columns] = dfSignalMappings[columns].astype(float)


def get_signals(signal_set_id):
    """Get list of signal IDs for a given signal set `signal_set_id` (string)."""
    mask_signal_mappings = (
        (dfSignalMappings['SignalSetID'] == signal_set_id)
        )
    return list(dfSignalMappings[mask_signal_mappings].index.drop_duplicates())

def get_signal_columns(signal_set_id, signal_id):
    """
    Get array of column number(s) in raw data for signal `signal_id` (string)
    from signal set `signal_set_id` (string).
    """
    mask_signal_mappings = (
        (dfSignalMappings['SignalSetID'] == signal_set_id)
        )
    if not signal_id in dfSignalMappings[mask_signal_mappings].index:
        msg = f'Unknown signal ID \'{signal_id}\''
        logger.error(msg)
        raise pyha.defs.PyhaError(msg)
    columns = np.array(
        [dfSignalMappings[mask_signal_mappings].loc[signal_id]['ColumnIndex']]).flatten()
    return columns

def get_signal_columns_and_weights(signal_set_id, signal_id):
    """
    Get array `columns` of column number(s) in raw data and the corresponding weights array
    `weights` for signal `signal_id` (string) from signal set `signal_set_id` (string).
    """
    mask_signal_mappings = (
        (dfSignalMappings['SignalSetID'] == signal_set_id)
        )
    if not signal_id in dfSignalMappings[mask_signal_mappings].index:
        msg = f'Unknown signal ID \'{signal_id}\''
        logger.error(msg)
        raise pyha.defs.PyhaError(msg)
    columns = np.array(
        [dfSignalMappings[mask_signal_mappings].loc[signal_id]['ColumnIndex']]).flatten()
    weights = np.array(
        [dfSignalMappings[mask_signal_mappings].loc[signal_id]['Weight']]).flatten()
    return columns, weights

def get_signal_delay_columns(signal_set_id, signal_id, delay_iid):
    """
    Get array of column number(s) of legacy CSV data for signal `signal_id` (string),
    signal set `signal_set_id` (string), and (integer) delay index `delay_iid`.
    In legacy CSV data, the data is stored in an `N_delta` x `M` array, where `N_delta` is the
    number of detunings and `M = N_s * N_dlys` is the product of the number of signals, `N_s`,
    and delays, `N_d`.
    The signal with column index `i` and the j-th delay (i, j: zero-indexed) is stored in column
    `i+j*N_s`.
    In non-legacy data saved to NPZ archives the data is stored in a `N_delta` x `N_s` x `N_dlys`
    array instead and the function `get_signal_columns` should be used instead.
    """
    columns = get_signal_columns(signal_set_id, signal_id)
    return columns + delay_iid * dfSignalSets.loc[signal_set_id]['NLegacySignalColumns']

def get_signal_delay_columns_and_weights(signal_set_id, signal_id, delay_iid):
    """
    Get array of column number(s) and the corresponding weights array `weights`
    for legacy CSV data for signal `signal_id` (string),
    signal set `signal_set_id` (string), and (integer) delay index `delay_iid`.
    In legacy CSV data, the data is stored in an `N_delta` x `M` array, where `N_delta` is the
    number of detunings and `M = N_s * N_dlys` is the product of the number of signals, `N_s`,
    and delays, `N_d`.
    The signal with column index `i` and the j-th delay (i, j: zero-indexed) is stored in column
    `i+j*N_s`.
    In non-legacy data saved to NPZ archives the data is stored in a `N_delta` x `N_s` x `N_dlys`
    array instead and the function `get_signal_columns_and_weights` should be used instead.
    """
    columns, weights = get_signal_columns_and_weights(signal_set_id, signal_id)
    return columns + delay_iid * dfSignalSets.loc[signal_set_id]['NLegacySignalColumns'], weights

def get_non_composite_signals(signal_set_id):
    """
    Get non-composite signals, that is signals that correspond to a single column, for signal
    set `signal_set_id` (string).
    The sorted signal IDs `signal_ids_sorted` and the corresponding column indices
    `columns` are returned.
    """
    mask_signal_mappings = dfSignalMappings['SignalSetID'] == signal_set_id
    signal_ids_mask = (
        dfSignalMappings[mask_signal_mappings].reset_index().groupby(by='SignalID').size() == 1)
    signal_ids = signal_ids_mask[signal_ids_mask].index.values
    # Sort signals by ColumnIndex
    signal_ids_sorted = (
        dfSignalMappings[mask_signal_mappings]
        .loc[signal_ids].sort_values(by='ColumnIndex').index.values)
    columns = (
        dfSignalMappings[mask_signal_mappings]
        .loc[signal_ids_sorted]['ColumnIndex'].values)
    return signal_ids_sorted, columns

def to_json(dir_json=None):
    """
    Output DataFrames `dfSignalSets`, `dfSignals`, and `dfSignalMappings` as correspondingly named
    JSON files to directory `dir_json` (default is None, in which case the directory
    `static/signals` is used).
    The DataFrames are first converted to dicts using `pd.DataFrame.to_dict`, using
    `option='index'` for `dfSignalSets` and `dfSignals`, but `option='dict'` for `dfSignalMappings`.
    """
    dir_json = 'static/signals' if dir_json is None else dir_json
    # Write to JSON file
    for df, name, orient in zip(
            [dfSignalSets, dfSignals, dfSignalMappings],
            ['dfSignalSets', 'dfSignals', 'dfSignalMappings'],
            ['index', 'index', 'split']):
        df_dict = df.to_dict(orient=orient)
        filepath = Path(dir_json, f'{name}.json')
        with open(filepath, 'w') as f:
            json.dump(df_dict, f, indent=4)
        logger.info(
            'Wrote DataFrame \'%s\' to file \'%s\'', name, filepath)
