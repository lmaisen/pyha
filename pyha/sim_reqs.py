# -*- coding: utf-8 -*-
"""
Created on Tue Mar 30 12:08:37 2021

@author: Lothar Maisenbacher/MPQ

Definition of class `SimReqs`, which manages simulation requirements.
"""

import numpy as np

# pyhs
import pyhs.files
import pyhs.gen

# pyha
import pyha.generic_container_class
import pyha.defs
import pyha.trajs

logger = pyhs.gen.get_command_line_logger()

class SimReqs(pyha.generic_container_class.GenericContainerClass):
    """
    Class managing simulation requirements.
    """

    SIM_REQS_FORMAT = {
        'ColumnsGen': [
            ],
        'ColumnsFloat': [
            'AlphaOffset', 'ThetaL', '2SnPLaserPower',
            'AnalysisParams_ScaleCorrectionLFS',
            ],
        'ColumnsStr': [
            'SimReqUID', 'DataGroupID',
            'dfTrajParams_TrajUID', 'TrajParams_CommonJSONFile', 'DetEffUID',
            'dfSimGridInters_BM_GridUID', 'dfSimGridInters_BM_DenseGridUID',
            'dfSimGridInters_BM_GridInterUID',
            'dfSimGridInters_LFS_GridUID', 'dfSimGridInters_LFS_DenseGridUID',
            'dfSimGridInters_LFS_GridInterUID',
            'AddFitFuncIDs', 'ForceAnalysisFitFuncID',
            ],
        'ColumnsBool': [
            'FitsPerformed_BM', 'FitsPerformed_LFS',
            ],
        'ColumnsInt': [
            ],
        'ColumnsDate': [
            ],
        'IndexName': 'SimReqUID',
        'SetNumericalIndex': False,
        'TagColumn': '',
        'LoadFromFiles': True,
        'FilebasePattern': '*_req',
        }

    DF_FORMATS = {
        'dfSimReqs': SIM_REQS_FORMAT,
        }

    def __init__(self):
        """
        Initialize simulation requirements class.
        """
        super().__init__()

        # Init class managing trajectory set metadata
        self.Trajs = pyha.trajs.Trajs()

    @property
    def dfSimReqs(self):
        """Get pandas DataFrame `dfSimReqs`."""
        return self.dfs['dfSimReqs']

    @dfSimReqs.setter
    def dfSimReqs(self, dfSimReqs):
        """Set pandas DataFrame `dfSimReqs`."""
        self.dfs['dfSimReqs'] = dfSimReqs

    def load_sim_requirements(self, filepath):
        """
        Load simulation requirements from Excel file at path `filepath` (str of pathlib.Path)
        into DataFrame `dfSimReqs`.
        """
        req_format = self.DF_FORMATS['dfSimReqs']

        # Format for columns holding additional trajectory set metadata
        traj_df_id = 'dfTrajParams'
        traj_format = self.Trajs.df_formats[traj_df_id]

        # Date and string columns expected in file
        date_columns = (
            req_format['ColumnsDate']
            + [f'{traj_df_id}_{column}' for column in traj_format['ColumnsDate']]
            )
        date_columns = np.unique(date_columns)
        str_columns = (
            req_format['ColumnsStr']
            + [f'{traj_df_id}_{column}' for column in traj_format['ColumnsStr']]
            )
        str_columns = np.unique(str_columns)

        # Load simulation requirements DataFrame
        dfSimReqs = pyhs.files.load_table_file(
            filepath,
            date_columns=date_columns,
            str_columns=str_columns,
            index_col=req_format['IndexName'],
            )
        logger.info(
            'Loaded %d simulation requirement(s) from file \'%s\'',
            len(dfSimReqs), filepath)

        # Add columns of trajectory set parameters found in `dfSimReqs` to data format
        for key in self.DTYPE_COLUMN_KEYS:
            traj_columns_avail = [
                f'{traj_df_id}_{column}' for column in traj_format[key]
                if f'{traj_df_id}_{column}' in dfSimReqs.columns]
            self.df_formats['dfSimReqs'][key] = list(np.unique(
                self.DF_FORMATS['dfSimReqs'][key]
                + traj_columns_avail
                ))

        # Cast to data format
        dfSimReqs = self.cast_containers_to_data_format(
            dfSimReqs, 'dfSimReqs')

        self.dfs['dfSimReqs'] = dfSimReqs
