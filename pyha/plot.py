# -*- coding: utf-8 -*-
"""
Created on Sat Jul  4 14:45:05 2020

@author: Lothar Maisenbacher/MPQ
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import datetime
import matplotlib.dates
import itertools
import scipy.stats
import textwrap
from pathlib import Path
from matplotlib.container import ErrorbarContainer

# pyhs
import pyhs.gen
logger = pyhs.gen.get_command_line_logger()
import pyhs.fit
import pyhs.statfunc
import pyhs.units

# pyha
import pyha.data_ext
import pyha.util.plot

import pyha.plts as plts
plts.set_params()

myFit = pyhs.fit.Fit()

def pad_ax_lim(lim, lim_padding):
    """
    Add padding list `lim_padding` to axes limit list `lim`.
    """
    if lim_padding is not None:
        lim_padding = np.array([lim_padding]).flatten()
        if len(lim_padding) == 1:
            lim_padding_ = np.repeat(lim_padding, 2)
        else:
            lim_padding_ = lim_padding
        lim = np.array([
            np.min(lim)-lim_padding_[0]*np.ptp(lim),
            np.max(lim)+lim_padding_[1]*np.ptp(lim)])
    return lim

class PlotGen:
    """
    pyha plot generator, used to ensure common style of plots.
    """
    PLOT_PARAMS_DEFAULT = {
        'SaveDir': plts.out_folder,
        'SaveDPI': 1200,
        }

    def __init__(self, plot_params=None, data_sets=None,
                 create_fig=True, add_axes=True,
                 fig_kwargs=None, DataClass=None):

        self.plot_params = self.PLOT_PARAMS_DEFAULT
        if plot_params is not None:
            self.plot_params = {
                **self.plot_params,
                **plot_params
                }
        self.data_sets = None
        if data_sets is not None:
            self.data_sets = data_sets
        self.fig = None
        if create_fig:
            if fig_kwargs is None:
                fig_kwargs = {}
            self.create_figure(**fig_kwargs)
        self.axs = None
        if add_axes:
            self.add_axes()
        self.DataClass = DataClass
        self.filepath = None

    def create_figure(self, contrained_layout=True, **kwargs):
        """
        Create new figure if not open or return figure handle.
        """
        fig_size = self.plot_params.get('FigSize', [plts.pagewidth*0.8, 3.5])
        fig = plt.figure(
            self.plot_params.get('FigName', 'default'),
            figsize=fig_size, constrained_layout=contrained_layout,
            **kwargs)
        fig.set_size_inches(fig_size)
        self.fig = fig

    def add_axes(self, data_sets=None, clear_fig=True):
        """
        Add axes to figure, with axes defined in plot_params.
        """
        fig = self.fig
        plot_params = self.plot_params

        if clear_fig:
            fig.clf()
        if data_sets is not None:
            self.data_sets = data_sets
        if self.data_sets is not None and len(self.data_sets) > 0:
            num_axes = -1
            for _, data_set in self.data_sets.items():
                if data_set.get('Axes') is not None:
                    num_axes = np.max([num_axes, data_set.get('Axes')])
            num_axes = 1 if num_axes == -1 else num_axes+1
        else:
            num_axes = plot_params.get('NumAxes', 1)
        num_rows = plot_params.get('NumRows', num_axes)
        make_space_for_y_label = False
        for i_axis in range(num_axes):
            # Make sure plot parameters always include axis parameters
            if f'AxParams{i_axis}' not in plot_params.keys():
                self.plot_params[f'AxParams{i_axis}'] = {}
            ax_params = plot_params[f'AxParams{i_axis}']
            make_space_for_y_label |= ax_params.get('YLabelOutside', False)
        num_cols = int(np.ceil((num_axes)/num_rows))
        width_ratios = plot_params.get('WidthRatios', [1/num_cols]*num_cols)
        width_ratios = list(np.array(width_ratios)/np.sum(width_ratios))
        if make_space_for_y_label or plot_params.get('YLabelOutside', False):
            y_label_width_scaling = plot_params.get(
                'YLabelWidthScaling', 1.5 if num_cols == 1 else 1)
            num_cols_ = num_cols+1
            num_first_col = 1
            width_ratios = (
                [fig.get_size_inches()[0]/(150/(y_label_width_scaling))]
                + width_ratios)
        else:
            num_cols_ = num_cols
            num_first_col = 0
        gs_main = gridspec.GridSpec(
            nrows=1, ncols=num_cols_, figure=fig,
            width_ratios=width_ratios[:num_cols_],
            height_ratios=[1],
            )
        axs = np.zeros(num_axes, dtype=object)
        i_axis = 0
        for i_gs_sub in range(num_first_col, num_cols_):
            if plot_params.get('NumRowsPerColumns') is not None:
                num_rows_ = plot_params['NumRowsPerColumns'][i_gs_sub-num_first_col]
            else:
                num_rows_ = num_rows
            height_ratios = np.ones(num_rows)
            for i_subaxis in range(num_rows):
                if i_axis+i_subaxis >= num_axes:
                    continue
                ax_params = plot_params[f'AxParams{i_axis+i_subaxis:d}']
                if ax_params.get('HeightRatio') is not None:
                    height_ratios[i_subaxis] = ax_params['HeightRatio']
                else:
                    height_ratios[i_subaxis] = 1
            gs = gridspec.GridSpecFromSubplotSpec(
                nrows=num_rows, ncols=1,
                subplot_spec=gs_main[i_gs_sub],
                width_ratios=[1],
                height_ratios=height_ratios,
                )
            for i_subaxis in range(num_rows_):
                if i_axis < num_axes:
                    ax_params = plot_params[f'AxParams{i_axis:d}']
                    subplot_params = {}
                    sharex_i_axis = (
                        0 if ax_params.get('ShareXWith') is None else ax_params['ShareXWith'])
                    if (plot_params.get('ShareX', False) \
                            or ax_params.get('ShareXWith') is not None) \
                            and sharex_i_axis < i_axis:
                        subplot_params['sharex'] = axs[sharex_i_axis]
                    sharey_i_axis = (
                        0 if ax_params.get('ShareYWith') is None else ax_params['ShareYWith'])
                    if (plot_params.get('ShareY', False) \
                            or ax_params.get('ShareYWith') is not None) \
                            and sharey_i_axis < i_axis:
                        subplot_params['sharey'] = axs[sharey_i_axis]
                    axs[i_axis] = fig.add_subplot(
                        gs[i_subaxis], projection=ax_params.get('Projection'),
                        **subplot_params)
                    i_axis += 1

        self.axs = axs

    def set_axs_params(self, **kwargs):
        """
        Format axes as defined in plot parameters.
        """
        for i_ax, ax in enumerate(self.axs):
            self.set_ax_params(i_ax, **kwargs)
        self.set_axs_pos()

    def set_axs_pos(self):
        """
        Set position and size of axes equal to other axes.
        Needs to be called after all other axes adjustments have been done.
        """
        redrawn = False
        for i_ax, ax in enumerate(self.axs):
            ax_params = self.plot_params.get(f'AxParams{i_ax:d}', {})
            if ax_params.get('SetHeightEqualToAx') is not None:
                if not redrawn:
                    self.fig.canvas.draw()
                    redrawn = True
                ref_pos = (
                    self.axs[ax_params['SetHeightEqualToAx']].get_position())
                pos = self.axs[i_ax].get_position()
                self.axs[i_ax].set_position(
                    [pos.bounds[0], ref_pos.bounds[1],
                     pos.bounds[2], ref_pos.bounds[3]])

    def update_ax_params(self, i_ax, ax_params):
        """
        Update axis parameters of axis with ID `i_ax` with values of dict
        `ax_params`.
        """
        self.plot_params[f'AxParams{i_ax}'] = {
            **self.plot_params[f'AxParams{i_ax}'],
            **ax_params
            }

    def update_leg_params(self, i_ax, leg_params):
        """
        Update legend parameters of axis with ID `i_ax` with values of dict
        `leg_params`.
        """
        self.plot_params[f'AxParams{i_ax}']['LegParams'] = {
            **self.plot_params[f'AxParams{i_ax}'].get('LegParams', {}),
            **leg_params
            }

    @staticmethod
    def select_leg_handles_for_ax(ax, leg_handles):
        """
        Select the legend handles from dict `leg_handles` that contain children of Matplotlib
        axes object `ax` and return the keys of the selected entries of `leg_handles`.
        """
        keys_leg = []
        for key, elem in leg_handles.items():
            if isinstance(elem, ErrorbarContainer) and \
                    np.any([subelem in ax.get_children() for subelem in elem]):
                keys_leg.append(key)
            elif elem in ax.get_children():
                keys_leg.append(key)
        return np.array(keys_leg)

    def set_ax_params(self, i_ax, ax_params_default=None, **kwargs):
        """
        Format axis with ID `i_ax`, as defined in dict `self.plot_params`.
        Additional parameters for this axis are searched for in `self.plot_params`
        under the key `AxParams{i_ax}`.
        """
        plot_params = self.plot_params
        fig = self.fig
        ax = self.axs[i_ax]

        plot_params['LegHandles'] = {
            **plot_params.get('LegHandles', {}),
            **kwargs.get('leg_handles', {}),
            }
        plot_params['LegLabels'] = {
            **plot_params.get('LegLabels', {}),
            **kwargs.get('leg_labels', {}),
            }
        plot_params['LegSort'] = {
            **plot_params.get('LegSort', {}),
            **kwargs.get('leg_sort', {}),
            }

        ax_params_predefined = {
            'XScaleType': plot_params.get('XScaleType', 'linear'),
            'YScaleType': plot_params.get('YScaleType', 'linear'),
            'XLim': plot_params.get('XLim'),
            'XLimPadding': plot_params.get('XLimPadding'),
            'ApplyXLimPadding': plot_params.get('ApplyXLimPadding', True),
            'YLim': plot_params.get('YLim'),
            'YLimPadding': plot_params.get('YLimPadding'),
            'ApplyYLimPadding': plot_params.get('ApplyYLimPadding', True),
            'ShowLegend': plot_params.get('ShowLegend', True),
            'XLabelOutside': plot_params.get('XLabelOutside', False),
            'YLabelOutside': plot_params.get('YLabelOutside', False),
            'LogTicks': plot_params.get('LogTicks'),
            'ZOrderLegend': plot_params.get('ZOrderLegend', 4),
            'Rasterize': plot_params.get('Rasterize', False),
            'AspectRatio': plot_params.get('AspectRatio'),
            'EqualLim': plot_params.get('EqualLim', False),
            'SubplotLabelX': plot_params.get('SubplotLabelX', 0.015),
            'SubplotLabelY': plot_params.get('SubplotLabelY', 0.02),
            'SubplotLabelHA': plot_params.get('SubplotLabelHA','left'),
            'SubplotLabelVA': plot_params.get('SubplotLabelVA','top'),
            'SubplotLabelParams': plot_params.get('SubplotLabelParams', {}),
            'ZOrderSubplotLabel': plot_params.get('ZOrderSubplotLabel', 5),
            'XTickLabelsRotation': plot_params.get('XTickLabelsRotation', None),
            'LabelWrapWidth': plot_params.get('LabelWrapWidth', None),
            }
        if ax_params_default is None:
            ax_params_default = {}
        ax_params_i_ax = plot_params.get(f'AxParams{i_ax:d}', {})
        ax_params = {
            **ax_params_predefined,
            **ax_params_default,
            **ax_params_i_ax,
            }
        # Set y-scale type
        ax.set_yscale(ax_params['YScaleType'])
        # Set axes limits
        def set_ax_lim(ax_identifier, get_func, set_func):

            lim = None
            padding = ax_params[f'{ax_identifier}LimPadding']
            if not ax_params[f'Apply{ax_identifier}LimPadding']:
                padding = None
            lim_curr = np.array(get_func())
            if ax_params[f'{ax_identifier}Lim'] is not None:
                lim = np.array(ax_params[f'{ax_identifier}Lim'])
                mask_nan = pd.isnull(lim)
                lim[mask_nan] = lim_curr[mask_nan]
            elif padding is not None:
                lim = lim_curr
            if lim is not None:
                if padding is not None:
                    lim = pad_ax_lim(
                        lim, ax_params[f'{ax_identifier}LimPadding'])
                set_func(lim)
        set_ax_lim('X', ax.get_xlim, ax.set_xlim)
        set_ax_lim('Y', ax.get_ylim, ax.set_ylim)
        # Add legend
        if ax_params['ShowLegend']:
            leg_handles = plot_params.get('LegHandles', {})
            leg_labels = plot_params.get('LegLabels', {})
            leg_sort = plot_params.get('LegSort', {})
            keys_leg = self.select_leg_handles_for_ax(ax, leg_handles)
            sort_keys = [leg_sort.get(key, 0) for key in keys_leg]
            keys_leg = keys_leg[np.argsort(sort_keys)]
            if i_ax is not None:
                leg_params_i_ax = ax_params.get('LegParams', {})
            else:
                leg_params_i_ax = {}
            leg_params = {
                **plts.get_leg_params(**{
                    **plot_params.get('LegParams', {}),
                    **leg_params_i_ax,
                    })}
            h_leg = None
            if len([leg_handles[key] for key in keys_leg]) > 0:
                h_leg = ax.legend(
                    [leg_handles[key] for key in keys_leg],
                    [leg_labels[key] for key in keys_leg],
                    **leg_params
                    )
            elif len(ax.get_legend_handles_labels()[0]) > 0:
                h_leg = ax.legend(
                    **leg_params
                    )
            if h_leg is not None:
                if leg_params.get('title') is not None:
                    h_leg.set_title(
                        leg_params['title'],
                        prop=leg_params.get('prop'),
                        )
                    if leg_params.get('fontsize') is not None:
                        h_leg.get_title().set_fontsize(leg_params['fontsize'])
                if ax_params.get('ZOrderLegend') is not None:
                    h_leg.set_zorder(ax_params.get('ZOrderLegend'))
        # Adjust ticks of log scale
        if ax_params['LogTicks'] is not None:
            log_tick = ax_params['LogTicks']
            ax.set_yticks(np.logspace(
                log_tick[0], log_tick[1],
                log_tick[1]-log_tick[0]+1))
            locmin = plt.LogLocator(
                base=10.0, subs=(np.linspace(0.1, 0.9, 9)),
                numticks=log_tick[1]-log_tick[0]+2)
            ax.yaxis.set_minor_locator(locmin)
            ax.yaxis.set_minor_formatter(plt.NullFormatter())
        # Set x-ticks
        if ax_params.get('XTicks') is not None:
            ax.set_xticks(ax_params['XTicks'])
        if ax_params.get('XTickLabels') is not None:
            ax.set_xticklabels(ax_params['XTickLabels'])
        if ax_params.get('XScaleType') == 'linear' and ax_params.get('LinXTicks') is not None:
            ax.set_xticks(ax_params.get('LinXTicks'))
        if ax_params.get('RemoveXTickLabels', False):
            plt.setp(ax.get_xticklabels(), visible=False)
        if ax_params.get('RemoveXTicks', False):
            ax.set_xticks([])
        # Set y-ticks
        if ax_params.get('YTicks') is not None:
            ax.set_yticks(ax_params['YTicks'])
        if ax_params.get('YScaleType') == 'linear' and ax_params.get('LinYTicks') is not None:
            ax.set_yticks(ax_params.get('LinYTicks'))
        if ax_params.get('RemoveYTickLabels', False):
            plt.setp(ax.get_yticklabels(), visible=False)
        if ax_params.get('RemoveYTicks', False):
            ax.set_yticks([])
        # Set x-label
        x_label = ax_params.get('CustomXLabel')
        if x_label is not None:
            x_label_ = (
                textwrap.fill(x_label, width=width, break_long_words=False)
                if (width := ax_params.get('LabelWrapWidth')) is not None else x_label)
            if ax_params['XLabelOutside'] and fig is not None:
                ax.set_xlabel(
                    ax_params.get('XLabelOutsideDummy', ' '))
                fig.text(
                    ax_params.get('XLabelOutsideX', 0.5),
                    ax_params.get('XLabelOutsideY', 0.06),
                    x_label_,
                    va='center',
                    ha='center')
            else:
                ax.set_xlabel(x_label_, **ax_params.get('XLabelParams', {}))
        # Set y-label
        y_label = ax_params.get('CustomYLabel')
        if y_label is not None:
            y_label_ = (
                textwrap.fill(y_label, width=width, break_long_words=False)
                if (width := ax_params.get('LabelWrapWidth')) is not None else y_label)
            if ax_params['YLabelOutside'] and fig is not None:
                fig.text(
                    ax_params.get('YLabelOutsideX', 0.03),
                    ax_params.get('YLabelOutsideY', 0.5),
                    y_label_,
                    va='center',
                    ha='center',
                    rotation='vertical')
            else:
                ax.set_ylabel(y_label_, **ax_params.get('YLabelParams', {}))
        # Rasterize axis area
        ax.set_rasterized(ax_params['Rasterize'])
        # Set aspect ratio
        if ax_params.get('AspectRatio') is not None:
            ax.set_aspect(ax_params['AspectRatio'])
        # Set equal limits on x- and y-axis
        if ax_params['EqualLim']:
            ax.set_xlim(
                np.min([ax.get_xlim(), ax.get_ylim()]),
                np.max([ax.get_xlim(), ax.get_ylim()]),
                )
            ax.set_ylim(
                np.min([ax.get_xlim(), ax.get_ylim()]),
                np.max([ax.get_xlim(), ax.get_ylim()]),
                )
        # Rotate x-tick labels and right-align, as used for times and dates
        if ax_params.get('XTickLabelsRotation') is not None:
            rotation = ax_params['XTickLabelsRotation']
            for label in ax.get_xticklabels():
                label.set_ha('right')
                label.set_rotation(rotation)
        # Set date formatter for x-axis
        if ax_params.get('XDateFormatter') is not None:
            ax.xaxis.set_major_formatter(
                matplotlib.dates.DateFormatter(
                    ax_params['XDateFormatter']))
        # Set axes title
        if ax_params.get('Title') is not None:
            ax.set_title(
                ax_params['Title'],
                **plts.get_title_params(
                    **ax_params.get('TitleParams', {}),
                    ))
        # Add subfigure identifier
        if ax_params.get('SubplotLabel') is not None:
            height_ = ax.get_position().height
            subplot_label_params = {
                **{'fontweight': 'bold'},
                **ax_params.get('SubplotLabelParams', {})
                }
            h_subplot_label = ax.text(
                ax_params['SubplotLabelX'],
                1 - ax_params['SubplotLabelY'] - 0.01*height_,
                ax_params['SubplotLabel'],
                transform=ax.transAxes,
                ha=ax_params['SubplotLabelHA'],
                va=ax_params['SubplotLabelVA'],
                **subplot_label_params
                )
            # print(1 - ax_params['SubplotLabelY'] - 0.01*height_)
            if ax_params['ZOrderSubplotLabel'] is not None:
                h_subplot_label.set_zorder(
                    ax_params['ZOrderSubplotLabel'])

        plot_params[f'AxParams{i_ax:d}'] = ax_params
        self.plot_params = plot_params

    def set_fig_params(self):
        """
        Format figure, as defined in dict `self.plot_params`.
        """
        if self.plot_params.get('Title') is not None:
            self.fig.suptitle(
                self.plot_params['Title'],
                **plts.get_title_params(
                    **self.plot_params.get('TitleParams', {}),
                    ))

    def get_save_filepath(self):
        """
        Get filepath (without file extension) to which plot should be saved
        as determined from dict `plot_params`.
        """
        plot_params = {
            **self.PLOT_PARAMS_DEFAULT,
            **self.plot_params,
            }

        if plot_params.get('SavePrefixTime', True):
            filename_prefix = (
                datetime.datetime.utcnow().strftime(
                    self.plot_params.get('SavePrefixTimeFormat', '%Y-%m-%d'))
                + ' ')
        else:
            filename_prefix = ''
        filename = (
            filename_prefix
            + plot_params.get('SaveSuffix', ''))
        filename = pyha.util.plot.truncate_filename(
            filename, plot_params.get('SaveFilenameOverhead', 36))
        filepath = Path(
            plot_params.get('SaveDir', ''), plot_params.get('SaveSubdir', ''), filename)
        self.filepath = filepath
        return filepath

    def save_plot(self, exts=None, **kwargs):
        """
        Save figure to filepath as determined in dict `plot_params`.
        """
        plot_params = {
            **self.PLOT_PARAMS_DEFAULT,
            **self.plot_params,
            }

        if exts is None:
            exts = ['.pdf']

        filepath = Path(self.get_save_filepath())
        if plot_params.get('Save', False):
            # Create output dir if it doesn't exist
            dir_ = filepath.absolute().parent
            dir_exists = dir_.exists()
            create_dir = plot_params.get('CreateDir', False)
            if not dir_exists and create_dir:
                dir_.mkdir(parents=True)
                dir_exists = True
            elif not dir_exists and not create_dir:
                logger.warning(
                    f'Directory \'{dir_}\' does not exist, '
                    + 'not created as set in plot parameters '
                    + '\'CreateDir\' and plot not saved')
            if dir_exists:
                for ext in exts:
                    filepath_ext = Path(str(filepath) + ext)
                    if filepath_ext.exists() and not plot_params.get('SaveOverwrite', False):
                        logger.warning(
                            f'File \'{filepath_ext}\' already exists, '
                            + 'not overwritten as set in plot parameters '
                            + '(\'SaveOverwrite\')')
                    else:
                        self.fig.savefig(
                            filepath_ext,
                            dpi=plot_params['SaveDPI'],
                            **kwargs)
                        logger.info(
                            f'Wrote file \'{filepath_ext}\''
                            + f' (with {plot_params["SaveDPI"]} DPI)')
        else:
            if plot_params.get('SaveDisabledWarning', True):
                logger.info(
                    'Plot not saved, as saving is disabled in plot parameters')

    def gen_plot(self, data_sets=None,
                 return_fit_results=False):
        """
        Generate plot of data defined in dict `data_sets`.
        """
        plot_params = self.plot_params

        if self.fig is None:
            self.create_figure(self.plot_params)
        if self.axs is None:
            self.add_axes(data_sets=data_sets)
        axs = self.axs
        if data_sets is not None:
            self.data_sets = data_sets
        data_sets = self.data_sets

        leg_handles = {}
        leg_labels = {}
        leg_sort = {}
        hist_ranges = {}
        fit_results = {}
        data_sets_color = {}
        for i_data_set, (data_set_id, data_set) in enumerate(data_sets.items()):

            i_ax = data_set.get('Axes', 0)
            ax = axs[i_ax]
            ax_params = {
                'XLabelType': plot_params.get(
                    'XLabelType', plot_params.get('LabelType', '')),
                'YLabelType': plot_params.get(
                    'YLabelType', plot_params.get('LabelType', '')),
                'XLim': plot_params.get('XLim'),
                'YLim': plot_params.get('YLim'),
                **plot_params.get(f'AxParams{i_ax:d}', {}),
                }

            # Add defaults
            data_set = {
                'PlotType': plot_params.get('PlotType', 'XY'),
                'LabelMeanFormat': plot_params.get('LabelMeanFormat'),
                'LabelSDFormat': plot_params.get('LabelSDFormat'),
                'LabelCVFormat': plot_params.get('LabelCVFormat', '.1f'),
                'LabelRCSFormat': plot_params.get('LabelRCSFormat'),
                'ShowGaussFit': plot_params.get('ShowGaussFit', False),
                'NormHistWGaussFit': plot_params.get('NormHistWGaussFit', False),
                'ShowRedChiSqDist': plot_params.get('ShowRedChiSqDist', False),
                'LabelXMean': plot_params.get('LabelXMean', False),
                'LabelXCV': plot_params.get('LabelXCV', False),
                'LabelXSD': plot_params.get('LabelXSD', False),
                'LabelYMean': plot_params.get('LabelYMean', False),
                'LabelYCV': plot_params.get('LabelYCV', False),
                'LabelYSD': plot_params.get('LabelYSD', False),
                'Orientation': plot_params.get('Orientation', 'V'),
                'BasisParams': plot_params.get('BasisParams'),
                'Source': plot_params.get('Source', 'Custom'),
                'LinearFit': plot_params.get('LinearFit', False),
                'CompactUncertExpNot': False,
                'HistNumBins': plot_params.get('HistNumBins', 10),
                'FitChi2Shift': plot_params.get('FitChi2Shift', False),
                **data_set
                }

            data_set = {
                'XMeanPrefixExp': data_set.get('MeanPrefixExp'),
                'YMeanPrefixExp': data_set.get('MeanPrefixExp'),
                'XSDPrefixExp': data_set.get('SDPrefixExp'),
                'YSDPrefixExp': data_set.get('SDPrefixExp'),
                'XSlopePrefixExp': data_set.get('SlopePrefixExp'),
                'YSlopePrefixExp': data_set.get('SlopePrefixExp'),
                'XLabelMeanFormat': data_set['LabelMeanFormat'],
                'YLabelMeanFormat': data_set['LabelMeanFormat'],
                'XLabelSDFormat': data_set['LabelSDFormat'],
                'YLabelSDFormat': data_set['LabelSDFormat'],
                'XLabelCVFormat': data_set['LabelCVFormat'],
                'YLabelCVFormat': data_set['LabelCVFormat'],
                'XLabelRCSFormat': data_set['LabelRCSFormat'],
                'YLabelRCSFormat': data_set['LabelRCSFormat'],
                'XWAvgCompactUncertSigErrDigits': data_set.get(
                    'WAvgCompactUncertSigErrDigits'),
                'YWAvgCompactUncertSigErrDigits': data_set.get(
                    'WAvgCompactUncertSigErrDigits'),
                'XWAvgRCSCompactUncertSigErrDigits': data_set.get(
                    'WAvgRCSCompactUncertSigErrDigits', 1),
                'YWAvgRCSCompactUncertSigErrDigits': data_set.get(
                    'WAvgRCSCompactUncertSigErrDigits', 1),
                **data_set,
                }

            data_source = data_set['Source']
            if data_source in ['dfAll_scans', 'dfAll_points', 'dfAll_scans_sd']:
                dataclass_source = True
            else:
                dataclass_source = False
            if dataclass_source:
                if self.DataClass is None:
                    raise Exception('Data class (DataClass) not defined')
                if data_set.get('DataClassDataframe') is None:
                    if data_source == 'dfAll_scans':
                        dfAll_data_set, _ = self.DataClass.getDataPreview(
                            group='dfData_ScanUID')
                    elif data_source == 'dfAll_points':
                        dfAll_data_set, _ = self.DataClass.getDataPreview(
                            group='dfData_UID')
                    elif data_source == 'dfAll_scans_sd':
                        dfAll_data_set = (
                            pyha.data_ext.get_data_preview_scan_sd(self.DataClass))
                else:
                    dfAll_data_set = data_set['DataClassDataframe']
            elif data_source == 'Custom':
                dataclass_source = False
            else:
                logger.info(
                    f'Unknown data source \'{data_source}\' for data set \'{data_set_id}\'')
                continue

            # Get data class parameter IDs for x and y data
            param_id_x = data_set.get('ParamX')
            param_id_y = data_set.get('ParamY')
            if self.DataClass is None and \
                    ((param_id_x is not None) or (param_id_y is not None)):
                logger.info(
                    'Parameter ID defined (ParamX and/or ParamY),'
                    + ' but no DataClass containing data format metadata supplied')
                param_id_x = None
                param_id_y = None


            if dataclass_source:

                data_mask = data_set.get('DataMask')
                if data_mask is None:
                    data_mask = pd.Series(True, index=dfAll_data_set.index)

                if param_id_x is None:
                    logger.info(
                        'Parameter ID for x-data (\'ParamX\') not defined '
                        + f'for data set {data_set_id}, skipping')
                    continue

                x = dfAll_data_set[data_mask][param_id_x]
                x_sigma = None

                if data_set['PlotType'] in ['Hist']:
                    y = None
                    y_sigma = None
                else:
                    if param_id_y is None:
                        logger.info(
                            'Parameter ID for y-data (\'ParamY\') not defined '
                            + f'for data set {data_set_id}, skipping')
                        continue
                    y = dfAll_data_set[data_mask][param_id_y]
                    y_sigma = None

                weights = None

            else:
                x = data_set.get('XData')
                x_sigma = data_set.get('XSigma')
                y = data_set.get('YData')
                y_sigma = data_set.get('YSigma')
                weights = data_set.get('Weights')

            x = np.atleast_1d(x) if x is not None else None
            x_sigma = np.atleast_1d(x_sigma) if x_sigma is not None else None
            y = np.atleast_1d(y) if y is not None else None
            y_sigma = np.atleast_1d(y_sigma) if y_sigma is not None else None
            weights = np.atleast_1d(weights) if weights is not None else None

            # Statistics
            label = data_set.get('Label', '')
            x_mean = np.nan
            x_sd = np.nan
            x_cv = np.nan
            x_sem = np.nan
            # x_w_avg = None
            numeric_x = False
            num_x = -1
            if x is not None and np.issubdtype(x.dtype, np.number):
                num_x = len(x)
                x_mean = np.mean(x) if num_x > 0 else np.nan
                x_sd = np.std(x, ddof=1) if num_x > 1 else np.nan
                x_cv = x_sd/x_mean if x_mean != 0. else np.nan
                x_sem = x_sd/np.sqrt(num_x) if num_x > 0 else np.nan
                # x_w_avg = pyhs.statfunc.avg_uncorr_meas(
                #     x, x_sigma, weights=weights)
                numeric_x = True
            elif x is not None:
                num_x = len(x)
            y_sd = np.nan
            y_mean = np.nan
            y_cv = np.nan
            y_sem = np.nan
            y_w_avg = None
            numeric_y = False
            num_y = -1
            if y is not None and np.issubdtype(y.dtype, np.number):
                num_y = len(y)
                y_mean = np.mean(y) if num_y > 0 else np.nan
                y_sd = np.std(y, ddof=1) if num_y > 1 else np.nan
                y_cv = y_sd/y_mean if y_mean != 0. else np.nan
                y_sem = y_sd/np.sqrt(num_y) if num_y > 0 else np.nan
                y_w_avg = pyhs.statfunc.avg_uncorr_meas(
                    y, y_sigma, weights=weights)
                numeric_y = True
            elif y is not None:
                num_y = len(y)

            if numeric_x and numeric_y:
                # Linear correlation coefficient
                _, r_cov = pyhs.statfunc.get_cov_r(x, y)
                r_cov_p = pyhs.statfunc.get_p_value_of_r(r_cov, num_x)
            else:
                r_cov = np.nan
                r_cov_p = np.nan

            param_label_subs = {}

            # Get substitutions from DataClass
            for id_, param_id in zip(['X', 'Y'], [param_id_x, param_id_y]):

                subs = {}

                if param_id is not None \
                        and param_id in self.DataClass.dfDataFormat.index:
                    param_row = self.DataClass.dfDataFormat.loc[param_id]
                    subs = {
                        **subs,
                        **pyha.util.plot.get_param_subs(param_row)
                        }
                    subs['LabelMeanFormat'] = subs['NumberFormat']
                    subs['LabelSDFormat'] = subs['NumberFormat']
                    subs['LabelRCSFormat'] = subs['NumberFormat']
                    subs['WAvgCompactUncertSigErrDigits'] = subs['NSigUncertDigits']
                # Overwrite substitutions from data_set if given
                overwrite_subs = (
                    [
                        'BaseUnit', 'Symbol', 'PrefixExp',
                        'MeanPrefixExp', 'SDPrefixExp', 'SlopePrefixExp',
                        'LabelMeanFormat', 'LabelSDFormat', 'LabelRCSFormat',
                        'WAvgCompactUncertSigErrDigits',
                        ]
                    + list(param_label_subs.keys()))
                overwrite_subs = list(np.unique(overwrite_subs))
                for overwrite_sub in overwrite_subs:
                    if data_set.get(f'{id_}{overwrite_sub}') is not None:
                        subs[overwrite_sub] = data_set[f'{id_}{overwrite_sub}']

                # Set defaults for non-available subs
                default_subs = {
                    'BaseUnit': '',
                    'Symbol': '',
                    'PrefixExp': 1.,
                    'MeanPrefixExp': 1.,
                    'SDPrefixExp': 1.,
                    'SlopePrefixExp': 1.,
                    'LabelMeanFormat': '.2f',
                    'LabelSDFormat': '.2f',
                    'LabelRCSFormat': '.1f',
                    'WAvgCompactUncertSigErrDigits': 1,
                    }
                subs = {
                    **default_subs,
                    **subs,
                    }

                param_label_subs = {
                    **param_label_subs,
                    **{f'{id_}{key}': elem for key, elem in subs.items()}
                    }

            # Set up unit prefixes
            x_unit = pyhs.units.Units(
                param_label_subs['XPrefixExp'],
                baseUnit=param_label_subs['XBaseUnit'])
            y_unit = pyhs.units.Units(
                param_label_subs['YPrefixExp'],
                baseUnit=param_label_subs['YBaseUnit'])

            # Get labels from data class
            if param_id_x is not None \
                    and param_id_x in self.DataClass.dfDataFormat.index:
                x_label_dataset, _ = pyha.util.plot.get_param_label(
                    self.DataClass, param_id_x, unit_prefix=x_unit.unitPrefix,
                    fit_param_only=data_set.get('XLabelFitParamOnly', True),
                    include_symbol=data_set.get('XLabelInclSymbol', True))
                x_label_dataset_wo_unit, _ = pyha.util.plot.get_param_label(
                    self.DataClass, param_id_x, include_unit=False,
                    fit_param_only=data_set.get('XLabelFitParamOnly', True),
                    include_symbol=data_set.get('XLabelInclSymbol', True))
            else:
                x_label_dataset = None
                x_label_dataset_wo_unit = None
            if param_id_y is not None \
                    and param_id_y in self.DataClass.dfDataFormat.index:
                y_label_dataset, _ = pyha.util.plot.get_param_label(
                    self.DataClass, param_id_y, unit_prefix=y_unit.unitPrefix,
                    fit_param_only=data_set.get('YLabelFitParamOnly', True),
                    include_symbol=data_set.get('YLabelInclSymbol', True))
                y_label_dataset_wo_unit, _ = pyha.util.plot.get_param_label(
                    self.DataClass, param_id_y, include_unit=False,
                    fit_param_only=data_set.get('XLabelFitParamOnly', True),
                    include_symbol=data_set.get('XLabelInclSymbol', True))
            else:
                y_label_dataset = None
                y_label_dataset_wo_unit = None

            # Scale xy-data
            x_lim = data_set.get('XLim')
            y_lim = data_set.get('YLim')
            if x is not None:
                if x_unit.scaleFactor != 1.:
                    x_scaled = x * x_unit.scaleFactor
                else:
                    x_scaled = x
            if y is not None:
                if y_unit.scaleFactor != 1.:
                    y_scaled = y * y_unit.scaleFactor
                else:
                    y_scaled = y
            x_sigma_scaled = x_sigma * x_unit.scaleFactor if x_sigma is not None else None
            y_sigma_scaled = y_sigma * y_unit.scaleFactor if y_sigma is not None else None

            if x_lim is not None:
                if x_unit.scaleFactor != 1.:
                    x_lim = np.atleast_1d(x_lim) * x_unit.scaleFactor
                ax_params['XLim'] = x_lim
            else:
                x_lim = ax_params.get('XLim')
            if y_lim is not None:
                if y_unit.scaleFactor != 1.:
                    y_lim = np.atleast_1d(y_lim) * y_unit.scaleFactor
                ax_params['YLim'] = y_lim
            else:
                y_lim = ax_params.get('YLim')

            def get_unit_spaced(unit):

                return ' '+unit if unit != '' else ''

            def get_unit_parenthesis(unit):

                return f' ({unit})' if unit != '' else ''

            # Built label
            stat_labels = []
            unit_ = pyhs.units.Units(
                param_label_subs['XMeanPrefixExp']  / x_unit.scaleFactor,
                baseUnit=x_unit.baseUnit)
            x_mean_str = (
                f'{unit_.scaleFactor*x_mean:{param_label_subs["XLabelMeanFormat"]}}')
            x_mean_unit = unit_.unitName
            x_mean_unit_spaced = get_unit_spaced(x_mean_unit)
            x_mean_compact_uncert_str = pyhs.statfunc.compact_uncert_str(
                unit_.scaleFactor*x_mean, unit_.scaleFactor*x_sem,
                sig_err_digits=data_set.get('CompactUncertSigErrDigits', 2),
                exp_notation=data_set['CompactUncertExpNot'])
            x_mean_compact_uncert_exp_str = pyhs.statfunc.compact_uncert_str(
                unit_.scaleFactor*x_mean, unit_.scaleFactor*x_sem,
                sig_err_digits=data_set.get('CompactUncertSigErrDigits', 2),
                exp_notation=True)
            if data_set['LabelXMean']:
                stat_labels.append(
                    f'Mean: {x_mean_str}{x_mean_unit_spaced}')
            unit_ = pyhs.units.Units(
                param_label_subs['XSDPrefixExp']  / x_unit.scaleFactor,
                baseUnit=x_unit.baseUnit)
            x_sd_str = f'{unit_.scaleFactor*x_sd:{param_label_subs["XLabelSDFormat"]}}'
            x_sd_unit = unit_.unitName
            x_sd_unit_spaced  = get_unit_spaced(x_sd_unit)
            if data_set['LabelXSD']:
                stat_labels.append(
                    f'SD: {x_sd_str}{x_sd_unit_spaced}')
            x_cv_str = f'{100*x_cv:{data_set["XLabelCVFormat"]}}'
            cv_unit = r'%'
            cv_unit_spaced = r'$\,$%'
            if data_set['LabelXCV']:
                stat_labels.append(
                    f'CV: {x_cv_str}{cv_unit_spaced}')
            unit_ = pyhs.units.Units(
                    param_label_subs['YMeanPrefixExp'] / y_unit.scaleFactor,
                    baseUnit=y_unit.baseUnit)
            y_mean_str = f'{unit_.scaleFactor*y_mean:{param_label_subs["YLabelMeanFormat"]}}'
            y_mean_unit = unit_.unitName
            y_mean_unit_spaced = get_unit_spaced(y_mean_unit)
            y_mean_compact_uncert_str = pyhs.statfunc.compact_uncert_str(
                unit_.scaleFactor*y_mean, unit_.scaleFactor*y_sem,
                sig_err_digits=data_set.get('CompactUncertSigErrDigits', 1),
                exp_notation=data_set['CompactUncertExpNot'])
            y_mean_compact_uncert_exp_str = pyhs.statfunc.compact_uncert_str(
                unit_.scaleFactor*y_mean, unit_.scaleFactor*y_sem,
                sig_err_digits=data_set.get('CompactUncertSigErrDigits', 2),
                exp_notation=True)
            if data_set['LabelYMean']:
                stat_labels.append(
                    f'Mean: {y_mean_str}{y_mean_unit_spaced}')
            if y_w_avg is not None:
                y_w_avg_compact_uncert_str = pyhs.statfunc.compact_uncert_str(
                    unit_.scaleFactor*y_w_avg['Mean'],
                    unit_.scaleFactor*y_w_avg['Sigma'],
                    sig_err_digits=param_label_subs['YWAvgCompactUncertSigErrDigits'],
                    exp_notation=data_set['CompactUncertExpNot'])
                y_w_avg_rcs_compact_uncert_str = pyhs.statfunc.compact_uncert_str(
                    y_w_avg['RedChiSq'],
                    y_w_avg['RedChiSqSigma'],
                    sig_err_digits=data_set.get('YWAvgRCSCompactUncertSigErrDigits', 2))
            unit_ = pyhs.units.Units(
                param_label_subs['YSDPrefixExp']  / y_unit.scaleFactor,
                baseUnit=y_unit.baseUnit)
            y_sd_str = f'{unit_.scaleFactor*y_sd:{param_label_subs["YLabelSDFormat"]}}'
            y_sd_unit = unit_.unitName
            y_sd_unit_spaced  = get_unit_spaced(y_sd_unit)
            if data_set['LabelYSD']:
                stat_labels.append(
                    f'SD: {y_sd_str}{y_sd_unit_spaced}')
            y_cv_str = f'{100*y_cv:{data_set["YLabelCVFormat"]}}'
            if data_set['LabelYCV']:
                stat_labels.append(
                    f'CV: {y_cv_str}{cv_unit_spaced}')

            # Scale slope from linear fit and built unit
            x_slope_unit = pyhs.units.Units(
                param_label_subs['XSlopePrefixExp']  / x_unit.scaleFactor,
                baseUnit=x_unit.baseUnit)
            y_slope_unit = pyhs.units.Units(
                param_label_subs['YSlopePrefixExp']  / y_unit.scaleFactor,
                baseUnit=y_unit.baseUnit)
            slope_scale_factor = (
                (y_slope_unit.scaleFactor / y_unit.scaleFactor)
                / (x_slope_unit.scaleFactor / x_unit.scaleFactor)
                )
            if '/' in x_slope_unit.unitName:
                x_slope_unit_par = f'({x_slope_unit.unitName})'
            else:
                x_slope_unit_par = x_slope_unit.unitName
            if x_slope_unit.unitName == '' and y_slope_unit.unitName == '':
                slope_unit = ''
            elif x_slope_unit.unitName == '':
                slope_unit = y_slope_unit.unitName
            elif y_slope_unit.unitName == '':
                slope_unit = '1/'+x_slope_unit_par
            else:
                slope_unit = y_slope_unit.unitName+'/'+x_slope_unit_par

            if len(stat_labels) > 0:
                if label != '':
                    label += '\n'
                label += ', '.join(stat_labels)

            param_label_subs = {
                **param_label_subs,
                'ParamXLabel': x_label_dataset,
                'ParamYLabel': y_label_dataset,
                'NX': num_x,
                'NY': num_y,
                'XBaseUnit': x_unit.baseUnit,
                'XUnit': x_unit.unitName,
                'XUnitPrefix': x_unit.unitPrefix,
                'ParamXLabelwoUnit': x_label_dataset_wo_unit,
                'ParamYLabelwoUnit': y_label_dataset_wo_unit,
                'YBaseUnit': y_unit.baseUnit,
                'YUnit': y_unit.unitName,
                'YUnitPrefix': y_unit.unitPrefix,
                'XMean': x_mean_str,
                'XMeanUnit': x_mean_unit,
                'XMeanCompactUncert': x_mean_compact_uncert_str,
                'XMeanCompactUncertExp': x_mean_compact_uncert_exp_str,
                'XSD': x_sd_str,
                'XSDUnit': x_sd_unit,
                'XCV': x_cv_str,
                'YMean': y_mean_str,
                'YMeanUnit': y_mean_unit,
                'YMeanCompactUncert': y_mean_compact_uncert_str,
                'YMeanCompactUncertExp': y_mean_compact_uncert_exp_str,
                'YSD': y_sd_str,
                'YSDUnit': y_sd_unit,
                'YCV': y_cv_str,
                'CVUnit': cv_unit,
                'CVUnitSpaced': cv_unit_spaced,
                'CorrCoeff': f'{r_cov:.2f}',
                'CorrCoeffPValue': (
                    f'{pyhs.statfunc.format_number(r_cov_p, 2, exp_not=True)}'
                    if r_cov_p != 0. else '0'
                    ),
                'SlopeUnit': slope_unit,
                'SlopeUnitSpaced': get_unit_spaced(slope_unit),
                }

            for id_, key in itertools.product(
                    ['X', 'Y'],
                    ['Unit', 'Symbol', 'MeanUnit', 'SDUnit']):

                param_label_subs[f'{id_}{key}Spaced'] = get_unit_spaced(
                    param_label_subs[f'{id_}{key}'])
                param_label_subs[f'{id_}{key}Par'] = get_unit_parenthesis(
                    param_label_subs[f'{id_}{key}'])

            if y_w_avg is not None:
                param_label_subs = {
                    **param_label_subs,
                    'YWAvgMean': (
                        f'{unit_.scaleFactor*y_w_avg["Mean"]:{param_label_subs["YLabelMeanFormat"]}}'),
                    'YWAvgSigma': (
                        f'{unit_.scaleFactor*y_w_avg["Sigma"]:{param_label_subs["YLabelMeanFormat"]}}'),
                    'YWAvgRCS': (
                        f'{y_w_avg["RedChiSq"]:{param_label_subs["YLabelRCSFormat"]}}'),
                    'YWAvgPValue': (
                        f'{pyhs.statfunc.format_number(y_w_avg["PValue"], 2)}'),
                    'YWAvgSignificance': (
                        f'{y_w_avg["Significance"]:.1f}'),
                    'YWAvgPValueSignificance': (
                        r'$p$ = '
                        + f'{pyhs.statfunc.format_number(y_w_avg["PValue"], 2)}'
                        + (
                            f' ({y_w_avg["Significance"]:.1f}' + r'$\,\sigma$)'
                            if y_w_avg["Significance"] > 0 else '')
                        ),
                    'YWAvgPValueSignificanceNewLine': (
                        r'$p$ = '
                        + f'{pyhs.statfunc.format_number(y_w_avg["PValue"], 2)}'
                        + (
                            '\n'+f'({y_w_avg["Significance"]:.1f}' + r'$\,\sigma$)'
                            if y_w_avg["Significance"] > 0 else '')
                        ),
                    'YWAvgCompactUncert': y_w_avg_compact_uncert_str,
                    'YWAvgRCSCompactUncert': y_w_avg_rcs_compact_uncert_str,
                    }

            if data_set['LinearFit'] and x is not None and y is not None:

                if y_sigma_scaled is None:
                    y_err = np.ones(len(x))
                else:
                    y_err = y_sigma_scaled

                # Fit data with linear function
                pstart = [0, np.mean(y_scaled) if len(y_scaled) > 0 else np.nan]
                fit_func = myFit.fit_funcs['Linear']
                fit_result, error_msg = myFit.do_fit(
                    fit_func,
                    x_scaled,
                    y_scaled,
                    y_err,
                    out_func=logger.info, remove_nan_points=True)
                sr_linear_fit = myFit.fit_result_to_dict(
                    fit_result, fit_func)
                fit_results[data_set_id] = sr_linear_fit

                label_id = f'{i_ax:d}D{data_set_id}XYLinearFit'

                slope_offset_corr_coeff = (
                     sr_linear_fit['Cov01_Value']
                     /(sr_linear_fit['Slope_Sigma']*sr_linear_fit['Offset_Sigma']))
                lf_offset_compact_uncert_str = pyhs.statfunc.compact_uncert_str(
                    sr_linear_fit['Offset_Value'],
                    sr_linear_fit['Offset_Sigma'],
                    sig_err_digits=data_set.get('LFOffsetCompactUncertSigErrDigits', 1),
                    exp_notation=data_set['CompactUncertExpNot'])
                lf_slope_compact_uncert_str = pyhs.statfunc.compact_uncert_str(
                    sr_linear_fit['Slope_Value'] * slope_scale_factor,
                    sr_linear_fit['Slope_Sigma'] * slope_scale_factor,
                    sig_err_digits=data_set.get('LFSlopeCompactUncertSigErrDigits', 1),
                    exp_notation=data_set['CompactUncertExpNot'])
                lf_rcs_compact_uncert_str = pyhs.statfunc.compact_uncert_str(
                    sr_linear_fit['RedChiSq'],
                    np.sqrt(2/(len(x)-2)) if not np.isnan(sr_linear_fit['RedChiSq']) else np.nan,
                    sig_err_digits=data_set.get('LFRCSCompactUncertSigErrDigits', 2))

                param_label_subs = {
                    **param_label_subs,
                    'LFSlopeOffsetCorrCoeff': f'{slope_offset_corr_coeff:.2f}',
                    'LFOffsetMean': (
                        f'{sr_linear_fit["Offset_Value"]:{data_set.get("LFOffsetFormat", ".2f")}}'),
                    'LFOffsetSigma': (
                        f'{sr_linear_fit["Offset_Sigma"]:{data_set.get("LFOffsetFormat", ".2f")}}'),
                    'LFSlopeMean': (
                        f'{sr_linear_fit["Slope_Value"]*slope_scale_factor:{data_set.get("LFSlopeFormat", ".2f")}}'),
                    'LFSlopeSigma': (
                        f'{sr_linear_fit["Slope_Sigma"]*slope_scale_factor:{data_set.get("LFSlopeFormat", ".2f")}}'),
                    'LFOffsetCompactUncert': lf_offset_compact_uncert_str,
                    'LFSlopeCompactUncert': lf_slope_compact_uncert_str,
                    'LFRCSCompactUncert': lf_rcs_compact_uncert_str,
                    }

            ax.autoscale(enable=True, axis='x')

            if data_set['PlotType'] == 'XY':

                data_sets_color[data_set_id] = None
                h_data = None
                label_id = f'{i_ax:d}D{data_set_id}XY'
                if data_set.get('ShowData', True):

                    if data_set.get('BasisParams') == 'Line':
                        basis_params = plts.get_line_params
                    elif data_set.get('BasisParams') == 'Errorbar':
                        basis_params = plts.get_errorbar_params
                    else:
                        basis_params = plts.get_point_params
                    x_err = (
                        x_sigma_scaled if data_set.get('ShowXErr', True)
                        else None)
                    y_err = (
                        y_sigma_scaled if data_set.get('ShowYErr', True)
                        else None)
                    set_color_to_data_set_id = data_set.get('SetColorToDataSetID')
                    if set_color_to_data_set_id is not None and \
                            set_color_to_data_set_id in data_sets_color:
                        color_override = {
                            'color': data_sets_color[set_color_to_data_set_id]}
                    else:
                        color_override = {}
                    if data_set.get('ShowYErrAsFillBetween', False):
                        h_fillbetween = ax.fill_between(
                            x_scaled,
                            y_scaled-y_err,
                            y_scaled+y_err,
                            **plts.get_fill_params(**{
                                **plot_params.get('XYFillBetweenParams', {}),
                                **ax_params.get('XYFillBetweenParams', {}),
                                **data_set.get('XYFillBetweenParams', {}),
                                **color_override,
                                }))
                        data_sets_color[data_set_id] = h_fillbetween.get_facecolor()
                        if data_set.get('Label') is not None:
                            leg_handles[label_id] = h_fillbetween
                    else:
                        h_data = ax.errorbar(
                            x_scaled,
                            y_scaled,
                            xerr=x_err,
                            yerr=y_err,
                            **basis_params(**{
                                **plot_params.get('XYParams', {}),
                                **ax_params.get('XYParams', {}),
                                **data_set.get('XYParams', {}),
                                'zorder': (
                                    data_set['ZOrderXY']
                                    if data_set.get('ZOrderXY') is not None
                                    else None),
                                **color_override,
                                }))
                        data_sets_color[data_set_id] = h_data[0].get_color()
                        if data_set.get('Label') is not None:
                            leg_handles[label_id] = (
                                h_data if data_set.get('ShowErrorbarInLegend', False)
                                else h_data[0])
                    if data_set.get('Label') is not None:
                        leg_labels[label_id] = (
                            pyha.util.plot.do_subs(label, param_label_subs))
                        leg_sort[label_id] = data_set.get('LegSort', 0)

                if data_set.get('XLimFit') is not None:
                    fit_x_lim = data_set['XLimFit']
                elif x_lim is not None and not np.any(pd.isnull(x_lim)):
                    fit_x_lim = x_lim
                else:
#                    ax.autoscale(enable=True, axis='x')
                    fit_x_lim = ax.get_xlim()

                h_w_avg = None
                if data_set.get('ShowYWAvg', False):

                    set_color_to_data_set_id = data_set.get('SetColorToDataSetID')
                    if set_color_to_data_set_id is not None and \
                            set_color_to_data_set_id in data_sets_color:
                        color_override = {
                            'facecolor': data_sets_color[set_color_to_data_set_id]}
                    else:
                        color_override = {}
                    if y_w_avg is not None:
                        h_w_avg = ax.fill_between(
                            [np.min(fit_x_lim), np.max(fit_x_lim)],
                            np.repeat(
                                y_unit.scaleFactor
                                *(y_w_avg['Mean']-y_w_avg['Sigma']),
                                2),
                            np.repeat(
                                y_unit.scaleFactor
                                *(y_w_avg['Mean']+y_w_avg['Sigma']),
                                2),
                            **plts.get_fill_params(**{
                                'facecolor': (
                                    h_data[0].get_color() if h_data is not None
                                    else None),
                                **plot_params.get(
                                    'YWAvgParams', {'alpha': 0.3}),
                                **ax_params.get('YWAvgParams', {}),
                                **data_set.get('YWAvgParams', {}),
                                **color_override,
                                }))
                        if data_sets_color[data_set_id] is None:
                            data_sets_color[data_set_id] = (
                                h_w_avg.get_facecolor()[0])
                        if data_set.get('LabelYWAvg') is not None:
                            label_id = f'{i_ax:d}D{data_set_id}YWAvg'
                            leg_handles[label_id] = h_w_avg
                            leg_labels[label_id] = (
                                pyha.util.plot.do_subs(
                                    data_set['LabelYWAvg'], param_label_subs))
                            leg_sort[label_id] = data_set.get('LegSort', 0)
                        if data_set.get('ZOrderYWAvg') is not None:
                            h_w_avg.set_zorder(data_set['ZOrderYWAvg'])

                # Plot shaded area (band) around weighted average with user-defined half-height
                # given by key 'YWAvgBandHeight' in `data_set`
                h_band = None
                if data_set.get('ShowYWAvgBand', False):

                    set_color_to_data_set_id = data_set.get('SetColorToDataSetID')
                    if set_color_to_data_set_id is not None and \
                            set_color_to_data_set_id in data_sets_color:
                        color_override = {
                            'facecolor': data_sets_color[set_color_to_data_set_id]}
                    else:
                        color_override = {}
                    if y_w_avg is not None:
                        h_band = ax.fill_between(
                            [np.min(fit_x_lim), np.max(fit_x_lim)],
                            np.repeat(
                                y_unit.scaleFactor
                                *(y_w_avg['Mean']-data_set.get('YWAvgBandHeight', 0.)),
                                2),
                            np.repeat(
                                y_unit.scaleFactor
                                *(y_w_avg['Mean']+data_set.get('YWAvgBandHeight', 0.)),
                                2),
                            **plts.get_fill_params(**{
                                'facecolor': (
                                    h_data[0].get_color() if h_data is not None
                                    else None),
                                **plot_params.get(
                                    'YWAvgBandParams', {'alpha': 0.3}),
                                **ax_params.get('YWAvgBandParams', {}),
                                **data_set.get('YWAvgBandParams', {}),
                                **color_override,
                                }))
                        if data_sets_color[data_set_id] is None:
                            data_sets_color[data_set_id] = (
                                h_band.get_facecolor()[0])
                        if data_set.get('LabelYWAvgBand') is not None:
                            label_id = f'{i_ax:d}D{data_set_id}YWAvgBand'
                            leg_handles[label_id] = h_band
                            leg_labels[label_id] = (
                                pyha.util.plot.do_subs(
                                    data_set['LabelYWAvgBand'], param_label_subs))
                            leg_sort[label_id] = data_set.get('LegSort', 0)
                        if data_set.get('ZOrderYWAvgBand') is not None:
                            h_band.set_zorder(data_set['ZOrderYWAvgBand'])

                h_w_avg_mean = None
                if data_set.get('ShowYWAvgMean', False):

                    set_color_to_data_set_id = data_set.get('SetColorToDataSetID')
                    if set_color_to_data_set_id is not None and \
                            set_color_to_data_set_id in data_sets_color:
                        color_override = {
                            'color': data_sets_color[set_color_to_data_set_id]}
                    else:
                        color_override = {}
                    if y_w_avg is not None:
                        h_w_avg_mean = ax.plot(
                            [np.min(fit_x_lim), np.max(fit_x_lim)],
                            np.repeat(
                                y_unit.scaleFactor*(y_w_avg['Mean']),
                                2),
                            **plts.get_line_params(**{
                                'color': (
                                    h_data[0].get_color() if h_data is not None
                                    else None),
                                **plot_params.get('YWAvgMeanParams', {}),
                                **ax_params.get('YWAvgMeanParams', {}),
                                **data_set.get('YWAvgMeanParams', {}),
                                **color_override,
                                }))
                        if data_sets_color[data_set_id] is None:
                            data_sets_color[data_set_id] = (
                                h_w_avg_mean[0].get_color())
                        if data_set.get('LabelYWAvgMean') is not None:
                            label_id = f'{i_ax:d}D{data_set_id}YWAvgMean'
                            leg_handles[label_id] = h_w_avg_mean[0]
                            leg_labels[label_id] = (
                                pyha.util.plot.do_subs(
                                    data_set['LabelYWAvgMean'], param_label_subs))
                            leg_sort[label_id] = data_set.get('LegSort', 0)
                        if data_set.get('ZOrderYWAvgMean') is not None:
                            h_w_avg.set_zorder(data_set['ZOrderYWAvgMean'])

                if data_set['LinearFit'] and (
                        data_set.get('ShowLinearFit', False)
                        or data_set.get('ShowLinearFitUncert', False)):

                    x_fit = np.linspace(*fit_x_lim, 1000)
                    y_lf = fit_func['Func'](x_fit, *fit_result['Popt'])
                    y_lf_sigma = fit_func['Sigma_func'](
                        x_fit, fit_result['Popt'], fit_result['Pcov'])

                    h_lf = None
                    if data_set.get('ShowLinearFit', False):
                        h_lf = ax.plot(
                            x_fit,
                            y_lf,
                            **plts.get_line_params(**{
                                **plot_params.get('LinearFitParams', {}),
                                **ax_params.get('LinearFitParams', {}),
                                **data_set.get('LinearFitParams', {})
                            }))

                        label_id = f'{i_ax:d}D{data_set_id}LF'
                        if data_set.get('LabelLF') is not None:
                            leg_handles[label_id] = h_lf[0]
                            leg_labels[label_id] = (
                                pyha.util.plot.do_subs(
                                    data_set['LabelLF'], param_label_subs))
                            leg_sort[label_id] = data_set.get('LegSort', 0)
                        if data_set.get('ZOrderLF') is not None:
                            h_lf[0].set_zorder(data_set['ZOrderLF'])

                    if data_set.get('ShowLinearFitUncert', False):
                        h_lf_uncert = ax.fill_between(
                            x_fit,
                            y_lf-y_lf_sigma,
                            y_lf+y_lf_sigma,
                            **plts.get_fill_params(**{
                                'facecolor': (
                                    h_lf[0].get_color() if h_lf is not None
                                    else None),
                                **plot_params.get('LFUncertParams', {}),
                                **ax_params.get('LFUncertParams', {}),
                                **data_set.get('LFUncertParams', {})
                                }))

                        if data_set.get('LabelLFUncert') is not None:
                            label_id = f'{i_ax:d}D{data_set_id}LFUncert'
                            leg_handles[label_id] = h_lf_uncert
                            leg_labels[label_id] = (
                                pyha.util.plot.do_subs(
                                    data_set['LabelLFUncert'], param_label_subs))
                            leg_sort[label_id] = data_set.get('LegSort', 0)
                        if data_set.get('ZOrderLFUncert') is not None:
                            h_w_avg.set_zorder(data_set['ZOrderLFUncert'])

            elif data_set['PlotType'] == 'VLine':

                if data_set.get('ShowData', True):
                    if data_set.get('BasisParams') == 'Points':
                        basis_params = plts.get_point_params
                    else:
                        basis_params = plts.get_line_params
                    if ax_params.get('YLim') is not None:
                        y_vline = ax_params['YLim']
                    else:
                        y_vline = ax.get_ylim()
                    np.tile(x_scaled, 2)
                    h_vline = ax.errorbar(
                        np.tile(x_scaled, 2),
                        y_vline,
                        **basis_params(**{
                            **plot_params.get('VLineParams', {}),
                            **ax_params.get('VLineParams', {}),
                            **data_set.get('VLineParams', {})
                            }))
                    if data_set.get('Label') is not None:
                        label_id = f'{i_ax:d}D{data_set_id}VLine'
                        leg_handles[label_id] = h_vline[0]
                        leg_labels[label_id] = (
                            pyha.util.plot.do_subs(label, param_label_subs))
                        leg_sort[label_id] = data_set.get('LegSort', 0)

            elif data_set['PlotType'] == 'HLine':

                if data_set.get('ShowData', True):
                    if data_set.get('BasisParams') == 'Points':
                        basis_params = plts.get_point_params
                    else:
                        basis_params = plts.get_line_params
                    if ax_params.get('XLim') is not None:
                        x_hline = ax_params['XLim']
                    else:
                        x_hline = ax.get_xlim()
                    h_hline = ax.errorbar(
                        x_hline,
                        np.tile(y_scaled, 2),
                        **basis_params(**{
                            **plot_params.get('HLineParams', {}),
                            **ax_params.get('HLineParams', {}),
                            **data_set.get('HLineParams', {})
                            }))
                    if data_set.get('Label') is not None:
                        label_id = f'{i_ax:d}D{data_set_id}HLine'
                        leg_handles[label_id] = h_hline[0]
                        leg_labels[label_id] = (
                            pyha.util.plot.do_subs(label, param_label_subs))
                        leg_sort[label_id] = data_set.get('LegSort', 0)

            elif data_set['PlotType'] == 'Hist':

                if data_set.get('HistRange') is not None:
                    hist_range = np.array(data_set['HistRange']) * x_unit.scaleFactor
                elif data_set.get('HistRangeFromDataset') is not None \
                        and data_set['HistRangeFromDataset'] in hist_ranges:
                    hist_range = hist_ranges[data_set['HistRangeFromDataset']]
                else:
                    hist_range = None
                if hist_range is not None and np.any(np.isnan(hist_range)):
                    hist_range = None
                if np.issubdtype(x.dtype, np.number) and np.any(np.isnan(x)):
                    logger.error(
                        f'Histogram data of data set \'{data_set_id}\' '
                        + 'contains NaN, skipping')
                    continue
                weighted_hist, bin_edges = np.histogram(
                    x * x_unit.scaleFactor,
                    bins=data_set['HistNumBins'],
                    range=hist_range,
                    )
                hist_ranges[data_set_id] = [np.min(bin_edges), np.max(bin_edges)]
                x_bins = bin_edges[:-1] + np.diff(bin_edges)/2

                # Fit Gaussian
                if data_set['ShowGaussFit'] or data_set['NormHistWGaussFit']:
                    pstart = [
                        np.mean(weighted_hist*x_bins)/np.mean(weighted_hist),
                        np.max(weighted_hist),
                        0,
                        np.std(x * x_unit.scaleFactor),
                        ]
                    fit_func_id = 'Gaussian'
                    fit_func = myFit.fit_funcs[fit_func_id]
                    fit_gaussian, _ = myFit.do_fit(
                        fit_func, x_bins, weighted_hist, np.ones(len(x_bins)),
                        warning_as_error=False, pstart=pstart
                        )
                    popt = fit_gaussian['Popt']
                    sr_fit = myFit.fit_result_to_series(fit_gaussian, fit_func)
                    fit_results[data_set_id] = sr_fit

                    param_label_subs = {
                        **param_label_subs,
                        'GFCFRValue': (
                            f'{sr_fit["CFR_Value"]:{data_set.get("GFCFRFormat", ".2f")}}'),
                        'GFGammaGValue': (
                            f'{sr_fit["GammaG_Value"]:{data_set.get("GFGammaGFormat", ".2f")}}'),
                        'GFSDValue': (
                            f'{sr_fit["GammaG_Value"]/(2*np.sqrt(2*np.log(2))):{data_set.get("GFGammaGFormat", ".2f")}}'),
                        }
                else:
                    popt = None
                    sr_fit = {}

                if data_set.get('NormHist', False):
                    if data_set.get('NormHistFixed') is not None:
                        hist_norm = data_set.get('NormHistFixed')
                    else:
                        hist_norm = (
                            sr_fit.get('Amp_Value', np.nan) if data_set['NormHistWGaussFit']
                            else np.max(weighted_hist))
                else:
                    hist_norm = 1.

                # Fit chi^2 distribution
                if data_set['ShowRedChiSqDist']:

                    dof = data_set.get(
                        'Chi2DOF', 1)

                    # Get RCS dist
                    rcs_mode = x_bins[np.argmax(weighted_hist)]
                    if data_set['FitChi2Shift']:
                        rcs_shift = rcs_mode-1
                        fit_func_rcs = myFit.get_fixed_fit_func(
                            myFit.fit_funcs['RedChiSqShifted'],
                            {'RCSDOF': dof})
                        amp_0 = fit_func_rcs['Func'](
                            1+rcs_shift, 1, rcs_shift)
                        pstart = [np.nan, rcs_shift]
                    elif data_set.get('ShiftChi2ToMode', False):
                        rcs_shift = rcs_mode-1
                        fit_func_rcs = myFit.get_fixed_fit_func(
                            myFit.fit_funcs['RedChiSqShifted'],
                            {'RCSShift': rcs_shift, 'RCSDOF': dof})
                        amp_0 = fit_func_rcs['Func'](
                            1+rcs_shift, 1)
                        pstart = [np.nan]
                    else:
                        rcs_shift = 0.
                        fit_func_rcs = myFit.get_fixed_fit_func(
                            myFit.fit_funcs['RedChiSq'],
                            {'RCSDOF': dof})
                        amp_0 = fit_func_rcs['Func'](
                            1+rcs_shift, 1)
                        pstart = [np.nan]
                    if amp_0 == 0. or hist_norm == 0.:
                        pstart[0] = np.nan
                    else:
                        pstart[0] = np.max(weighted_hist)/hist_norm/amp_0

                    # Fit amplitude and shift
                    fit_result_rcs, error_msg = myFit.do_fit(
                        fit_func_rcs,
                        x_bins, weighted_hist, np.ones(len(x_bins)),
                        pstart=pstart)
                    sr_rcs_fit = myFit.fit_result_to_dict(
                        fit_result_rcs, fit_func_rcs)
                    if data_set['FitChi2Shift']:
                        rcs_shift = sr_rcs_fit['RCSShift_Value']

                    param_label_subs_rcs = {
                        'RCSShiftValue': (
                            f'{rcs_shift:{data_set.get("RCSShiftFormat", ".2f")}}'),
                        }
                    param_label_subs = {
                        **param_label_subs,
                        **param_label_subs_rcs}

                else:
                    fit_func_rcs = None
                    fit_result_rcs = None
                    sr_rcs_fit = {}
                    rcs_shift = None

                if data_set.get('ShowHist', True):
                    if data_set['Orientation'] == 'H':
                        bar_func = ax.barh
                    else:
                        bar_func = ax.bar
                    h_hist = bar_func(
                        x_bins,
                        (
                            weighted_hist/hist_norm if hist_norm != 0.
                            else np.full(len(weighted_hist), np.nan)),
                        np.ptp(x_bins)/(len(x_bins)-1),
                        **plts.get_hist_params(**{
                            **plot_params.get('HistParams', {}),
                            **ax_params.get('HistParams', {}),
                            **data_set.get('HistParams', {})
                            }))
                    if data_set.get('Label') is not None:
                        label_id = f'{i_ax:d}D{data_set_id}Hist'
                        leg_handles[label_id] = h_hist[0]
                        leg_labels[label_id] = (
                            pyha.util.plot.do_subs(label, param_label_subs))
                        leg_sort[label_id] = data_set.get('LegSort', 0)

                # Plot Gaussian fit
                if data_set['ShowGaussFit']:
                    x_plot = np.linspace(
                        np.min(x_bins), np.max(x_bins), 1000)
                    if data_set['Orientation'] == 'H':
                        x_orient = fit_func['Func'](x_plot, *popt)/hist_norm
                        y_orient = x_plot
                    else:
                        x_orient = x_plot
                        y_orient = fit_func['Func'](x_plot, *popt)/hist_norm
                    h_gauss_fit = ax.plot(
                        x_orient,
                        y_orient,
                        **plts.get_line_params(**{
                            **plot_params.get('GaussFitParams', {}),
                            **data_set.get('GaussFitParams', {})
                            }))
                    if data_set.get('LabelGaussFit') is not None:
                        label_id = f'{i_ax:d}D{data_set_id}HistGaussFit'
                        leg_handles[label_id] = h_gauss_fit[0]
                        leg_labels[label_id] = (
                            pyha.util.plot.do_subs(
                                data_set['LabelGaussFit'], param_label_subs))
                        leg_sort[label_id] = data_set.get('LegSort', 0)

                # Show chi^2 distribution
                if data_set['ShowRedChiSqDist']:

                    dof = data_set.get(
                        'Chi2DOF', 1)
                    if data_set.get('Chi2ConfidenceLevel') is not None:
                        chi2_confidence_level = data_set.get(
                            'Chi2ConfidenceLevel')
                        x_rcs = np.linspace(
                            scipy.stats.chi2.ppf(chi2_confidence_level, dof)/dof+rcs_shift,
                            scipy.stats.chi2.ppf(1-chi2_confidence_level, dof)/dof+rcs_shift,
                            1000
                            )
                    else:
                        x_rcs = np.linspace(
                            np.min(x_bins), np.max(x_bins), 1000)

                    y_rcs = fit_func_rcs['Func'](
                        x_rcs, *fit_result_rcs['Popt'])

                    if data_set['Orientation'] == 'H':
                        x_orient = y_rcs
                        y_orient = x_rcs
                    else:
                        x_orient = x_rcs
                        y_orient = y_rcs

                    h_rcs_dist = ax.plot(
                        x_orient,
                        y_orient/hist_norm,
                        **plts.get_line_params(**{
                            **plot_params.get('RCSDistParams', {}),
                            **data_set.get('RCSDistParams', {})
                            }))
                    if data_set.get('LabelRCS') is not None:
                        label_id = f'{i_ax:d}D{data_set_id}HistRCS'
                        leg_handles[label_id] = h_rcs_dist[0]
                        leg_labels[label_id] = (
                            pyha.util.plot.do_subs(
                                data_set['LabelRCS'], param_label_subs))
                        leg_sort[label_id] = data_set.get('LegSort', 0)

                # Set labels
                label_orient_key = (
                    'CustomYLabel' if data_set['Orientation'] == 'H' else 'CustomXLabel')
                if ax_params['XLabelType'] == 'Dataset':
                    if x_label_dataset is not None:
                        ax_params[label_orient_key] = x_label_dataset
                    if data_set.get('CustomXLabel') is not None:
                        ax_params[label_orient_key] = pyha.util.plot.do_subs(
                            data_set[label_orient_key], param_label_subs)
                label_orient_key = (
                    'CustomXLabel' if data_set['Orientation'] == 'H' else 'CustomYLabel')
                if ax_params.get(label_orient_key) is None:
                    if data_set.get('NormHist', False):
                        ax_params[label_orient_key] = 'Norm. occurences'
                    else:
                        ax_params[label_orient_key] = 'Occurences'

            if data_set['PlotType'] != 'Hist':

                # Set labels
                if ax_params['XLabelType'] == 'Dataset':
                    if x_label_dataset is not None:
                        ax_params['CustomXLabel'] = x_label_dataset
                    if data_set.get('CustomXLabel') is not None:
                        ax_params['CustomXLabel'] = pyha.util.plot.do_subs(
                            data_set['CustomXLabel'], param_label_subs)
                if ax_params['YLabelType'] == 'Dataset':
                    if y_label_dataset is not None:
                        ax_params['CustomYLabel'] = y_label_dataset
                    if data_set.get('CustomYLabel') is not None:
                        ax_params['CustomYLabel'] = pyha.util.plot.do_subs(
                            data_set['CustomYLabel'], param_label_subs)

            plot_params[f'AxParams{i_ax:d}'] = ax_params

        self.plot_params = plot_params
        self.set_axs_params(
            leg_handles=leg_handles,
            leg_labels=leg_labels,
            leg_sort=leg_sort,
            )

        self.set_fig_params()

        if return_fit_results:
            return fit_results
