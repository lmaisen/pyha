# -*- coding: utf-8 -*-
"""
Created on Mon Aug 17 19:26:49 2020

@author: Lothar Maisenbacher/MPQ

Plot generator for various analysis plots.
"""

import numpy as np
from pathlib import Path

# pyha
import pyha.def_params
import pyha.def_latex
import pyha.plot
import pyha.exp_analysis

import pyha.plts as plts
plts.set_params()

# Latex
ltx = pyha.def_latex.defs

def plot_delays(dfScansFitParams=None, analysis_params=None,
                super_figs=None, super_sets=None,
                DataClass=None,
                plot_params=None):
    """
    Generate plots of line scan fit results vs delays for
    various fit parameters and the corresponding simulation data.
    """
    super_set_figs = {}
    super_set_fit_results = {}
    for super_fig_id, super_fig in super_figs.items():

        ind_delays_delay_ids = super_fig['DelayIDs']
        sim_ids = ['BigModel', 'LFS']

        # Data super sets for this figure
        super_sets_fig = {
            key: elem for key, elem in super_sets.items()
            if elem['FigID'] == super_fig_id}

        num_rows = np.max([elem['Axes'] for _, elem in super_sets_fig.items()])+1
        num_axes = num_rows
        if plot_params is None:
            plot_params = {}
        plot_params_super_set = {
            'YLimPadding': [0.1, 0.1],
            'YWAvgParams': {'alpha': 0.5},
            'LFUncertParams': {'alpha': 0.3},
            'ParamIDX': 'dfDelayVels_V_Mean_Value',
            **plot_params,
            'FigName': super_fig['FigName'],
            'FigSize': (
                super_fig.get('FigSize', plot_params.get('FigSize', [plts.pagewidth*1, 7]))),
            'SaveSuffix': super_fig.get('SaveSuffix'),
            'NumAxes': num_axes,
            'NumRows': num_rows,
            }

        param_id_x = plot_params_super_set['ParamIDX']

        # Set up axis
        height_ratios = {
            elem['Axes']: elem.get('HeightRatio', 1)
            for _, elem in super_sets_fig.items()}
        ax_params = {
            elem['Axes']: elem.get('AxParams', {})
            for _, elem in super_sets_fig.items()}
        for i_axes in range(num_axes):
            plot_params_super_set[f'AxParams{i_axes:d}'] = {
                'YLabelType': 'Dataset',
                'XLabelType': 'Dataset',
                'ShareX': i_axes != 0,
                'ShareXWith': 0 if i_axes != 0 else None,
                'XLim': analysis_params.get('XLim', [0, 265]),
                'HeightRatio': height_ratios.get(i_axes, 1.),
                **ax_params.get(i_axes, {}),
                }

        # Set up x-axis label and ticks
        i_ax_x_label = num_rows-1
        plot_params_super_set[f'AxParams{i_ax_x_label:d}'] = {
            'XLabelType': 'Dataset',
            'RemoveXTickLabels': False,
            **plot_params_super_set.get(f'AxParams{i_ax_x_label:d}', {}),
            }

        # Plot
        data_sets = {}
        mcs_channel_ids = analysis_params['MCSchannelIDs']
        for i_super_set, (super_set_id, super_set) in enumerate(super_sets_fig.items()):

            super_set = {
                'ShowExpData': True,
                'UseSDUncert': False,
                'ScaleUncertByRCS': False,
                'NormSimRatio': False,
                **super_set,
                }

            i_ax = super_set['Axes']
            param_id_y = super_set['ParamID']
            param_row = DataClass.dfDataFormat.loc[param_id_y]
            param_has_uncert = param_row['SigmaName'] != ''

            for i_cs_channel, mcs_channel_id in enumerate(mcs_channel_ids):

                detector = pyha.def_params.detectors_mcs[mcs_channel_id]

                (x_w_avgs, w_avgs, y_sigma, y_sim, y_sd_sim,
                 w_avgs_ratio_exp_sim, y_sigma_ratio_exp_sim, param_has_uncert) = (
                    pyha.exp_analysis.get_scan_averaged_delay_results(
                        dfScansFitParams, analysis_params,
                        super_set, DataClass.dfDataFormat, mcs_channel_id,
                        ind_delays_delay_ids, sim_ids, param_id_x,
                        param_id_weights=analysis_params.get('ParamIDWeights')))

                if ((not param_has_uncert and not super_set['UseSDUncert'])
                        or super_set.get('RemoveYSigma', False)):
                    y_sigma = None

                y_prefix_exp = super_set.get(
                    'YPrefixExp', param_row['PrefixExp'])
                number_format = super_set.get(
                    'YNumberFormat', param_row['NumberFormat'])
                num_sig_uncert_digits = super_set.get(
                    'NSigUncertDigits', param_row['NSigUncertDigits'])
                # Show weighted average
                if param_row['ShowUncert']:
                    label_mean = '${YWAvgCompactUncert}${YMeanUnitSpaced}'
                else:
                    label_mean = '${YWAvgMean}${YMeanUnitSpaced}'

                data_set_ = {
                    'PlotType': 'XY',
                    'BasisParams': 'Errorbar',
                    'Source': 'Custom',
                    'Axes': i_ax,
                    'ParamX': param_id_x,
                    'ParamY': param_id_y,
                    'XData': x_w_avgs['Mean'],
                    'LabelMeanFormat': number_format,
                    'YPrefixExp': y_prefix_exp,
                    'LinearFit': True,
                    'WAvgCompactUncertSigErrDigits': num_sig_uncert_digits,
                    'WAvgRCSCompactUncertSigErrDigits': 2,
                    'LFSlopeCompactUncertSigErrDigits': num_sig_uncert_digits,
                    'LFOffsetCompactUncertSigErrDigits': num_sig_uncert_digits,
                    'LFOffsetFormat': number_format,
                    'LFSlopeFormat': super_set.get('LFSlopeFormat', number_format),
                    'LFRCSCompactUncertSigErrDigits': 2,
                    'CustomYLabel': (
                        super_set.get('CustomYLabel', '${YSymbol}${YUnitPar}')),
                    'CompactUncertExpNot': number_format.endswith('e'),
                    **super_set.get('Dataset', {}),
                    }

                if super_set['ShowExpData']:
                    data_sets[f'S{i_super_set}C{mcs_channel_id}'] = {
                        **data_set_,
                        'YData': w_avgs.get(super_set.get('YWAvgKey', 'Mean')),
                        'YSigma': y_sigma,
                        'Label': super_set.get(
                            'Label',
                            f'{detector["ShortName"]}'),
                        'LabelYWAvg': super_set.get(
                            'LabelYWAvg',
                            (
                            'Avg.: '
                            + label_mean
                            + ', ' + ltx['chiSqRed'] + ' = '
                            + '${YWAvgRCSCompactUncert}'
                            )),
                        'LabelYWAvgMean': super_set.get(
                            'LabelYWAvgMean',
                            (
                            'Avg.: '
                            + label_mean
                            + ', ' + ltx['chiSqRed'] + ' = '
                            + '${YWAvgRCSCompactUncert}'
                            )),
                        'LabelLF': super_set.get(
                            'LabelLF',
                            (
                            'Extrapol.: '
                            '${LFSlopeCompactUncert}${SlopeUnitSpaced}'
                            + ', ${LFOffsetCompactUncert}${YUnitSpaced}'
                            + ',\n' + ltx['chiSqRed'] + ' = '
                            + '${LFRCSCompactUncert}'
                            )),
                        'ShowData': super_set.get('ShowData', True),
                        'ShowYWAvg': super_set.get('ShowYWAvg', False),
                        'ShowYWAvgMean': super_set.get('ShowYWAvgMean', False),
                        'ShowLinearFit': super_set.get('ShowLinearFit', False),
                        'ShowLinearFitUncert': super_set.get('ShowLinearFitUncert', False),
                        'XYParams': {
                            **detector['PointParams'],
                            },
                        'LinearFitParams': {'color': detector['PointParams'].get('color')},
                        }

                for sim_id in sim_ids:
                    if not super_set.get(f'ShowSim{sim_id}', False):
                        continue

                    simulation = pyha.def_params.simulations[sim_id]

                    data_set_id = f'S{i_super_set}C{mcs_channel_id}Sim{sim_id}'
                    data_sets[data_set_id] = {
                        **data_set_,
                        'YData': y_sim[sim_id],
                        'YSigma': y_sigma,
                        'Label': (
                            f'{simulation["ShortName"]}, {detector["ShortNameLC"]}'),
                        'LabelYWAvg': (
                            'Exp. weighted avg.: ${YWAvgMean}${YMeanUnitSpaced}'
                            ),
                        'LabelYWAvgMean': (
                            'Exp. weighted avg.: ${YWAvgMean}${YMeanUnitSpaced}'
                            ),
                        'LabelLF': (
                            'Exp. weighted lin. fit.:\n'
                            + '${LFSlopeMean}${SlopeUnitSpaced}'
                            + ', ${LFOffsetMean}${YUnitSpaced}'
                            ),
                        'ShowData': super_set.get(f'ShowDataSim{sim_id}', True),
                        'ShowYWAvg': super_set.get(f'ShowYWAvgSim{sim_id}', False),
                        'ShowYWAvgMean': super_set.get(f'ShowYWAvgMeanSim{sim_id}', False),
                        'ShowLinearFit': super_set.get(f'ShowLinearFitSim{sim_id}', False),
                        'ShowLinearFitUncert': super_set.get(
                            f'ShowLinearFitUncertSim{sim_id}', False),
                        'XYParams': {
                            **detector['PointParams'],
                            **simulation['PointParams'],
                            },
                        'YWAvgMeanParams': {
                            'linestyle': ':',
                            'alpha': 1,
                            },
                        'ShowYErr': False,
                        }
                    if not super_set['ShowExpData']:
                        data_sets[data_set_id] = {
                            **data_sets[data_set_id],
                            'CustomYLabel': (
                                simulation['ShortName'] + ' ${YSymbol}${YUnitPar}'),
                            }
                    if super_set.get(f'ShowSimRatio{sim_id}', False):
                        if super_set['NormSimRatio']:
                            norm = np.mean(w_avgs_ratio_exp_sim[sim_id]['Mean'])
                        else:
                            norm = 1
                        data_sets[data_set_id] = {
                            **data_sets[data_set_id],
                            'YPrefixExp': 1,
                            'YData': w_avgs_ratio_exp_sim[sim_id]['Mean']/norm,
                            'YSigma': y_sigma_ratio_exp_sim[sim_id]/norm,
                            'YBaseUnit': '',
                            'CustomYLabel': (
                                'Ratio ${YSymbol}${YUnitPar} exp./'+simulation['Abbr']+' sim.'
                                + (' (norm.)' if super_set['NormSimRatio'] else '')
                                ),
                            'LabelMeanFormat': '.3f',
                            'XYParams': {
                                **detector['PointParams'],
                                },
                            'ShowYErr': True,
                            }

                    if super_set.get('DumpData', False):
                        filename = (
                            super_set.get('DumpDataPrefix', '')
                            +', '.join(analysis_params['DataGroupIDs'])
                            +f' S{super_set_id}C{mcs_channel_id}Sim{sim_id}'
                            )
                        filedir = Path(super_set.get('DumpDataDir', ''))
                        if not filedir.exists():
                            filedir.mkdir(parents=True)
                        np.savez(
                            Path(filedir, filename),
                            **{
                                'XData': x_w_avgs['Mean'],
                                'YData': y_sim[sim_id],
                                'YSigma': y_sigma,
                                })

            if super_set.get('ShowHLine', False):
                data_sets[f'S{i_super_set}C{mcs_channel_id}HLine'] = {
                    **data_set_,
                    'PlotType': 'HLine',
                    'XData': None,
                    'YData': super_set['HLineY'],
                    'HLineParams': {
                        'color': '0.3', 'linewidth': 1, 'linestyle': '--',
                        **super_set.get('HLineParams', {}),
                        },
                    'Label': super_set.get('HLineLabel'),
                    }

        super_set_figs[super_fig_id] = pyha.plot.PlotGen(
            plot_params_super_set, data_sets=data_sets, DataClass=DataClass)
        super_set_fit_results[super_fig_id] = super_set_figs[super_fig_id].gen_plot(
            return_fit_results=True)

    return super_set_figs, super_set_fit_results
