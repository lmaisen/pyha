# -*- coding: utf-8 -*-
"""
Created on Sat Jun 27 10:46:01 2020

@author: Lothar Maisenbacher/MPQ

Functions extending `pyh.data.pyh_data`.
"""

# pyhs
import pyhs.gen

# pyha
import pyha.util.data

logger = pyhs.gen.get_command_line_logger()

def get_data_preview_scan_sd(DataClass):
    """
    Group data in data point DataFrame `dfData` by line scans and find standard
    deviation of values of line scans.
    """
    groupColumn = 'ScanUID'
    dfData =  DataClass.dfsPreview['dfData']
    dfDataScanGrouped = dfData[dfData['Scan']].groupby(by=groupColumn, sort=False)

    dfDataScanSD = dfDataScanGrouped.std(numeric_only=True)
    dfDataScanSD[groupColumn] = dfDataScanSD.index

    sourcesToAdd = []
    # Merge dfScans
    sourceToAdd = 'dfScans'
    sourcesToAdd.append(sourceToAdd)
    dfToAdd = DataClass.dfsPreview[sourceToAdd]
    dfAll_scans_sd = dfDataScanSD.rename(columns=lambda x: 'dfData'+'_'+x). \
        merge(
            dfToAdd.rename(columns=lambda x: sourceToAdd+'_'+x),
            how='left', left_index=True, right_index=True
        )

    # Fill na values with default value and ensure correct Dtype of columns
    mask_data_format = (
        (DataClass.dfDataFormat['Source'].isin(sourcesToAdd))
        & (~DataClass.dfDataFormat['IsIndex']))
    for _, row in DataClass.dfDataFormat[mask_data_format].iterrows():
        dfAll_scans_sd[row['Source']+'_'+row['Name']] = (
            dfAll_scans_sd[row['Source']+'_'+row['Name']].fillna(
                pyha.util.data.DATA_FORMAT_DTYPES[row['Dtype']](row['DefaultValue'])))
        dfAll_scans_sd[row['Source']+'_'+row['Name']] = (
            pyha.util.data.cast_series_to_data_format(
                dfAll_scans_sd[row['Source']+'_'+row['Name']], row['Dtype']))

    return dfAll_scans_sd
