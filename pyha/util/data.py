# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ

Various functions used to convert or cast data to various dtypes or structures.
There are two main ways in which data format metadata is handled
throughout the family of pyh packages. From this data format metadata,
DataFrames can be constructed with well-defined columns with a fixed dtype.
The first way is a DataFrame,
typically called `dfDataFormat`, which each row describing a data format,
e.g. its dtype, default value, default output/print format, unit, ...
This is e.g. used to describe DataFrames containing experimental data.
The second, leaner way is a dictionary, typically called `df_format`,
which contains less metadata, e.g. no units or output data format.
This is e.g. used to describe DataFrames containing experimental and simulation metadata.
"""

import base64
from collections import OrderedDict
import numpy as np
import pandas as pd
import json
import datetime

# pyhs
import pyhs.gen

logger = pyhs.gen.get_command_line_logger()

# Python dtypes corresponding to string entires in DataFrame `dfDataFormat`,
# which contains data format metadata.
DATA_FORMAT_DTYPES = {
    'Timestamp': pd.Timestamp,
    'Str': str,
    'Int': int,
    'Bool': bool,
    'Float': float,
    }
DATA_FORMAT_DTYPES_DEFAULT_VALUE = {
    'Timestamp': pd.Timestamp(np.nan),
    'Str': '',
    'Int': -1,
    'Bool': False,
    'Float': np.nan,
    }

class MyEncoder(json.JSONEncoder):
    """
    Encoder to make certain data types (numpy, pd.Timestamp) JSON-serializable by casting to a
    serializable native Python data type.
    """
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        elif isinstance(obj, np.bool_):
            return bool(obj)
        elif isinstance(obj, (pd.Timestamp, datetime.datetime)):
            return str(obj)
        else:
            return super(MyEncoder, self).default(obj)

def dictToDfRow(dict_):
    return pd.DataFrame.from_dict([dict_])

def dfRowToDict(df, iloc=0):
    return {column: df[column].iloc[iloc] for column in df.columns}

def orderedDictToDf(dict_):
    return pd.DataFrame.from_dict(dict_, orient='index').sort_values(by='IID')

def dfToOrderedDict(df):
    return OrderedDict(df.to_dict(orient='index'))

@np.deprecate(new_name='add_iid_to_dict_of_dicts')
def addIIDToDictofDicts(dict_, iid_key='IID'):
    for i, (key, dict__) in enumerate(dict_.items()):
        dict_[key] = {**dict__, iid_key: i}
    return dict_

def add_iid_to_dict_of_dicts(dict_, iid_key='IID'):
    """
    Add integer ID (IID) to each dict in dict `dict_`, using key `iid_key` (str), starting from 0
    and in the order of the elements in dict `dict_`.
    """
    for i, (key, dict__) in enumerate(dict_.items()):
        dict_[key] = {**dict__, iid_key: i}
    return dict_

def cast_df_to_data_format(dfToCast, source, dfDataFormat,
                           undefined_columns_warning=True):
    """
    Cast DataFrame `dfToCast` of type `source` (str) to data format as defined
    in data format metadata `dfDataFormat` (DataFrame).
    Missing columns will be added and filled with default values.
    Missing values in existing columns will be filled with default values.
    The dtype of all columns will be cast to the specified dtype.
    If `undefined_columns_warning` is set to True, a warning will be issued
    if `dfToCast` contains columns not described in `dfDataFormat`.
    """
    mask = (dfDataFormat['Source'] == source) & (~dfDataFormat['IsIndex'])
    # Make sure all required columns are present, adding empty (`np.nan`) ones if not.
    # Columns not defined in the default DataFrame are kept, but
    # ordered after the default columns.
    columns = dfDataFormat[mask]['Name'].values
    dfToCast = dfToCast.reindex(
        columns=[
            *columns,
            *dfToCast.columns[~np.in1d(dfToCast.columns, columns)]],
        fill_value=np.nan)
    for _, row in dfDataFormat[mask].iterrows():
        dfToCast[row['Name']] = dfToCast[row['Name']].fillna(
            DATA_FORMAT_DTYPES[row['Dtype']](row['DefaultValue']))
        dfToCast[row['Name']] = cast_series_to_data_format(
            dfToCast[row['Name']], row['Dtype'])
    mask_columns = np.isin(dfToCast.columns, dfDataFormat[mask]['Name'])
    undefined_columns = dfToCast.columns[~mask_columns]
    if undefined_columns_warning and (len(undefined_columns) > 0):
        logger.warning(
            'DataFrame to be cast (type \'%s\') contains columns '
            + 'not defined in data format (%s)',
            source, ", ".join(undefined_columns)
            )
    mask_index = (dfDataFormat['Source'] == source) & (dfDataFormat['IsIndex'])
    dfToCast.index = dfToCast.index.astype(
        DATA_FORMAT_DTYPES[dfDataFormat[mask_index].iloc[0]['Dtype']])
    dfToCast.index.name = dfDataFormat[mask_index].iloc[0]['Name']
    return dfToCast

@np.deprecate(new_name='cast_df_to_data_format')
def castDfToDataFormat(*args, **kwargs):

    return cast_df_to_data_format(*args, **kwargs)

def cast_series_to_data_format(series, dtype_id):
    """
    Cast `series` (Pandas series) to dtype identified with `dtype_id` (str),
    with the Python dtypes corresponding to each `dtype_id` specified in
    constant DATA_FORMAT_DTYPES.
    """
    dtype = DATA_FORMAT_DTYPES[dtype_id]
    if dtype is pd.Timestamp:
        series = pd.to_datetime(series)
    else:
        series = series.astype(dtype)
    return series

@np.deprecate(new_name='cast_series_to_data_format')
def castSeriesToDataFormat(*args, **kwargs):

    return cast_series_to_data_format(*args, **kwargs)

def init_df_with_data_format(source, dfDataFormat, indices=None):
    """
    Initialize empty DataFrame of type `source` (str) as defined in
    data format metadata `dfDataFormat` (DataFrame).
    If `indices` (array) is supplied, rows for each index in `indicies` will
    be added, each filled with the default value of each column.
    """
    if indices is None:
        indices = []
    mask = (dfDataFormat['Source'] == source) & (~dfDataFormat['IsIndex'])
    columns = dfDataFormat[mask]['Name'].values
    dfToInit = pd.DataFrame(columns=columns, index=indices)
    for _, row in dfDataFormat[mask].iterrows():
        if len(indices) > 0:
            dfToInit[row['Name']] = (
                DATA_FORMAT_DTYPES[row['Dtype']](row['DefaultValue']))
        dfToInit[row['Name']] = cast_series_to_data_format(
            dfToInit[row['Name']], row['Dtype'])
    mask = (dfDataFormat['Source'] == source) & (dfDataFormat['IsIndex'])
    dfToInit.index = dfToInit.index.astype(
        DATA_FORMAT_DTYPES[dfDataFormat[mask].iloc[0]['Dtype']])
    dfToInit.index.name = dfDataFormat[mask].iloc[0]['Name']
    return dfToInit

@np.deprecate(new_name='init_df_with_data_format')
def initDfWithDataFormat(*args, **kwargs):

    return init_df_with_data_format(*args, **kwargs)

def get_df_columns_from_data_format(source, dfDataFormat):
    """
    Return array of column names, excluding the index column,
    of DataFrame of type `source` (str) as defined in
    data format metadata `dfDataFormat` (DataFrame).
    """
    mask = (dfDataFormat['Source'] == source) & (~dfDataFormat['IsIndex'])
    return dfDataFormat[mask]['Name'].values

def get_empty_data_format():
    """
    Get empty DataFrame `dfDataFormat`,
    which is a container for data format metadata.
    """
    dfDataFormat = pd.DataFrame(
        columns=['DataType', 'DefaultValue', 'Dtype', 'LongName', 'Name', 'Source',
           'Unit', 'Symbol', 'IsIndex', 'Plot', 'SigmaName', 'SourceType',
           'MultipleChannels', 'Ordering', 'PlotScaling', 'DefaultX', 'DefaultY',
           'FitFuncID', 'FitParamID', 'FitDerivID', 'FitDerivParamID',
           'PrefixExp', 'NumberFormat', 'ShowUncert', 'NSigUncertDigits',
           ]
        )

    return dfDataFormat

def cast_data_format(dfDataFormat):
    """
    Cast columns into correct dtype and fill empty columns
    with default values for DataFrame `dfDataFormat`,
    containing data format metadata.
    """
    # Make sure all required columns are present, adding empty ones if not.
    # Columns not defined in the default DataFrame (see `get_empty_data_format`) are kept, but
    # ordered after the default columns.
    dfDataFormat = dfDataFormat.reindex(
        columns=[
            *get_empty_data_format().columns,
            *dfDataFormat.columns[
                ~np.in1d(dfDataFormat.columns, get_empty_data_format().columns)]],
        fill_value=None)

    dfDataFormat['Ordering'] = dfDataFormat['Ordering'].fillna(
        dfDataFormat['Ordering'].max() + 1)
    dfDataFormat['IsIndex'] = dfDataFormat['IsIndex'].fillna(False)
    dfDataFormat['IsIndex'] = dfDataFormat['IsIndex'].astype(bool)
    dfDataFormat['SigmaName'] = dfDataFormat['SigmaName'].fillna('')
    dfDataFormat['PlotScaling'] = dfDataFormat['PlotScaling'].fillna(1.)
    dfDataFormat['DefaultX'] = dfDataFormat['DefaultX'].fillna(False)
    dfDataFormat['DefaultX'] = dfDataFormat['DefaultX'].astype(bool)
    dfDataFormat['DefaultY'] = dfDataFormat['DefaultY'].fillna(False)
    dfDataFormat['DefaultY'] = dfDataFormat['DefaultY'].astype(bool)
    dfDataFormat['Plot'] = dfDataFormat['Plot'].fillna(True)
    dfDataFormat['Plot'] = dfDataFormat['Plot'].astype(bool)
    dfDataFormat['MultipleChannels'] = dfDataFormat['MultipleChannels'].fillna(False)
    dfDataFormat['MultipleChannels'] = (
        dfDataFormat['MultipleChannels'].astype(bool))
    dfDataFormat['FitFuncID'] = dfDataFormat['FitFuncID'].fillna('')
    dfDataFormat['FitParamID'] = dfDataFormat['FitParamID'].fillna('')
    dfDataFormat['FitDerivID'] = dfDataFormat['FitDerivID'].fillna('')
    dfDataFormat['FitDerivParamID'] = dfDataFormat['FitDerivParamID'].fillna('')
    dfDataFormat['Symbol'] = dfDataFormat['Symbol'].fillna('')
    dfDataFormat['PrefixExp'] = dfDataFormat['PrefixExp'].fillna(1.)
    dfDataFormat['NumberFormat'] = dfDataFormat['NumberFormat'].fillna('.1f')
    dfDataFormat['ShowUncert'] = dfDataFormat['ShowUncert'].fillna(True)
    dfDataFormat['ShowUncert'] = dfDataFormat['ShowUncert'].astype(bool)
    dfDataFormat['NSigUncertDigits'] = dfDataFormat['NSigUncertDigits'].fillna(1)
    dfDataFormat['NSigUncertDigits'] = (
        dfDataFormat['NSigUncertDigits'].astype(int))

    return dfDataFormat

def drop_duplicates_and_reindex_data_format(dfDataFormat):
    """
    Drop duplicate entries on columns `source` and `name`, set index, and sort
    DataFrame `dfDataFormat`, containing data format metadata.
    """
    dfDataFormat.drop_duplicates(
        subset=['Source', 'Name'], keep='last', inplace=True)
    dfDataFormat.loc[:, 'Source_Name'] = dfDataFormat.apply(
        lambda row: f'{row["Source"]}_{row["Name"]}', axis=1)
    dfDataFormat.set_index(['Source_Name'], inplace=True)
    dfDataFormat = dfDataFormat.sort_values(by='Ordering')

    return dfDataFormat

def load_data_format_from_file(filepath):
    """
    Load data format metadata from JSON file `filepath` and convert to DataFrame `dfDataFormat`.
    """
    with open(filepath, 'r') as f:
        dfDataFormat_dict = json.load(f)
    logger.info(
        'Read data format metadata from file \'%s\'', filepath)

    dfDataFormat = pd.DataFrame.from_dict(dfDataFormat_dict, orient='index')
    dfDataFormat.index.name = 'Source_Name'

    return dfDataFormat

def get_default_value(dfDataFormat, data_format_id):
    """
    Get default value for data format with ID `data_format_id` stored in
    data format metadata DataFrame `dfDataFormat`.
    """
    row = dfDataFormat.loc[data_format_id]
    return DATA_FORMAT_DTYPES[row['Dtype']](row['DefaultValue'])

def cast_df_to_data_format_dict(df, df_format):
    """
    Cast DataFrame `df` into format given in DataFrame format dictionary
    `df_format`, which defines the data format of all columns and which
    column to use as index.
    """
    # Make sure that index name is set according to data format
    df = set_df_index_name(df, df_format)

    # Set index as column for casting
    if df_format['IndexName'] in df.columns:
        df.drop(columns=df_format['IndexName'], inplace=True)
    df = df.reset_index()

    # Add missing columns, keeping ordering of columns already in DataFrame
    # and adding missing columns at end of existing columns
    columns = np.array(get_columns_in_data_format_dict(df_format))
    df = df.reindex(
        columns=[
            *df.columns,
            *columns[~np.in1d(columns, df.columns)]],
        fill_value=np.nan)

    # Add default values, adapt data format
    for column in df_format['ColumnsFloat']:
        df[column] = df[column].fillna(np.nan)
    for column in df_format['ColumnsStr']:
        df[column] = df[column].fillna('')
    for column in df_format['ColumnsBool']:
        df[column] = df[column].fillna(False)
    for column in df_format['ColumnsInt']:
        df[column] = df[column].fillna(-1)
    df[df_format['ColumnsFloat']] = (
        df[df_format['ColumnsFloat']].astype(float))
    if len(df_format['ColumnsStr']) > 0:
        df[df_format['ColumnsStr']] = (
            df[df_format['ColumnsStr']]
            .astype(str))
    if len(df_format['ColumnsBool']) > 0:
        df[df_format['ColumnsBool']] = (
            df[df_format['ColumnsBool']]
            .astype(bool))
    df[df_format['ColumnsInt']] = (
        df[df_format['ColumnsInt']].astype(int))
    df[df_format['ColumnsDate']] = (
        df[df_format['ColumnsDate']].astype('datetime64[ns]'))

    df = df.set_index(
        df_format['IndexName'],
        drop=not df_format.get('KeepIndexAsColumn', False))

    return df

def get_columns_in_data_format_dict(df_format):
    """
    Get list of all columns contained in DataFrame format
    dictionary `df_format`.
    """
    columns_ = [
        df_format[key] for key in [
            'ColumnsStr', 'ColumnsFloat', 'ColumnsBool',
            'ColumnsInt', 'ColumnsDate', 'ColumnsGen']]
    return [elem for l in columns_ for elem in l]

def set_df_index_name(df, df_format):
    """
    Set name of index of DataFrame `df` as specified in DataFrame format
    dictionary `df_format`.
    """
    df.index.name = df_format['IndexName']
    return df

def drop_duplicates_and_reindex(df, df_format, subset=None):
    """
    Drop duplicate entries of DataFrame `df`, using DataFrame format
    dictionary `df_format`.
    If entry 'SetNumericalIndex' of the format is True, the index is reset to
    numerical values.
    Use `subset` to supply a list of columns which will be used to identify duplicates.
    If `subset` is set to None, the default list as given by entry 'DuplicateSubset' of
    the DataFrame format dictionary. If set to an empty list, all columns will be used.
    """
    # Drop duplicates and set numerical index if specified
    if subset is None:
        subset = df_format.get(
            'DuplicatesSubset', None)
    if isinstance(subset, list) and len(subset) == 0:
        subset = None
    if df_format.get('SetNumericalIndex', False):
        df = df.drop_duplicates(subset=subset, ignore_index=True, inplace=False)
        df = set_df_index_name(df, df_format)
    else:
        if (keep_index_as_column := df_format.get('KeepIndexAsColumn', False)):
            df = df.drop(columns=df_format['IndexName'], inplace=False)
        if df_format.get('UniqueIndex', False):
            df = df[~df.index.duplicated(keep='last')]
            if keep_index_as_column:
                df[df_format['IndexName']] = df.index
        else:
            df = (
                df
                .reset_index()
                .drop_duplicates(
                    subset=subset)
                .set_index(
                    df_format['IndexName'],
                    drop=not keep_index_as_column))
    return df

def encode_b64_value(value):
    """
    From `base64.standard_b64encode` documentation:
    Encode bytes-like object `value` using the standard Base64 alphabet
    and return the encoded bytes.
    """
    return base64.standard_b64encode('{:f}'.format(value).encode())

def decode_b64_value(encoded_value):
    """
    From `base64.standard_b64decode` documentation:
    Decode bytes-like object or ASCII string `encoded_value` using the standard Base64 alphabet
    and return the decoded bytes.
    """
    return float(base64.standard_b64decode(encoded_value))

@np.deprecate
def serializeDataFormat(dfDataFormat):
    """Deprecated."""
    dfDataFormat_serial = dfDataFormat.copy()
    for dtype_id, _ in DATA_FORMAT_DTYPES.items():
        dfDataFormat_serial['DefaultValue'+dtype_id] = (
            DATA_FORMAT_DTYPES_DEFAULT_VALUE[dtype_id])
        indices = dfDataFormat[dfDataFormat['Dtype'] == dtype_id].index
        dfDataFormat_serial.loc[indices, 'DefaultValue'+dtype_id] = (
            dfDataFormat.loc[indices, 'DefaultValue'])
        dfDataFormat_serial['DefaultValue'+dtype_id] = (
            cast_series_to_data_format(
                dfDataFormat_serial['DefaultValue'+dtype_id], dtype_id))
    del dfDataFormat_serial['DefaultValue']
    return dfDataFormat_serial

def is_float(value):
    """Check if `value` is of dtype float."""
    try:
        float(value)
        return True
    except (ValueError, TypeError):
        return False

def is_int(value):
    """Check if `value` is of dtype int."""
    try:
        value_float = float(value)
        return value_float.is_integer()
    except (ValueError, TypeError):
        return False

def get_mask_unique(df, unique_param):
    """
    Mask DataFrame `df` with values of unique parameters (keys)
    in `unique_param` (dict/Series).
    """
    mask_unique = np.ones(len(df), dtype=bool)
    for column in unique_param.keys():
        if pd.isna(unique_param[column]):
            continue
        if (not isinstance(unique_param[column], str)
                and is_float(unique_param[column])
                and np.isnan(unique_param[column])):
            mask_unique &= (np.isnan(df[column]))
        else:
            mask_unique &= (df[column] == unique_param[column])
    return mask_unique

def convert_float_columns_to_str(df, columns):
    """
    Convert columns `columns` (list/array) in DataFrame `df` to string
    if column dtype is float. Returns a copy of `df`.
    This is necessary because some Pandas functions such as `groupby`
    cannot handle nan values.
    """
    df_str = df.copy()
    for column in columns:
        if column in df.columns \
        and isinstance(df[column].dtype, np.dtype) \
        and np.issubdtype(df[column].dtype, np.floating):
            df_str[column] = df_str[column].astype(str)
    return df_str

def convert_str_columns_to_float(df_str, df_template, columns):
    """
    Convert columns `columns` (list/array) in DataFrame `df` to float
    if column dtype is float in template DataFrame `df_template`.
    Returns a copy of `df`.
    This is necessary because some Pandas functions such as `groupby`
    cannot handle nan values.
    """
    df = df_str.copy()
    for column in columns:
        if column in df.columns and column in df_template.columns \
        and np.issubdtype(df_template[column].dtype, np.floating):
            df[column] = df[column].astype(float)
    return df

def value_to_str(value, quote_str=False):
    """
    Convert `value` to dtype str for output.
    If `quote_str` is set to True (default is False), `value` of dtype str
    will be enclosed in single quotation marks.
    """
    if isinstance(value, np.datetime64):
        value_str = pd.to_datetime(value).strftime('%Y-%m-%d %H-%M-%S')
    elif isinstance(value, (bool, np.bool_)):
        value_str = str(value)
    elif is_int(value):
        value_str = f'{int(value):d}'
    elif is_float(value):
        value_str = f'{float(value):.6e}'
    else:
        value_str = str(value)
        if quote_str:
            value_str = f'\'{value_str}\''
    return value_str

def str_to_list(str_, delimiter=';'):
    """
    Convert a string `str_` of multiple values delimited by str `delimiter`
    into list, returning None if input string is of zero length.
    """
    if str_ == '':
        out = None
    else:
        out = str_.split(delimiter)
    return out

def elems_to_str(elems, quote_str=True):
    """
    Convert `elems`, which can be array-like or scale, into dtype str by converting each
    entry in `elems` into dtype str, and then combining these strings
    into one comma-separated string.
    If `quote_str` is set to True (default), any entries in `elems` of dtype str
    will be enclosed in single quotation marks.
    """
    return ', '.join(
        [value_to_str(elem, quote_str) for elem in np.atleast_1d(elems)])

def dump_to_json(obj, filepath, encoder=MyEncoder, **kwargs):
    """
    Dump object `obj` to JSON file `filepath`, using `json.dump`.
    An optional encoder `encoder` can be specified (derived from `json.JSONEncoder`).
    As default, `MyEncoder` specified here is used. If set to None, the default encoder of
    `json.dump` is used.
    Additional keyword arguments are passed to `json.dump`.
    """
    with open(filepath, 'w') as f:
        json.dump(obj, f, indent=4, cls=encoder, **kwargs)
