# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import itertools
from string import Template

# pyhs
import pyhs.fit

# pyha
import pyha.util.data

myFit = pyhs.fit.Fit()

def compactRangeStr(l, sep=', '):
    """ Compact list `l` into string by converting any subsequent integers into range. """
    mask_float = [pyha.util.data.is_int(elem) for elem in l]
    inds_start = np.hstack((0, np.where(np.diff(mask_float))[0]+1))
    inds_end = np.hstack((inds_start[1:], -1))
    l_range = []
    for ind_start, ind_end in zip(inds_start, inds_end):
        if ind_end == -1:
            l_sub = l[ind_start:]
            mask_sub = mask_float[ind_start:]
        else:
            l_sub = l[ind_start:ind_end]
            mask_sub = mask_float[ind_start:ind_end]
        if np.all(mask_sub):
            l_float = [int(elem) for elem in l_sub]
            G = (
                list(x) for _, x in itertools.groupby(
                    l_float, lambda x, c=itertools.count(): next(c)-x))
            l_range.append(sep.join('-'.join(map(str,(g[0],g[-1])[:len(g)])) for g in G))
        else:
            l_range.append(sep.join([str(elem) for elem in l_sub]))
    return sep.join(l_range)

def truncate_filename(filename, overhead=4):
    """ Truncate string `filename` to `196-overhead` characters. """
    max_filename_len = 196-overhead
    return (
        (filename[:max_filename_len-3] + '...') if len(filename) > max_filename_len else filename)

def getAxisLabel(DataClass, dfPlotParamID, unit_prefix='',
                 fit_param_only=False, name_suffix='',
                 include_unit=True, include_symbol=False):

    param_row = DataClass.dfDataFormat.loc[dfPlotParamID]
    subs_fit_param = get_fit_param_subs(param_row)
    subs = subs_fit_param
    if (fit_param_only and (param_row['FitParamID'] != '')
            and ((param_row['FitDerivID'] == '')
            or (param_row['FitDerivID'] != '' and param_row['FitDerivParamID'] != ''))):
        label = '${FitParamLabel}'
    else:
        label = param_row['LongName']
    label += name_suffix
    if include_symbol:
        label += (
            ' ' + param_row['Symbol'] if param_row['Symbol'] != ''
            else ''
            )
    if include_unit:
        label += (' ({}{})'.format(unit_prefix, param_row['Unit'])
            if param_row['Unit'] != ''
            else (' ({})'.format(unit_prefix) if unit_prefix != '' else ''
            ))
    label = Template(label).safe_substitute(subs)
    return label

def get_fit_param_subs(param_row):
    """
    Get dict of substitutions for the fit parameter defined in the
    `dfDataFormat` row `param_row`.
    """
    if ((param_row['FitParamID'] != '')
            and ((param_row['FitDerivID'] == '')
            or (param_row['FitDerivID'] != '' and param_row['FitDerivParamID'] != ''))):
        fit_param_id = param_row['FitParamID']
        fit_deriv_id = param_row['FitDerivID']
        fit_deriv_param_id = param_row['FitDerivParamID']
        # Get fit parameter plot label
        subs = {
            'FitParamName': myFit.fit_param_names[fit_param_id],
            'FitParamSymbol': myFit.fit_param_symbols[fit_param_id],
            'FitParamUnit': myFit.fit_param_units[fit_param_id],
            'Unit': myFit.fit_param_units[fit_param_id],
            'BaseUnit': myFit.fit_param_units[fit_param_id],
            }
        if fit_param_id in myFit.fit_param_plot_labels_custom:
            label_ = myFit.fit_param_plot_labels_custom[fit_param_id]
        else:
            label_ = myFit.fit_param_plot_label_default
        label_t = Template(label_)
        fit_param_plot_label = label_t.safe_substitute(subs)
        subs = {
            **subs,
            **{'FitParamOnlyLabel': fit_param_plot_label}
            }
        if fit_deriv_param_id != '':
            unit = (
                Template(myFit.fit_deriv_param_units[fit_deriv_param_id])
                .safe_substitute(subs))
            subs = {
                **subs,
                **{
                    'FitDerivName': (
                        Template(myFit.fit_deriv_names[fit_deriv_id])
                        .safe_substitute(subs)),
                    'FitDerivParamName': (
                        Template(myFit.fit_deriv_param_names[fit_deriv_param_id])
                        .safe_substitute(subs)),
                    'FitDerivParamSymbol': (
                        Template(myFit.fit_deriv_param_symbols[fit_deriv_param_id])
                        .safe_substitute(subs)),
                    'FitDerivParamUnit': unit,
                    'Unit': unit,
                    'BaseUnit': unit,
                    }
                }
            if param_row['Symbol'] == '':
                subs['Symbol'] = subs['FitDerivParamSymbol']
            if fit_deriv_param_id in myFit.fit_deriv_param_plot_labels_custom:
                label_ = myFit.fit_deriv_param_plot_labels_custom[fit_deriv_param_id]
            else:
                label_ = myFit.fit_deriv_param_plot_label_default
            label_t = Template(label_)
            label = label_t.safe_substitute(subs)
            subs = {
                **subs,
                'FitDerivParamLabel': label,
                'FitParamLabel': label,
                }
        else:
            if param_row['Symbol'] == '':
                subs['Symbol'] = subs['FitParamSymbol']
            subs = {
                **subs,
                'FitParamLabel': subs['FitParamOnlyLabel'],
                }
    else:
        subs = {}

    return subs

def get_param_subs(param_row):
    """
    Get dict of substitutions for the parameter defined in the
    `dfDataFormat` row `param_row`.
    """
    subs = {
        'Unit': param_row['Unit'],
        'BaseUnit': param_row['Unit'],
        'Symbol': param_row['Symbol'],
        'Name': param_row['LongName'],
        'PrefixExp': param_row['PrefixExp'],
        'NumberFormat': param_row['NumberFormat'],
        'ShowUncert': param_row['ShowUncert'],
        'NSigUncertDigits': param_row['NSigUncertDigits'],
        **get_fit_param_subs(param_row),
        }
    return subs

def get_param_label(DataClass, param_id, unit_prefix='',
                    fit_param_only=False, name_suffix='',
                    include_unit=True, include_symbol=False):
    """
    Built label for the parameter with ID `param_id` (string), as defined in
    `dfDataFormat` from DataClass.
    """
    param_row = DataClass.dfDataFormat.loc[param_id]
    subs = get_param_subs(param_row)
    if (fit_param_only and (param_row['FitParamID'] != '')
            and ((param_row['FitDerivID'] == '')
            or (param_row['FitDerivID'] != '' and param_row['FitDerivParamID'] != ''))):
        label = '${FitParamLabel}'
    else:
        label = '${Name}'
    label += name_suffix
    if include_symbol:
        label += (
            ' ${Symbol}' if param_row['Symbol'] != ''
            else ''
            )
    if include_unit:
        label += (
            ' ({}{})'.format(unit_prefix, param_row['Unit'])
            if param_row['Unit'] != ''
            else (' ({})'.format(unit_prefix) if unit_prefix != '' else ''
            ))
    label = Template(label).safe_substitute(subs)
    subs = {
        **subs,
        'Label': label}
    return label, subs

def setAxisLabel(DataClass, ax, dfPlotParamID, iAxis,
                 unit_prefix='', fit_param_only=False, name_suffix='', **kwargs):

    # Set axis labels
    label = getAxisLabel(
        DataClass, dfPlotParamID, unit_prefix, fit_param_only, name_suffix)
    if iAxis==0:
        ax.set_xlabel(label, **kwargs)
    if iAxis==1:
        ax.set_ylabel(label, **kwargs)

def getUnit(DataClass, dfPlotParamID, parenthesis=True, space=' ', empty=''):

    paramRow = DataClass.dfDataFormat.loc[dfPlotParamID]
    if paramRow['Unit'] != '':
        unit_str = space+'('+paramRow['Unit']+')' if parenthesis else space+paramRow['Unit']
    else:
        unit_str = empty
    return unit_str

def setAxisLabels(DataClass, ax, dfPlotParamIDs,
                  unit_prefixes=['', ''], name_suffixes=['',''],
                  **kwargs):

    # Set axis labels
    for iAxis, (unit_prefix, name_suffix) in enumerate(zip(unit_prefixes, name_suffixes)):
        setAxisLabel(
            DataClass, ax, dfPlotParamIDs[iAxis], iAxis, unit_prefix=unit_prefix,
            name_suffix=name_suffix,
            **kwargs)

def getLineColors(num_colors, unique_colors=10, cmap=None):

    if cmap is None:
        if unique_colors <= 10:
            cmap = plt.cm.tab10
        elif unique_colors <= 20:
            cmap = plt.cm.tab20
        else:
            cmap = plt.cm.tab20
            unique_colors = 20
    else:
        cmap = cmap
    colors = cmap(np.linspace(0, 1, unique_colors))
    colors = np.tile(colors, [int(np.ceil(num_colors/unique_colors)), 1])[:num_colors]
    return colors

def getLineColor(i_color, unique_colors=10):

    colors = getLineColors(i_color+1, unique_colors=unique_colors)
    return colors[-1]

def plotSelData(DataClass, ax, sel_data, dfPlotParamIDs,
                unit_prefixes=['', ''], **kwargs):

    hs = []
    default_kwargs = {
        'marker': 'o',
        'linestyle': '',
        }
    kwargs = {**default_kwargs, **kwargs}
    # Set axis labels
    setAxisLabels(DataClass, ax, dfPlotParamIDs, unit_prefixes)
    # Plot multi-channel data for at least one axis
    if np.any(sel_data['plotMultipleChannels']):
        for ch_iid in sel_data['chActiveIID']:
            for delay_iid in sel_data['dlysActiveIID']:
                # Built plot mask common to both axes
                # The mask filters data that is available
                # (according to the dataMask from the filters),
                # but cannot be plotted, e.g. nan or strings
                commonPlotMask = np.ones(
                    (sel_data['nPlotPoints']), dtype=bool)
                for iAxis, _ in enumerate(['x', 'y']):
                    if sel_data['plotMultipleChannels'][iAxis]:
                        commonPlotMask = (
                            commonPlotMask
                            & sel_data['plotMCSDataMask'][iAxis, ch_iid, delay_iid])
                    else:
                        commonPlotMask = (
                            commonPlotMask
                            & sel_data['plotDataMask'][iAxis])
                plotMask = commonPlotMask
                plotData = np.zeros((2, 2, np.sum(plotMask)))
                # Update plot data
                date_data = np.zeros(2, dtype=bool)
                for iAxis, _ in enumerate(['x','y']):
                    if sel_data['plotMultipleChannels'][iAxis]:
                        plotData[0, iAxis] = (
                            sel_data['plotMCSData'][0, iAxis, ch_iid, delay_iid, plotMask])
                        plotData[1, iAxis] = (
                            sel_data['plotMCSData'][1, iAxis, ch_iid, delay_iid, plotMask])
                    else:
                        plotData[0, iAxis] = (
                            sel_data['plotData'][0, iAxis, plotMask])
                        plotData[1, iAxis] = (
                            sel_data['plotData'][1, iAxis, plotMask])
                        if dfPlotParamIDs[iAxis] == 'Timestamp_Datetime':
                            date_data[iAxis] = True
                x = plotData[0, 0]
                x_err = plotData[1, 0]
                y = plotData[0, 1]
                y_err = plotData[1, 1]
                # Convert float unix timestamp to datetime
                if date_data[0]:
                    x = pd.to_datetime(x)
                    x_err = None
                if date_data[1]:
                    y = pd.to_datetime(y)
                    y_err = None
                if np.any(plotMask):
                    h = ax.errorbar(
                        x,
                        y,
                        yerr=y_err,
                        xerr=x_err,
                        **kwargs
                        )
                    hs.append(h)
    # Plot single-channel data for all axes
    else:
        plotMask = np.all(sel_data['plotDataMask'], axis=0)
        plotData = sel_data['plotData']
        x = plotData[0, 0, plotMask]
        x_err = plotData[1, 0, plotMask]
        y = plotData[0, 1, plotMask]
        y_err = plotData[1, 1, plotMask]
        # Convert float unix timestamp to datetime
        if dfPlotParamIDs[0] == 'Timestamp_Datetime':
            x = pd.to_datetime(x)
            x_err = None
        if dfPlotParamIDs[1] == 'Timestamp_Datetime':
            y = pd.to_datetime(y)
            y_err = None
        if np.any(plotMask):
            h = ax.errorbar(
                x,
                y,
                yerr=y_err,
                xerr=x_err,
                **kwargs
                )
            hs.append(h)

    return hs

def compact_uncert_str(value, uncert, sig_uncert_digits=1,
                       digits=-1, exp_not=False, tex=True):
    """
    Convert given value `value` (float) with attached uncertainty error `uncert` (float)
    into string of format `value(errpr)`.
    """
    notation = 'f'
    if any([np.isnan(value), np.isnan(uncert)]):
        output = '(NaN)(NaN)'
    else:
        uncert = np.abs(uncert)
        if exp_not:
            scale = int(np.floor(np.log10(np.abs(value))))
            scaleStr = (' $10^{{{}}}$'.format(scale) if tex
                        else 'E{:d}'.format(scale))
            value = value*10**-scale
            uncert = uncert*10**-scale
        else:
            scale = 1
            scaleStr = ''
        if digits == -1:
            digits = -int(np.floor(np.log10(uncert)))+sig_uncert_digits-1
        if digits < 0:
            output = (
                '{1:.{3}{0}}({2:.0{0}}){4}'
                .format(notation, value, uncert, 0, scaleStr))
        else:
            if uncert > 1:
                output = (
                    '{1:.{3}{0}}({2:.{4}{0}}){5}'
                    .format(
                        notation, value, uncert,
                        digits, digits, scaleStr))
            else:
                output = (
                    '{1:.{3}{0}}({2:.0{0}}){4}'
                    .format(
                        notation, value, uncert*10**digits,
                        digits, scaleStr))
    return output

def get_symbol(DataClass, param_id, parenthesis=False, space=' ', empty=''):
    """
    Get symbol for parameter with ID `param_id` (string).
    """
    param_row = DataClass.dfDataFormat.loc[param_id]
    if param_row['Symbol'] != '':
        symbol_str = (
            space+'('+param_row['Symbol']+')' if parenthesis
            else space+param_row['Symbol'])
    else:
        symbol_str = empty
    return symbol_str

class MyTemplate(Template):
    """
    Subclass `Template` to allow identifier to start with numeric characters
    as well.
    """
    idpattern = r'[_a-z0-9][_a-z0-9]*'

def do_subs(label, label_subs):
    """
    Apply subsitutions defined in dict `label_subs` to string `label`.
    """
    if label is None:
        label_ = None
    else:
        label_t = MyTemplate(label)
        label_ = label_t.safe_substitute({
            **label_subs
            })

    return label_

def do_f_subs(label, label_subs):
    """
    Apply (formatted) substitutions defined in dict `label_subs` to string `label`.
    """
    class DictWMissing(dict):
        def __missing__(self, key):
            return '#NOT_FOUND#'

    label_subs_w_missing = DictWMissing(**label_subs)

    try:
        label_f = label.format_map(label_subs_w_missing)
    except ValueError:
        return '#FORMAT_ERROR#'
    else:
        return label_f

def get_hist_range(x):
    """Return 1D array with two entries, minimum of `x` and maximum of `x`."""
    return np.array([np.min(x), np.max(x)])
