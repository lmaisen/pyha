# -*- coding: utf-8 -*-
"""
Created on Fri Feb  7 11:58:48 2020

@author: Lothar Maisenbacher/MPQ
"""

import numpy as np

import pyha.def_params
from pyha.def_shorthands import sp__

def build_plot_label(unique_param, label_switches, unique_columns_label=None):

    switches_to_columns = {
        '2SnPLaserPower': [sp__+'2SnPLaserPower'],
        'AlphaOffset': [sp__+'AlphaOffset'],
        'TransverseVelVrec': [sp__+'TransverseVelVrec'],
        'CalcSubset': [sp__+'CalcSubset'],
        'SignalID': [sp__+'SignalID'],
        'DeltapDecay1': [sp__+'DeltapDecay1'],
        'ThetaL': [sp__+'ThetaL'],
        }

    grid_switches_to_columns = {
        'GridUID': [sp__+'GridUID'],
        'FS': ['dfSimGrids_FS'],
        'CD': ['dfSimGrids_QI'],
        'TrajUID': ['dfSimGrids_TrajUID'],
        'GridInterUID': ['dfSimGrids_GridInterUID'],
        'MaxTransverseMomentum': ['dfSimGrids_MaxTransverseMomentum'],
        'NTransverseMomentumSteps': ['dfSimGrids_NTransverseMomentumSteps'],
        'BackdecayModel': ['dfSimGrids_Backdecay', 'dfSimGrids_BackdecayModel'],
        'NDeltapDecay': ['dfSimGrids_NDeltapDecay'],
        'DeltapDecayAvg': ['dfSimGrids_DeltapDecayAvg', 'dfSimGrids_NDeltapDecayAvg'],
        'NMCWFMTrajs': ['dfSimGrids_NMCWFMTrajs'],
        'NMCWFMTimeSteps': ['dfSimGrids_NMCWFMTimeSteps'],
        'NMCWFMLookupCalcTimeSteps': ['dfSimGrids_NMCWFMLookupCalcTimeSteps'],
        'NMCWFMLookupInterTimeSteps': ['dfSimGrids_NMCWFMLookupInterTimeSteps'],
        'NMCWFMLookupProbPoints': ['dfSimGrids_NMCWFMLookupProbPoints'],
        'InputState': ['dfSimGrids_InputState'],
        'InputStateParamsID': ['dfSimGrids_InputStateParamsID'],
        'InputState_wx': ['dfSimGrids_InputState_wx'],
        'InputState_x0': ['dfSimGrids_InputState_x0'],
        'EHIncludeOffDiagElements': ['dfSimGrids_EHIncludeOffDiagElements'],
        }

    if unique_columns_label is not None:
        switches = {
            switch: (label_switches[switch] if switch in label_switches
            else False)
            and (all([column in unique_columns_label for column in columns]))
            for switch, columns in switches_to_columns.items()
            }
        grid_switches = {
            switch: (label_switches['Grid_'+switch] if 'Grid_'+switch in label_switches
            else False)
            and (all([column in unique_columns_label for column in columns]))
            for switch, columns in grid_switches_to_columns.items()
            }
    else:
        switches = {
            switch: (label_switches[switch] if switch in label_switches
            else False)
            for switch, columns in switches_to_columns.items()
            }
        grid_switches = {
            switch: (label_switches['Grid_'+switch] if 'Grid_'+switch in label_switches
            else False)
            for switch, columns in grid_switches_to_columns.items()
            }

    labels_sim_params = []
    if switches['2SnPLaserPower']:
        elems = np.array([unique_param[sp__+'2SnPLaserPower']]).flatten()
        formatter = '.1f' if np.min(elems) < 10 else '.0f'
        labels_sim_params.append(
            '/'.join([f'{elem:{formatter}}' for elem in elems])
            +' µW')
    if switches['AlphaOffset']:
        labels_sim_params.append('{:.1f} mrad'.format(unique_param[sp__+'AlphaOffset']))
    if switches['ThetaL']:
        labels_sim_params.append('{:.1f} °'.format(unique_param[sp__+'ThetaL']))
    if switches['TransverseVelVrec']:
        labels_sim_params.append(
            r'{:.1f} $v_\mathrm{{rec}}$'.format(unique_param[sp__+'TransverseVelVrec']))
    if switches['SignalID']:
        labels_sim_params.append(unique_param[sp__+'SignalID'])
    if switches['CalcSubset']:
        labels_sim_params.append('#{:d}'.format(unique_param[sp__+'CalcSubset']))
    if switches['DeltapDecay1'] \
            and 'dfSimGrids_DeltapDecayAvg' in unique_param \
            and unique_param['dfSimGrids_DeltapDecayAvg'] == '':
        labels_sim_params.append(
            r'$\Delta p_{{\mathrm{{D}},1}}$ = {:.3f} $\hbar K_\mathrm{{L}}$'
            .format(unique_param[sp__+'DeltapDecay1']))

    labels_grid = []
    if label_switches['Grid']:
        # Built label containing simulation grid metadata
        if grid_switches['GridUID']:
            labels_grid.append(
                'grid ' + '/'.join(np.array([unique_param[sp__+'GridUID']]).flatten()))
        if grid_switches['FS']:
            grid_fs = unique_param['dfSimGrids_FS']
            labels_grid.append(
                pyha.def_params.fs_combinations.get(grid_fs, {'Symbol': grid_fs})['Symbol'])
        if grid_switches['CD']:
            labels_grid.append(
                'CD ' + ('on' if unique_param['dfSimGrids_QI'] else 'off'))
        if grid_switches['TrajUID']:
            labels_grid.append('traj. ' + unique_param['dfSimGrids_TrajUID'])
        if grid_switches['GridInterUID']:
            labels_grid.append(
                'grid inter. ' + '/'.join(np.array([unique_param[sp__+'GridInterUID']]).flatten()))
        if grid_switches['MaxTransverseMomentum']:
            labels_grid.append(
                r'$N_{k,\mathrm{max}}$ = '
                + '{:d}'.format(unique_param['dfSimGrids_MaxTransverseMomentum']))
        if grid_switches['NTransverseMomentumSteps']:
            labels_grid.append(
                r'$M_{k}$ = '
                + '{:d}'.format(unique_param['dfSimGrids_NTransverseMomentumSteps']))
        if grid_switches['BackdecayModel']:
            labels_grid.append(
                'BD ' + (unique_param['dfSimGrids_BackdecayModel']
                         if unique_param['dfSimGrids_Backdecay'] else 'off'))
        if grid_switches['NDeltapDecay']:
            labels_grid.append(
                r'$N_{\Delta p_\mathrm{Decay}}$ = '
                + (' {:d}'.format(unique_param['dfSimGrids_NDeltapDecay'])
                   if unique_param['dfSimGrids_NDeltapDecay'] != -1 else r'$\infty$'))
        if grid_switches['DeltapDecayAvg'] \
                and 'dfSimGrids_DeltapDecayAvg' in unique_param \
                and unique_param['dfSimGrids_DeltapDecayAvg'] != '':
            labels_grid.append(
                r'$\Delta p_\mathrm{D,1}$ avg. ' + unique_param['dfSimGrids_DeltapDecayAvg']
                + (' ({:d})'.format(unique_param['dfSimGrids_NDeltapDecayAvg'])
                   if unique_param['dfSimGrids_NDeltapDecayAvg'] != -1 else ''))
        if grid_switches['NMCWFMTrajs']:
            labels_grid.append(
                r'$N_\mathrm{{tr}}$ = {:.1e}'.format(unique_param['dfSimGrids_NMCWFMTrajs']))
        if grid_switches['NMCWFMTimeSteps']:
            labels_grid.append(
                r'$N_\mathrm{{t}}$ = {:.1e}'.format(unique_param['dfSimGrids_NMCWFMTimeSteps']))
        if grid_switches['NMCWFMLookupCalcTimeSteps']:
            labels_grid.append(
                r'$N_\mathrm{{t,c}}$ = {:.1e}'.format(unique_param['dfSimGrids_NMCWFMLookupCalcTimeSteps']))
        if grid_switches['NMCWFMLookupInterTimeSteps']:
            labels_grid.append(
                r'$N_\mathrm{{t,i}}$ = {:.1e}'.format(unique_param['dfSimGrids_NMCWFMLookupInterTimeSteps']))
        if grid_switches['NMCWFMLookupProbPoints']:
            labels_grid.append(
                r'$N_\mathrm{{p}}$ = {:.1e}'.format(unique_param['dfSimGrids_NMCWFMLookupProbPoints']))
        if grid_switches['InputState'] \
                and 'dfSimGrids_InputState' in unique_param:
            if grid_switches['InputStateParamsID']:
                labels_grid.append(
                    f'{unique_param["dfSimGrids_InputState"]} '
                    + f'({unique_param["dfSimGrids_InputStateParamsID"]})')
            else:
                labels_grid.append(unique_param['dfSimGrids_InputState'])
        if grid_switches['InputState_wx']:
            labels_grid.append(
                r'$w_x$ = {:.4e} $\lambda$'.format(unique_param['dfSimGrids_InputState_wx']))
        if grid_switches['InputState_x0']:
            labels_grid.append(
                r'$x_0$ = {:.3f} $\lambda$'.format(unique_param['dfSimGrids_InputState_x0']))
        if grid_switches['EHIncludeOffDiagElements']:
            labels_grid.append(
                r'$\rho_{{k,k^\prime \neq k}}$ {}'.format(
                    ' incl.' if unique_param['dfSimGrids_EHIncludeOffDiagElements']
                    else ' dropped'))

    return labels_sim_params, labels_grid

def get_label_subs(unique_param, unique_columns_label=None):

    switches_to_columns = {
        '2SnPLaserPower': [sp__+'2SnPLaserPower'],
        'AlphaOffset': [sp__+'AlphaOffset'],
        'TransverseVelVrec': [sp__+'TransverseVelVrec'],
        'CalcSubset': [sp__+'CalcSubset'],
        'SignalID': [sp__+'SignalID'],
        'DeltapDecay1': [sp__+'DeltapDecay1'],
        'ThetaL': [sp__+'ThetaL'],
        'DetEffUID': [sp__+'DetEffUID'],
        }

    grid_switches_to_columns = {
        'GridUID': [sp__+'GridUID'],
        'FS': ['dfSimGrids_FS'],
        'CD': ['dfSimGrids_QI'],
        'TrajUID': ['dfSimGrids_TrajUID'],
        'MaxTransverseMomentum': ['dfSimGrids_MaxTransverseMomentum'],
        'NTransverseMomentumSteps': ['dfSimGrids_NTransverseMomentumSteps'],
        'BackdecayModel': ['dfSimGrids_Backdecay', 'dfSimGrids_BackdecayModel'],
        'NDeltapDecay': ['dfSimGrids_NDeltapDecay'],
        'DeltapDecayAvg': ['dfSimGrids_DeltapDecayAvg', 'dfSimGrids_NDeltapDecayAvg'],
        'NMCWFMTrajs': ['dfSimGrids_NMCWFMTrajs'],
        'NMCWFMTimeSteps': ['dfSimGrids_NMCWFMTimeSteps'],
        'NMCWFMLookupCalcTimeSteps': ['dfSimGrids_NMCWFMLookupCalcTimeSteps'],
        'NMCWFMLookupInterTimeSteps': ['dfSimGrids_NMCWFMLookupInterTimeSteps'],
        'NMCWFMLookupProbPoints': ['dfSimGrids_NMCWFMLookupProbPoints'],
        'InputState': ['dfSimGrids_InputState'],
        'InputStateParamsID': ['dfSimGrids_InputStateParamsID'],
        'InputState_wx': ['dfSimGrids_InputState_wx'],
        'InputState_x0': ['dfSimGrids_InputState_x0'],
        'EHIncludeOffDiagElements': ['dfSimGrids_EHIncludeOffDiagElements'],
        }

    label_subs = {
        **{key: '' for key in switches_to_columns.keys()},
        **{key: '' for key in grid_switches_to_columns.keys()},
        }

    if unique_columns_label is not None:
        switches = {
            switch: True
            and (all([column in unique_columns_label for column in columns]))
            for switch, columns in switches_to_columns.items()
            }
        grid_switches = {
            switch: True
            and (all([column in unique_columns_label for column in columns]))
            for switch, columns in grid_switches_to_columns.items()
            }
    else:
        switches = {
            switch: True
            for switch, columns in switches_to_columns.items()
            }
        grid_switches = {
            switch: True
            for switch, columns in grid_switches_to_columns.items()
            }

    if switches['2SnPLaserPower']:
        elems = np.array([unique_param[sp__+'2SnPLaserPower']]).flatten()
        label_subs['2SnPLaserPower'] = (
            '/'.join(['{:.0f}'.format(elem) for elem in elems])
            +' µW')
    if switches['AlphaOffset']:
        label_subs['AlphaOffset'] = (
            '{:.1f} mrad'.format(unique_param[sp__+'AlphaOffset']))
    if switches['ThetaL']:
        label_subs['ThetaL'] = (
            f'{unique_param[sp__+"ThetaL"]:.1f} °')
    if switches['TransverseVelVrec']:
        label_subs['TransverseVelVrec'] = (
            r'{:.1f} $v_\mathrm{{rec}}$'.format(unique_param[sp__+'TransverseVelVrec']))
    if switches['SignalID']:
        label_subs['SignalID'] = unique_param[sp__+'SignalID']
    if switches['DetEffUID']:
        label_subs['DetEffUID'] = unique_param[sp__+'DetEffUID']
    if switches['CalcSubset']:
        label_subs['CalcSubset'] = ('#{unique_param[sp__+"CalcSubset"]:d}')
    if switches['DeltapDecay1'] \
            and 'dfSimGrids_DeltapDecayAvg' in unique_param \
            and unique_param['dfSimGrids_DeltapDecayAvg'] == '':
        label_subs['DeltapDecay1'] = (
            r'$\Delta p_{{\mathrm{{D}},1}}$ = {:.3f} $\hbar K_\mathrm{{L}}$'
            .format(unique_param[sp__+'DeltapDecay1']))

    # Built label containing simulation grid metadata
    if grid_switches['GridUID']:
        label_subs['GridUID'] = (
            'grid ' + '/'.join(np.array([unique_param[sp__+'GridUID']]).flatten()))
    if grid_switches['FS']:
        grid_fs = unique_param['dfSimGrids_FS']
        label_subs['FS'] = (
            pyha.def_params.fs_combinations.get(grid_fs, {'Symbol': grid_fs})['Symbol'])
    if grid_switches['CD']:
        label_subs['CD'] = (
            'CD ' + ('on' if unique_param['dfSimGrids_QI'] else 'off'))
    if grid_switches['TrajUID']:
        label_subs['TrajUID'] = ('traj. ' + unique_param['dfSimGrids_TrajUID'])
    if grid_switches['MaxTransverseMomentum']:
        label_subs['MaxTransverseMomentum'] = (
            r'$N_{k,\mathrm{max}}$ = '
            + '{:d}'.format(unique_param['dfSimGrids_MaxTransverseMomentum']))
    if grid_switches['NTransverseMomentumSteps']:
        label_subs['NTransverseMomentumSteps'] = (
            r'$M_{k}$ = '
            + '{:d}'.format(unique_param['dfSimGrids_NTransverseMomentumSteps']))
    if grid_switches['BackdecayModel']:
        label_subs['BackdecayModel'] = (
            'BD ' + (unique_param['dfSimGrids_BackdecayModel']
                     if unique_param['dfSimGrids_Backdecay'] else 'off'))
    if grid_switches['NDeltapDecay']:
        label_subs['NDeltapDecay'] = (
            r'$N_{\Delta p_\mathrm{Decay}}$ = '
            + (' {:d}'.format(unique_param['dfSimGrids_NDeltapDecay'])
               if unique_param['dfSimGrids_NDeltapDecay'] != -1 else r'$\infty$'))
    if grid_switches['DeltapDecayAvg'] \
            and 'dfSimGrids_DeltapDecayAvg' in unique_param \
            and unique_param['dfSimGrids_DeltapDecayAvg'] != '':
        label_subs['DeltapDecayAvg'] = (
            r'$\Delta p_\mathrm{D,1}$ avg. ' + unique_param['dfSimGrids_DeltapDecayAvg']
            + (' ({:d})'.format(unique_param['dfSimGrids_NDeltapDecayAvg'])
               if unique_param['dfSimGrids_NDeltapDecayAvg'] != -1 else ''))
    if grid_switches['NMCWFMTrajs']:
        label_subs['NMCWFMTrajs'] = (
            r'$N_\mathrm{{tr}}$ = {:.1e}'.format(unique_param['dfSimGrids_NMCWFMTrajs']))
    if grid_switches['NMCWFMTimeSteps']:
        label_subs['NMCWFMTimeSteps'] = (
            r'$N_\mathrm{{t}}$ = {:.1e}'.format(unique_param['dfSimGrids_NMCWFMTimeSteps']))
    if grid_switches['NMCWFMLookupCalcTimeSteps']:
        label_subs['NMCWFMLookupCalcTimeSteps'] = (
            r'$N_\mathrm{{t,c}}$ = {:.1e}'.format(unique_param['dfSimGrids_NMCWFMLookupCalcTimeSteps']))
    if grid_switches['NMCWFMLookupInterTimeSteps']:
        label_subs['NMCWFMLookupInterTimeSteps'] = (
            r'$N_\mathrm{{t,i}}$ = {:.1e}'.format(unique_param['dfSimGrids_NMCWFMLookupInterTimeSteps']))
    if grid_switches['NMCWFMLookupProbPoints']:
        label_subs['NMCWFMLookupProbPoints'] = (
            r'$N_\mathrm{{p}}$ = {:.1e}'.format(unique_param['dfSimGrids_NMCWFMLookupProbPoints']))
    if grid_switches['InputState'] \
            and 'dfSimGrids_InputState' in unique_param:
        if grid_switches['InputStateParamsID']:
            label_subs['InputState'] = (
                f'{unique_param["dfSimGrids_InputState"]} '
                + f'({unique_param["dfSimGrids_InputStateParamsID"]})')
        else:
            label_subs['InputState'] = (unique_param['dfSimGrids_InputState'])
    if grid_switches['InputState_wx']:
        label_subs['InputState_wx'] = (
            r'$w_x$ = {:.4e} $\lambda$'.format(unique_param['dfSimGrids_InputState_wx']))
    if grid_switches['InputState_x0']:
        label_subs['InputState_x0'] = (
            r'$x_0$ = {:.3f} $\lambda$'.format(unique_param['dfSimGrids_InputState_x0']))
    if grid_switches['EHIncludeOffDiagElements']:
        label_subs['EHIncludeOffDiagElements'] = (
            r'$\rho_{{k,k^\prime \neq k}}$ {}'.format(
                ' incl.' if unique_param['dfSimGrids_EHIncludeOffDiagElements']
                else ' dropped'))

    return label_subs
