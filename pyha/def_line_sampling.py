# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ

Define the line or frequency samplings, that is lists of frequency points,
used to create or fit simulation data.

The metadata of the frequency samplings and the actual frequency points are read from JSON
files `freq_samplings.json` and `freq_lists.json`, respectively, which are searched for in the
directory `reldir_freq_samplings` (defined below).

The `H2S6P2019` repository (https://gitlab.mpcdf.mpg.de/lmaisen/H2S6P2019) includes a script to
generate the JSON files:
`H2S6P2019/tools/create_freq_sampling.py`
"""

import numpy as np
import pandas as pd
import scipy.constants
import json
from pathlib import Path

# pyhs
import pyhs.gen

# pyha
import pyha.defs
import pyha.def_params
import pyha.def_atomic

logger = pyhs.gen.get_command_line_logger()

# Input files
script_dir = Path(__file__).resolve().parent
reldir_freq_samplings = 'static/freq_samplings'
dir_freq_samplings = Path(script_dir, reldir_freq_samplings).resolve()

filepath_freq_samplings = Path(dir_freq_samplings, 'freq_samplings.json')
with open(filepath_freq_samplings, 'r') as f:
    freq_samplings_metadata = json.load(f)
logger.info(
    f'Read frequency samplings metadata from file \'{filepath_freq_samplings}\'')
filepath_freq_lists = Path(dir_freq_samplings, 'freq_lists.json')
with open(filepath_freq_lists, 'r') as f:
    freq_lists = json.load(f)
logger.info(
    f'Read frequency lists from file \'{filepath_freq_lists}\'')
# Convert frequency lists to arrays
for key, fixed_freq_sampling in freq_lists.items():
    freq_lists[key]['Freqs'] = np.array(fixed_freq_sampling['Freqs'])
    freq_lists[key]['FreqsUnique'] = (
        np.array(fixed_freq_sampling['FreqsUnique']))

def get_freq_sampling_metadata(freq_sampling, fixed_freq_list_id=None):
    """
    Get metadata `freq_sampling_metadata` (dict) of frequency sampling `freq_sampling` (string)
    (if the frequency sampling is fixed ('Fixed'), the fixed frequency list ID
    `fixed_freq_list_id` (string) must be supplied).
    Metadata includes fields for the number of frequency points `NFreqs` and
    the number of unique frequency points `NFreqsUnique`.
    """
    if freq_sampling not in freq_samplings_metadata:
        msg = f'Unknown frequency sampling \'{freq_sampling}\''
        logger.error(msg)
        raise pyha.defs.PyhaError(msg)
    if freq_sampling == 'Fixed':
        if fixed_freq_list_id is None:
            msg = 'Fixed frequency sampling selected, but not frequency list ID given'
            logger.error(msg)
            raise pyha.defs.PyhaError(msg)
        if fixed_freq_list_id not in freq_lists:
            msg = f'Unknown frequency list ID \'{fixed_freq_list_id}\''
            logger.error(msg)
            raise pyha.defs.PyhaError(msg)
        freq_sampling_metadata = freq_lists[fixed_freq_list_id]
    else:
        freq_sampling_metadata = freq_samplings_metadata[freq_sampling]
    return freq_sampling_metadata

def get_freq_list(freq_sampling, fixed_freq_list_id=None, scan=None):
    """
    Get frequency list ID `freq_list_id` (string) and frequency list `freq_list`
    (dict) corresponding to frequency sampling `freq_sampling` (string)
    (if the frequency sampling is fixed ('Fixed'), the fixed frequency list ID
    `fixed_freq_list_id` (string) must be supplied).
    Some frequency samplings require additional metadata `scan` (dict).
    Frequency samplings whose name `freq_sampling` starts with '_' are treated as placeholders:
    an empty frequency list ID `freq_list_id` and a frequency list `freq_list` with placeholder
    entries are returned.
    """
    if freq_sampling == 'Fixed':
        if fixed_freq_list_id is None:
            msg = 'Fixed frequency sampling selected, but no frequency list ID given'
            logger.error(msg)
            raise pyha.defs.PyhaError(msg)
        else:
            freq_list_id = fixed_freq_list_id
    elif freq_sampling == 'ExpH2S6P2019':
        if scan is None or 'AlphaOffset' not in scan.keys():
            msg = (
                f'Frequency sampling \'{freq_sampling}\' requires scan metadata'
                +' with key \'AlphaOffset\''
                +' (supply dict with keyword argument \'scan\')')
            logger.error(msg)
            raise pyha.defs.PyhaError(msg)
        # Used in hydrogen 2S-6P 2019 measurement campaign (H2S6P2019)
        if np.abs(scan['AlphaOffset']) <= 6:
            freq_list_id = 'H2S6P2019_alphaOffsetleq6mrad'
        elif np.abs(scan['AlphaOffset']) <= 10:
            freq_list_id = 'H2S6P2019_alphaOffsetleq10mrad'
        else:
            freq_list_id = 'H2S6P2019_alphaOffsetgtr10mrad'
    elif freq_sampling == 'ExpAlphaH2S6P2019':
        # Used for alpha offset alignment
        # in hydrogen 2S-6P 2019 measurement campaign (H2S6P2019)
        freq_list_id = 'H2S6P2019_alphaOffsetAlign'
    elif freq_sampling.startswith('_'):
        freq_list_id = ''
        freq_list = {
            'Freqs': np.array([]), 'NFreqs': -1, 'FreqsUnique': np.array([]), 'NFreqsUnique': -1}
    else:
        msg = f'Frequency sampling \'{freq_sampling}\' not implemented'
        logger.error(msg)
        raise pyha.defs.PyhaError(msg)
    if not freq_sampling.startswith('_'):
        if freq_list_id not in freq_lists:
            msg = f'Unknown frequency list ID \'{freq_list_id}\''
            logger.error(msg)
            raise pyha.defs.PyhaError(msg)
        freq_list = freq_lists[freq_list_id]

    return freq_list_id, freq_list

def get_delta_nu(v_mean, alpha_offset, fs='6P12', isotope='H'):
    """
    Get line splitting `delta_nu` (Hz) of the two Doppler components in 2S-nP spectroscopy,
    for a given angle `alpha_offset` (mrad) between laser and atomic beam and
    a given atom speed `v_mean`.
    The frequency of the atomic transition is found through the transition identifier `fs` (str)
    and the isotope `isotope` (str). Default is `fs='6P12' and `isotope='H'`, corresponding to the
    2S-6P transition in atomic hydrogen.
    The factor 1.05 seems to help with fit stability.
    """
    return (
        1.05*2*alpha_offset*1e-3
        *pyha.def_atomic.atomic_properties[isotope+fs]['TransitionFreq']
        /scipy.constants.c*v_mean)

def get_pstarts(delay=None, x=None, y=None, params=None):
    """
    Get start parameters `pstarts` (dict) for fits of doublet line shape functions
    (`VoigtDoublet` and `VoigtDoubletEqualAmps`, which correspond to the keys of `pstarts`).
    A delay definition (dict) can be passed as `delay` (default None), and counts `y` vs
    frequency `x` can also be given.
    Additional (input) parameters such as `DelayID` and `AlphaOffset` to refine the start
    parameters can be passed as `params` (dict), with the input parameters as keys.
    """
    if params is None:
        params = {}
    alpha_offset = params.get('AlphaOffset', np.nan)
    pstarts_doublet = {}
    if delay is not None and not np.isnan(alpha_offset):
        delta_nu = get_delta_nu(
            delay['V_Mean_Value'], params['AlphaOffset'],
            )
        if alpha_offset > 5:
            pstarts_doublet = {
                'CFR1': -delta_nu/2,
                'CFR2': delta_nu/2,
                'GammaL': 5e6,
                'GammaG': 3e6,
                'Amp1': np.max(y),
                'Amp2': np.max(y),
                'Amp': np.max(y),
                'Offset': 0.,
                }
        else:
            pstarts_doublet = {
                'DeltaNu': 1e6,
                }
    pstarts = {
        'VoigtDoublet': pstarts_doublet,
        'VoigtDoubletEqualAmps': pstarts_doublet,
    }
    return pstarts

def get_freq_list_and_count_weighting(sim_params, ExpMetadata=None):
    """
    Get frequency list and count weighting for simulation parameters `sim_params` (dict).

    Parameters
    ----------
    sim_params : dict
        Simulation parameters.
        The key 'FreqSampling' defining the frequency sampling (str) is required
        (see `get_freq_list`.).
    ExpMetadata : instance of `pyha.exp_metadata.Metadata`, optional
        Instance of experimental metadata class `pyha.exp_metadata.Metadata`,
        necessary for count weighting 'ShotNoiseOffsetMetadata',
        where relative offset of simulation data is determined from metadata.
        The default is None, in which case using the count weighting 'ShotNoiseOffsetMetadata' will
        throw an error.

    Returns
    -------
    freq_list_id : str
        Frequency list ID, see `get_freq_list`.
    freq_list : dict
        Frequency list, see `get_freq_list`.
    count_weighting : str
        Count weighting used.
    sigma_func : callable
        Function `sigma_func(y)` that returns standard deviations for given counts `y`
        (float or list-like of float).
    rel_offset : float
        The relative offset that is to be applied to the simulation counts as determined by the
        count weighting.

    """
    freq_list_id, freq_list = get_freq_list(
        sim_params['FreqSampling'], sim_params.get('FixedFreqListID'), sim_params)

    if sim_params.get('CountWeighting') is None:
        msg = (
            '`get_freq_sampling_and_count_weighting`: '
            +'Count weighting (`sim_params[\'CountWeighting\']`) is not defined, '
            +'assuming uniform weighting (`sim_params[\'CountWeighting\'] = \'Uniform\'`)')
        logger.warning(msg)
        count_weighting = 'Uniform'
    else:
        count_weighting = sim_params['CountWeighting']

    if count_weighting == 'Uniform':
        sigma_func = np.vectorize(lambda y: 1)
        rel_offset = 0.
    elif count_weighting == 'ShotNoiseOffsetLaserPower':
        sigma_func = np.sqrt
        if sim_params.get('FS') is None \
                or sim_params.get('2SnPLaserPower') is None:
            msg = (
                'Count weighting \'ShotNoiseOffsetLaserPower\' selected'
                +', but no transition identifier (`sim_params[\'FS\']`)'
                +' and/or 2S-nP laser power (`sim_params[\'2SnPLaserPower\']`) supplied')
            logger.error(msg)
            raise pyha.defs.PyhaError(msg)
        laser_power_equiv = (
            pyha.def_params.convert_to_laser_power_equivalent_2snp12(
                sim_params['FS'], sim_params['2SnPLaserPower']))
        rel_offset_vs_laser_power = {
            10.: 0.12,
            20.: 0.07,
            30.: 0.05,
            }
        rel_offset = rel_offset_vs_laser_power.get(laser_power_equiv, 0.)
    elif count_weighting == 'ShotNoiseOffsetMetadata':
        sigma_func = np.sqrt
        if ExpMetadata is None:
            msg = (
                'Count weighting \'ShotNoiseOffsetMetadata\' requires an instance of '
                +'`pyha.exp_metadata.Metadata` to be passed as argument `ExpMetadata`')
            logger.error(msg)
            raise pyha.defs.PyhaError(msg)
        if (data_group_id := sim_params.get('DataGroupID')) is None:
            msg = (
                'Count weighting \'ShotNoiseOffsetMetadata\' selected'
                +', but data group ID (`sim_params[\'DataGroupID\']`) not supplied')
            logger.error(msg)
            raise pyha.defs.PyhaError(msg)
        if data_group_id not in ExpMetadata.dfDataGroups.index:
            msg = (
                f'Data group ID \'{data_group_id}\' not in `ExpMetadata.dfDataGroups`')
            logger.error(msg)
            raise pyha.defs.PyhaError(msg)
        rel_offset = ExpMetadata.dfDataGroups.loc[data_group_id]['CountWeightingRelOffset']
    else:
        msg = (
            f'Unknown count weighting \'{count_weighting}\''
            +' (from `sim_params[\'CountWeighting\']`)')
        logger.error(msg)
        raise pyha.defs.PyhaError(msg)

    return freq_list_id, freq_list, count_weighting, sigma_func, rel_offset

def select_line_samplings(df, line_samplings, mask=None, column_prefix=None, verbose=True):
    """
    Select entries of DataFrame `df` that correspond to given line samplings `line_samplings`.

    Parameters
    ----------
    df : pandas DataFrame
        DataFrame from which entries are selected.
    line_samplings : dict or list of dicts
        Dict(s) defining line sampling.
    mask : pandas Series or None (default), optional
        Mask for `df` to select rows to be considered.
        The returned mask `mask_line_sampling` is the logical and of this mask and the selection
        on line samplings.
        The default is None, for which all of `df` is considered.
    column_prefix : str or None (default), optional
        Prefix to column names of `df`. The default is None, for which no prefix is added.
    verbose : bool, optional
        Verbose output. The default is True.

    Returns
    -------
    mask_line_sampling : pandas Series
        Mask for `df`, only selecting requested line samplings.

    """
    column_prefix = '' if column_prefix is None else column_prefix
    mask = pd.Series(True, index=df.index) if mask is None else mask
    mask_line_sampling = pd.Series(False, index=df[mask].index)
    for line_sampling in np.atleast_1d(line_samplings):
        mask_line_sampling_ = (
            (df.loc[mask, [f'{column_prefix}{key}' for key in line_sampling.keys()]]
             == [elem for _, elem in line_sampling.items()])
            .all(axis=1))
        if verbose:
            logger.info(
                'Found %d simulation scans(s) for line sampling %s',
                mask_line_sampling_.sum(), line_sampling)
        mask_line_sampling |= mask_line_sampling_
    return mask_line_sampling

def select_symmetric_freqs(arr):
    """
    Find the indices of elements in the array that have their symmetric pair or are zero.
    This is useful to symmetrize a frequency sampling.

    This function returns an array of indices. When these indices are used to index
    the original array, the resulting array contains only the elements that are zero
    or have their symmetric pair (positive and negative), preserving the original order.

    Parameters:
    arr (list or array-like): The original array of floats.

    Returns:
    np.ndarray: An array of indices corresponding to elements in `arr` that are zero
                or have their symmetric pair.
    """
    np_arr = np.array(arr)

    # Find the symmetric pairs
    positives = np_arr[np_arr > 0]
    negatives = np_arr[np_arr < 0]
    symmetric = np.intersect1d(positives, -negatives)

    # Create an array of indices for numbers that are zero or have their symmetric pair
    indices = np.array([
        i for i, num in enumerate(arr) if num == 0 or abs(num) in symmetric])

    return indices
