# -*- coding: utf-8 -*-
"""
Created on Tue Dec 14 09:45:24 2021

@author: Lothar Maisenbacher/MPQ

Generic container class from which classes managing various (meta)data are derived.
This class mainly makes methods to manage DataFrames available.
"""

import pandas as pd
import json
import copy
import warnings
from pathlib import Path

# pyhs
import pyhs.gen

# pyha
import pyha.util.data

logger = pyhs.gen.get_command_line_logger()

class GenericContainerClass:
    """
    Generic container class from which classes managing various (meta)data are derived.
    This class mainly makes methods to manage DataFrames available.
    """

    # List of data format keys containing column names
    DTYPE_COLUMN_KEYS = [
        'ColumnsGen', 'ColumnsFloat', 'ColumnsStr',
        'ColumnsBool', 'ColumnsInt', 'ColumnsDate',
        ]

    # Example data format
    EXAMPLE_COLUMNS_GEN = [
        ]
    EXAMPLE_COLUMNS_FLOAT = [
        'FloatColumn',
        ]
    EXAMPLE_COLUMNS_STR = [
        'StrColumn', 'ExampleUID', 'AbsPath',
        ]
    EXAMPLE_COLUMNS_BOOL = [
        'BoolColumn',
        ]
    EXAMPLE_COLUMNS_INT = [
        'IntColumn',
        ]
    EXAMPLE_COLUMNS_DATE = [
        'DateColumn',
        ]
    EXAMPLE_FORMAT = {
        'ColumnsGen': EXAMPLE_COLUMNS_GEN,
        'ColumnsFloat': EXAMPLE_COLUMNS_FLOAT,
        'ColumnsStr': EXAMPLE_COLUMNS_STR,
        'ColumnsBool': EXAMPLE_COLUMNS_BOOL,
        'ColumnsInt': EXAMPLE_COLUMNS_INT,
        'ColumnsDate': EXAMPLE_COLUMNS_DATE,
        'IndexName': 'ExampleUID',
        'KeepIndexAsColumn': True,
        }

    # Dict of data formats
    DF_FORMATS = {
        # 'dfExample': EXAMPLE_FORMAT,
        }

    def __init__(self):
        """
        Initialize generic container class.
        """
        # Deepcopy of `df_formats`, `df_formats`, which will be used as DataFrame format in this
        # instance.
        # This preserves `df_formats` even if `df_formats` is modified at run-time.
        self.df_formats = copy.deepcopy(self.DF_FORMATS)

        # Init empty DataFrames
        self.dfs = {df_id: pd.DataFrame() for df_id in self.df_formats.keys()}

        # Init data containers
        self.init_data_containers()

        # Save currently empty data containers as templates
        self.df_templates = self.dfs.copy()

    def init_data_containers(self):
        """
        Initialize DataFrames that hold (meta)data.
        """
        # Init empty DataFrames
        dfs = {}
        for df_id, _ in self.df_formats.items():
            dfs[df_id] = self.init_data_container(df_id)
        self.dfs = dfs

    def init_data_container(self, df_id):
        """
        Initialize DataFrame as specified in DataFrame format with ID `df_id` (str).
        """
        # Init empty DataFrames
        df_format = self.df_formats[df_id]
        df = (
            pd.DataFrame(columns=self.get_columns(df_id))
            .set_index(
                df_format['IndexName'],
                drop=not df_format.get('KeepIndexAsColumn', False)))
        df = self.cast_containers_to_data_format(
            df, df_id)
        return df

    def cast_containers_to_data_format(self, df, df_id):
        """
        Cast columns of DataFrame `df` into correct DType, add default values,
        as specified in DataFrame format with ID `df_id` (str).
        """
        df_format = self.df_formats[df_id]
        df = pyha.util.data.cast_df_to_data_format_dict(
            df, df_format)
        # Make sure column `MeasDate` contains only a date, i.e. hours, minutes,
        # seconds is set to zero
        if 'MeasDate' in df:
            df['MeasDate'] = df['MeasDate'].apply(
                lambda t: pd.to_datetime(t.date()))
        return df

    def make_df_names_available(self):
        """
        Deprecated.
        """
        warnings.warn(
            'Calling the method `make_df_names_available` is no longer necessary, please remove all'
            +' calls',
            FutureWarning)

    def set_df_index_name(self, df, df_id):
        """
        Set name of index of DataFrame `df` as specified in DataFrame format
        with ID `df_id` (str).
        """
        df_format = self.df_formats[df_id]
        df = pyha.util.data.set_df_index_name(df, df_format)
        return df

    def drop_duplicates_and_reindex(self, df, df_id, subset=None):
        """
        Drop duplicate entries of DataFrame `df`, using DataFrame format
        with ID `df_id` (str).
        `subset` (None (default) or list) is passed to `pyha.util.data.drop_duplicates_and_reindex`,
        see comments there.
        """
        df_format = self.df_formats[df_id]
        num_entries = len(df)
        df = pyha.util.data.drop_duplicates_and_reindex(
            df, df_format, subset=subset)
        if num_entries-len(df) > 0:
            logger.info(
                'Dropped %d duplicate entries for \'%s\'',
                num_entries-len(df), df_id)
        return df

    def write_df_to_csv(self, df, df_id, filepath):
        """
        Write DataFrame `df`, using DataFrame format with ID `df_id` (str),
        to CSV file with filepath `filepath` (str).
        If entry 'KeepIndexAsColumn' of the DataFrame format is set to True (default is False),
        the column containing a copy of the index is not written to file, as otherwise there is a
        duplicate column in the file (the index and the column containing a copy of it).
        """
        df_format = self.df_formats[df_id]
        columns = df.columns
        if df_format.get('KeepIndexAsColumn', False) and df_format['IndexName'] in columns:
            columns = columns.drop(df_format['IndexName'])
        df.to_csv(filepath, columns=columns)

    def get_columns(self, df_id):
        """Get list of all columns contained in DataFrame format with ID `df_id` (str)."""
        return pyha.util.data.get_columns_in_data_format_dict(self.df_formats[df_id])

    def dump_series_to_json(self, sr, filepath, df_id, **kwds):
        """
        Dump pandas Series `sr` to JSON file at path `filepath` (str or `pathlib.Path`),
        with data format given by DataFrame format with ID `df_id` (str).
        The Series is cast to this data format before dumping.
        Keyword arguments are passed to `json.dump` through `pyha.util.data.dump_to_json`.
        """
        # Add to DataFrame, cast data format
        df = pd.concat([self.df_templates[df_id], sr.to_frame().T])
        df = self.cast_containers_to_data_format(df, df_id)
        sr_cast = df.loc[sr.name]
        pyha.util.data.dump_to_json(dict(sr_cast), filepath, **kwds)

    def read_series_from_json(self, filepath, df_id):
        """
        Read pandas Series from JSON file at path `filepath` (str or `pathlib.Path`),
        with data format given by DataFrame format with ID `df_id` (str).
        The JSON is read, the retrieved object is converted to a Series, which is converted to a
        DataFrame with the given data format, the data is cast to that data format, and the
        resulting DataFrame row is converted back to Series `sr`, which is returned.
        """
        df_format = self.df_formats[df_id]
        with open(filepath, 'r') as f:
            obj = json.load(f)
        # Convert to single-row DataFrame
        df_add_raw = pd.DataFrame(obj, index=[obj[df_format['IndexName']]])
        # Add to DataFrame, cast data format
        df = pd.concat([self.df_templates[df_id], df_add_raw])
        df = self.cast_containers_to_data_format(df, df_id)
        sr_cast = df.loc[obj[df_format['IndexName']]]
        return sr_cast

    def read_df_from_csv(self, filepath, df_id, safe=False):
        """
        Read DataFrame from file at `filepath`, with data format given by DataFrame format with ID
        `df_id` (str).
        If `safe` is set to True (default False), the
        """
        filepath = Path(filepath)
        if not safe:
            df = pd.read_csv(
                filepath,
                parse_dates=self.df_formats[df_id]['ColumnsDate'],
                index_col=self.df_formats[df_id]['IndexName'],
                )
        else:
            df = pd.read_csv(
                filepath,
                index_col=self.df_formats[df_id]['IndexName'],
                )
            date_columns = [
                column for column in self.df_formats[df_id]['ColumnsDate'] if column in df.columns]
            for date_column in date_columns:
                df[date_column] = pd.to_datetime(df[date_column], format='ISO8601')
        df['AbsPath'] = filepath.absolute()
        return df
