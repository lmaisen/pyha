# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ

Definition of class `Metadata`, which manages experimental metadata.
"""

import numpy as np
import pandas as pd
import itertools
import datetime
from pathlib import Path

# pyhs
import pyhs.gen
import pyhs.files
import pyhs.time

# pyha
import pyha.defs
import pyha.generic_container_class
import pyha.util.data

logger = pyhs.gen.get_command_line_logger()

## Shorthands
# Experimental and simulation parameters in DataFrames
from pyha.def_shorthands import ep__

class Metadata(pyha.generic_container_class.GenericContainerClass):
    """
    Class managing metadata of experimental data,
    including metadata of freezing cycles (FCs),
    and scans, identified by measurement day and scan DID.
    """

    # List of file extensions/types that will be read,
    # currently possible is .csv (CSV, comma separated),
    # .xslx (Excel),
    # .dat (treated as CSV)
    FILE_EXT_TO_READ = ['xlsx']

    DAYS_COLUMNS_GEN = [
        ]
    DAYS_COLUMNS_DATE = [
        'MeasDate',
        'TimestampCryopumpOn1',
        'TimestampCryopumpOff1',
        'TimestampCryopumpValveClosed',
        'TimestampH2ValvesOpened',
        ]
    DAYS_COLUMNS_FLOAT = [
        'ThetaL_Slow',
        'ThetaL_Fast',
        'DayNozzleTemp',
        'Day1S2SLaserPower',
        'NozzleShimThickness',
        'GNSSCorrectionUTC',
        'GNSSCorrectionCs',
        ]
    DAYS_COLUMNS_STR = [
        'MeasSession',
        'MeasRun',
        'NozzleType',
        'DetectorType',
        'DayTags',
        'DayComments',
        'NozzleFeedInsulation',
        ]
    DAYS_COLUMNS_BOOL = [
        '1S2SLaserPowerStabilized',
        'DayContToH2S6P2019Precision',
        'GNSSCorrectionAvailable',
        ]
    DAYS_COLUMNS_INT = [
        ]
    DAYS_FORMAT = {
        'ColumnsGen': DAYS_COLUMNS_GEN,
        'ColumnsFloat': DAYS_COLUMNS_FLOAT,
        'ColumnsStr': DAYS_COLUMNS_STR,
        'ColumnsBool': DAYS_COLUMNS_BOOL,
        'ColumnsInt': DAYS_COLUMNS_INT,
        'ColumnsDate': DAYS_COLUMNS_DATE,
        'IndexName': 'MeasDate',
        'SetNumericalIndex': False,
        'TagColumn': 'DayTags',
        'LoadFromFiles': True,
        'FilebasePattern': '*_days',
        'DuplicatesSubset': ['MeasDate'],
        }

    FCS_COLUMNS_GEN = [
        ]
    FCS_COLUMNS_FLOAT = [
        'DaytimeFCMin',
        'DaytimeFCMax',
        'H2Flow',
        'FCDuration',
        'FC1S2SLaserPowerMin',
        'FC1S2SLaserPowerMax',
        'FC1S2SLaserPowerMean',
        ]
    FCS_COLUMNS_STR = [
        'FCUID',
        'FCName',
        'Isotope',
        'FS',
        'FCTags',
        'FCComments',
        ]
    FCS_COLUMNS_BOOL = [
        'FCContToH2S6P2019Precision',
        ]
    FCS_COLUMNS_INT = [
        'FC',
        'FCNScansPrecisionAll',
        'FCNScansPrecision',
        'FCNScansPrecisionPossIssues',
        'FCNScansPrecisionAlphaOffset',
        ]
    FCS_COLUMNS_DATE = [
        'MeasDate',
        'TimestampFCMin',
        'TimestampFCMax',
        'Helper_PlotTime',
        ]
    FCS_FORMAT = {
        'ColumnsGen': FCS_COLUMNS_GEN,
        'ColumnsFloat': FCS_COLUMNS_FLOAT,
        'ColumnsStr': FCS_COLUMNS_STR,
        'ColumnsBool': FCS_COLUMNS_BOOL,
        'ColumnsInt': FCS_COLUMNS_INT,
        'ColumnsDate': FCS_COLUMNS_DATE,
        'IndexName': 'FCUID',
        'SetNumericalIndex': True,
        'TagColumn': 'FCTags',
        'LoadFromFiles': True,
        'FilebasePattern': '*_fcs',
        'DuplicatesSubset': ['MeasDate', 'FC'],
        }

    FCDAYS_FORMAT = {
        'ColumnsGen': list(np.unique(
            DAYS_FORMAT['ColumnsGen']+FCS_FORMAT['ColumnsGen'])),
        'ColumnsFloat': list(np.unique(
            DAYS_FORMAT['ColumnsFloat']+FCS_FORMAT['ColumnsFloat'])),
        'ColumnsStr': list(np.unique(
            DAYS_FORMAT['ColumnsStr']+FCS_FORMAT['ColumnsStr'])),
        'ColumnsBool': list(np.unique(
            DAYS_FORMAT['ColumnsBool']+FCS_FORMAT['ColumnsBool'])),
        'ColumnsInt': list(np.unique(
            DAYS_FORMAT['ColumnsInt']+FCS_FORMAT['ColumnsInt'])),
        'ColumnsDate': list(np.unique(
            DAYS_FORMAT['ColumnsDate']+FCS_FORMAT['ColumnsDate'])),
        'IndexName': 'FCUID',
        'LoadFromFiles': False,
        }

    FINESSE_FORMAT = {
        'ColumnsGen': [],
        'ColumnsFloat': [
            'Finesse_Value',
            'Finesse_SD',
            'Linewidth_Value',
            'Linewidth_Sigma',
            'Peak_Value',
            'Peak_Sigma',
            'PiezoOffset_Value',
            'InputPower_Value',
            'InputPower_Sigma',
            'ModeMatching_Value',
            'ModeMatching_SD',
            'ModeMatching_Area_Value',
            'ModeMatching_Area_SD',
            'Finesse_Value_LabBook',
            'Finesse_SD_LabBook',
            'Linewidth_Value_LabBook',
            'Linewidth_Sigma_LabBook',
            'Peak_Value_LabBook',
            'Peak_Sigma_LabBook',
            'PiezoOffset_Value_LabBook',
            'InputPower_Value_LabBook',
            'InputPower_Sigma_LabBook',
            ],
        'ColumnsStr': [
            'FileprefixFirstMeas',
            'FileprefixLastMeas',
            'FinesseTags',
            'FinesseComments',
            ],
        'ColumnsBool': [],
        'ColumnsInt': [
            'FinesseID',
            'NMeas',
            'NMaxFits',
            'NFits',
            ],
        'ColumnsDate': [
            'MeasDate',
            'TimestampFirstMeas',
            'TimestampLastMeas',
            ],
        'IndexName': 'FinesseID',
        'SetNumericalIndex': True,
        'TagColumn': 'FinesseTags',
        'LoadFromFiles': True,
        'FilebasePattern': '*_finesse',
        }

    ECTRANSMISSION_FORMAT = {
        'ColumnsGen': [],
        'ColumnsFloat': [
            'PD243Itrans_Labbook',
            'PD243Isphere_LabBook',
            'PM243Itrans',
            'PD243Itrans_Mean',
            'PD243Isphere_Mean',
            'PD243Itrans_PreAmpGain',
            'Ratio_PD243ItransPD243Isphere',
            'Ratio_PM243ItransPD243Isphere',
            'Ratio_PM243ItransPD243Itrans',
            'Ratio_PtrPMPcirc',
            'Ratio_PinPinPM',
            ],
        'ColumnsStr': [
            'ECTransmissionTags',
            'ECTransmissionComments',
            ],
        'ColumnsBool': [
            'ContToH2S6P2019Precision',
            ],
        'ColumnsInt': [
            'ECTransmissionID',
            ],
        'ColumnsDate': [
            'Timestamp',
            'MeasDate',
            ],
        'IndexName': 'ECTransmissionID',
        'SetNumericalIndex': True,
        'TagColumn': 'ECTransmissionTags',
        'LoadFromFiles': True,
        'FilebasePattern': '*_ectransmission',
        }

    SCAN_GROUPS_FORMAT = {
        'ColumnsGen': [],
        'ColumnsFloat': [
            'F1S2SSetTo',
            'AADopplerSlope',
            'AASetTo',
            'AASetFrom',
            'EFieldRange',
            'Scan2SnPLaserPower',
            'AlphaOffset',
            ],
        'ColumnsStr': [
            'FSOverride',
            'ScanType',
            'EFieldDirect',
            'ScanGroupTags',
            'ScanGroupsComments',
            'ScanDIDExcl',
            'ScanDIDExcl_Ch0',
            'ScanDIDExcl_Ch1',
            'AADirect',
            ],
        'ColumnsBool': [
            'ChExcl_Ch0',
            'ChExcl_Ch1',
            'ScanMatchingWarning',
            ],
        'ColumnsInt': [
            'ScanGroupID',
            'FC',
            'ScanDIDMin1',
            'ScanDIDMax1',
            'ScanDIDMin2',
            'ScanDIDMax2',
            'ScanDIDMin3',
            'ScanDIDMax3',
            'ScanDIDMin4',
            'ScanDIDMax4',
            'NScansDefined',
            'NScansFound',
            'NScansDefined_Ch0',
            'NScansDefined_Ch1',
            ],
        'ColumnsDate': [
            'MeasDate',
            'Timestamp',
            ],
        'IndexName': 'ScanGroupID',
        'SetNumericalIndex': True,
        'TagColumn': 'ScanGroupTags',
        'LoadFromFiles': True,
        'FilebasePattern': '*_scangroups',
        }

    SCAN_GROUP_SCANS_FORMAT = {
        'ColumnsGen': [],
        'ColumnsFloat': [],
        'ColumnsStr': [
            'ScanUID',
            ],
        'ColumnsBool': [],
        'ColumnsInt': [
            'ScanGroupID',
            'ScanDID',
            ],
        'ColumnsDate': [],
        'IndexName': 'ScanGroupID',
        'LoadFromFiles': True,
        'FilebasePattern': '*_scangroupscans',
        }

    SCANS_META_FORMAT = {
        'ColumnsGen': [
            ],
        'ColumnsFloat': [
            'AlphaOffset',
            ],
        'ColumnsStr': [
            'ScanUID',
            'ScanType',
            'ScanTags',
            'DelaysExcl_RedChiSqCutoff_Ch0',
            'DelaysExcl_RedChiSqCutoff_Ch1',
            'DelaysExcl_RedChiSqCutoff_Avg',
            'DataGroupID',
            ],
        'ColumnsBool': [
            'ScanExcl',
            'ScanExcl_AFRBackreflectionMinCutoff',
            'ScanExcl_2SnPPMTMinCutoff',
            'ScanExcl_RedChiSqCutoff_Ch0',
            'ScanExcl_RedChiSqCutoff_Ch1',
            'ScanExcl_RedChiSqCutoff_Avg',
            'ScanExcl_NMinValidPoints',
            'ScanContToH2S6P2019Precision',
            'ChExcl',
            'ChExcl_Ch0',
            'ChExcl_Ch1',
            ],
        'ColumnsInt': [
            'ScanDID',
            'FC',
            'NPointsExcl_AFRBackreflectionMinCutoff',
            'NPointsExcl_2SnPPMTMinCutoff',
            'NPointsExcl_NMinValidPoints',
            ],
        'ColumnsDate': [
            'MeasDate',
            'Timestamp',
            'TimestampFirstPoint',
            'TimestampLastPoint',
            ],
        'IndexName': 'ScanUID',
        'TagColumn': 'ScanMetaTags',
        'LoadFromFiles': True,
        'DuplicatesSubset': ['MeasDate', 'ScanDID'],
        'FilebasePattern': '*_scansmeta',
        }

    SCANS_METAS_FORMAT = {
        'ColumnsGen': list(np.unique(
            DAYS_FORMAT['ColumnsGen']+FCS_FORMAT['ColumnsGen']
            +SCANS_META_FORMAT['ColumnsGen'])),
        'ColumnsFloat': list(np.unique(
            DAYS_FORMAT['ColumnsFloat']+FCS_FORMAT['ColumnsFloat']
            +SCANS_META_FORMAT['ColumnsFloat'])),
        'ColumnsStr': list(np.unique(
            DAYS_FORMAT['ColumnsStr']+FCS_FORMAT['ColumnsStr']
            +SCANS_META_FORMAT['ColumnsStr'])),
        'ColumnsBool': list(np.unique(
            DAYS_FORMAT['ColumnsBool']+FCS_FORMAT['ColumnsBool']
            +SCANS_META_FORMAT['ColumnsBool'])),
        'ColumnsInt': list(np.unique(
            DAYS_FORMAT['ColumnsInt']+FCS_FORMAT['ColumnsInt']
            +SCANS_META_FORMAT['ColumnsInt'])),
        'ColumnsDate': list(np.unique(
            DAYS_FORMAT['ColumnsDate']+FCS_FORMAT['ColumnsDate']
            +SCANS_META_FORMAT['ColumnsDate'])),
        'IndexName': 'ScanUID',
        'DuplicatesSubset': ['MeasDate', 'ScanDID'],
        'LoadFromFiles': False,
        }

    DATA_GROUPS_FORMAT = {
        'ColumnsGen': [
            ],
        'ColumnsFloat': [
            'AlphaOffset',
            'ThetaL',
            '2SnPLaserPower',
            'RedChiSqCutoff',
            'SpeedDistExpSupprCutOff_Sim',
            'SpeedDistExpSupprCutOff_Mean',
            'SpeedDistExpSupprCutOff_Sigma',
            'SpeedDistExpSupprCutOff_Min',
            'SpeedDistExpSupprCutOff_Max',
            'CountWeightingRelOffset',
            ],
        'ColumnsStr': [
            'MeasRun',
            'DataGroupID',
            'ScanType',
            'Isotope',
            'FS',
            'DataGroupTags',
            'DataGroupComments',
            'FitFuncID',
            '1S2SLaserPower',
            '1S2SDetuning',
            'ReqTrajUIDsBigModel_Init',
            'ReqTrajUIDsLFS_Init',
            'ReqTrajUIDsBigModel_rN10',
            'ReqTrajUIDsLFS_rN10',
            'ReqTrajUIDsBigModel_rN07',
            'ReqTrajUIDsLFS_rN07',
            ],
        'ColumnsBool': [],
        'ColumnsInt': [
            ],
        'ColumnsDate': [
            ],
        'IndexName': 'DataGroupID',
        'SetNumericalIndex': False,
        'TagColumn': 'DataGroupTags',
        'LoadFromFiles': True,
        'FilebasePattern': '*_datagroups',
        }

    SET_ANALYSES_FORMAT = {
        'ColumnsGen': [
            ],
        'ColumnsFloat': [
            '2SnPLaserPower',
            '2SnPPMTMinCutoff',
            'AFRBackreflectionMinCutoff',
            'AlphaOffset',
            'ThetaL',
            'Avg_MCS_Ch0_Ch1_CorrCoeff',
            'Avg_MCS_Ch0_Ch1_CorrCoeff_Used',
            'Slope_MCS_Ch0_Ch1_CorrCoeff',
            'Slope_MCS_Ch0_Ch1_CorrCoeff_Used',
            'Offset_MCS_Ch0_Ch1_CorrCoeff',
            'Offset_MCS_Ch0_Ch1_CorrCoeff_Used',
            'RedChiSqCutoff',
            ],
        'ColumnsStr': [
            'SetAnalysisUID',
            'DataGroupAnalysisUID',
            'DataGroupID',
            'AvgMode',
            'FCUID',
            'FreqSampling',
            'CountWeighting',
            'FitFuncID',
            'FitParamID',
            'DelaySetID',
            'DetEffUID',
            'Isotope',
            'FS',
            'MeasRun',
            'ParamX',
            'ScanType',
            'SimScanDataGroupID',
            'SimReqUID',
            'AnalysisParamGroupID',
            ],
        'ColumnsBool': [
            'MatchExpSim',
            'UseDetEff',
            'CorrectCFRBigModel',
            'CorrectCFRLFS',
            'CorrectSOD',
            'IgnoreAlphaOffsetBigModel',
            'RecalculateDerivs',
            'UseDataGroupDetCorr',
            'ScaleByScanFitRCS',
            ],
        'ColumnsInt': [
            'NMinValidPoints',
            'NScansDataGroup',
            'NScans',
            'NScans_Ch0',
            'NScans_Ch1',
            'NScansInitial',
            'NPointsExcl_AFRBackreflectionMinCutoff',
            'NScansExcl_AFRBackreflectionMinCutoff',
            'NPointsExcl_2SnPPMTMinCutoff',
            'NScansExcl_2SnPPMTMinCutoff',
            'NScansExcl_RedChiSqCutoff',
            'NScansExcl_NMinValidPoints',
            ],
        'ColumnsDate': [
            'Timestamp',
            'MeasDateMin',
            'MeasDateMax',
            ],
        'IndexName': 'SetAnalysisUID',
        'SetNumericalIndex': False,
        'TagColumn': '',
        'LoadFromFiles': True,
        'FilebasePattern': '*_setanalyses',
        }

    SET_ANALYSIS_VALUES_FORMAT = {
        'ColumnsGen': [
            ],
        'ColumnsFloat': [
            'Avg_RedChiSq', 'Avg_Sigma', 'Avg_Value', 'Avg_RedChiSqSigma', 'Avg_V_Mean_Value',
            'Avg_Correction_Value', 'Avg_Correction_Sigma', 'Avg_Slope_Value', 'Avg_Slope_Sigma',
            'Offset_RedChiSq', 'Offset_Sigma', 'Offset_Value', 'Offset_RedChiSqSigma',
            'Offset_V_Mean_Value', 'Offset_Correction_Value', 'Offset_Correction_Sigma',
            'Offset_Slope_Value', 'Offset_Slope_Sigma',
            'Slope_RedChiSq', 'Slope_Sigma', 'Slope_Value', 'Slope_RedChiSqSigma',
            'Slope_V_Mean_Value',
            'DlysAvg_RedChiSq_Mean', 'DlysLin_RedChiSq_Mean',
            ],
        'ColumnsStr': [
            'SetAnalysisUID',
            'MCSchannelID',
            ],
        'ColumnsBool': [
            ],
        'ColumnsInt': [
            'NScansCh',
            ],
        'ColumnsDate': [
            ],
        'IndexName': 'SetAnalysisUID',
        'SetNumericalIndex': False,
        'TagColumn': '',
        'LoadFromFiles': True,
        'FilebasePattern': '*_setanalysisvalues',
        }

    SET_ANALYSIS_DELAYS_FORMAT = {
        'ColumnsGen': [
            ],
        'ColumnsFloat': [
            ],
        'ColumnsStr': [
            'SetAnalysisUID',
            'MCSchannelID',
            'DelayID',
            ],
        'ColumnsBool': [
            ],
        'ColumnsInt': [
            ],
        'ColumnsDate': [
            ],
        'IndexName': 'SetAnalysisUID',
        'SetNumericalIndex': False,
        'TagColumn': '',
        'LoadFromFiles': True,
        'FilebasePattern': '*_setanalysisdelays',
        }

    DC_STARK_MEAS_FORMAT = {
        'ColumnsGen': [
            ],
        'ColumnsFloat': [
            '2SnPLaserPower',
            'ThetaL',
            'DCStarkParabolaFit_ChiSq',
            'DCStarkParabolaFit_RedChiSq',
            'DCStarkParabolaFit_QuadrCoeff_Value',
            'DCStarkParabolaFit_Center_Value',
            'DCStarkParabolaFit_Offset_Value',
            'DCStarkParabolaFit_QuadrCoeff_Sigma',
            'DCStarkParabolaFit_Center_Sigma',
            'DCStarkParabolaFit_Offset_Sigma',
            'DCStarkParabolaFit_Shift_Value',
            'DCStarkParabolaFit_Shift_Sigma',
            'DCStarkParabolaFit_Shift_Sigma_Uncorr',
            'DCStarkParabolaFit_Cov01_Value',
            'RedChiSqCutoffFirst',
            'RedChiSqCutoff',
            'AFRBackreflectionMinCutoff',
            '2SnPPMTMinCutoff',
            'EFieldStrength_Min',
            'EFieldStrength_Max',
            'EFieldStrength_AbsMin',
            'EFieldStrength_AbsMax',
            'Volt_Min',
            'Volt_Max',
            'Volt_AbsMin',
            'Volt_AbsMax',
            'EFieldStrengthPerVolt',
            ],
        'ColumnsStr': [
            'DCStarkMeasUID',
            'Isotope',
            'FS',
            'MCSchannelID',
            'DelayID',
            'EFieldDirect',
            'FitFuncID_FieldOff',
            'FitFuncID_FieldOn',
            'MatchedDataGroupID',
            'SimReqUID',
            ],
        'ColumnsBool': [
            'DCStarkParabolaFit_Error',
            ],
        'ColumnsInt': [
            'ScanGroupID',
            'FC',
            'NScansInScanGroup',
            'NScans',
            'NVoltPoints',
            'NScans_FieldOn',
            'NScans_FieldOff',
            'NScanFits',
            'NScanFits_FieldOn',
            'NScanFits_FieldOff',
            'NDelays',
            'NDelays_FieldOn',
            'NDelays_FieldOff',
            'NScansExcl',
            'NScansExcl_AFRBackreflectionMinCutoff',
            'NScansExcl_2SnPPMTMinCutoff',
            'NPointsExcl_AFRBackreflectionMinCutoff',
            'NPointsExcl_2SnPPMTMinCutoff',
            'NDelaysExcl_RedChiSqCutoff_FieldOn',
            'NDelaysExcl_RedChiSqCutoff_FieldOff',
            ],
        'ColumnsDate': [
            'MeasDate',
            'Timestamp',
            'TimestampMin',
            'TimestampMax',
            ],
        'IndexName': 'DCStarkMeasUID',
        'SetNumericalIndex': False,
        'TagColumn': '',
        'LoadFromFiles': True,
        'FilebasePattern': '*_dcstarkmeas',
        }

    SIM_UNCERT_DEFS_FORMAT = {
        'ColumnsGen': [
            ],
        'ColumnsFloat': [
            'Range_Min',
            'Range_Max',
            'UncertScalingFactor',
            ],
        'ColumnsStr': [
            'SimUncertID',
            'Name',
            'Type',
            'Unit',
            'LeadingOrder',
            'Range_Type',
            'SimReqUID_Type',
            'SimReqUID_Suffix',
            'SimReqUID_Suffix_List',
            'SimReqUID_Suffix_Min',
            'SimReqUID_Suffix_Max',
            ],
        'ColumnsBool': [
            'SameTrajSetAsRef',
            'AllSimReqsRequired',
            'Correlated',
            'IncludeInUncert',
            ],
        'ColumnsInt': [
            ],
        'ColumnsDate': [
            ],
        'IndexName': 'SimUncertID',
        'SetNumericalIndex': False,
        'TagColumn': '',
        'LoadFromFiles': True,
        'FilebasePattern': '*_simuncertdefs',
        }

    UNCERT_DEFS_FORMAT = {
        'ColumnsGen': [
            ],
        'ColumnsFloat': [
            ],
        'ColumnsStr': [
            'UncertID',
            'Name',
            'UnitType',
            ],
        'ColumnsBool': [
            'DeriveFromMeasFS',
            'IncludeInTotal',
            'IncludeInTotalCorr',
            'CorrelatedBetweenMeasFS',
            ],
        'ColumnsInt': [
            ],
        'ColumnsDate': [
            ],
        'IndexName': 'UncertID',
        'SetNumericalIndex': False,
        'TagColumn': '',
        'LoadFromFiles': True,
        'FilebasePattern': '*_uncertdefs',
        }

    DF_FORMATS = {
        'dfDays': DAYS_FORMAT,
        'dfFCs': FCS_FORMAT,
        'dfFCDays': FCDAYS_FORMAT,
        'dfScanGroups': SCAN_GROUPS_FORMAT,
        'dfScanGroupScans': SCAN_GROUP_SCANS_FORMAT,
        'dfScansMeta': SCANS_META_FORMAT,
        'dfScansMetas': SCANS_METAS_FORMAT,
        'dfFinesse': FINESSE_FORMAT,
        'dfECTransmission': ECTRANSMISSION_FORMAT,
        'dfDataGroups': DATA_GROUPS_FORMAT,
        'dfSetAnalyses': SET_ANALYSES_FORMAT,
        'dfSetAnalysisValues': SET_ANALYSIS_VALUES_FORMAT,
        'dfSetAnalysisDelays': SET_ANALYSIS_DELAYS_FORMAT,
        'dfDCStarkMeas': DC_STARK_MEAS_FORMAT,
        'dfSimUncertDefs': SIM_UNCERT_DEFS_FORMAT,
        'dfUncertDefs': UNCERT_DEFS_FORMAT,
        }

    def __init__(self, dataclass):
        """
        Initialize experimental metadata class.
        An instance of `pyh.data.pyh_data` muss be passed as argument `dataclass`.
        """
        super().__init__()

        self.DataClass = dataclass

    @property
    def dfDays(self):
        """Get pandas DataFrame `dfDays`."""
        return self.dfs['dfDays']

    @dfDays.setter
    def dfDays(self, dfDays):
        """Set pandas DataFrame `dfDays`."""
        self.dfs['dfDays'] = dfDays

    @property
    def dfFCs(self):
        """Get pandas DataFrame `dfFCs`."""
        return self.dfs['dfFCs']

    @dfFCs.setter
    def dfFCs(self, dfFCs):
        """Set pandas DataFrame `dfFCs`."""
        self.dfs['dfFCs'] = dfFCs

    @property
    def dfFCDays(self):
        """Get pandas DataFrame `dfFCDays`."""
        return self.dfs['dfFCDays']

    @dfFCDays.setter
    def dfFCDays(self, dfFCDays):
        """Set pandas DataFrame `dfFCDays`."""
        self.dfs['dfFCDays'] = dfFCDays

    @property
    def dfScanGroups(self):
        """Get pandas DataFrame `dfScanGroups`."""
        return self.dfs['dfScanGroups']

    @dfScanGroups.setter
    def dfScanGroups(self, dfScanGroups):
        """Set pandas DataFrame `dfScanGroups`."""
        self.dfs['dfScanGroups'] = dfScanGroups

    @property
    def dfScanGroupScans(self):
        """Get pandas DataFrame `dfScanGroupScans`."""
        return self.dfs['dfScanGroupScans']

    @dfScanGroupScans.setter
    def dfScanGroupScans(self, dfScanGroupScans):
        """Set pandas DataFrame `dfScanGroupScans`."""
        self.dfs['dfScanGroupScans'] = dfScanGroupScans

    @property
    def dfScansMeta(self):
        """Get pandas DataFrame `dfScansMeta`."""
        return self.dfs['dfScansMeta']

    @dfScansMeta.setter
    def dfScansMeta(self, dfScansMeta):
        """Set pandas DataFrame `dfScansMeta`."""
        self.dfs['dfScansMeta'] = dfScansMeta

    @property
    def dfScansMetas(self):
        """Get pandas DataFrame `dfScansMetas`."""
        return self.dfs['dfScansMetas']

    @dfScansMetas.setter
    def dfScansMetas(self, dfScansMetas):
        """Set pandas DataFrame `dfScansMetas`."""
        self.dfs['dfScansMetas'] = dfScansMetas

    @property
    def dfFinesse(self):
        """Get pandas DataFrame `dfFinesse`."""
        return self.dfs['dfFinesse']

    @dfFinesse.setter
    def dfFinesse(self, dfFinesse):
        """Set pandas DataFrame `dfFinesse`."""
        self.dfs['dfFinesse'] = dfFinesse

    @property
    def dfECTransmission(self):
        """Get pandas DataFrame `dfECTransmission`."""
        return self.dfs['dfECTransmission']

    @dfECTransmission.setter
    def dfECTransmission(self, dfECTransmission):
        """Set pandas DataFrame `dfECTransmission`."""
        self.dfs['dfECTransmission'] = dfECTransmission

    @property
    def dfDataGroups(self):
        """Get pandas DataFrame `dfDataGroups`."""
        return self.dfs['dfDataGroups']

    @dfDataGroups.setter
    def dfDataGroups(self, dfDataGroups):
        """Set pandas DataFrame `dfDataGroups`."""
        self.dfs['dfDataGroups'] = dfDataGroups

    @property
    def dfSetAnalyses(self):
        """Get pandas DataFrame `dfSetAnalyses`."""
        return self.dfs['dfSetAnalyses']

    @dfSetAnalyses.setter
    def dfSetAnalyses(self, dfSetAnalyses):
        """Set pandas DataFrame `dfSetAnalyses`."""
        self.dfs['dfSetAnalyses'] = dfSetAnalyses

    @property
    def dfSetAnalysisValues(self):
        """Get pandas DataFrame `dfSetAnalysisValues`."""
        return self.dfs['dfSetAnalysisValues']

    @dfSetAnalysisValues.setter
    def dfSetAnalysisValues(self, dfSetAnalysisValues):
        """Set pandas DataFrame `dfSetAnalysisValues`."""
        self.dfs['dfSetAnalysisValues'] = dfSetAnalysisValues

    @property
    def dfSetAnalysisDelays(self):
        """Get pandas DataFrame `dfSetAnalysisDelays`."""
        return self.dfs['dfSetAnalysisDelays']

    @dfSetAnalysisDelays.setter
    def dfSetAnalysisDelays(self, dfSetAnalysisDelays):
        """Set pandas DataFrame `dfSetAnalysisDelays`."""
        self.dfs['dfSetAnalysisDelays'] = dfSetAnalysisDelays

    @property
    def dfDCStarkMeas(self):
        """Get pandas DataFrame `dfDCStarkMeas`."""
        return self.dfs['dfDCStarkMeas']

    @dfDCStarkMeas.setter
    def dfDCStarkMeas(self, dfDCStarkMeas):
        """Set pandas DataFrame `dfSetAnalysisDelays`."""
        self.dfs['dfDCStarkMeas'] = dfDCStarkMeas

    @property
    def dfSimUncertDefs(self):
        """Get pandas DataFrame `dfSimUncertDefs`."""
        return self.dfs['dfSimUncertDefs']

    @dfSimUncertDefs.setter
    def dfSimUncertDefs(self, dfSimUncertDefs):
        """Set pandas DataFrame `dfSimUncertDefs`."""
        self.dfs['dfSimUncertDefs'] = dfSimUncertDefs

    @property
    def dfUncertDefs(self):
        """Get pandas DataFrame `dfUncertDefs`."""
        return self.dfs['dfUncertDefs']

    @dfUncertDefs.setter
    def dfUncertDefs(self, dfUncertDefs):
        """Set pandas DataFrame `dfUncertDefs`."""
        self.dfs['dfUncertDefs'] = dfUncertDefs

    def write_metadata(self, df_id, filepath, **kwargs):
        """
        Write metadata from DataFrame `df_id` stored in this class to file `filepath`.
        """
        pyhs.files.write_table_file(
            self.dfs[df_id],
            filepath,
            **kwargs,
            )

    def load_metadata(self, df_ids_to_load=None, metadata_dirs=None):
        """
        Load metadata from files into DataFrames stored in this class.
        The IDs of the DataFrames that should be loaded can be supplied by `df_ids_to_load` (list).
        If set to None (default), all available DataFrames will be loaded.
        The directories from which the metadata should be loaded can be supplied by `metadata_dirs`
        (list). If set to None (default), the default metadata directory as specified in
        `DataClass.settings.dir_metadata` will be searched.
        """
        if df_ids_to_load is None:
            df_ids_to_load = [
                df_id for df_id, elem in self.dfs.items()
                if self.df_formats[df_id].get('LoadFromFiles', False)]
        else:
            df_ids_to_load = list(np.atleast_1d(df_ids_to_load))
        dfs = {df_id: None for df_id in df_ids_to_load}
        df_templates = self.df_templates
        num_loaded = {df_id: 0 for df_id in dfs.keys()}

        if metadata_dirs is None:
            metadata_dirs = Path(self.DataClass.settings.dir_metadata)
        metadata_dirs = np.atleast_1d(metadata_dirs)
        metadata_dirs = [Path(dir_) for dir_ in metadata_dirs]

        for metadata_dir in metadata_dirs:

            logger.info(
                'Scanning directory \'%s\' for experimental metadata', metadata_dir.resolve())

            for df_id, df in dfs.items():

                # Load metadata
                df_filepaths = pyhs.files.find_table_files(
                    metadata_dir, self.FILE_EXT_TO_READ,
                    filebase_pattern=self.df_formats[df_id]['FilebasePattern'])
                if len(df_filepaths) == 0:
                    logger.info(
                        'Found no %s metadata file, not loading any data', df_id)
                else:
                    logger.info(
                        f'Found {len(df_filepaths):d} {df_id} metadata file(s)')
                    df_ = None
                    for df_filepath in df_filepaths:
                        try:
                            df__ = pyhs.files.load_table_file(
                                df_filepath,
                                date_columns=self.df_formats[df_id]['ColumnsDate'],
                                str_columns=self.df_formats[df_id]['ColumnsStr'],
                                index_col=self.df_formats[df_id]['IndexName'])
                        except pyhs.files.PyhsFileError as e:
                            logger.error(e)
                        else:
                            if df_ is None:
                                df_ = df__
                            else:
                                df_ = pd.concat([df_, df__])
                    num_loaded[df_id] = len(df_)
                    # Cast data format
                    df_ = self.cast_containers_to_data_format(
                        df_, df_id)
                    if df is None:
                        dfs[df_id] = df_
                    else:
                        dfs[df_id] = pd.concat([df, df_])

        # Use empty templates for DataFrames where no metadata was found
        dfs = {
            df_id: (df_templates[df_id] if df is None else df)
            for df_id, df in dfs.items()}

        # Convert daytimes (seconds in day) to datetime
        if 'dfFCs' in df_ids_to_load:
            dfFCs = dfs['dfFCs']
            daytime_start_str = 'Daytime'
            columns_daytimes = [
                elem for elem in self.FCS_COLUMNS_FLOAT if elem.startswith(daytime_start_str)]
            columns_date = ['Timestamp'+elem[len(daytime_start_str):] for elem in columns_daytimes]
            for column_daytime, column_date in zip(columns_daytimes, columns_date):
                dfFCs[column_date] = [
                    self.DataClass.dayTimedeltaToTime(
                        fc[column_daytime], refTimestamp=fc['MeasDate'])
                    for _, fc in dfFCs.iterrows()]
            # Add duration of FCs
            dfFCs['FCDuration'] = [
                elem.seconds for elem in
                dfFCs['TimestampFCMax']-dfFCs['TimestampFCMin']]
            dfs['dfFCs'] = dfFCs

        # Combine dfFCs and dfDays
        if 'dfFCs' in df_ids_to_load and 'dfDays' in df_ids_to_load:
            dfs['dfFCDays'] = (
                dfs['dfFCs']
                .reset_index()
                .merge(
                    dfs['dfDays'],
                    how='left',
                    left_on=['MeasDate'],
                    right_on=['MeasDate'],
                )
                .set_index(self.df_formats['dfFCs']['IndexName']))

        for df_id, df in dfs.items():

            # Cast to data format
            df = self.cast_containers_to_data_format(
                df, df_id)
            # Drop duplicates and set numerical index if specified
            df = self.drop_duplicates_and_reindex(df, df_id)
            # Cast to data format
            df = self.cast_containers_to_data_format(
                df, df_id)

            self.dfs[df_id] = df

            logger.info(
                f'Added {len(df)} entries to metadata {df_id}')

        # Combining dfDays, dfFCs, and dfScansMeta
        self.merge_scan_metas()

    def append_to_scans_meta(self, scan_dids, date, metadata):
        """
        Append scans with Scan DIDs `scan_dids` (ndarray), recorded on date `date` (pd.Timestamp),
        and with additional metadata `metadata` (dict), to metadata as stored in DataFrame
        `dfScansMeta` (stored in class).
        """
        metadata = {
            **self.default_metadata_append,
            **metadata,
            }
        dfScansMeta = self.dfScansMeta
        scan_dids = list(itertools.chain(*scan_dids))
        scan_dids_unique = np.unique(scan_dids)
        logger.info(
            'Adding list of {:d} scan DID(s) of type \'{}\' for date {}'
            .format(len(scan_dids_unique), metadata['ScanType'], date.strftime('%Y-%m-%d')))
        if len(scan_dids_unique) < len(scan_dids):
            logger.warning(
                '{:d} duplicate(s) have been removed from list'
                .format(len(scan_dids)-len(scan_dids_unique)))
            scan_dids = scan_dids_unique

        dfScansMeta_append = self.init_data_container('dfScansMeta')
        time_range = pyhs.time.meas_date_to_time_range(
            date,
            self.DataClass.DAY_SCAN_COUNT_ROLLOVER_TIME)
        mask = (
            (self.DataClass.dfScans['ScanDID'].isin(scan_dids))
            & (self.DataClass.dfScans['Timestamp'].between(*time_range))
            & (self.DataClass.dfScans['Completed'] == True)
            )
        if np.sum(mask) < len(scan_dids):
            logger.warning(
                'Found only {:d} of {:d} scan(s)'
                .format(np.sum(mask), len(scan_dids)))
            if np.sum(mask) != 0:
                in1d_ = np.in1d(scan_dids, self.DataClass.dfScans[mask]['ScanDID'])
                logger.warning(
                    'Did not find scan DID(s) {}'
                    .format(', '.join(
                        [str(elem) for elem in np.array(scan_dids)[~in1d_]])))
        dfScansMeta_append[['Timestamp', 'ScanDID']] = (
            self.DataClass.dfScans[mask][['Timestamp', 'ScanDID']])

        dfScansMeta_append = (
            self.set_df_index_name(dfScansMeta_append, 'dfScansMeta'))
        dfScansMeta_append['MeasDate'] = date
        for key, value in metadata.items():
            dfScansMeta_append[key] = value
        # Cast data format, append, cast format again
        dfScansMeta_append = self.cast_containers_to_data_format(
            dfScansMeta_append, 'dfScansMeta')
        dfScansMeta = pd.concat([dfScansMeta, dfScansMeta_append])
        dfScansMeta = self.cast_containers_to_data_format(
            dfScansMeta, 'dfScansMeta')
        # Drop duplicates and set numerical index if specified
        dfScansMeta = self.drop_duplicates_and_reindex(
            dfScansMeta, 'dfScansMeta')
        dfScansMeta = self.cast_containers_to_data_format(
            dfScansMeta, 'dfScansMeta')
        self.dfScansMeta = dfScansMeta

    def add_fcs_to_scan_meta(self, scan_uids=None):
        """
        Add freezing cycle metadata to scan metadata by matching timestamp of scan
        with start and stop timestamp of freezing cycles,
        under the condition that the measurement day matches.
        """
        dfScansMeta = self.dfs['dfScansMeta']
        dfFCs = self.dfs['dfFCs']
        if scan_uids is None:
            scan_uids = dfScansMeta.index
        dfScansMeta = dfScansMeta.loc[scan_uids]
        mask_fcs = (
            (dfFCs['TimestampFCMax'] >= dfScansMeta['Timestamp'].min())
            & (dfFCs['TimestampFCMin'] <= dfScansMeta['Timestamp'].max())
            )
        num_scans_matched = 0
        num_fcs_matched = 0
        for _, fc in dfFCs[mask_fcs].iterrows():
            mask_scans = (
                dfScansMeta['Timestamp'].between(
                    fc['TimestampFCMin'], fc['TimestampFCMax']))
            if np.sum(mask_scans) == 0:
                continue
            scan_dates = pd.to_datetime(
                dfScansMeta[mask_scans]['MeasDate'].unique())
            if len(scan_dates) > 1 or scan_dates[0] != fc['MeasDate']:
                logger.error(
                    f'Date of freezing cycle ({fc["Date"]}) does not match date(s) of '
                    + 'scan(s) ({})'.format(
                        ', '.join([str(elem) for elem in scan_dates])))
                continue
            dfScansMeta.loc[mask_scans, 'FC'] = fc['FC']
            num_scans_matched += np.sum(mask_scans)
            num_fcs_matched += 1
        logger.info(
            f'Matched {num_scans_matched:d} scan(s) to {num_fcs_matched:d} FC(s)')
        num_scans_not_matched = len(scan_uids) - num_scans_matched
        if num_scans_not_matched > 0:
            logger.warning(
                f'Could not match {num_scans_not_matched:d} scan(s) to FCs')
        self.dfScansMeta.loc[scan_uids] = self.cast_containers_to_data_format(
            dfScansMeta, 'dfScansMeta')

    def merge_scan_metas(self):
        """
        Add FC and day metadata from DataFrames `dfFCs` and `dfDays` to scan metadata stored in
        DataFrame `dfScansMeta` (all stored in class).
        """
        dfScansMetas = (
            self.dfs['dfScansMeta']
            .reset_index()
            .merge(
                self.dfs['dfFCs'].reset_index(),
                how='left',
                left_on=['MeasDate', 'FC'],
                right_on=['MeasDate', 'FC'],
            )
            .set_index(self.df_formats['dfScansMeta']['IndexName']))
        dfScansMetas = (
            dfScansMetas
            .reset_index()
            .merge(
                self.dfs['dfDays'].reset_index(),
                how='left',
                left_on=['MeasDate'],
                right_on=['MeasDate'],
            )
            .set_index(self.df_formats['dfScansMeta']['IndexName']))
        self.dfs['dfScansMetas'] = self.cast_containers_to_data_format(
            dfScansMetas, 'dfScansMetas')

    def update_excluded_scans(self, exclude_channels=False,
                              scan_types=None):
        """
        Update exclusions of scans based on scan metadata, as stored in DataFrame `dfScansMeta`
        (stored in class).
        """
        dfScansMeta = self.dfs['dfScansMeta']
        if scan_types is not None:
            mask = dfScansMeta['ScanType'].isin(scan_types)
        else:
            mask = pd.Series(True, index=dfScansMeta.index)
        columns_scan_excl = [
            column for column in dfScansMeta.columns
            if column.startswith('ScanExcl_')]
        columns_ch_excl = [
            column for column in dfScansMeta.columns
            if column.startswith('ChExcl_')]
        dfScansMeta.loc[mask, 'ChExcl'] = (
            dfScansMeta.loc[mask, columns_ch_excl].any(axis=1))
        columns_excl = columns_scan_excl
        if exclude_channels:
            columns_excl += columns_ch_excl
        num_ch_excl = np.sum(dfScansMeta.loc[mask, columns_ch_excl].any(axis=1))
        # Exclude scans for which any exclusion parameter is met
        # or for which all channels are excluded, independent on whether
        # channels are part of exclusion parameters
        dfScansMeta.loc[mask, 'ScanExcl'] = (
            dfScansMeta.loc[mask, columns_excl].any(axis=1)
            | dfScansMeta.loc[mask, columns_ch_excl].all(axis=1))
        dfScansMeta.loc[mask, 'ScanContToH2S6P2019Precision'] = (
            ~dfScansMeta.loc[mask, 'ScanExcl'])
        logger.info(
            f'Marked {np.sum(dfScansMeta.loc[mask, "ScanExcl"]):d} scan(s) as excluded'
            f', {"not " if not exclude_channels else ""}'
            + f'including {num_ch_excl:d} channel exclusion(s)')
        self.dfs['dfScansMeta'] = dfScansMeta

    def get_datagroup_scans(self, data_group_id, df_to_mask=None):
        """
        Get scan UIDs of non-excluded scans in data group `data_group_id` (str).
        """
        mask_scans_metas = (
            (self.dfs['dfScansMetas']['DataGroupID'] == data_group_id)
            & (self.dfs['dfScansMetas']['ScanExcl'] == False))
        scan_uids = self.dfs['dfScansMetas'][mask_scans_metas].index

        if df_to_mask is not None:
            mask_df = pd.Series(False, index=df_to_mask.index)
            mask_df.loc[scan_uids] = True
        else:
            mask_df = None

        return scan_uids, mask_df

    def get_fc_mask(self, y=2019, m=6, d=5, fcs=None):
        """
        Get mask for DataFrame `dfFCs` (stored in class( for given date
        (year `y`, month `m`, day `d` (ints)) and freezing cycle(s) `fcs` (list).
        """
        date = datetime.datetime(year=y, month=m, day=d)
        mask_fcs = (self.dfFCs['MeasDate'] == date)
        if fcs is None:
            logger.info(f'Found {mask_fcs.sum():d} requested FC(s).')
        else:
            fcs = np.array([fcs]).flatten()
            mask_fcs &= (self.dfFCs['FC'].isin(fcs))
            logger.info(
                f'Found {mask_fcs.sum():d} of {len(fcs):d} requested FC(s).')
        return mask_fcs

    def get_fc_times(self, y=2019, m=6, d=5, fcs=None):
        """
        Select start and stop time for given date
        (year `y`, month `m`, day `d` (ints)) and freezing cycle(s) `fcs` (list).
        """
        mask_fcs = self.get_fc_mask(y=y, m=m, d=d, fcs=fcs)
        return (np.array([
            np.min(self.dfFCs[mask_fcs]['TimestampFCMin'].values),
            np.max(self.dfFCs[mask_fcs]['TimestampFCMax'].values)]))

    def sel_fc(self, dfAll_scans, y=2019, m=6, d=5, fcs=None):
        """
        Select data from DataFrame `dfAll_scans` for given date
        (year `y`, month `m`, day `d` (ints)) and freezing cycle(s) `fcs` (list).
        """
        date = datetime.datetime(year=y, month=m, day=d)
        mask_fcs = (self.dfFCs['MeasDate'] == date)
        if fcs is None:
            logger.info(f'Found {mask_fcs.sum():d} FC(s).')
        else:
            fcs = np.array([fcs]).flatten()
            mask_fcs &= (self.dfFCs['FC'].isin(fcs))
            logger.info(
                f'Found {mask_fcs.sum():d} of {len(fcs):d} requested FC(s).')
        mask_scans = pd.Series(False, index=dfAll_scans.index)
        for _, fc in self.dfFCs[mask_fcs].iterrows():
            time_lim = [
                self.DataClass.dayTimedeltaToTime(
                    elem, refTimestamp=pd.to_datetime(fc['MeasDate']))
                for elem in fc[['DaytimeFCMin', 'DaytimeFCMax']]]
            mask_scans |= dfAll_scans['dfData_Timestamp'].between(*time_lim)
        return mask_scans

    def get_mask_tags_exclude_any(self, df_id, tags, df=None):
        """
        Get boolean mask for DataFrame with ID `df_id` where entries that
        contain any of the tags in `tags` (list) are excluded.
        """
        if df is None:
            df = self.dfs[df_id]
        tags = list(np.array([tags]).flatten())
        tags_column = self.df_formats[df_id]['TagColumn']
        return np.array([
            np.all([tag not in fc[tags_column].split(';')
            for tag in tags])
            for _, fc in df.iterrows()])

    def get_mask_tags_include_any(self, df_id, tags, df=None):
        """
        Get boolean mask for DataFrame with ID `df_id` where entries that
        contain any of the tags in `tags` (list) are included.
        """
        if df is None:
            df = self.dfs[df_id]
        tags = list(np.array([tags]).flatten())
        tags_column = self.df_formats[df_id]['TagColumn']
        return np.array([
            np.any([tag in fc[tags_column].split(';')
            for tag in tags])
            for _, fc in df.iterrows()])

    def get_ec_transmission(self, timestamp, meas_date=False):
        """
        Get entry from DataFrame `dfECTransmission` (stored in class)
        closest to `timestamp` (`pd.Timestamp`),
        under the condition that measurement date of `timestamp` is same or higher
        than entry.
        If `meas_date` is set to True, `timestamp` is treated as a measurement date,
        and the first entry on that measurement date is returned.
        """
        if meas_date:
            timestamp = pyhs.time.meas_date_to_timestamp(
                timestamp, self.DataClass.DAY_SCAN_COUNT_ROLLOVER_TIME)
        mask = (
            self.dfs['dfECTransmission']['MeasDate']
            <= self.DataClass.get_meas_dates(timestamp))
        if len(self.dfs['dfECTransmission']) == 0:
            msg = (
                'Experimental metadata class error: dfECTransmission has no entries, '
                + 'cannot get closest entry')
            logger.error(msg)
            raise pyha.defs.PyhaError(msg)
        if np.sum(mask) == 0:
            df = self.dfs['dfECTransmission']
        else:
            df = self.dfs['dfECTransmission'][mask]
        id_ = (np.abs(df['Timestamp'] - timestamp)).idxmin()
        sr = self.dfs['dfECTransmission'].loc[id_]
        if np.sum(mask) == 0:
            logger.warning(
                'Experimental metadata class warning: dfECTransmission has no entries '
                + f'before measurement date {pd.to_datetime(self.DataClass.get_meas_dates(timestamp)):%d.%m.%Y}'
                + f', using future entry from measurement date {pd.to_datetime(sr["MeasDate"]):%d.%m.%Y}'
                )
        return sr

    def add_fcs_plot_time(self, fc_uids=None,
                          meas_day_timedelta=None):
        """
        Add plot time to selected FCs within list of FC UIDs `fc_uids`.
        The plot time maintains the time distance between FCs within one
        measurement day, but reduces the gap between measurement days to
        `meas_day_timedelta` (`pd.Timedelta`) such that many FCs may be plotted
        on a single axis without too many breaks.
        """
        dfFCs = self.dfs['dfFCs']
        if fc_uids is not None:
            mask_fcs_total = (dfFCs.index.isin(fc_uids))
        else:
            mask_fcs_total = pd.Series(True, index=dfFCs.index)
        if meas_day_timedelta is None:
            meas_day_timedelta = pd.to_timedelta(5, 'h')
        plot_date = datetime.datetime(year=1970, month=1, day=1)
        for fc_uid, fc in dfFCs[mask_fcs_total].iterrows():

            mask_fcs_previous = (
                mask_fcs_total
                & (dfFCs['TimestampFCMin'] < fc['TimestampFCMin'])
                )
            if np.sum(mask_fcs_previous) > 0:
                fc_prev = (
                    dfFCs[mask_fcs_previous]
                    .iloc[dfFCs[mask_fcs_previous]['TimestampFCMin'].argmax()])
                if fc['MeasDate'] == dfFCs[mask_fcs_previous]['MeasDate'].max():
                    # Same day as previous FC
                    plot_date += (
                        fc['TimestampFCMin'] - fc_prev['TimestampFCMin'])
                else:
                    # Different day than previous FC
                    plot_date += (
                        fc_prev['TimestampFCMax'] - fc_prev['TimestampFCMin']
                        + meas_day_timedelta)

            dfFCs.loc[fc_uid, 'Helper_PlotTime'] = plot_date

        self.dfs['dfFCs'] = dfFCs

    def add_fcs_plot_time_to_scans(self, dfAll_scans, fc_uids=None):
        """
        Add the plot time of the selected FCs within list of FC UIDs `fc_uids`
        to the supplied `dfAll_scans` data frame.
        """
        dfFCs = self.dfs['dfFCs']
        if fc_uids is None:
            fc_uids = dfFCs.index
        for i_fc, fc_uid in enumerate(fc_uids):
            fc = dfFCs.loc[fc_uid]
            if pd.isna(fc['Helper_PlotTime']):
                continue
            mask_fc = (dfAll_scans['dfScans_ExpParams_FCUID'] == fc_uid)
            scan_uids_fc = dfAll_scans[mask_fc].index
            plot_dates_fc = (
                dfAll_scans[mask_fc]['dfScans_Timestamp']
                - fc['TimestampFCMin']
                + fc['Helper_PlotTime'])
            dfAll_scans.loc[scan_uids_fc, 'dfScans_Helper_PlotTime'] = (
                plot_dates_fc)

        return dfAll_scans

    def get_fcs_plot_time_labels(self, fc_uids=None, mode='FC'):
        """
        Get axis tick positions and labels based on the plot time for
        the selected FCs within list of FC UIDs `fc_uids`.
        """
        dfFCs = self.dfs['dfFCs']
        if fc_uids is None:
            fc_uids = dfFCs.index
        xticks = []
        xticklabels = []
        if mode in ['FC', 'FCName']:
            for i_fc, fc_uid in enumerate(fc_uids):
                fc = dfFCs.loc[fc_uid]
                # Built plot labels for FCs
                xticks.append(
                    fc['Helper_PlotTime'] + (fc['TimestampFCMax'] - fc['TimestampFCMin'])/2)
                timestamp_fc_mean = (
                    fc['TimestampFCMin']+(fc['TimestampFCMax']-fc['TimestampFCMin'])/2)
                if mode == 'FC':
                    xticklabels.append(
                        timestamp_fc_mean.strftime('%d.%m') + f' FC{fc["FC"]}')
                elif mode == 'FCName':
                    xticklabels.append(
                        timestamp_fc_mean.strftime('%d.%m') + f' {fc["FCName"]}')
        else:
            for meas_date in dfFCs.loc[fc_uids]['MeasDate'].unique():
                mask_fcs_meas_date = (
                    (dfFCs.index.isin(fc_uids))
                    & (dfFCs['MeasDate'] == meas_date))
                timestamp_fc_min_mean = dfFCs[mask_fcs_meas_date]['TimestampFCMin'].mean()
                timestamp_fc_max_mean = dfFCs[mask_fcs_meas_date]['TimestampFCMax'].mean()
                xticks.append(
                    dfFCs[mask_fcs_meas_date]['Helper_PlotTime'].mean()
                    +(timestamp_fc_max_mean-timestamp_fc_min_mean)/2)
                timestamp_fc_mean = (
                    timestamp_fc_min_mean+(timestamp_fc_max_mean-timestamp_fc_min_mean)/2)
                xticklabels.append(
                    timestamp_fc_mean.strftime('%d.%m'))

        return xticks, xticklabels

    def get_scan_group_scan_dids(self, scan_group, mcs_channel_id=None):
        """
        Get array of scan DIDs contained in scan group `scan_group` (string).
        """
        # Remove all scans for channel if flagged in scan group
        if mcs_channel_id is not None and \
                scan_group.get(f'ChExcl_{mcs_channel_id}', False):
            logger.info(
                f'Scan group \'{scan_group.name}\': '
                +f'Excluding MCS channel \'{mcs_channel_id}\'')
            return np.array([])

        # Find columns containing scan DIDs
        scan_did_columns_min = [
            int(elem[10:]) for elem in scan_group.keys()
            if elem.startswith('ScanDIDMin')]
        scan_did_columns_max = [
            int(elem[10:]) for elem in scan_group.keys()
            if elem.startswith('ScanDIDMax')]
        scan_dids_columns = np.intersect1d(scan_did_columns_min, scan_did_columns_max)
        # Assemble list of scan DIDs
        scan_dids = []
        for scan_did_column in scan_dids_columns:
            if scan_group[f'ScanDIDMin{scan_did_column:d}'] == -1:
                continue
            elif  scan_group[f'ScanDIDMax{scan_did_column:d}'] == -1:
                scan_dids += [scan_group[f'ScanDIDMin{scan_did_column:d}']]
            else:
                scan_dids += list(range(
                    scan_group[f'ScanDIDMin{scan_did_column:d}'],
                    scan_group[f'ScanDIDMax{scan_did_column:d}']+1))
        # Remove exluded scan DIDs
        scan_dids_excluded = [
            int(elem) for elem in scan_group['ScanDIDExcl'].split(';')
            if len(elem) > 0 and pyha.util.data.is_int(elem)]
        # Remove scans for given channel if flagged in scan group
        if mcs_channel_id is not None and \
                scan_group.get(f'ScanDIDExcl_{mcs_channel_id}', '') != '':
            scan_dids_excluded_ch = [
                int(elem) for elem in scan_group[f'ScanDIDExcl_{mcs_channel_id}'].split(';')
                if len(elem) > 0 and pyha.util.data.is_int(elem)]
            logger.info(
                f'Scan group \'{scan_group.name}\': '
                +f'Excluding {len(scan_dids_excluded_ch):d} scan(s) for '
                +f'MCS channel \'{mcs_channel_id}\'')
            scan_dids_excluded += scan_dids_excluded_ch
        scan_dids = np.array(scan_dids)[~np.in1d(scan_dids, scan_dids_excluded)]

        return scan_dids

    def get_scan_group_scan_uids(self, dfAll_scans, scan_group,
                                 mcs_channel_id=None):
        """
        Get array of scan UIDs contained in scan group `scan_group` (string),
        using the supplied DataFrame `dfAll_scans`.
        """
        scan_dids = self.get_scan_group_scan_dids(scan_group, mcs_channel_id)
        mask_scans = (
            (dfAll_scans[ep__+'MeasDate'] == scan_group['MeasDate'])
            & (dfAll_scans['dfScans_ScanDID'].isin(scan_dids))
            )
        return dfAll_scans[mask_scans].index.values


# Initialize metadata class
#ExpMetadata = Metadata(DataClass)
## Load metadata from Excel files
## This will search in the directory dir_metadata defined in pyh_settings
## and all subdirectories
#ExpMetadata.FILE_EXT_TO_READ = ['xlsx']
#ExpMetadata.load_metadata()
#ExpMetadata.merge_scan_metas()

#%%

#ExpMetadata.add_fcs_to_scan_meta()
#mask = ExpMetadata.dfScansMeta['FC'] == -1
#mask = (dfScansMeta['MeasDate'] == date)
#dfScansMeta[mask]

#dfScansMeta = dfScansMeta.drop_duplicates(
#    subset=['MeasDate', 'ScanDID'])
#print(len(dfScansMeta))

#ExpMetadata.dfFCs[ExpMetadata.get_fc_mask(y=2019, m=5, d=28)].iloc[-1]

#%% Write output

#import os
#filepath = os.path.join(
#    ExpMetadata.DataClass.settings.dir_metadata,
#    '2019',
#    datetime.datetime.utcnow().strftime('%Y-%m-%d')+'_2S-6P 2019 measurement_fcs.xlsx')
#ExpMetadata.write_metadata(
#    'dfFCs', filepath, overwrite=False)
