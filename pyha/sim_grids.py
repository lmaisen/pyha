# -*- coding: utf-8 -*-
"""
Created on Sat Feb  8 17:09:15 2020

@author: Lothar Maisenbacher/MPQ

Definition of class `SimGrids`, which manages simulation grids.
"""

import numpy as np
import pandas as pd
import glob
import time
import json
from pathlib import Path

# pyhs
import pyhs.gen
logger = pyhs.gen.get_command_line_logger()

# pyha
import pyha.defs
import pyha.def_signals
import pyha.generic_container_class
from pyha.def_shorthands import sp__
import pyha.util.data

# pyh
from pyh.exceptions import FileError

class SimGrids(pyha.generic_container_class.GenericContainerClass):
    """
    Class managing simulation grids,
    including metadata and simulation results.
    """

    GRID_COLUMNS_GEN = [
        ]
    GRID_COLUMNS_FLOAT = [
        'InputState_wx', 'InputState_x0',
        'InputState_r1', 'InputState_r2', 'InputState_L1', 'InputState_L2', 'InputState_sigmav',
        '2SnPLaserWaistRadius', 'DetectionBoundaryStart', 'DetectionBoundaryEnd',
        'DetectionDistance', 'SolveAbsTol', 'SolveRelTol', 'SolveInitialStepSize',
        ]
    GRID_COLUMNS_STR = [
        'GridUID', 'GridInterUID', 'DenseGridUID',
        'Model', 'Type', 'FS',
        'SimFile', 'TrajUID', 'SignalSetID', 'CalcFreqSampling', 'DelaySetID',
        'BackdecayModel', 'DeltapDecayAvg',
        'InputState', 'InputStateParamsID', '2SnPLaserBeamProfile',
        'AbsDir', 'AbsPath', 'OBEID', 'Isotope', 'CalcFixedFreqListID', 'Solver',
        ]
    GRID_COLUMNS_BOOL = [
        'QI', 'Backdecay',
        'MCWFWMUseRandomSeed', 'MCWFMCalcBDCorrOnly', 'MCWFMUseLookupTable',
        'EHCalcEmissionFromRho', 'EHIncludeOffDiagElements',
        '2SnPSOD', '2SnPBackreflection', '2SnPLaserBeamDivergence',
        'FitsFound', 'FitsLoaded', 'DirectDecaysOnly',
        ]
    GRID_COLUMNS_INT = [
        'MaxTransverseMomentum', 'NTransverseMomentumSteps',
        'NSubsets', 'NDeltapDecay', 'NDeltapDecayAvg',
        'NThreads', 'NMCWFMLookupCalcTimeSteps',
        'NMCWFMLookupInterTimeSteps', 'NMCWFMLookupProbPoints',
        'NMCWFMTrajsPerWorker', 'NMCWFMTrajs', 'NMCWFMMomentumSteps',
        'MCWFWMSeedFixed', 'NMCWFMTrajSteps', 'SolveMaxIters', 'NCPUs',
        ]
    GRID_COLUMNS = (
        GRID_COLUMNS_GEN+GRID_COLUMNS_FLOAT+GRID_COLUMNS_STR
        +GRID_COLUMNS_BOOL+GRID_COLUMNS_INT)
    GRID_FORMAT = {
        'ColumnsGen': GRID_COLUMNS_GEN,
        'ColumnsFloat': GRID_COLUMNS_FLOAT,
        'ColumnsStr': GRID_COLUMNS_STR,
        'ColumnsBool': GRID_COLUMNS_BOOL,
        'ColumnsInt': GRID_COLUMNS_INT,
        'ColumnsDate': ['Timestamp'],
        'IndexName': 'GridUID',
        'KeepIndexAsColumn': True,
        'UniqueIndex': True,
        }

    GRID_INTER_FORMAT = {
        'ColumnsGen': GRID_COLUMNS_GEN,
        'ColumnsFloat': GRID_COLUMNS_FLOAT,
        'ColumnsStr': GRID_COLUMNS_STR,
        'ColumnsBool': GRID_COLUMNS_BOOL,
        'ColumnsInt': GRID_COLUMNS_INT,
        'ColumnsDate': ['Timestamp'],
        'IndexName': 'GridInterUID',
        'KeepIndexAsColumn': True,
        'UniqueIndex': True,
        }

    GRID_SCANS_COLUMNS_GEN = []
    GRID_SCANS_COLUMNS_FLOAT = [
        '2SnPLaserPower', 'V', 'Vx',
        'AlphaOffset', 'DeltapDecay1', 'DeltapDecay2', 'DeltapDecay3',
        'VbinDelta', 'VbinMean', 'VbinStart', 'VbinEnd',
        'DCEFieldx', 'DCEFieldy', 'DCEFieldz',
        ]
    GRID_SCANS_COLUMNS_STR = [
        'SimScanUID',
        'GridUID', 'GridInterUID', 'FileUID', 'Filename', 'FilenameLegacy', 'CalcFreqListID',
        'AbsPath',]
    GRID_SCANS_COLUMNS_BOOL = ['Vbin']
    GRID_SCANS_COLUMNS_INT = [
        'SimScanIID', 'JobID', 'Subset', 'NTrajs', 'NTrajOutliers', 'TrajOffset',]
    GRID_SCANS_COLUMNS = (
        GRID_SCANS_COLUMNS_GEN+GRID_SCANS_COLUMNS_FLOAT+GRID_SCANS_COLUMNS_STR
        +GRID_SCANS_COLUMNS_BOOL+GRID_SCANS_COLUMNS_INT)
    GRID_SCANS_FORMAT = {
        'ColumnsGen': GRID_SCANS_COLUMNS_GEN,
        'ColumnsFloat': GRID_SCANS_COLUMNS_FLOAT,
        'ColumnsStr': GRID_SCANS_COLUMNS_STR,
        'ColumnsBool': GRID_SCANS_COLUMNS_BOOL,
        'ColumnsInt': GRID_SCANS_COLUMNS_INT,
        'ColumnsDate': [],
        'IndexName': 'SimScanUID',
        }

    GRIDS_SCANS_FORMAT = {
        'ColumnsGen': list(np.unique(
            GRID_FORMAT['ColumnsGen']+GRID_SCANS_FORMAT['ColumnsGen'])),
        'ColumnsFloat': list(np.unique(
            GRID_FORMAT['ColumnsFloat']+GRID_SCANS_FORMAT['ColumnsFloat'])),
        'ColumnsStr': list(np.unique(
            GRID_FORMAT['ColumnsStr']+GRID_SCANS_FORMAT['ColumnsStr'])),
        'ColumnsBool': list(np.unique(
            GRID_FORMAT['ColumnsBool']+GRID_SCANS_FORMAT['ColumnsBool'])),
        'ColumnsInt': list(np.unique(
            GRID_FORMAT['ColumnsInt']+GRID_SCANS_FORMAT['ColumnsInt'])),
        'ColumnsDate': list(np.unique(
            GRID_FORMAT['ColumnsDate']+GRID_SCANS_FORMAT['ColumnsDate'])),
        'IndexName': 'SimScanUID',
        'LoadFromFiles': False,
        }
    GRIDS_SCANS_COLUMNS = list(np.unique(GRID_COLUMNS + GRID_SCANS_COLUMNS))

    GRID_STATS_COLUMNS_GEN = [
        ]
    GRID_STATS_COLUMNS_FLOAT = [
        'CalcTime', 'PrepTime', 'AlphaOffset',
        'LorentzianNu0', 'LorentzianGamma',
        ]
    GRID_STATS_COLUMNS_STR = [
        'GridUID', 'FileUID', 'InputState', 'AbsPath',
        ]
    GRID_STATS_COLUMNS_BOOL = [
        ]
    GRID_STATS_COLUMNS_INT = [
        'JobID', 'Subset', 'Worker',
        'NMCWFMRandomNumbers', 'NMCWFMRandomNumberTrajs',
        'NMCWFMTrajStepsMaxNeeded', 'NMCWFMTrajRNGSyncLost',
        'NMCWFMTrajStepsMaxExceeded', 'NMCWFMBackdecays', 'NMCWFMBackdecaysTrajMax',
        'NMCWFMJumpTimeEquationError', 'NMCWFMJumpTimeEquationStartError',
        'MaxTransverseMomentum', 'NTransverseMomentumSteps', 'AlphaOffsetID',
        ]
    GRID_STATS_FORMAT = {
        'ColumnsGen': GRID_STATS_COLUMNS_GEN,
        'ColumnsFloat': GRID_STATS_COLUMNS_FLOAT,
        'ColumnsStr': GRID_STATS_COLUMNS_STR,
        'ColumnsBool': GRID_STATS_COLUMNS_BOOL,
        'ColumnsInt': GRID_STATS_COLUMNS_INT,
        'ColumnsDate': [],
        'IndexName': 'GridUID',
        }

    GRID_SCAN_STATS_FORMAT = {
        'ColumnsGen': [],
        'ColumnsFloat': ['SolveFEvs', 'ScanCalcTime',],
        'ColumnsStr': ['SimScanUID', 'AbsPath',],
        'ColumnsBool': [],
        'ColumnsInt': [],
        'ColumnsDate': ['Timestamp',],
        'IndexName': 'SimScanUID',
        }

    DF_FORMATS = {
        'dfSimGrids': GRID_FORMAT,
        'dfSimGridInters': GRID_INTER_FORMAT,
        'dfSimGridScans': GRID_SCANS_FORMAT,
        'dfSimGridsScans': GRIDS_SCANS_FORMAT,
        'dfSimGridStats': GRID_STATS_FORMAT,
        'dfSimGridScanStats': GRID_SCAN_STATS_FORMAT,
        }

    def __init__(self, grid_format='Grid'):
        """
        Initialize simulation grid class.
        The parameter `grid_format` (string) determines which type of simulation grid the class is
        configured for. Set to 'Grid' (default) for non-interpolated simulation grids,
        and to 'GridInter' for interpolated simulation grids.
        """
        super().__init__()

        # Init empty container (dict) for OBE metadata
        self.obes = {}

        # Grid format, either grids resulting from direct calculation of
        # lines ('Grid') or resulting from interpolation of calculated lines
        # ('GridInter')
        if grid_format == 'GridInter':
            self.df_id_grids = 'dfSimGridInters'
        else:
            self.df_id_grids = 'dfSimGrids'
        self.grid_index_name = self.df_formats[self.df_id_grids]['IndexName']
        self.grid_format = grid_format

    @property
    def dfSimGrids(self):
        """Get pandas DataFrame `dfSimGrids`."""
        return self.dfs['dfSimGrids']

    @dfSimGrids.setter
    def dfSimGrids(self, dfSimGrids):
        """Set pandas DataFrame `dfSimGrids`."""
        self.dfs['dfSimGrids'] = dfSimGrids

    @property
    def dfSimGridInters(self):
        """Get pandas DataFrame `dfSimGridInters`."""
        return self.dfs['dfSimGridInters']

    @dfSimGridInters.setter
    def dfSimGridInters(self, dfSimGridInters):
        """Set pandas DataFrame `dfSimGridInters`."""
        self.dfs['dfSimGridInters'] = dfSimGridInters

    @property
    def dfSimGridScans(self):
        """Get pandas DataFrame `dfSimGridScans`."""
        return self.dfs['dfSimGridScans']

    @dfSimGridScans.setter
    def dfSimGridScans(self, dfSimGridScans):
        """Set pandas DataFrame `dfSimGridScans`."""
        self.dfs['dfSimGridScans'] = dfSimGridScans

    @property
    def dfSimGridsScans(self):
        """Get pandas DataFrame `dfSimGridsScans`."""
        return self.dfs['dfSimGridsScans']

    @dfSimGridsScans.setter
    def dfSimGridsScans(self, dfSimGridsScans):
        """Set pandas DataFrame `dfSimGridsScans`."""
        self.dfs['dfSimGridsScans'] = dfSimGridsScans

    @property
    def dfSimGridStats(self):
        """Get pandas DataFrame `dfSimGridStats`."""
        return self.dfs['dfSimGridStats']

    @dfSimGridStats.setter
    def dfSimGridStats(self, dfSimGridStats):
        """Set pandas DataFrame `dfSimGridStats`."""
        self.dfs['dfSimGridStats'] = dfSimGridStats

    @property
    def dfSimGridScanStats(self):
        """Get pandas DataFrame `dfSimGridScanStats`."""
        return self.dfs['dfSimGridScanStats']

    @dfSimGridScanStats.setter
    def dfSimGridScanStats(self, dfSimGridScanStats):
        """Set pandas DataFrame `dfSimGridScanStats`."""
        self.dfs['dfSimGridScanStats'] = dfSimGridScanStats

    def cast_containers_to_data_format(self, df, df_id):
        """
        Cast columns of DataFrame `df` into correct DType, add default values,
        as specified in DataFrame format with ID `df_id` (str).
        """
        if df_id in ['dfSimGrids']:

            # Make sure all columns processed below are actually present in `df`
            columns = [
                'NDeltapDecay', 'CalcFreqSampling', 'CalcFixedFreqListID',
                'DelaySetID', 'MCWFMUseLookupTable', 'InputState', 'DirectDecaysOnly',
                ]
            df = df.reindex(
                columns=[
                    *columns,
                    *df.columns[~np.in1d(df.columns, columns)]],
                fill_value=np.nan)

            # Add default values, adapt data format of legacy grids
            df['NDeltapDecay'] = df['NDeltapDecay'].fillna(0)
            # Default calculated frequency sampling is 'Fixed'
            df['CalcFreqSampling'] = df['CalcFreqSampling'].fillna('Fixed')
            # Default calculated fixed frequeny list ID is 'Span128MHzPoints256' for fixed frequency
            # sampling, and '' (empty string) otherwise
            mask = df['CalcFreqSampling'] == 'Fixed'
            if 'CalcFixedFreqListID' not in df.columns:
                df['CalcFixedFreqListID'] = pd.NA
            df.loc[mask, 'CalcFixedFreqListID'] = df.loc[mask, 'CalcFixedFreqListID'].fillna(
                'Span128MHzPoints256')
            df.loc[~mask, 'CalcFixedFreqListID'] = df.loc[~mask, 'CalcFixedFreqListID'].fillna(
                '')
            # Default delay set ID is 'FixedV', which corresponds to no actual delays present,
            # but scans that correspond to a fixed atom speed
            df['DelaySetID'] = df['DelaySetID'].fillna('FixedV')
            df['MCWFMUseLookupTable'] = df['MCWFMUseLookupTable'].fillna(True)
            df['InputState'] = df['InputState'].fillna('PlaneWave')
            df['DirectDecaysOnly'] = df['DirectDecaysOnly'].fillna(True)

        if df_id in ['dfSimGridScans', 'dfSimGridsScans']:

            # Adapt data format of legacy scans
            if 'Ntraj' in df:
                df['NTrajs'] = df['Ntraj']
                del df['Ntraj']

        return super().cast_containers_to_data_format(df, df_id)

    def load_grids(
            self,
            grid_dirs, SimFitClass=None,
            load_grid_scan_data=True, load_grid_scan_data_latest_only=False,
            drop_duplicates=True, clear_current_data=False):
        """
        Load simulation grids from (list of) directories `grid_dirs` (str or list-like) into this
        class.
        An instance of `pyh.Fit.FitClass` can be provided as `SimFitClass`.
        If such an instance is provided, `load_grid_scan_data` (bool, default True) controls whether
        the fit results for the grids will be loaded into the instance. The fit results
        are searched for in the subdirectory 'Fits' in the directory of each grid.
        `load_grid_scan_data_latest_only` (bool, default False), only the most recent fit results
        file is loaded (determined from the file name, which starts with a date-time string).
        If `drop_duplicates` (bool, default True) is True, duplicate fit results, i.e.,
        duplicate fits to the same scans, are dropped from the `FitClass` instance.
        If `clear_current_data` (bool, default False) is True, all data currently loaded in
        `FitClass` is cleared before loading new data.
        """
        if clear_current_data:
            if SimFitClass is not None:
                # Clear fit class results
                SimFitClass.init_data_containers()
                SimFitClass.init_scan_fits_containers()
                SimFitClass.set_df_all()
                logger.info(
                    'Cleared all data from simulation fit class (SimFitClass)')
            self.init_data_containers()
            logger.info(
                'Cleared all data from simulation grid class')

        dfSimGrids = self.dfs[self.df_id_grids]
        dfSimGrids_template = self.df_templates[self.df_id_grids]
        dfSimGridScans = self.dfSimGridScans
        dfSimGridScans_template = self.df_templates['dfSimGridScans']
        dfSimGridStats = self.dfSimGridStats
        dfSimGridStats_template = self.df_templates['dfSimGridStats']
        dfSimGridScanStats = self.dfSimGridScanStats
        dfSimGridScanStats_template = self.df_templates['dfSimGridScanStats']
        dfSimGridsScans = self.dfSimGridsScans

        num_sim_grids_init = len(dfSimGrids)
        num_sim_grids_loaded = 0
        num_sim_grid_scans_init = len(dfSimGridScans)
        num_sim_grid_scans_loaded = 0
        grid_dirs = [] if grid_dirs is None else grid_dirs
        grid_dirs = np.atleast_1d(grid_dirs)
        for grid_dir in grid_dirs:

            metadata_dir = Path(grid_dir, 'Metadata')
            logger.info('Scanning folder \'%s\' for grid metadata', metadata_dir)

            # Load grids metadata
            df_filepaths = glob.glob(str(Path(metadata_dir, '*_grids.dat')))
            if len(df_filepaths) == 0:
                logger.info('Found no grid metadata file, not loading any data')
                continue
            logger.info('Found %d grid metadata file(s)', len(df_filepaths))
            dfSimGrids_ = dfSimGrids_template.copy()
            for df_filepath in df_filepaths:
                dfSimGrids__ = self.read_df_from_csv(
                    df_filepath, self.df_id_grids)
                # Adapt legacy format
                if 'SignalsSaved' in dfSimGrids__:
                    dfSimGrids__['SignalSetID'] = dfSimGrids__['SignalsSaved']
                    dfSimGrids__.drop(columns=['SignalsSaved'], inplace=True)
                mask_grid = (dfSimGrids__['Type'].isin(['MC', 'MCDpDavg']))
                if 'DelaySetID' not in dfSimGrids__[mask_grid]:
                    dfSimGrids__.loc[mask_grid, 'DelaySetID'] = '2S6P'
                dfSimGrids_ = pd.concat([dfSimGrids_, dfSimGrids__])
            dfSimGrids_['AbsDir'] = grid_dir
            num_grids_loaded = len(dfSimGrids_)
            # Drop duplicates
            self.t = dfSimGrids_
            dfSimGrids_ = self.drop_duplicates_and_reindex(
                dfSimGrids_, self.df_id_grids,
                subset=(
                    [elem for elem in dfSimGrids_.columns
                     if elem not in ['Timestamp', 'NSubsets']]))
            dfSimGrids_ = self.set_df_index_name(dfSimGrids_, self.df_id_grids)
            dfSimGrids_.sort_values('Timestamp', ascending=False, inplace=True)
            logger.info(
                'Found metadata for %d grid(s) (%s) (dropped %d duplicate(s))'
                +', keeping only most recent with UID \'%s\'',
                len(dfSimGrids_),
                ', '.join(['\'{}\''.format(grid_uid) for grid_uid in dfSimGrids_.index]),
                num_grids_loaded-len(dfSimGrids_),
                dfSimGrids_.index[0],
                )
            # Only keep most recent grid definition
            # (each grid directory by definition only contains a single grid)
            dfSimGrids_ = dfSimGrids_.iloc[0:1]
            # Cast data format to make sure grid UID is formatted correctly (must be str)
            dfSimGrids_ = self.cast_containers_to_data_format(
                dfSimGrids_, self.df_id_grids)
            grid_uid = dfSimGrids_.index[0]

            # Load grid scans metadata
            df_filepaths = glob.glob(str(Path(metadata_dir, '*_scans.dat')))
            logger.info('Found %d grid scans metadata file(s)', len(df_filepaths))
            dfSimGridScans_ = dfSimGridScans_template.copy()
            for df_filepath in df_filepaths:
                dfSimGridScans__ = self.read_df_from_csv(
                    df_filepath, 'dfSimGridScans')
                dfSimGridScans_ = pd.concat([dfSimGridScans_, dfSimGridScans__])
            num_grid_scans_loaded = len(dfSimGridScans_)
            dfSimGridScans_ = self.drop_duplicates_and_reindex(
                dfSimGridScans_, 'dfSimGridScans',
                subset=(
                    [elem for elem in dfSimGridScans_.columns
                     if elem not in ['Filename', 'FilenameLegacy', 'SimScanIID', 'JobID']]))
            dfSimGridScans_ = self.set_df_index_name(dfSimGridScans_, 'dfSimGridScans')
            # Set SimScanIID (numeric index) to actual position of each entry in the DataFrame,
            # as this index is used to locate the corresponding entries in arrays holding the actual
            # grid results
            dfSimGridScans_['SimScanIID'] = range(len(dfSimGridScans_))
            logger.info(
                'Added metadata for %d grid scan(s) (dropped %d duplicate(s))',
                len(dfSimGridScans_), num_grid_scans_loaded-len(dfSimGridScans_))

            # Load grid scan data
            file_info_paths = glob.glob(str(Path(grid_dir, 'Fits', '*_info.pyh')))
            file_info_paths = np.sort(file_info_paths)
            dfSimGrids_['FitsFound'] = len(file_info_paths) >= 1
            if load_grid_scan_data_latest_only:
                file_info_paths = file_info_paths[-1:]
            logger.info(
                'Found %d grid scan data file(s)', len(file_info_paths))
            dfSimGrids_['FitsLoaded'] = False
            if load_grid_scan_data and SimFitClass is not None:
                for file_info_path in file_info_paths:
                    logger.info(
                        'Loading grid scan data from \'%s\'', file_info_path)
                    try:
                        SimFitClass.load_data(file_info_path)
                    except FileError as e:
                        logger.error(e)
                    else:
                        dfSimGrids_['FitsLoaded'] = True

            # Load grid calculation stats
            stats_paths = glob.glob(str(Path(grid_dir, 'Stats', '*.stat')))
            stats_paths = np.sort(stats_paths)
            logger.info(
                'Found %d grid stats file(s)', len(stats_paths))
            dfSimGridStats_ = dfSimGridStats_template.copy()
            for df_filepath in stats_paths:
                dfSimGridStats__ = self.read_df_from_csv(
                    df_filepath, 'dfSimGridStats')
                dfSimGridStats_ = pd.concat([dfSimGridStats_, dfSimGridStats__])

            # Load grid scan calculation stats
            stats_paths = glob.glob(str(Path(grid_dir, 'Stats', '*_stats.dat')))
            stats_paths = np.sort(stats_paths)
            logger.info(
                'Found %d grid scan stats file(s)', len(stats_paths))
            dfSimGridScanStats_list = []
            for df_filepath in stats_paths:
                dfSimGridScanStats__ = self.read_df_from_csv(
                    df_filepath, 'dfSimGridScanStats', safe=True)
                dfSimGridScanStats_list.append(dfSimGridScanStats__)
            if len(dfSimGridScanStats_list) > 0:
                dfSimGridScanStats_ = pd.concat(dfSimGridScanStats_list)
            else:
                dfSimGridScanStats_ = dfSimGridScanStats_template.copy()

            # Load OBE metadata
            subdir_obe = 'OBE'
            dir_obe = Path(grid_dir, subdir_obe)
            obe = self.load_obe_metadata(dir_obe)
            if obe is not None:
                logger.info('Found OBE metadata in subdirectory \'%s\'', subdir_obe)
            else:
                logger.info('Could not find OBE metadata in subdirectory \'%s\'', subdir_obe)
            self.obes[grid_uid] = obe

            # Cast data format
            dfSimGrids_ = self.cast_containers_to_data_format(
                dfSimGrids_, self.df_id_grids)
            dfSimGridScans_ = self.cast_containers_to_data_format(
                dfSimGridScans_, 'dfSimGridScans')
            dfSimGridStats_ = self.cast_containers_to_data_format(
                dfSimGridStats_, 'dfSimGridStats')
            dfSimGridScanStats__ = self.cast_containers_to_data_format(
                dfSimGridScanStats_, 'dfSimGridScanStats')

            # Build combined DataFrame of `dfSimGrids` and `dfSimGridScans`.
            # If a column is present in both in `dfSimGrids` and `dfSimGridScans`, the column
            # from `dfSimGridScans` is used.
            grid_columns = dfSimGrids_.columns.difference(dfSimGridScans_.columns)
            dfSimGridsScans_ = (
                dfSimGrids_[grid_columns]
                .merge(
                    dfSimGridScans_,
                    left_index=True,
                    right_on=self.df_formats[self.df_id_grids]['IndexName'],
                ))
            dfSimGridsScans_ = self.cast_containers_to_data_format(
                dfSimGridsScans_, 'dfSimGridsScans')

            dfSimGrids = pd.concat([dfSimGrids, dfSimGrids_])
            dfSimGridScans = pd.concat([dfSimGridScans, dfSimGridScans_])
            dfSimGridStats = pd.concat([dfSimGridStats, dfSimGridStats_])
            dfSimGridScanStats = pd.concat([dfSimGridScanStats, dfSimGridScanStats_])
            dfSimGridsScans = pd.concat([dfSimGridsScans, dfSimGridsScans_])

            num_sim_grids_loaded += len(dfSimGrids_)
            num_sim_grid_scans_loaded += len(dfSimGridScans_)

        dfs = {
            self.df_id_grids: dfSimGrids,
            'dfSimGridScans': dfSimGridScans,
            'dfSimGridStats': dfSimGridStats,
            'dfSimGridScanStats': dfSimGridScanStats,
            'dfSimGridsScans': dfSimGridsScans,
            }

        for df_id, df in dfs.items():

            # Cast to data format
            df = self.cast_containers_to_data_format(
                df, df_id)

            # Drop duplicates
            df = self.drop_duplicates_and_reindex(
                df, df_id)

            self.dfs[df_id] = df

        if drop_duplicates and SimFitClass is not None:
            # Drop duplicate results of simulation scan analysis, as identified
            # in columns subset, keeping latest data.
            # Associated fits will be removed as well.
            SimFitClass.drop_sim_duplicates()

        logger.info(
            'Added/reloaded metadata for %d/%d grid(s) and %d/%d grid scan(s) in total',
            len(self.dfs[self.df_id_grids])-num_sim_grids_init,
            num_sim_grids_loaded-len(self.dfs[self.df_id_grids])+num_sim_grids_init,
            len(self.dfSimGridScans)-num_sim_grid_scans_init,
            num_sim_grid_scans_loaded-len(self.dfSimGridScans)+num_sim_grid_scans_init)

    def add_grid_metadata_to_scans(self, dfAll_scans, column_prefix=None):
        """
        Add combined metadata of simulation grids and simulation scans, saved within class in
        DataFrame `dfSimGridsScans`, to supplied DataFrame `dfAll_scans`.
        A merge between the two DataFrames on the index of `dfSimGridsScans`,
        which is the simulation scan UID 'SimScanUID',
        and the corresponding column 'dfScans_SimParams_SimScanUID' of `dfAll_scans`,
        is used to this end.
        For rows of `dfAll_scans` where 'dfScans_SimParams_SubsetAvg' is True,
        no metadata with a matching simulation scan UID is available, as these rows correspond to
        a combination of multiple simulation scans from the simulation grid
        (i.e. the average over multiple subsets).
        In this case, only the metadata of the grid underlying these simulation scans is merged.
        The metadata columns are prefixed with `column_prefix` (str) (plus '_'). If set to None
        (default), 'dfSimGrids' is used.
        The merge DataFrame `dfAll_scans_g` is returned.
        """
        # Add grid metadata
        if column_prefix is None:
            column_prefix = 'dfSimGrids'
        subset_avg_mask = (dfAll_scans[sp__+'SubsetAvg'] == True)
        dfAll_scans_g = (
            dfAll_scans[~subset_avg_mask]
            .merge(
                self.dfs['dfSimGridsScans'].rename(columns=lambda x: column_prefix+'_'+x),
                how='inner', left_on=sp__+'SimScanUID', right_index=True))
        if np.sum(subset_avg_mask) > 0:
            # Add average of subsets, for which no dfSimGridScans entry is available
            dfAll_scans_g_ = (
                dfAll_scans[subset_avg_mask]
                .merge(
                    self.dfs[self.df_id_grids].rename(columns=lambda x: column_prefix+'_'+x),
                    how='inner', left_on=sp__+'GridUID', right_index=True))
            dfAll_scans_g = dfAll_scans_g.append(dfAll_scans_g_, sort=False)
        dfAll_scans_g.index.name = 'ScanUID'
        return dfAll_scans_g

    def get_grid_metadata(self, grid_uid):
        """
        Get metadata (pandas Series) of grid with UID `grid_uid` (str).
        Metadata for this grid must be loaded first.
        """
        if not grid_uid in self.dfs[self.df_id_grids].index:
            msg = (
                f'Metadata of grid \'{grid_uid}\' not loaded '
                +'(use function load_grids(dir_grids), '
                +'where dir_grids is a list of directories, to load the metadata)')
            logger.error(msg)
            raise pyha.defs.PyhaError(msg)

        return self.dfs[self.df_id_grids].loc[grid_uid]

    @staticmethod
    def load_obe_metadata(dir_obe):
        """
        Load metadata of OBEs (optical Bloch equations) of a grid, stored in JSON files
        ('info.json', 'parameters.json', 'states.json', 'signals.json', 'initial.json')
        in directory `dir_obe`. If all JSON files are found, a dict with the contents of the files
        with keys 'Info', 'Parameters', 'States', 'Signals', 'InitialState', respectively, is
        returned, otherwise None.
        """
        obe_files_exist = np.all([
            Path(dir_obe, f'{content}.json').is_file()
            for content in ['info', 'parameters', 'states', 'signals', 'initial']])
        if obe_files_exist:
            with open(Path(dir_obe, 'info.json'), 'r') as f:
                info = json.load(f)
            with open(Path(dir_obe, 'parameters.json'), 'r') as f:
                parameters = json.load(f)
            with open(Path(dir_obe, 'states.json'), 'r') as f:
                states = json.load(f)
            with open(Path(dir_obe, 'signals.json'), 'r') as f:
                signals = json.load(f)
            with open(Path(dir_obe, 'initial.json'), 'r') as f:
                initial_state = json.load(f)
            obe = {
                'Info': info,
                'Parameters': parameters,
                'States': states,
                'Signals': signals,
                'InitialState': initial_state,
                }
        else:
            obe = None

        return obe

    def load_grid_scans(self, grid_uid, ignore_missing_files=False):
        """
        Load simulation scans (detunings, signals, populations)
        for grid with UID `grid_uid` (str) from NPZ archive(s) from disk.
        Metadata for this grid must be loaded first.
        If `ignore_missing_files` is set to True (default is False), missing files when loading
        results from multiple files are ignored and the missing results are replaced with `np.nan`.
        """
        grid = self.get_grid_metadata(grid_uid)
        dfScans = (
            self.dfs['dfSimGridScans'][
                self.dfs['dfSimGridScans'][self.grid_index_name] == grid_uid])
        unique_filenames = dfScans['Filename'].unique()
        logger.info(
            'Loading %d scan(s) of grid \'%s\' from %d file(s) in directory \'%s\'',
            len(dfScans), grid_uid, len(unique_filenames),
            Path(grid['AbsDir'], 'Lines'))
        if len(unique_filenames) == 1:
            # For legacy files, the filename might be empty and the results are stored in a single
            # NPZ archive with the filebase matching the grid UID
            if unique_filenames[0] == '':
               filename = f'{grid_uid}.npz'
               logger.warning(
                   'No filename defined in grid scans metadata.'
                   +f' This can be the case for legacy grids, trying \'{filename}\'.')
            else:
                filename = unique_filenames[0]
            # Results are contained in single file
            start_time = time.perf_counter()
            filepath_npz = Path(grid['AbsDir'], 'Lines', filename)
            try:
                with np.load(filepath_npz) as scans_lines:
                    scans_detunings = scans_lines['Detunings']
                    scans_signals = scans_lines['Signals']
                    scans_populations = scans_lines['Populations']
            except FileNotFoundError as e:
                msg = (
                    f'Could not load scans of grid \'{grid_uid}\' from file \'{filepath_npz}\' '
                    f'(error was \'{e}\')')
                logger.error(msg)
                raise pyha.defs.PyhaError(msg)
            logger.info(
                'Loaded %d scan(s) from file \'%s\' in %.2f s (size on disk is %.2f MB)',
                len(scans_detunings), filename, time.perf_counter()-start_time,
                Path(filepath_npz).stat().st_size/2**20)
        else:
            # Results are contained in multiple files
            # if self.obes.get(grid_uid) is not None:
            #     # OBE metadata available for this grid, can pre-construct arrays holding results.
            #     # Additionally, the number of frequency points (implemented) and
            #     # the number of delays (currently not implemented) needs to be known.
            #     obe = self.obes[grid_uid]
            #     freq_sampling_metadata = pyha.def_line_sampling.get_freq_sampling_metadata(
            #         grid['CalcFreqSampling'], grid['CalcFixedFreqListID'])
            #     num_freqs = freq_sampling_metadata['NFreqsUnique']
            #     num_signals = len(obe['Info']['SignalsToSave'])
            #     num_populations = len(obe['Info']['PopulationsToSave'])
            #     scans_detunings = np.zeros((len(dfScans), num_freqs))
            #     scans_signals = np.zeros(
            #         (len(dfScans), num_freqs, num_signals))
            #     scans_populations = np.zeros(
            #         (len(dfScans), num_freqs, num_populations))
            # Open first file to determine array dimensions
            filepath_npz = Path(grid['AbsDir'], 'Lines', unique_filenames[0])
            try:
                with np.load(filepath_npz) as scans_lines:
                    scans_detunings_shape = scans_lines['Detunings'].shape
                    scans_signals_shape = scans_lines['Signals'].shape
                    scans_populations_shape = scans_lines['Populations'].shape
            except FileNotFoundError as e:
                msg = (
                    f'Could not load scans from file \'{filepath_npz}\' (error was \'{e}\')')
                logger.error(msg)
                raise pyha.defs.PyhaError(msg)
            num_freqs = scans_detunings_shape[1]
            num_signals = scans_signals_shape[2]
            num_populations = scans_populations_shape[2]
            if grid['TrajUID'] == '':
                scans_detunings = np.zeros((len(dfScans), num_freqs))
                scans_signals = np.zeros(
                    (len(dfScans), num_freqs, num_signals))
                scans_populations = np.zeros(
                    (len(dfScans), num_freqs, num_populations))
            else:
                num_delays = scans_signals_shape[3]
                scans_detunings = np.zeros((len(dfScans), num_freqs))
                scans_signals = np.zeros(
                    (len(dfScans), num_freqs, num_signals, num_delays))
                scans_populations = np.zeros(
                    (len(dfScans), num_freqs, num_populations, num_delays))
            for filename in unique_filenames:
                start_time = time.perf_counter()
                filepath_npz = Path(grid['AbsDir'], 'Lines', filename)
                mask = dfScans['Filename'] == filename
                try:
                    with np.load(filepath_npz) as scans_lines:
                        scans_detunings[mask] = scans_lines['Detunings']
                        scans_signals[mask] = scans_lines['Signals']
                        scans_populations[mask] = scans_lines['Populations']
                except FileNotFoundError as e:
                    if not ignore_missing_files:
                        msg = (
                            f'Could not load scans from file \'{filepath_npz}\''
                            +f' (error was \'{e}\')')
                        logger.error(msg)
                        raise pyha.defs.PyhaError(msg)
                    else:
                        scans_detunings[mask] = np.nan
                        scans_signals[mask] = np.nan
                        scans_populations[mask] = np.nan
                        logger.warning(
                            f'Could not load scans from file \'{filepath_npz}\','
                            +' ignored and missing results returned as `np.nan` as'
                            +' `ignore_missing_files` is True'
                            +f' (error was \'{e}\')')
                else:
                    logger.info(
                        'Loaded %d scan(s) from file \'%s\' in %.2f s (size on disk is %.2f MB)',
                        mask.sum(), filename, time.perf_counter()-start_time,
                        Path(filepath_npz).stat().st_size/2**20)

        return scans_detunings, scans_signals, scans_populations

    def load_dense_grid(self, grid_uid, dense_grid_uid):
        """
        Load dense grid with UID `dense_grid_uid` (str)
        for grid with UID `grid_uid` (str).
        """
        grid = self.get_grid_metadata(grid_uid)

        dir_dense_grids = Path(grid['AbsDir'], 'DenseGrids')

        filepath_dense_grid_metadata = Path(dir_dense_grids, f'{dense_grid_uid}.json')
        with open(filepath_dense_grid_metadata, 'r') as f:
            dense_grid = json.load(f)
        # Add default values
        dense_grid = {
            'TransverseVelParametrization': 'AlphaOffset',
            **dense_grid
            }
        logger.info(
            f'Loaded metadata for dense grid \'{dense_grid_uid}\' '
            +f'from file \'{filepath_dense_grid_metadata}\'')

        # Convert lists from JSON file to arrays
        keys = [
            'CalcAlphaOffsets',
            'CalcVxs',
            'CalcSpeeds',
            'CalcPowers',
            'InterpAlphaOffsets',
            'InterpVxs',
            'InterpSpeeds',
            'InterpPowers',
            ]
        for key in [key for key in keys if key in dense_grid.keys()]:
            dense_grid[key] = np.array(dense_grid[key])

        # Convert timestamp from string
        dense_grid['Timestamp'] = (
            pd.Timestamp(dense_grid['Timestamp']) if 'Timestamp' in dense_grid.keys()
            else pd.NaT)

        dense_grid['AbsDir'] = dir_dense_grids

        return dense_grid

    @staticmethod
    def load_dense_grid_data(dense_grid, signal_id, mmap_mode='r'):
        """
        Load dense grid data `dense_grid_data` (4D array) for dense grid `dense_grid` (dict)
        and signal ID `signal_id` (str) from a NumPy NPY archive on disk.
        The parameter `mmap_mode` is passed to `numpy.load` and sets the memory-map mode.
        If set to None, no memory map is created and the dense grid data is loaded into memory.
        If set to 'r' (default), a memory map is created and only array elements actually accessed
        are loaded into memory.
        Note that if the signal to be loaded is made up of multiple signals saved in
        separate archives, the stacking of the archives done here will load all data into memory,
        irrespective of the value of `mmap_mode`.
        """
        # Get columns corresponding to signal ID
        signal_columns = pyha.def_signals.get_signal_columns(
            dense_grid['DenseSignalSetID'],
            signal_id)

        # Stack signal columns, each saved in separate file
        for i, signal_column in enumerate(signal_columns):
            filepath_dense_grid = Path(
                dense_grid['AbsDir'],
                f'{dense_grid["DenseGridUID"]}_sc{signal_column:d}.npy')
            dense_grid_data_ = np.load(filepath_dense_grid, mmap_mode=mmap_mode)
            logger.info(
                f'Loaded dense grid \'{dense_grid["DenseGridUID"]}\' '
                +f'for signal \'{signal_id}\' '
                +f'from file \'{filepath_dense_grid}\'')
            if i == 0:
                dense_grid_data = dense_grid_data_
            else:
                dense_grid_data = dense_grid_data + dense_grid_data_

        return dense_grid_data
