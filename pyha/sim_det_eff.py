# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ

Class that handles data from simulations of detection efficiency.
"""

import datetime
import numpy as np
import pandas as pd
from pathlib import Path

# pyhs
import pyhs.gen
# Set-up command line logger
logger = pyhs.gen.set_up_command_line_logger()

# pyha
import pyha.def_atomic

class DetEff:
    """ Class that handles data from simulations of detection efficiency. """

    DET_EFF_FILE_COLUMNS = {
        'Theta': 1,
        'Phi': 2,
        'Photons': [3],
        'TopPh': [7],
        'TopEl': [7],
        'Top': [6, 7],
        'Bottom': [8, 9],
        'BottompPh': [8],
        'BottomEl': [9],
        }

    def __init__(self):

        self.init_data_containers()

    def init_data_containers(self):
        """
        Init empty data containers for detection efficiencies.
        """
        # Init variables
        self.det_eff_data_cache = {}

        # Init empty DataFrame for detection efficiency simulations
        self.dfSimDetEffs = pd.DataFrame(columns=(
            [
                'Timestamp', 'SubDir', 'AbsDir',
                'Filename', 'AbsPath', 'FileExists',
                'Loaded',
                'ElectroFlightVersion',
                'Geometry',
                'GeometryID',
                'SetID',
                'SetName',
                'NEFieldIter',
                'AngularDistribution',
                'PhotoelEnergyDist',
                'CEMInputPotential',
                'WireMeshTransparency',
                'NPhDirections',
                'NTrajs',
                'PPhGr',
                'PPhAl',
                'PElGr',
                'PElAl',
                'YElGr',
                'YElAl',
                'DetEffElCEM',
                'DetEffPhCEM',
                'DetEffElCEMCapFrontside',
                'DetEffElCEMCapSideWalls'
            ]))
        self.dfSimDetEffs.index.name = 'DetEffUID'
        self.cast_data_format()

    def cast_data_format(self):
        """
        Cast data format of detection efficiency DataFrame `dfSimDetEffs`.
        """
        # Cast data format
        self.dfSimDetEffs['Loaded'] = self.dfSimDetEffs['Loaded'].fillna(False)
        self.dfSimDetEffs['FileExists'] = self.dfSimDetEffs['FileExists'].fillna(-1)
        columns = ['NPhDirections', 'NEFieldIter', 'FileExists']
        self.dfSimDetEffs[columns] = self.dfSimDetEffs[columns].astype(int)
        columns = ['NTrajs']
        self.dfSimDetEffs[columns] = self.dfSimDetEffs[columns].astype(np.int64)

    def get_det_eff(self, det_eff_uid):
        """
        Get detection efficiency metadata `det_eff` (pandas series) for UID `det_eff_uid` (str).
        """
        det_eff = self.dfSimDetEffs.loc[det_eff_uid]
        return det_eff

    def load_files(self):
        """
        Load detection efficiency from disk for all available simulation into cache.
        """
        for det_eff_uid, _ in self.dfSimDetEffs.iterrows():
            self.load_file(det_eff_uid)

    def load_file(self, det_eff_uid):
        """
        Load detection efficiency from disk for UID `det_eff_uid` (str) into cache.
        """
        det_eff = self.get_det_eff(det_eff_uid)
        file_exists = Path(det_eff['AbsPath']).is_file()
        if file_exists:
            det_eff_data_ = np.loadtxt(
                det_eff['AbsPath'],
                skiprows=25)
            self.det_eff_data_cache[det_eff_uid] = det_eff_data_
            num_trajs_ = int(np.sum(det_eff_data_[:, 3]))
            logger.info(
                'Loaded detection efficiency simulation \'{}\', ' \
                'containing {:.2e} photon trajectories'
                .format(det_eff_uid, num_trajs_))
            self.dfSimDetEffs.loc[det_eff_uid, 'Timestamp'] = (
                datetime.datetime.fromtimestamp(Path(det_eff['AbsPath']).stat().st_mtime))
            self.dfSimDetEffs.loc[det_eff_uid, 'Loaded'] = True
        else:
            logger.info(
                'Could not load detection efficiency simulation \'{}\', file \'{}\' not found'
                .format(det_eff_uid, det_eff['AbsPath']))
            num_trajs_ = -1
        self.dfSimDetEffs.loc[det_eff_uid, 'NTrajs'] = num_trajs_
        self.dfSimDetEffs.loc[det_eff_uid, 'FileExists'] = file_exists

    def get_det_eff_data(self, det_eff_uid, scale=True):
        """
        Get detection efficiency data as saved to file from cache.
        If `scale` (bool) is set to True, detection efficieny of both photons
        and electrons is scaled by global factor 'DetEffScalingCEM'
        saved in detection efficiency DataFrame.
        """
        det_eff = self.get_det_eff(det_eff_uid)
        if not det_eff['Loaded']:
            self.load_file(det_eff_uid)
        det_eff_data = self.det_eff_data_cache[det_eff_uid].copy()
        if scale:
            det_eff_data[:, 6:10] *= (
                self.dfSimDetEffs.loc[det_eff_uid]['DetEffScalingCEM'])
        return det_eff_data

    def get_det_eff_detector(self, det_eff_uid, detector):
        """
        Get detection efficiency with UID `det_eff_uid` (str) for detector `detector` (str).
        """
        det_eff_data = self.get_det_eff_data(det_eff_uid)
        theta = det_eff_data[:, self.DET_EFF_FILE_COLUMNS['Theta']]
        phi = det_eff_data[:, self.DET_EFF_FILE_COLUMNS['Phi']]
        num_photons = np.sum(
            det_eff_data[:, self.DET_EFF_FILE_COLUMNS['Photons']],
            axis=1)
        counts = np.sum(
            det_eff_data[:, self.DET_EFF_FILE_COLUMNS[detector]],
            axis=1)
        det_eff = counts/num_photons
        return theta, phi, det_eff

    def get_det_eff_weights(self, det_eff_uid, detector, theta_l):
        """
        Calculate detection weights for spherical components for
        detection efficiency with UID `det_eff_uid`, detector `detector` (str)
        and laser polarization angle `theta_l` (deg, float).
        """
        if np.isnan(theta_l) or det_eff_uid == '' or detector == '':
            det_eff_set_to_unity = True
            weight_sigmam = 1
            weight_pi = 1
            weight_sigmap = 1
        else:
            det_eff_set_to_unity = False
            theta_l_rad = np.pi/180 * theta_l
            theta, phi, det_eff = self.get_det_eff_detector(det_eff_uid, detector)
            weight_pi = np.mean(
                pyha.def_atomic.decay_spherical_comp_int_pi(
                    theta_l_rad, theta, phi)
                * det_eff)
            weight_sigma = np.mean(
                pyha.def_atomic.decay_spherical_comp_int_sigma(
                    theta_l_rad, theta, phi)
                * det_eff)
            # Different detection efficiencies for sigma+/- not implemented yet
            weight_sigmap = weight_sigma
            weight_sigmam = weight_sigma

        return {
            'DetEffSetToUnity': det_eff_set_to_unity,
            -1: weight_sigmam,
            0: weight_pi,
            1: weight_sigmap}
