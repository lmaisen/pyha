# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ

Definition of various shorthands used for simulation data.
"""

## Shorthands
# Simulation parameters in DataFrames
sp__ = 'dfScans_SimParams_'
dsp__ = 'dfData_SimParams_'
sp_ = 'SimParams_'
dsp_ = 'SimParams_'
# Experimental parameters in DataFrames
ep__ = 'dfScans_ExpParams_'
dep__ = 'dfData_ExpParams_'
ep_ = 'ExpParams_'
dep_ = 'ExpParams_'
