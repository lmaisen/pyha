# -*- coding: utf-8 -*-
"""
Created on Sun May 31 20:10:21 2020

@author: Lothar Maisenbacher/MPQ
"""

class PyhaError(Exception):
    """A pyha error occured."""
