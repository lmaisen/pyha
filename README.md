# pyha

Data analysis package of pythonic hydrogen (pyh), a family of Python packages for the precision laser spectroscopy of the 2S-nP transitions in atomic hydrogen and its isotopes.

## Installation

To install this package directly from this repository, use (with HTTPS)

```
pip install git+https://gitlab.mpcdf.mpg.de/lmaisen/pyha.git
```
or (with SSH)
```
pip install git+ssh://git@gitlab.mpcdf.mpg.de:lmaisen/pyha.git
```

Alternatively, the package can be install as a local copy, useful when developing. For this, clone this repository and run `pip install -e .` in the root directory (containing `setup.py`). The `-e` flag ensures that the files in the local copy of the repository are used when importing the package elsewhere and changes to these files will be directly visible, as opposed to a normal installation, where the package files are imported from a dedicated directory holding all installed packages (see [`pip install` documentation](https://pip.pypa.io/en/stable/cli/pip_install/)).